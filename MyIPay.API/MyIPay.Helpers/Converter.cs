﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Dynamic;
using System.Collections;
using System.Web.Script.Serialization;
using System.Linq;
using System.Collections.ObjectModel;

namespace MyIPay.Helpers
{
    public class Converter
    {
        public static string XmlSerializer<T>(T dataToSerialize)
        {
            try
            {
                if (dataToSerialize == null) return null;

                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                serializer.Serialize(stringwriter, dataToSerialize, emptyNamepsaces);
                return stringwriter.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            }
            catch //(Exception ex)
            {
                throw;
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"XmlSerializer Error Log :- {ex.Message}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"XmlSerializer Error Log :- {ex.InnerException}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"XmlSerializer Error Log :- {ex.StackTrace}");

                //return null;
            }
        }

        public static T XmlDeserializer<T>(string xmlText)
        {
            try
            {
                if (string.IsNullOrEmpty(xmlText)) return default(T);

                var stringReader = new System.IO.StringReader(xmlText);
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
            catch //(Exception ex)
            {
                throw;
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"XmlDeserializer Error Log :- {ex.Message}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"XmlDeserializer Error Log :- {ex.InnerException}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"XmlDeserializer Error Log :- {ex.StackTrace}");

                //return default(T);
            }
        }

        public static string JsonSerializer<T>(T dataToSerialize)
        {
            try
            {
                if (dataToSerialize == null) return null;

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream();
                ser.WriteObject(ms, dataToSerialize);
                string jsonString = Encoding.UTF8.GetString(ms.ToArray());
                ms.Close();

                return jsonString;

            }
            catch //(Exception ex)
            {
                throw;
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"JsonSerializer Error Log :- {ex.Message}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"JsonSerializer Error Log :- {ex.InnerException}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"JsonSerializer Error Log :- {ex.StackTrace}");

                //return null;
            }
        }


        public static T JsonDeserialize<T>(string jsonString)
        {
            try
            {
                if (string.IsNullOrEmpty(jsonString)) return default(T);

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                T obj = (T)ser.ReadObject(ms);
                return obj;
            }
            catch //(Exception ex)
            {
                throw;
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"JsonDeserialize Error Log :- {ex.Message}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"JsonDeserialize Error Log :- {ex.InnerException}");
                //Utility.SaveRequestAndResponseLog("Converter_Error", $"JsonDeserialize Error Log :- {ex.StackTrace}");

                //return default(T);
            }
        }
    }


    public class SingleOrArrayConverter<T> : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(List<T>));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            if (token.Type == JTokenType.Array)
            {
                return token.ToObject<List<T>>();
            }
            return new List<T> { token.ToObject<T>() };
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    public sealed class DynamicJsonConverter : JavaScriptConverter
    {
        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            if (dictionary == null)
                throw new ArgumentNullException("dictionary");

            return type == typeof(object) ? new DynamicJsonObject(dictionary) : null;
        }

        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Type> SupportedTypes
        {
            get { return new ReadOnlyCollection<Type>(new List<Type>(new[] { typeof(object) })); }
        }

        #region Nested type: DynamicJsonObject

        private sealed class DynamicJsonObject : DynamicObject
        {
            private readonly IDictionary<string, object> _dictionary;

            public DynamicJsonObject(IDictionary<string, object> dictionary)
            {
                _dictionary = dictionary ?? throw new ArgumentNullException("dictionary");
            }

            public override string ToString()
            {
                var sb = new StringBuilder("{");
                ToString(sb);
                return sb.ToString();
            }

            private void ToString(StringBuilder sb)
            {
                var firstInDictionary = true;
                foreach (var pair in _dictionary)
                {
                    if (!firstInDictionary)
                        sb.Append(",");
                    firstInDictionary = false;
                    var value = pair.Value;
                    var name = pair.Key;
                    if (value is string)
                    {
                        sb.AppendFormat("{0}:\"{1}\"", name, value);
                    }
                    else if (value is IDictionary<string, object>)
                    {
                        new DynamicJsonObject((IDictionary<string, object>)value).ToString(sb);
                    }
                    else if (value is ArrayList)
                    {
                        sb.Append(name + ":[");
                        var firstInArray = true;
                        foreach (var arrayValue in (ArrayList)value)
                        {
                            if (!firstInArray)
                                sb.Append(",");
                            firstInArray = false;
                            if (arrayValue is IDictionary<string, object>)
                                new DynamicJsonObject((IDictionary<string, object>)arrayValue).ToString(sb);
                            else if (arrayValue is string)
                                sb.AppendFormat("\"{0}\"", arrayValue);
                            else
                                sb.AppendFormat("{0}", arrayValue);

                        }
                        sb.Append("]");
                    }
                    else
                    {
                        sb.AppendFormat("{0}:{1}", name, value);
                    }
                }
                sb.Append("}");
            }

            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                if (!_dictionary.TryGetValue(binder.Name, out result))
                {
                    // return null to avoid exception.  caller can check for null this way...
                    result = null;
                    return true;
                }

                result = WrapResultObject(result);
                return true;
            }

            public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
            {
                if (indexes.Length == 1 && indexes[0] != null)
                {
                    if (!_dictionary.TryGetValue(indexes[0].ToString(), out result))
                    {
                        // return null to avoid exception.  caller can check for null this way...
                        result = null;
                        return true;
                    }

                    result = WrapResultObject(result);
                    return true;
                }

                return base.TryGetIndex(binder, indexes, out result);
            }

            private static object WrapResultObject(object result)
            {
                if (result is IDictionary<string, object> dictionary)
                    return new DynamicJsonObject(dictionary);

                if (result is ArrayList arrayList && arrayList.Count > 0)
                {
                    return arrayList[0] is IDictionary<string, object>
                        ? new List<object>(arrayList.Cast<IDictionary<string, object>>().Select(x => new DynamicJsonObject(x)))
                        : new List<object>(arrayList.Cast<object>());
                }

                return result;
            }
        }

        #endregion
    }
}
