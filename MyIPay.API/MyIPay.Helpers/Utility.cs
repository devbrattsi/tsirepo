﻿using System;
using System.Globalization;
using System.IO;

namespace MyIPay.Helpers
{
    public class Utility
    {
        private static string _saveRequestAndResponseLog = AppSettings.SaveRequestAndResponseLog;
        public static void SaveRequestAndResponseLog(string fileName, string message)
        {
            if (_saveRequestAndResponseLog == "True")
                WriteLog(fileName, message);

        }
        public static void WriteLog(string message)
        {
            var dirPath = AppDomain.CurrentDomain.BaseDirectory + "RequestAndResponseLogs\\";

            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

            var filePath = $"{dirPath}log_{DateTime.Now:MMddyyyy}.txt".Replace("/", "");

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (var w = File.AppendText(filePath))
            {
                w.WriteLine("---------------------------");
                w.WriteLine("[{0:MM/dd/yyyy hh:mm:ss tt}]: {1}", DateTime.Now, message);
                w.WriteLine("---------------------------");
                w.Close();
            }
        }


        public static void WriteLog(string fileName, string message)
        {
            var dirPath = AppDomain.CurrentDomain.BaseDirectory + "RequestAndResponseLogs\\";

            if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath).Create();

            var filePath = $"{dirPath}{fileName}_Log_{DateTime.Now:MMddyyyy}.txt".Replace("/", "");

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (var w = File.AppendText(filePath))
            {
                w.WriteLine("---------------------------");
                w.WriteLine("[{0:MM/dd/yyyy hh:mm:ss tt}]: {1}", DateTime.Now, message);
                w.WriteLine("---------------------------");
                w.Close();
            }
        }

        
    }
}
