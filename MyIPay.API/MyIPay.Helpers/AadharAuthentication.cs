﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace MyIPay.Helpers
{
    public class AadharAuthentication
    {
        private void GenerateXml()
        {
            ClsAadhaar adhaar = new ClsAadhaar();
            adhaar.uid = Convert.ToInt64("");//txtid.Text);
            string struid = Convert.ToString(adhaar.uid);
            adhaar.name = "";// txtname.Text;
            string strname = adhaar.name;

            XmlDocument XDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = XDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
            XDoc.AppendChild(xmlDeclaration);


            XmlElement XElemRoot = XDoc.CreateElement("Auth");
            XElemRoot.SetAttribute("xmlns", "http://www.uidai.gov.in/authentication/uid-auth-request/1.0");
            XElemRoot.SetAttribute("ac", "public");
            XElemRoot.SetAttribute("lk", "MEWs4XwP0AzUVGSlKwZkMqeHJqyOvzIfz1rxEFm1uu0cRhoxjeWcIqY");
            XElemRoot.SetAttribute("sa", "public");
            XElemRoot.SetAttribute("tid", "public");
            XElemRoot.SetAttribute("txn", "UKC:public:" + DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            XElemRoot.SetAttribute("uid", struid);
            XElemRoot.SetAttribute("ver", "1.6");
            XDoc.AppendChild(XElemRoot);


            XmlElement Xsource = XDoc.CreateElement("Uses");
            Xsource.SetAttribute("bio", "n");
            Xsource.SetAttribute("bt", "n");
            Xsource.SetAttribute("otp", "n");

            Xsource.SetAttribute("pa", "n");
            Xsource.SetAttribute("pfa", "n");
            Xsource.SetAttribute("pi", "y");

            Xsource.SetAttribute("pin", "n");

            XElemRoot.AppendChild(Xsource);


            XmlElement Xsource1 = XDoc.CreateElement("Meta");

            Xsource1.SetAttribute("fdc", "NC");
            Xsource1.SetAttribute("idc", "NA");
            Xsource1.SetAttribute("lot", "P");
            Xsource1.SetAttribute("lov", "560103");
            Xsource1.SetAttribute("pip", "127.0.0.1");
            Xsource1.SetAttribute("udc", "UKC:SampleClient");
            XElemRoot.AppendChild(Xsource1);

            XmlElement Xsource2 = XDoc.CreateElement("Skey");

            Xsource2.SetAttribute("ci", "20150922");
            Xsource2.InnerText = SSKEY();
            XElemRoot.AppendChild(Xsource2);

            XmlElement Xsource3 = XDoc.CreateElement("Data");
            Xsource3.InnerText = EncryptPID();
            XElemRoot.AppendChild(Xsource3);

            XmlElement Xsource4 = XDoc.CreateElement("Hmac");
            Xsource4.InnerText = Hmac;
            XElemRoot.AppendChild(Xsource4);

            XDoc.Save(@"E:\test.xml");
            SendForAuthentication();


        }

        private void SendForAuthentication()
        {

            SingXML();
            string url = "http://auth.uidai.gov.in/1.6/public/9/9/MKzX8dnY5qyuO4z8neQPDqrSCMAU5pCS32obnzl83xwtFdi45gwK6QA";
            StreamReader sr1 = new StreamReader("E:\\test-signed.xml");
            String Conn = sr1.ReadToEnd();
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            byte[] requestBytes = System.Text.Encoding.ASCII.GetBytes(Conn);
            req.Method = "POST";
            req.ContentType = "text/xml";
            req.ContentLength = requestBytes.Length;
            Stream requestStream = req.GetRequestStream();
            requestStream.Write(requestBytes, 0, requestBytes.Length);
            requestStream.Close();
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            StreamReader sr = new StreamReader(res.GetResponseStream(), System.Text.Encoding.Default);
            string backstr = sr.ReadToEnd();
            XmlDocument xdocl = new XmlDocument();
            xdocl.LoadXml(backstr);
            xdocl.Save("E:\\test1.xml");
            sr.Close();
            res.Close();
        }

        String Hmac;
        private String EncryptPID()
        {
            GenratePID();
            string original = File.ReadAllText(@"E:\pid.xml");

            byte[] encrypted;

            RijndaelManaged myRijndael = new RijndaelManaged();


            encrypted = EncryptStringToBytes(original, SKey1, myRijndael.IV);

            Hmac = GetSHA256(original);
            byte[] Hmac1;
            Hmac1 = EncryptStringToBytes(Hmac, SKey1, myRijndael.IV);
            Hmac = Convert.ToBase64String(Hmac1);
            return Convert.ToBase64String(encrypted);

        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;

        }
        private byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }
        private void GenratePID()
        {
            ClsAadhaar adhaar = new ClsAadhaar();

            adhaar.name = "";//txtname.Text;
            string strname = adhaar.name;
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement XElemRoot = xmlDoc.CreateElement("Pid");
            XElemRoot.SetAttribute("ts", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            XElemRoot.SetAttribute("ver", "1.0");
            xmlDoc.AppendChild(XElemRoot);
            XmlElement Xsource = xmlDoc.CreateElement("pi");
            Xsource.SetAttribute("name", strname);
            XElemRoot.AppendChild(Xsource);
            xmlDoc.Save("E:\\pid.xml");

        }
        public string pidencryptionaes256(string demographic, string key)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(key); // 256-AES key
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(demographic);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB; // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7; // better lang support
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);

        }
        private string GetSHA256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            using (SHA256Managed algorithm = new SHA256Managed())
            {
                byte[] buffer2 = algorithm.TransformFinalBlock(bytes, 0, bytes.Length);
                byte[] hash = algorithm.Hash;
                return Convert.ToBase64String(algorithm.Hash);
            }


        }

        private void SingXML()
        {
            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(@"E:\test.xml");
                xmlDoc.PreserveWhitespace = true;

                X509Certificate2 uidCert = new X509Certificate2(@"D:\uidai-auth-client-1.6-bin\uidai-auth-client-1.6-bin\uidai-auth-client-1.6-bin/public-may2012.p12", "public", X509KeyStorageFlags.DefaultKeySet);
                using (RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)uidCert.PrivateKey)
                {
                    SignXml(xmlDoc, uidCert);
                }

                xmlDoc.Save("E:\\test-signed.xml");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {

            }

        }

        private void SignXml(XmlDocument XDoc, X509Certificate2 uidCert)
        {

            RSACryptoServiceProvider rsaKey = (RSACryptoServiceProvider)uidCert.PrivateKey;

            if (XDoc == null)
                throw new ArgumentException("XDoc");
            if (rsaKey == null)
                throw new ArgumentException("Key");

            SignedXml signedXml = new SignedXml(XDoc);
            signedXml.SigningKey = rsaKey;
            Reference reference = new Reference();
            reference.Uri = "";
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);
            signedXml.AddReference(reference);
            KeyInfo keyInfo = new KeyInfo();
            KeyInfoX509Data clause = new KeyInfoX509Data();
            clause.AddSubjectName(uidCert.Subject);
            clause.AddCertificate(uidCert);
            keyInfo.AddClause(clause);
            signedXml.KeyInfo = keyInfo;
            signedXml.ComputeSignature();
            XmlElement xmlDigitalSignature = signedXml.GetXml();
            System.Console.WriteLine(signedXml.GetXml().InnerXml);
            XDoc.DocumentElement.AppendChild(XDoc.ImportNode(xmlDigitalSignature, true));


        }


        UnicodeEncoding ByteConverter = new UnicodeEncoding();
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        public byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //X509Store store = new X509Store(StoreLocation.CurrentUser);
                //store.Open(OpenFlags.ReadOnly);
                //X509Certificate2Collection certcollection = store.Certificates;
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.PreserveWhitespace = true;
                //X509Certificate2 uidCert = new X509Certificate2(@"D:\uidai-auth-client-1.6-bin\uidai-auth-client-1.6-bin\uidai-auth-client-1.6-bin/uidai_auth_stage.cer", "public", X509KeyStorageFlags.DefaultKeySet);

                using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                {
                    rsa.ImportParameters(RSAKeyInfo);

                    encryptedData = rsa.Encrypt(DataToEncrypt, DoOAEPPadding);
                }


                return encryptedData;
            }

            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }

        }

        byte[] SKey1 = null;

        private String SSKEY()
        {

            RijndaelManaged sessionKey = new RijndaelManaged();
            sessionKey.KeySize = 256;
            byte[] encryptedtext;
            sessionKey.GenerateKey();
            X509Certificate2 uidCert = new X509Certificate2(@"D:\uidai-auth-client-1.6-bin\uidai-auth-client-1.6-bin\uidai-auth-client-1.6-bin/uidai_auth_stage.cer", "public", X509KeyStorageFlags.DefaultKeySet);

            RSACryptoServiceProvider RSA = (RSACryptoServiceProvider)uidCert.PublicKey.Key;
            {
                RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
                encryptedtext = RSAEncrypt(sessionKey.Key, RSAKeyInfo, false);
            }
            SKey1 = sessionKey.Key;
            return Convert.ToBase64String(encryptedtext);

        }




        static int[,] d = new int[,]
        {
             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
             {1, 2, 3, 4, 0, 6, 7, 8, 9, 5},
             {2, 3, 4, 0, 1, 7, 8, 9, 5, 6},
             {3, 4, 0, 1, 2, 8, 9, 5, 6, 7},
             {4, 0, 1, 2, 3, 9, 5, 6, 7, 8},
             {5, 9, 8, 7, 6, 0, 4, 3, 2, 1},
             {6, 5, 9, 8, 7, 1, 0, 4, 3, 2},
             {7, 6, 5, 9, 8, 2, 1, 0, 4, 3},
             {8, 7, 6, 5, 9, 3, 2, 1, 0, 4},
             {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
        };
        static int[,] p = new int[,]
        {
             {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
             {1, 5, 7, 6, 2, 8, 3, 0, 9, 4},
             {5, 8, 0, 3, 7, 9, 6, 1, 4, 2},
             {8, 9, 1, 6, 0, 4, 3, 5, 2, 7},
             {9, 4, 5, 3, 1, 2, 6, 8, 7, 0},
             {4, 2, 8, 6, 5, 7, 3, 9, 0, 1},
             {2, 7, 9, 3, 8, 0, 6, 4, 1, 5},
             {7, 0, 4, 6, 9, 1, 3, 2, 5, 8}
        };

        static int[] inv = { 0, 4, 3, 2, 1, 5, 6, 7, 8, 9 };

        public static bool validateVerhoeff(string num)
        {
            int c = 0; int[] myArray = StringToReversedIntArray(num);
            for (int i = 0; i < myArray.Length; i++)
            {
                c = d[c, p[(i % 8), myArray[i]]];
            }
            return c == 0;

        }
        private static int[] StringToReversedIntArray(string num)
        {
            int[] myArray = new int[num.Length];
            for (int i = 0; i < num.Length; i++)
            {
                myArray[i] = int.Parse(num.Substring(i, 1));
            }
            Array.Reverse(myArray); return myArray;
        }



    }

    public class ClsAadhaar
    {
        public long uid { get; set; }
        public string name { get; set; }
        public string dob { get; set; }
        public string dobt { get; set; }
        public string gender { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string building { get; set; }
        public string landmark { get; set; }

        public string street { get; set; }
        public string locality { get; set; }
        public string vtc { get; set; }
        public string district { get; set; }
        public string state { get; set; }
        public string pincode { get; set; }
    }
}