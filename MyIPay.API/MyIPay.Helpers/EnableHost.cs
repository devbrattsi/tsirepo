﻿using System.Net;

namespace MyIPay.Helpers
{
    public class EnableHost
    {
        public static void EnableTrustedHosts()
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            //Trust all certificates
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
        }
    }
}
