﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using System.Text;

namespace MyIPay.Helpers
{
    public class EncrypterReg
    {
        const string ASYMMETRIC_ALGO = "RSA/ECB/PKCS1Padding";
        DateTime certExpiryDate;
        RsaKeyParameters publicKey;
        const int SYMMETRIC_KEY_SIZE = 128;
        const int IV_SIZE_BITS = 96;
        const int AAD_SIZE_BITS = 128;
        const int AUTH_TAG_SIZE_BITS = 128;
        byte[] PublicKey = null;

        public EncrypterReg()
        { }
        public EncrypterReg(string publicKeyFileName)
        {
            FileStream stream = null;
            try
            {
                byte[] fileBytes = File.ReadAllBytes(publicKeyFileName);
                PublicKey = new byte[fileBytes.Length];
                Array.Copy(fileBytes, PublicKey, fileBytes.Length);
                X509Certificate certificate = new X509CertificateParser().ReadCertificate(fileBytes);
                this.publicKey = (RsaKeyParameters)certificate.GetPublicKey();
                this.certExpiryDate = certificate.NotAfter;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize encryption module", ex);
            }
            finally
            {
                if (stream != null)
                {
                    try
                    {
                        stream.Close();
                    }
                    catch
                    {
                    }
                }
            }
        }
        public EncrypterReg(byte[] publicKeyBytes)
        {
            try
            {
                PublicKey = new byte[publicKeyBytes.Length];
                Array.Copy(publicKeyBytes, PublicKey, publicKeyBytes.Length);
                X509Certificate certificate = new
                X509CertificateParser().ReadCertificate(publicKeyBytes);
                this.publicKey = (RsaKeyParameters)certificate.GetPublicKey();
                this.certExpiryDate = certificate.NotAfter;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not initialize encryption module", ex);
            }
        }
        public string EncryptUsingPublicKey(byte[] data)
        {
            try
            {
                IBufferedCipher cipher = CipherUtilities.GetCipher(ASYMMETRIC_ALGO);
                cipher.Init(true, this.publicKey);
                return Convert.ToBase64String(cipher.DoFinal(data));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string EncryptUsingSessionKey(byte[] skey, byte[] data)
        {
            try
            {
                PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new AesEngine());
                cipher.Init(true, new KeyParameter(skey));
                byte[] sourceArray = new byte[cipher.GetOutputSize(data.Length)];
                int num2 = cipher.ProcessBytes(data, 0, data.Length, sourceArray, 0);
                int num3 = cipher.DoFinal(sourceArray, num2);
                byte[] destinationArray = new byte[num2 + num3];
                Array.Copy(sourceArray, 0, destinationArray, 0, destinationArray.Length);
                return Convert.ToBase64String(destinationArray);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }




        public string GenerateSessionKey()
        {
            try
            {
                SecureRandom random = new SecureRandom();
                KeyGenerationParameters parameters = new KeyGenerationParameters(random,
                SYMMETRIC_KEY_SIZE);
                CipherKeyGenerator keyGenerator = GeneratorUtilities.GetKeyGenerator("AES");

                keyGenerator.Init(parameters);
                return Convert.ToBase64String(keyGenerator.GenerateKey());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string GenerateSha256Hash(byte[] message)
        {
            try
            {
                IDigest digest = new Sha256Digest();
                digest.Reset();
                byte[] buffer = new byte[digest.GetDigestSize()];
                digest.BlockUpdate(message, 0, message.Length);
                digest.DoFinal(buffer, 0);

                return Convert.ToBase64String(buffer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string GetCertificateIdentifier()
        {
            try
            {
                return this.certExpiryDate.ToString("yyyyMMdd");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private byte[] getIV(string ts)
        {
            try
            {
                return GetLastBits(ts, IV_SIZE_BITS / 8);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private byte[] GetLastBits(string ts, int bits)
        {
            try
            {
                byte[] tsBytes = Encoding.Default.GetBytes(ts);
                byte[] LastBits = new byte[bits];
                Array.Copy(tsBytes, tsBytes.Length - bits, LastBits, 0, bits);
                return LastBits;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private byte[] getAad(string ts)
        {
            try
            {
                return GetLastBits(ts, AAD_SIZE_BITS / 8);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public byte[] GetPublicKeyBytes()
        {
            return PublicKey;
        }
    }
}
