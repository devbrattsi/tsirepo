﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MyIPay.Helpers
{
    public class Eko
    {
        public static string GenerateSecretKeyTimestamp()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        }

        public static string GenerateSecretKey(string timestamp)
        {
            // Initializing key in some variable. You will receive this key from Eko via email
            String key = AppSettings.EkoLiveAuthenticationPassword;

            var keyBytes = Encoding.UTF8.GetBytes(key);

            // Encode it using base64
            string encodedKey = Convert.ToBase64String(keyBytes);

            //Getting bytes from Encode Key
            var encodedKeyBytes = Encoding.UTF8.GetBytes(encodedKey);

            // Get secret_key_timestamp: current timestamp in milliseconds as STRING (which works as a salt)

            string secretKeyTimeStamp = timestamp;

            var secretKeyTimeStampBytes = Encoding.UTF8.GetBytes(secretKeyTimeStamp);

            var hashHMAC = HashHMAC(encodedKeyBytes, secretKeyTimeStampBytes);

            string secretKey = Convert.ToBase64String(hashHMAC);

            return secretKey;
        }


        private static byte[] HashHMAC(byte[] keyByte, byte[] secretKeyTimeStampBytes)
        {
            HMACSHA256 hmac = new HMACSHA256(keyByte);
            hmac.Initialize();
            byte[] rawHmac = hmac.ComputeHash(secretKeyTimeStampBytes);
            return rawHmac;
        }

    }


    public enum EkoServiceCodes
    {
        AEPS = 1,
        //CashCollectionCDM = 2,
        //CashCollectionCash =3,
        PanVerification =4,
        Ekyc = 5
    }
}
