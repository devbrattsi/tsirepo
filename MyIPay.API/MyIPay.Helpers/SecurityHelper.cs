﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MyIPay.Helpers
{
    public class SecurityHelper
    {
        public const string Encryptionkey = "MyIPayDefaultPassword#08$04&19@94";// "MYIPAYDEFAULTPASSWORD";//16102018";//

        public static string GenerateSecretKeyTimestamp()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        }

        public static string GenerateSecretKey(string timestamp)
        {
            // Initializing key in some variable. You will receive this key from Eko via email
            string key = AppSettings.ApiAuthenticationPassword;

            var keyBytes = Encoding.UTF8.GetBytes(key);

            // Encode it using base64
            string encodedKey = Convert.ToBase64String(keyBytes);

            //Getting bytes from Encode Key
            var encodedKeyBytes = Encoding.UTF8.GetBytes(encodedKey);

            // Get secret_key_timestamp: current timestamp in milliseconds as STRING (which works as a salt)

            var secretKeyTimeStampBytes = Encoding.UTF8.GetBytes(timestamp);

            var hashHMAC = GenerateHmacSha256Hash(encodedKeyBytes, secretKeyTimeStampBytes);

            string secretKey = Convert.ToBase64String(hashHMAC);

            return secretKey;
        }



        public static string Encrypt(string clearText)
        {
            var clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(Encryptionkey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(),
                            CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }

                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            try
            {
                var cipherBytes = Convert.FromBase64String(cipherText);
                using (var encryptor = Aes.Create())
                {
                    var pdb = new Rfc2898DeriveBytes(Encryptionkey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    if (encryptor != null)
                    {
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);

                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(),
                                CryptoStreamMode.Write))
                            {
                                cs.Write(cipherBytes, 0, cipherBytes.Length);
                                cs.Close();
                            }

                            cipherText = Encoding.Unicode.GetString(ms.ToArray());
                        }
                    }
                }
            }
            catch (FormatException fx)
            {
                cipherText = "";
            }
            catch (Exception ex)
            {
                cipherText = "";
            }
            return cipherText;
        }

        public static string Encrypt(string clearText, string key)
        {
            var clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(),
                            CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }

                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText, string key)
        {
            try
            {
                var cipherBytes = Convert.FromBase64String(cipherText);
                using (var encryptor = Aes.Create())
                {
                    var pdb = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    if (encryptor != null)
                    {
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);

                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(),
                                CryptoStreamMode.Write))
                            {
                                cs.Write(cipherBytes, 0, cipherBytes.Length);
                                cs.Close();
                            }

                            cipherText = Encoding.Unicode.GetString(ms.ToArray());
                        }
                    }
                }
            }
            catch (FormatException fx)
            {
                cipherText = "";
            }
            catch (Exception ex)
            {
                cipherText = "";
            }
            return cipherText;
        }

        public static DateTime UnixTimeMillisecondsToDateTime(double milliseconds)
        {
            double timestamp = Convert.ToDouble(milliseconds);
            // Format our new DateTime object to start at the UNIX Epoch
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            // Add the timestamp (number of seconds since the Epoch) to be converted
            return dateTime.AddMilliseconds(timestamp);
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            var textBytes = Encoding.UTF8.GetBytes(text);

            //compute hash from the bytes of text  
            md5.ComputeHash(textBytes);

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }

        public static byte[] GenerateHmacSha256Hash(byte[] keyByte, byte[] secretKeyTimeStampBytes)
        {
            HMACSHA256 hmac = new HMACSHA256(keyByte);
            hmac.Initialize();
            byte[] rawHmac = hmac.ComputeHash(secretKeyTimeStampBytes);
            return rawHmac;
        }

        public static string GenerateHmacSha256Hash(string key, string secretKey)
        {

            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);

            HMACSHA256 hmac = new HMACSHA256(keyBytes);
            hmac.Initialize();
            byte[] rawHmac = hmac.ComputeHash(secretKeyBytes);

            return Convert.ToBase64String(rawHmac);
        }

        //public static string GenerateHmacSha256Hash(string key, string secretKey)
        //{
        //    byte[] keyBytes = Encoding.UTF8.GetBytes(key);
        //    byte[] secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);

        //    HMACSHA256 hmac = new HMACSHA256(keyBytes);
        //    hmac.Initialize();
        //    byte[] rawHmac = hmac.ComputeHash(secretKeyBytes);

        //    StringBuilder stringBuilder = new StringBuilder();

        //    for (int i = 0; i < rawHmac.Length; i++)
        //    {
        //        //change it into 2 hexadecimal digits  
        //        //for each byte  
        //        stringBuilder.Append(rawHmac[i].ToString("x2"));
        //    }

        //    return stringBuilder.ToString();
        //}

        public static string GenerateSha256Hash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            using (SHA256Managed algorithm = new SHA256Managed())
            {
                byte[] buffer2 = algorithm.TransformFinalBlock(bytes, 0, bytes.Length);
                byte[] hash = algorithm.Hash;
                return Convert.ToBase64String(algorithm.Hash);
            }
        }



        public static string GenerateSha512Hash(string text)
        {
            using (SHA512Managed algorithm = new SHA512Managed())
            {
                byte[] bytes = Encoding.UTF8.GetBytes(text);

                byte[] result = algorithm.ComputeHash(bytes);

                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 0; i < result.Length; i++)
                {
                    //change it into 2 hexadecimal digits  
                    //for each byte  
                    stringBuilder.Append(result[i].ToString("x2"));
                }

                return stringBuilder.ToString();
            }
        }

        public static string GenerateFeSessionId()
        {
            int maxSize = 20;
            int minSize = 10;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();

            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);

            data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(minSize, maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        private static long GenerateId()
        {
            int _min = 100000000;
            int _max = 999999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public static string GenerateClientReferenceId()
        {
            return $"{Constants.AppUserName}{GenerateId()}";
        }

        public static string GenerateClientReferenceId(string prefixText)
        {
            return $"{prefixText}{GenerateSecretKeyTimestamp()}";
        }
       
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
