﻿namespace MyIPay.Helpers
{
    public class SqlQueryConstants
    {
        public const string SpGetYatraCcfByUsp = "EXECUTE sp_GetYatraCcfByUsp @agentId,@usp,@amount";
        public const string SpWoohooOauthTokenCreateOrUpdate = "EXECUTE usp_WoohooOauthTokens_CreateOrUpdate @ConsumerKey,@Token,@TokenSecret";
        public const string SpGetGiftCardCcf = "EXECUTE [woohoo].[sp_GetGiftCardCcf] @agentId,@amount";


    }
}
