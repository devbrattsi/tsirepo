﻿namespace MyIPay.Helpers
{
    public class Constants
    {
        public const int SuccessStatusCode = 0;
        public const int FailureStatusCode = 1;
        public const string AcceptRemarks = "Accepted";
        public const string RejectRemarks = "Rejected";
        public const string AgentIdNotExist = "Failed! Agent id does not exist.";
        public const string InsufficientBalance = "Transaction is failed due to Insufficient Balance in Agent Wallet.";
        public const string BalanceAvailable = "Success! Balance Available.";
        public const string InvalidTimestampOrSecretKey = "Invalid timestamp or secret key.";
        
        public const string InvalidUserIdOrPassword = "Invalid user id or password.";
        public const string InvalidAgentCode = "Invalid agent code.";
        public const string InvalidVirtualAccountNumber = "Invalid virtual account number.";
        public const int EditableAmountCode = 0;
        public const string DuplicateISurePayId = "Duplicate ISurePay ID.";
        public const string DuplicateUtrNumber = "Duplicate UTR Number.";
        public const string DuplicateISureId = "Duplicate IBANK_Transaction_Id.";

        public const string Accept = "Accept";
        public const string Reject = "Reject";
        public const string InvalidClientCode = "Invalid Client Code.";

        public const string SuccessMsg = "Success";
        public const string FailedMsg = "Fail";
        public const string Failed = "Failed";

        public const string AuthenticationType = "AUTHORIZE_USER";
        public const string SuccessTokenResponsecode = "200";
        public const string Authrize_User = "AUTHORIZE_USER";
        public const string ValidToken = "Token is valid.";
        public const string AuthorizeUser = "Authorize User";

        public const string InvalidTokenResponsecode = "401";
        public const string UnAuthrize_User = "UNAUTHORIZE_USER";
        public const string InvalidToken = "Invalid token.";
        public const string UnAuthorizeUser = "UnAuthorize User";

        public const string ExpireTokenResponsecode = "400";
        public const string TokenExpire = "Token expired.";

        public const string BookingSuccessfullyCancelledResponseCode = "00";
        public const string BookingSuccessfullyCancelled = "Booking is Successfully Cancelled & Refunded.";

        public const string SignatureMismatchResponseCode = "01";
        public const string ParameterMissingOrInvalidResponsecode = "02";
        public const string Invalid_User = "Invalid_User";
        public const string ParameterMissing = "Required parameters is Missing or Invalid.";
        public const string RequiredDataMismatch = "Required data mismatch";

        public const string TechnicalFailureResponsecode = "03";
        public const string AlreadyCancelledAndRefundedResponsecode = "03";
        public const string AlreadyCancelledAndRefunded = "This Order Id is Already Cancelled & Refunded.";


        public const string TechnicalFailure = "Failed! Technical failure.";
        public const string ErrorInApi = "Failed! Error in API";

        public const string SignatureMismatch = "Signature Mismatch.";



        public const string InvalidAmount = "Failed! Amount can't be negative or zero.";
        public const string InvalidTransactionAmount = "Failed! Invalid Transaction Amount.";
        public const string OrderDetailFound = "Success! Order detail found.";
        public const string InvalidOrderId = "Failed! Invalid order id.";
        public const string DuplicateOrderId = "Failed! Duplicate order id found.";
        public const string RecordNotFound = "Failed!  Record does not found for cancellation.";

        public const string BookingCancellationDone = "Success! Booking cancellation successfully done. Transaction amount has been refunded.";
        public const string BookingAlreadyCanceled = "Failed! Booking has been already canceled.";
        public const string OrderCanNotCancelledResponseCode = "06";
        public const string OrderCanNotCancelled = "This Order Id cannot Cancelled.";

        public const string RefundAmountIsLargerThanReamingAmountResponseCode = "04";
        public const string RefundAmountIsLargerThanReamingAmount = "Refund Amount is more than Transaction Amount.";

        public const string OrderIdAlreadyFullyRefundedResponseCode = "05";
        public const string OrderIdAlreadyFullyRefunded = "This Order Id Already Fully Refunded.";

        public const string ErrorInBookingCancellationApiResponseCode = "07";
        public const string ErrorInBookingCancellationApi = "Problem To Parse the Request Data in Booking Cancellation Api";

        public const string InvalidMerchantId = "Failed! Invalid merchant id.";
        public const string InvalidMerchantIdResponseCode = "09";
        public const string TransactionFailed = "Transaction is failed.";
        public const string TransactionSuccessful = "Transaction is successful.";
        public const string ProblemToParseRequestData = "Problem to Parse the Request Data in payment deduction api.";
        public const string TransactionAmountMisMatch = "Transaction Amount MisMatch.";


        // Service URL for Avenue API
        public const string BillFetchRequestAvenueServiceURL = "billpay/extBillCntrl/billFetchRequest/xml?";
        public const string BillPayRequestAvenueServiceURL = "billpay/extBillPayCntrl/billPayRequest/xml?";
        public const string TransactionStatusAvenueServiceURL = "billpay/transactionStatus/fetchInfo/xml?";
        public const string ComplaintsRegisterAvenueServiceURL = "billpay/extComplaints/register/xml?";
        public const string ComplaintsStatusAvenueServiceURL = "billpay/extComplaints/track/xml?";
        public const string MDMAvenueServiceURL = "billpay/extMdmCntrl/mdmRequest/xml?";
        public const string DepositEnquiryServiceURL = "billpay/enquireDeposit/fetchDetails/xml?";

        //public const string AccessCode = "AVAE24TD60XB83NUXN";
        //public const string Version = "1.0";
        //public const string InstituteId = "TS01";
        //public const string Key = "B51AE19828B8CBB698480FCBD30B4A01";

        // Authentication details for Avenue API
        public const string AccessCode = "AVGH36RT10GG56WMUR";
        public const string Version = "1.0";
        public const string InstituteId = "CC37";
        public const string Key = "325515514B6E58350C3CC154E055FA80";


        public const string Distributor = "DISTRIBUTOR";

        public const string Refund = "R";
        public const string Commission = "C";
        public const string AgentCommissionCreditedSuccessfully = "Agent Commission Successfully Refunded.";
        public const string TSI = "TSI";
        public const string GC = "GC";


        #region Custom Response Status
        public const string Unknown = "UNKNOWN";
        public const string Error = "ERROR";
        public const string Success = "SUCCESS";
        public const string Failure = "FAILURE";
        public const string Initiated = "INITIATED";
        public const string RefundPending = "REFUND PENDING";
        public const string Refunded = "REFUNDED";
        public const string Hold = "HOLD";
        #endregion

        #region BillDesk
        public const string BillDeskTraceId = "BD-Traceid";
        public const string BillDeskTimestamp = "BD-Timestamp";
        public const string Authorization = "Authorization";
        public const string ContentType = "Content-Type";


        #endregion
        public const string AppUserName = "TSI";


        #region AEPS
        //public const int SuccessStatusCode = 0;
        //public const int FailureStatusCode = 1;
        //public const string AcceptRemarks = "Accepted";
        //public const string RejectRemarks = "Rejected";
        //public const string AgentIdNotExist = "Failed! Agent id does not exist.";
        //public const string InsufficientBalance = "Transaction is failed due to Insufficient Balance in Agent Wallet.";
        // public const string BalanceAvailable = "Success! Balance Available.";
        //public const string InvalidTimestampOrSecretKey = "Invalid timestamp or secret key.";


        //public const string InvalidUserIdOrPassword = "Invalid user id or password.";
        //public const string InvalidAgentCode = "Invalid agent code.";
        //public const int EditableAmountCode = 0;
        //public const string DuplicateISurePayId = "Duplicate ISurePay ID.";
        //public const string DuplicateISureId = "Duplicate IBANK_Transaction_Id.";

        //public const string Accept = "Accept";
        //public const string Reject = "Reject";
        // public const string InvalidClientCode = "Invalid Client Code.";

        //public const string SuccessMsg = "Success";
        //public const string FailedMsg = "Fail";
        //public const string Failed = "Failed";

        //public const string AuthenticationType = "AUTHORIZE_USER";
        //public const string SuccessTokenResponsecode = "200";
        //public const string Authrize_User = "AUTHORIZE_USER";
        //public const string ValidToken = "Token is valid.";
        //public const string AuthorizeUser = "Authorize User";

        //public const string InvalidTokenResponsecode = "401";
        //public const string UnAuthrize_User = "UNAUTHORIZE_USER";
        //public const string InvalidToken = "Invalid token.";
        //public const string UnAuthorizeUser = "UnAuthorize User";

        //public const string ExpireTokenResponsecode = "400";
        //public const string TokenExpire = "Token expired.";

        //public const string BookingSuccessfullyCancelledResponseCode = "00";
        //public const string BookingSuccessfullyCancelled = "Booking is Successfully Cancelled & Refunded.";

        //public const string SignatureMismatchResponseCode = "01";
        //public const string ParameterMissingOrInvalidResponsecode = "02";
        //public const string Invalid_User = "Invalid_User";
        //public const string ParameterMissing = "Required parameters is Missing or Invalid.";
        //public const string RequiredDataMismatch = "Required data mismatch";

        //public const string TechnicalFailureResponsecode = "03";
        //public const string AlreadyCancelledAndRefundedResponsecode = "03";
        //public const string AlreadyCancelledAndRefunded = "This Order Id is Already Cancelled & Refunded.";


        //public const string TechnicalFailure = "Failed! Technical failure.";
        //public const string ErrorInApi = "Failed! Error in API";

        //public const string SignatureMismatch = "Signature Mismatch.";



        //public const string InvalidAmount = "Failed! Amount can't be negative or zero.";
        //public const string OrderDetailFound = "Success! Order detail found.";
        //public const string InvalidOrderId = "Failed! Invalid order id.";
        //public const string DuplicateOrderId = "Failed! Duplicate order id found.";
        //public const string RecordNotFound = "Failed!  Record does not found for cancellation.";

        //public const string BookingCancellationDone = "Success! Booking cancellation successfully done. Transaction amount has been refunded.";
        //public const string BookingAlreadyCanceled = "Failed! Booking has been already canceled.";
        //public const string OrderCanNotCancelledResponseCode = "06";
        //public const string OrderCanNotCancelled = "This Order Id cannot Cancelled.";

        //public const string RefundAmountIsLargerThanReamingAmountResponseCode = "04";
        //public const string RefundAmountIsLargerThanReamingAmount = "Refund Amount is more than Transaction Amount.";

        //public const string OrderIdAlreadyFullyRefundedResponseCode = "05";
        //public const string OrderIdAlreadyFullyRefunded = "This Order Id Already Fully Refunded.";

        //public const string ErrorInBookingCancellationApiResponseCode = "07";
        //public const string ErrorInBookingCancellationApi = "Problem To Parse the Request Data in Booking Cancellation Api";

        //public const string InvalidMerchantId = "Failed! Invalid merchant id.";
        //public const string InvalidMerchantIdResponseCode = "09";
        //public const string TransactionFailed = "Transaction is failed.";
        //public const string TransactionSuccessful = "Transaction is successful.";
        //public const string ProblemToParseRequestData = "Problem to Parse the Request Data in payment deduction api.";
        //public const string TransactionAmountMisMatch = "Transaction Amount MisMatch.";

        public const string TransactionIdExist = "Transaction Id already exist.";
        public const string InvalidUsp = "Invalid Usp";
        public const string InvalidRequest = "Invalid Request";
        //public const string Distributor = "DISTRIBUTOR";
        //public const string AppUserName = "TSI";
        public const string BalanceEnquiry = "Balance Enquiry";
        public const string CashWithdrawal = "Cash Withdrawal";
        public const string CW = "CW";
        public const string BE = "BE";

        #region AEPS
        public const string LanguageCode = "en";
        public const int IndicatorForUid = 0;

        #region Transaction Type
        public const string Merchant = "M";
        public const string Cashwithdraw = "C";
        public const string BillPaymentsPrepaidPhone = "P";
        public const string BillPaymentsPostpaidPhone = "Q";
        public const string Electricity = "E";
        #endregion

        #region Payment Type
        public const string LastUsedBank = "LU";
        public const string Bank = "B";
        public const string PayTM = "P";
        public const string Mpesa = "M";
        public const string Mobikwik = "K";
        public const string Bitcoin = "C";
        #endregion


        public const string TransactionTimestamp = "trnTimestamp";
        public const string Hash = "hash";
        public const string DeviceIMEI = "deviceIMEI";
        public const string Eskey = "eskey";
        public const string EncryptedJson = "encryptedJson";


        public const string Username = "Username";
        public const string Password = "Password";
        public const string Agency = "Agency";
        public const string Subscriber = "Subscriber";
        #endregion


        //#region Custom Response Status
        //public const string Unknown = "UNKNOWN";
        //public const string Error = "ERROR";
        //public const string Success = "SUCCESS";
        //public const string Failure = "FAILURE";
        //public const string Initiated = "INITIATED";
        //public const string RefundPending = "REFUND PENDING";
        //public const string Refunded = "REFUNDED";
        //public const string Hold = "HOLD";
        //#endregion

        #endregion

        #region Toffee
        public const string GrantTypePassword = "password";
        public const string GrantTypeRefreshToken = "refresh_token";
        #endregion

        public const string TsiHeadOfficeLatLong = "28.519349, 77.280026";
        public const double TsiHeadOfficeLat = 28.519349;
        public const double TsiHeadOfficeLong = 77.280026;



        public const string TokenMissing = "Token missing";
        public const string OAuthCallback = "oauth_callback";
        public const string Oob = "oob";
        public const string OAuthConsumerKey = "oauth_consumer_key";
        public const string OAuthNonce = "oauth_nonce";
        public const string OAuthTimestamp = "oauth_timestamp";
        public const string OAuthToken = "oauth_token";
        public const string OAuthSignatureMethod = "oauth_signature_method";
        public const string OAuthVersion = "oauth_version";
        public const string OAuthVersionValue = "1.0";
        public const string OAuthSignatureMethodType = "HMAC-SHA1";
        public const string OAuthVerifier = "oauth_verifier";
        public const string ApplicationJson = "application/json";
        public const string OAuthTokenSecret = "oauth_token_secret";
        public const string HttpMethodGet = "GET";
        public const string HttpMethodPost = "POST";
        public const string GiftCardRequestIdPrefix = "TSIGC";
        public const string MyIPayApi = "MyIPay API";
        public const string GiftCard = "Gift Card";

        public const string Basic = "Basic";
        public const string ActionUrl = "ActionUrl";

        public const string NotFound = "Failed!  Record does not found.";


    }


    public enum TxStatus
    {
        Unknown = -2,
        Error = -1,
        Success = 0,
        Failure = 1,
        Initiated = 2,
        RefundPending = 3,
        Refunded = 4,
        Hold = 5
    }


    public class ClaimConstants
    {
        public const string SuperAdmin = "Super Admin";
        public const string Admin = "Admin";
        public const string SubAdmin = "Sub Admin";
        public const string User = "User";
        public const string Distributor = "Distributor";

        public const string ClaimsUserAgentId = "Claims.User.AgentId";
        public const string ClaimsDistributerId = "Claims.DistributerId";
        public const string ClaimsCcfTemplateName = "Claims.CcfTemplateName";
        public const string ClaimsDistributorAgentId = "Claims.Distributor.AgentId";
    }

    public enum USP
    {
        AEPS,//Aadhaar Enabled Payment System
        BBPS,
        BE,//Balance Enquiry
        KIOSK,
        PSPCL,
        AT,
        IRCTC,
        IRCTCAC, //IRCTC AC
        IRCTCNONAC, //IRCTC NON - AC
        HTL,//Hotel
        TRVL_INS, // Travel Insurance
        DMT, // Domestic Money Transfer
        INMT, //Indo Nepal Money Transfer
        DMTVER, // Add Recipient Bank Details Verification
        GC,//GIFT_CARD,
        FOREX,
        MOBILE_INSURANCE,
        INTERNATIONAL_SIM_CARD,
        PR, // Prepaid Recharge
        TPDDL,
        INSURANCE,
        HI, // Health Insurance
        MPPKVVCL,
        TI // Toffee Insurance
    }

    public enum Partner
    {
        Airtel,
        Avenue,
        Eko,
        ISure,
        FingPay,
        GoProcessing,
        Matrix,
        Yatra,
        QwikCilver
    }

    public enum Mode
    {
        C,//C for Cancellation
        D,//D for Demo
        F,//F for Failure
        L//L for Live||Success
    }


    public enum TransactionStatus
    {
        Success = 0,
        Fail = 1,
        Initiated = 2,
        RefundPending = 3,
        Refunded = 4
    }


    public enum Platform
    {
        Web = 0,
        Mobile = 1
    }
    public enum CustomResponseStatusCode
    {
        Unknown = -2,
        Error = -1,
        Success = 0,
        Failure = 1,
        Initiated = 2,
        RefundPending = 3,
        Refunded = 4,
        Hold = 5
    }
}
