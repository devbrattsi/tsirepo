﻿using System;
using System.Configuration;

namespace MyIPay.Helpers
{
    public class AppSettings
    {
        public static string ApiAuthenticationPassword => ConfigurationManager.AppSettings["ApiAuthenticationPassword"].ToString();
        public static string UatSecretKeyTimeStamp => ConfigurationManager.AppSettings["UatSecretKeyTimeStamp"].ToString();
        public static string UatSecretKey => ConfigurationManager.AppSettings["UatSecretKey"].ToString();
        public static string ISureUserId => ConfigurationManager.AppSettings["ISureUserId"].ToString();
        public static string ISurePassword => ConfigurationManager.AppSettings["ISurePassword"].ToString();
        public static string ISureClientCode => ConfigurationManager.AppSettings["ISureClientCode"].ToString();

        public static string SaveRequestAndResponseLog = ConfigurationManager.AppSettings["SaveRequestAndResponseLog"].ToString();


        //Key for dmt live environment
        public static string EkoLiveApiBaseUrl => ConfigurationManager.AppSettings["EkoLiveApiBaseUrl"].ToString();
        public static string EkoLiveDeveloperKey => ConfigurationManager.AppSettings["EkoLiveDeveloperKey"].ToString();
        public static long EkoLiveInitiatorId => Convert.ToInt64(ConfigurationManager.AppSettings["EkoLiveInitiatorId"]);
        public static string EkoLiveAuthenticationPassword => ConfigurationManager.AppSettings["EkoLiveAuthenticationPassword"].ToString();

        //Key for dmt uat environment
        public static string EkoUatApiBaseUrl => ConfigurationManager.AppSettings["EkoUatApiBaseUrl"].ToString();
        public static string EkoUatDeveloperKey => ConfigurationManager.AppSettings["EkoUatDeveloperKey"].ToString();
        public static long EkoUatInitiatorId => Convert.ToInt64(ConfigurationManager.AppSettings["EkoUatInitiatorId"]);

        public static string EkoDmtMode = ConfigurationManager.AppSettings["EkoDmtMode"].ToString();



        public static string ForexApiBaseUrl = ConfigurationManager.AppSettings["ForexApiBaseUrl"].ToString();

        public static string AvenueBaseUrl = ConfigurationManager.AppSettings["AvenueBaseUrl"].ToString();
        public static string PspclBaseUrl = ConfigurationManager.AppSettings["PSPCLBaseURL"].ToString();

        public static string PspclUsername = ConfigurationManager.AppSettings["PSPCL_Username"].ToString();
        public static string PspclPassword = ConfigurationManager.AppSettings["PSPCL_Password"].ToString();


        public static string EkoUatUserCode => ConfigurationManager.AppSettings["EkoUatUserCode"].ToString();
        public static string EkoLiveUserCode => ConfigurationManager.AppSettings["EkoLiveUserCode"].ToString();
        public static string YatraApiBaseUrl => ConfigurationManager.AppSettings["YatraApiBaseUrl"].ToString();

        public static string YatraAesApiBaseUrl => ConfigurationManager.AppSettings["YatraAesApiBaseUrl"].ToString();

        public static string Mode => ConfigurationManager.AppSettings["Mode"].ToString();
        public static string SaltForYatra => ConfigurationManager.AppSettings["SaltForYatra"].ToString();
        public static string YatraMerchantId => ConfigurationManager.AppSettings["YatraMerchantId"].ToString();

        #region Airtel 

        #region Bank
        public static string AirtelBankMode = ConfigurationManager.AppSettings["AirtelBankMode"].ToString();
        public static string AirtelBankLiveApiBaseUrl => ConfigurationManager.AppSettings["AirtelBankLiveApiBaseUrl"].ToString();
        public static string AirtelBankUatApiBaseUrl => ConfigurationManager.AppSettings["AirtelBankUatApiBaseUrl"].ToString();
        public static string AirtelBankPartnerId => ConfigurationManager.AppSettings["AirtelBankPartnerId"].ToString();
        public static string AirtelBankSalt => ConfigurationManager.AppSettings["AirtelBankSalt"].ToString();
        #endregion

        #region CashDrop
        public static string AirtelCashDropMode = ConfigurationManager.AppSettings["AirtelCashDropMode"].ToString();
        public static string AirtelCashDropUatBaseUrl => ConfigurationManager.AppSettings["AirtelCashDropUatBaseUrl"].ToString();
        public static string AirtelCashDropUatPartnerId => ConfigurationManager.AppSettings["AirtelCashDropUatPartnerId"].ToString();
        public static string AirtelCashDropUatSalt => ConfigurationManager.AppSettings["AirtelCashDropUaSalt"].ToString();
        public static string AirtelCashDropUatUsername => ConfigurationManager.AppSettings["AirtelCashDropUatUsername"].ToString();
        public static string AirtelCashDropUatPassword => ConfigurationManager.AppSettings["AirtelCashDropUatPassword"].ToString();


        public static string AirtelCashDropLiveBaseUrl => ConfigurationManager.AppSettings["AirtelCashDropLiveBaseUrl"].ToString();
        public static string AirtelCashDropLivePartnerId => ConfigurationManager.AppSettings["AirtelCashDropLivePartnerId"].ToString();
        public static string AirtelCashDropLiveSalt => ConfigurationManager.AppSettings["AirtelCashDropLiveSalt"].ToString();
        public static string AirtelCashDropLiveUsername => ConfigurationManager.AppSettings["AirtelCashDropLiveUsername"].ToString();
        public static string AirtelCashDropLivePassword => ConfigurationManager.AppSettings["AirtelCashDropLivePassword"].ToString();
        #endregion

        #endregion


        #region Go Processing
        //Key for Prepaid Recharge
        public static long Goid => Convert.ToInt64(ConfigurationManager.AppSettings["Goid"].ToString());
        public static string MobileApiKey => ConfigurationManager.AppSettings["MobileApiKey"].ToString();
        public static string DthApiKey => ConfigurationManager.AppSettings["DthApiKey"].ToString();
        public static string DataCardApiKey => ConfigurationManager.AppSettings["DataCardApiKey"].ToString();
        public static string ReturnType => "json";
        public static string BaseUrl => ConfigurationManager.AppSettings["PrepaidRechargeBaseUrl"].ToString();
        #endregion

        #region Walnut
        public static string WalnutMode => ConfigurationManager.AppSettings["WalnutMode"].ToString();
        public static string WalnutUatBaseUrl => ConfigurationManager.AppSettings["WalnutUatBaseUrl"].ToString();
        public static string WalnutUatToken => ConfigurationManager.AppSettings["WalnutUatToken"].ToString();
        public static string WalnutUatUsername => ConfigurationManager.AppSettings["WalnutUatUsername"].ToString();
        public static string WalnutUatPassword => ConfigurationManager.AppSettings["WalnutUatPassword"].ToString();

        public static string WalnutLiveBaseUrl => ConfigurationManager.AppSettings["WalnutLiveBaseUrl"].ToString();
        public static string WalnutLiveToken => ConfigurationManager.AppSettings["WalnutLiveToken"].ToString();
        public static string WalnutLiveUsername => ConfigurationManager.AppSettings["WalnutLiveUsername"].ToString();
        public static string WalnutLivePassword => ConfigurationManager.AppSettings["WalnutLivePassword"].ToString();

        public static string WalnutOtherBaseUrl => ConfigurationManager.AppSettings["WalnutOtherBaseUrl"].ToString();
        public static string WalnutOtherToken => ConfigurationManager.AppSettings["WalnutOtherToken"].ToString();
        public static string WalnutOtherUsername => ConfigurationManager.AppSettings["WalnutOtherUsername"].ToString();
        public static string WalnutOtherPassword => ConfigurationManager.AppSettings["WalnutOtherPassword"].ToString();
        #endregion


        #region Tpddl Config
        public static string TpddlBaseUrl = ConfigurationManager.AppSettings["TpddlBaseUrl"].ToString();
        public static string TpddlUsername = ConfigurationManager.AppSettings["TpddlUsername"].ToString();
        public static string TpddlPassword = ConfigurationManager.AppSettings["TpddlPassword"].ToString();
        public static string TpddlMerchentId = ConfigurationManager.AppSettings["TpddlMerchentId"].ToString();
        public static string TpddlCounterNo = ConfigurationManager.AppSettings["TpddlCounterNo"].ToString();
        public static string TpddlBankName = ConfigurationManager.AppSettings["TpddlBankName"].ToString();
        public static string TpddlBankCode = ConfigurationManager.AppSettings["TpddlBankCode"].ToString();

        public static string TsiTpddlApiBaseUrl = ConfigurationManager.AppSettings["TsiTpddlApiBaseUrl"].ToString();
        public static string TsiTpddlApiUsername = ConfigurationManager.AppSettings["TsiTpddlApiUsername"].ToString();
        public static string TsiTpddlApiPassword = ConfigurationManager.AppSettings["TsiTpddlApiPassword"].ToString();
        #endregion

        public static string HostServer => ConfigurationManager.AppSettings["HostServer"].ToString();

        #region BillDesk
        public static string BillDeskMode => ConfigurationManager.AppSettings["BillDeskMode"].ToString();
        public static string BillDeskUatApiBaseUrl => ConfigurationManager.AppSettings["BillDeskUatApiBaseUrl"].ToString();
        public static string BillDeskLiveApiBaseUrl => ConfigurationManager.AppSettings["BillDeskLiveApiBaseUrl"].ToString();
        public static string BillDeskClientId => ConfigurationManager.AppSettings["BillDeskClientId"].ToString();
        public static string BillDeskSourceId => ConfigurationManager.AppSettings["BillDeskSourceId"].ToString();
        public static string BillDeskSecretKey => ConfigurationManager.AppSettings["BillDeskSecretKey"].ToString();
        #endregion

        #region Mp Vidyut Vitaran
        public static string MpVidyutVitaranMode => ConfigurationManager.AppSettings["MpVidyutVitaranMode"].ToString();
        public static string MpVidyutVitaranApdrpUatApiBaseUrl => ConfigurationManager.AppSettings["MpVidyutVitaranApdrpUatApiBaseUrl"].ToString();
        public static string MpVidyutVitaranApdrpLiveApiBaseUrl => ConfigurationManager.AppSettings["MpVidyutVitaranApdrpLiveApiBaseUrl"].ToString();
        public static string MpVidyutVitaranGateWayType => ConfigurationManager.AppSettings["MpVidyutVitaranGateWayType"].ToString();
        public static string MpVidyutVitaranCheckSumKey => ConfigurationManager.AppSettings["MpVidyutVitaranCheckSumKey"].ToString();

        public static string MpVidyutVitaranRmsUatApiBaseUrl => ConfigurationManager.AppSettings["MpVidyutVitaranRmsUatApiBaseUrl"].ToString();
        public static string MpVidyutVitaranRmsLiveApiBaseUrl => ConfigurationManager.AppSettings["MpVidyutVitaranRmsLiveApiBaseUrl"].ToString();
        public static string MpVidyutVitaranRmsApiUsername => ConfigurationManager.AppSettings["MpVidyutVitaranRmsApiUsername"].ToString();
        public static string MpVidyutVitaranRmsApiPassword => ConfigurationManager.AppSettings["MpVidyutVitaranRmsApiPassword"].ToString();
        public static string MpVidyutVitaranRmsAgency => ConfigurationManager.AppSettings["MpVidyutVitaranRmsAgency"].ToString();

        #endregion

        #region Prabhu Money
        public static string PrabhuBankMode => ConfigurationManager.AppSettings["PrabhuBankMode"].ToString();
        public static string PrabhuBankLiveApiUsername => ConfigurationManager.AppSettings["PrabhuBankLiveApiUsername"].ToString();
        public static string PrabhuBankUatApiUsername => ConfigurationManager.AppSettings["PrabhuBankUatApiUsername"].ToString();
        public static string PrabhuBankLiveApiPassword => ConfigurationManager.AppSettings["PrabhuBankLiveApiPassword"].ToString();
        public static string PrabhuBankUatApiPassword => ConfigurationManager.AppSettings["PrabhuBankUatApiPassword"].ToString();
        #endregion

        #region AEPS
        //public static string ApiAuthenticationPassword => ConfigurationManager.AppSettings["ApiAuthenticationPassword"].ToString();
        //public static string UatSecretKeyTimeStamp => ConfigurationManager.AppSettings["UatSecretKeyTimeStamp"].ToString();
        //public static string UatSecretKey => ConfigurationManager.AppSettings["UatSecretKey"].ToString();

        //public static string SaveRequestAndResponseLog = ConfigurationManager.AppSettings["SaveRequestAndResponseLog"].ToString();
        //public static string Mode => ConfigurationManager.AppSettings["Mode"].ToString();

        public static string AepsBaseUrl => ConfigurationManager.AppSettings["AepsBaseUrl"].ToString();
        public static string AepsApiUsername => "FingPay";
        public static string AepsApiPassword => "FingPay@1234";

        //This is username of  the super merchant
        public static string AepsUsername => ConfigurationManager.AppSettings["AepsUsername"].ToString();

        //This is password of  the  super merchant
        public static string AepsPassword => ConfigurationManager.AppSettings["AepsPassword"].ToString();
        public static int AepsSuperMerchantId => Convert.ToInt32(ConfigurationManager.AppSettings["AepsSuperMerchantId"].ToString());
        public static string AepsCertificatePath => "fingpay_public_production.cer";

        //public static string HostServer => ConfigurationManager.AppSettings["HostServer"].ToString();

        #endregion

        #region Toffee
        public static string ToffeeMode => ConfigurationManager.AppSettings["ToffeeMode"].ToString();
        public static string ToffeeUatBaseUrl => ConfigurationManager.AppSettings["ToffeeUatBaseUrl"].ToString();
        public static int ToffeeUatClientId => Convert.ToInt32(ConfigurationManager.AppSettings["ToffeeUatClientId"].ToString());
        public static string ToffeeUatClientSecret => ConfigurationManager.AppSettings["ToffeeUatClientSecret"].ToString();
        public static string ToffeeUatUsername => ConfigurationManager.AppSettings["ToffeeUatUsername"].ToString();
        public static string ToffeeUatPassword => ConfigurationManager.AppSettings["ToffeeUatPassword"].ToString();


        public static string ToffeeLiveBaseUrl => ConfigurationManager.AppSettings["ToffeeLiveBaseUrl"].ToString();
        public static int ToffeeLiveClientId => Convert.ToInt32(ConfigurationManager.AppSettings["ToffeeLiveClientId"].ToString());
        public static string ToffeeLiveClientSecret => ConfigurationManager.AppSettings["ToffeeLiveClientSecret"].ToString();
        public static string ToffeeLiveUsername => ConfigurationManager.AppSettings["ToffeeLiveUsername"].ToString();
        public static string ToffeeLivePassword => ConfigurationManager.AppSettings["ToffeeLivePassword"].ToString();
        #endregion


        #region QwikCilver

        public static string QwikCilverMode => ConfigurationManager.AppSettings["QwikCilverMode"].ToString();

        public static string QwikCilverUatBaseUrl => ConfigurationManager.AppSettings["QwikCilverUatBaseUrl"].ToString();
        public static string QwikCilverUatConsumerKey => ConfigurationManager.AppSettings["QwikCilverUatConsumerKey"].ToString();
        public static string QwikCilverUatConsumerSecret => ConfigurationManager.AppSettings["QwikCilverUatConsumerSecret"].ToString();
        public static string QwikCilverUatUsername => ConfigurationManager.AppSettings["QwikCilverUatUsername"].ToString();
        public static string QwikCilverUatPassword => ConfigurationManager.AppSettings["QwikCilverUatPassword"].ToString();

        public static string QwikCilverLiveBaseUrl => ConfigurationManager.AppSettings["QwikCilverLiveBaseUrl"].ToString();
        public static string QwikCilverLiveConsumerKey => ConfigurationManager.AppSettings["QwikCilverLiveConsumerKey"].ToString();
        public static string QwikCilverLiveConsumerSecret => ConfigurationManager.AppSettings["QwikCilverLiveConsumerSecret"].ToString();
        public static string QwikCilverLiveUsername => ConfigurationManager.AppSettings["QwikCilverLiveUsername"].ToString();
        public static string QwikCilverLivePassword => ConfigurationManager.AppSettings["QwikCilverLivePassword"].ToString();

        #endregion


        #region MongoDb

        //getting mongo url
        private static  string MongoUrl => ConfigurationManager.AppSettings["MongoUrl"];
        //getting mongo db name
        private static  string MongoDbName => ConfigurationManager.AppSettings["MongoDbName"];

        #endregion
    }
}
