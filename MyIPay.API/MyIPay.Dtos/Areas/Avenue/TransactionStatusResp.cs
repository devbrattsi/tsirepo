﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    /// <summary>
    /// Response Model for Transaction Status  
    /// </summary>
    #region Transaction Status Response Model
    //[XmlRoot(ElementName = "transaction")]
    //public class Transaction
    //{
    //    [XmlElement(ElementName = "agentId")]
    //    public string AgentId { get; set; }
    //    [XmlElement(ElementName = "amount")]
    //    public string Amount { get; set; }
    //    [XmlElement(ElementName = "billerId")]
    //    public string BillerId { get; set; }
    //    [XmlElement(ElementName = "txnDate")]
    //    public string TxnDate { get; set; }
    //    [XmlElement(ElementName = "txnReferenceId")]
    //    public string TxnReferenceId { get; set; }
    //    [XmlElement(ElementName = "txnStatus")]
    //    public string TxnStatus { get; set; }
    //}

    [XmlRoot(ElementName = "txnList")]
    public class TxnList
    {
        //[XmlElement(ElementName = "transaction")]
        //public Transaction Transaction { get; set; }
        [XmlElement(ElementName = "agentId")]
        public string AgentId { get; set; }
        [XmlElement(ElementName = "amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "billerId")]
        public string BillerId { get; set; }
        [XmlElement(ElementName = "txnDate")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "txnReferenceId")]
        public string TxnReferenceId { get; set; }
        [XmlElement(ElementName = "txnStatus")]
        public string TxnStatus { get; set; }
    }

    [XmlRoot(ElementName = "transactionStatusResp")]
    public class TransactionStatusResp
    {
        [XmlElement(ElementName = "responseCode")]
        public string ResponseCode { get; set; }
        [XmlElement(ElementName = "responseReason")]
        public string ResponseReason { get; set; }
        [XmlElement(ElementName = "txnList")]
        public List<TxnList> TxnList { get; set; }
        [XmlElement(ElementName = "errorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
    }
    #endregion
}
