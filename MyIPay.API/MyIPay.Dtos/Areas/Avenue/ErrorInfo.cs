﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    /// <summary>
    /// INVALID Response
    /// </summary>
    #region Invalid Response
    [XmlRoot(ElementName = "error")]
    public class Error
    {
        [XmlElement(ElementName = "errorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "errorMessage")]
        public string ErrorMessage { get; set; }
    }

    [XmlRoot(ElementName = "errorInfo")]
    public class ErrorInfo
    {
        [XmlElement(ElementName = "error")]
        public Error Error { get; set; }
    }
    #endregion
}
