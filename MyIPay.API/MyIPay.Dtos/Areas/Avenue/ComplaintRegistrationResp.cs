﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    /// <summary>
    ///  Complaint Registration Response model
    /// </summary>
    #region Complaint Registration Response model
    [XmlRoot(ElementName = "complaintRegistrationResp")]
    public class ComplaintRegistrationResp
    {
        [XmlElement(ElementName = "complaintAssigned")]
        public string ComplaintAssigned { get; set; }
        [XmlElement(ElementName = "complaintId")]
        public string ComplaintId { get; set; }
        [XmlElement(ElementName = "responseCode")]
        public string ResponseCode { get; set; }
        [XmlElement(ElementName = "responseReason")]
        public string ResponseReason { get; set; }
        [XmlElement(ElementName = "errorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
    }
    #endregion
}
