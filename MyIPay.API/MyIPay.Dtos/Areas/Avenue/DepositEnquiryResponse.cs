﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    /// <summary>
    /// RESPONSE FORMAT FOR DEPOSIT ENQUIRY
    /// </summary>
    /// 
     #region RESPONSE FORMAT FOR DEPOSIT ENQUIRY
    [XmlRoot(ElementName = "entry")]
    public class Entry
    {
        [XmlElement(ElementName = "agentId")]
        public string AgentId { get; set; }
        [XmlElement(ElementName = "transactionId")]
        public string TransactionId { get; set; }
        [XmlElement(ElementName = "requestId")]
        public string RequestId { get; set; }
        [XmlElement(ElementName = "amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "transType")]
        public string TransType { get; set; }
        [XmlElement(ElementName = "datetime")]
        public string Datetime { get; set; }
    }

    [XmlRoot(ElementName = "transaction")]
    public class Transaction
    {
        [XmlElement(ElementName = "entry")]
        public List<Entry> Entry { get; set; }
    }

    [XmlRoot(ElementName = "DepositEnquiryResponse")]
    public class DepositEnquiryResponse
    {
        [XmlElement(ElementName = "instituteId")]
        public string InstituteId { get; set; }
        [XmlElement(ElementName = "currentBalance")]
        public string CurrentBalance { get; set; }
        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; }
        [XmlElement(ElementName = "transaction")]
        public Transaction Transaction { get; set; }
        [XmlElement(ElementName = "errorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
    }
    #endregion
}
