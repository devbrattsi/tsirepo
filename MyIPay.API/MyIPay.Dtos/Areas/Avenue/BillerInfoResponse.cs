﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    ///<summary>
    ///Response Model for Biller Info Api
    /// </summary>
    #region Biller Info Response Model
    [XmlRoot(ElementName = "paramInfo")]
    public class ParamInfo
    {
        [XmlElement(ElementName = "paramName")]
        public string ParamName { get; set; }
        [XmlElement(ElementName = "dataType")]
        public string DataType { get; set; }
        [XmlElement(ElementName = "isOptional")]
        public string IsOptional { get; set; }
    }

    [XmlRoot(ElementName = "billerInputParams")]
    public class BillerInputParams
    {
        [XmlElement(ElementName = "paramInfo")]
        public List<ParamInfo> ParamInfo { get; set; }
    }

    [XmlRoot(ElementName = "biller")]
    public class Biller
    {
        [XmlElement(ElementName = "billerId")]
        public string BillerId { get; set; }
        [XmlElement(ElementName = "billerName")]
        public string BillerName { get; set; }
        [XmlElement(ElementName = "billerCategory")]
        public string BillerCategory { get; set; }
        [XmlElement(ElementName = "billerAdhoc")]
        public string BillerAdhoc { get; set; }
        [XmlElement(ElementName = "billerCoverage")]
        public string BillerCoverage { get; set; }
        [XmlElement(ElementName = "billerFetchRequiremet")]
        public string BillerFetchRequiremet { get; set; }
        [XmlElement(ElementName = "billerPaymentExactness")]
        public string BillerPaymentExactness { get; set; }
        [XmlElement(ElementName = "billerInputParams")]
        public BillerInputParams BillerInputParams { get; set; }
        [XmlElement(ElementName = "billerAmountOptions")]
        public string BillerAmountOptions { get; set; }
        [XmlElement(ElementName = "billerPaymentModes")]
        public string BillerPaymentModes { get; set; }
    }

    [XmlRoot(ElementName = "billerInfoResponse")]
    public class BillerInfoResponse
    {
        [XmlElement(ElementName = "responseCode")]
        public string ResponseCode { get; set; }
        [XmlElement(ElementName = "biller")]
        public List<Biller> Biller { get; set; }
    }
    #endregion
}
