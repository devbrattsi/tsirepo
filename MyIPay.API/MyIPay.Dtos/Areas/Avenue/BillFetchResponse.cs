﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    ///<summary>
    ///Response Model for Bill Fetch Api
    ///</summary>
    #region Bill Fetch Api Response Model

    [XmlRoot(ElementName = "option")]
    public class Option
    {
        [XmlElement(ElementName = "amountName")]
        public string AmountName { get; set; }
        [XmlElement(ElementName = "amountValue")]
        public string AmountValue { get; set; }
    }

    [XmlRoot(ElementName = "amountOptions")]
    public class AmountOptions
    {
        [XmlElement(ElementName = "option")]
        public List<Option> Option { get; set; }
    }

    [XmlRoot(ElementName = "billerResponse")]
    public class BillerResponse
    {
        [XmlElement(ElementName = "amountOptions")]
        public AmountOptions AmountOptions { get; set; }
        [XmlElement(ElementName = "billAmount")]
        public string BillAmount { get; set; }
        [XmlElement(ElementName = "billDate")]
        public string BillDate { get; set; }
        [XmlElement(ElementName = "billNumber")]
        public string BillNumber { get; set; }
        [XmlElement(ElementName = "billPeriod")]
        public string BillPeriod { get; set; }
        [XmlElement(ElementName = "customerName")]
        public string CustomerName { get; set; }
        [XmlElement(ElementName = "dueDate")]
        public string DueDate { get; set; }
    }

    [XmlRoot(ElementName = "info")]
    public class Info
    {
        [XmlElement(ElementName = "infoName")]
        public string InfoName { get; set; }
        [XmlElement(ElementName = "infoValue")]
        public string InfoValue { get; set; }
    }

    [XmlRoot(ElementName = "additionalInfo")]
    public class AdditionalInfo
    {
        [XmlElement(ElementName = "info")]
        public List<Info> Info { get; set; }
    }

    [XmlRoot(ElementName = "billFetchResponse")]
    public class BillFetchResponse
    {
        [XmlElement(ElementName = "responseCode")]
        public string ResponseCode { get; set; }
        [XmlElement(ElementName = "inputParams")]
        public InputParams InputParams { get; set; }
        [XmlElement(ElementName = "billerResponse")]
        public BillerResponse BillerResponse { get; set; }
        [XmlElement(ElementName = "additionalInfo")]
        public AdditionalInfo AdditionalInfo { get; set; }
        [XmlElement(ElementName = "errorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
    }

    [XmlRoot(ElementName = "input")]
    public class Input
    {
        [XmlElement(ElementName = "paramName")]
        public string ParamName { get; set; }
        [XmlElement(ElementName = "paramValue")]
        public string ParamValue { get; set; }
    }

    [XmlRoot(ElementName = "inputParams")]
    public class InputParams
    {
        [XmlElement(ElementName = "input")]
        public List<Input> Input { get; set; }
    }
    #endregion
}
