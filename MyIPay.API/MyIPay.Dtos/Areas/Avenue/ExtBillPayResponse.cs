﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    /// <summary>
    /// Response Model for Bill Payment Api
    /// </summary>
    /// 
    #region Bill Payment Api Response Model
    [XmlRoot(ElementName = "ExtBillPayResponse")]
    public class ExtBillPayResponse
    {
        [XmlElement(ElementName = "responseCode")]
        public string ResponseCode { get; set; }
        [XmlElement(ElementName = "responseReason")]
        public string ResponseReason { get; set; }
        [XmlElement(ElementName = "txnRefId")]
        public string TxnRefId { get; set; }
        [XmlElement(ElementName = "txnRespType")]
        public string TxnRespType { get; set; }
        [XmlElement(ElementName = "inputParams")]
        public InputParams InputParams { get; set; }
        [XmlElement(ElementName = "CustConvFee")]
        public string CustConvFee { get; set; }
        [XmlElement(ElementName = "RespAmount")]
        public string RespAmount { get; set; }
        [XmlElement(ElementName = "RespBillDate")]
        public string RespBillDate { get; set; }
        [XmlElement(ElementName = "RespBillNumber")]
        public string RespBillNumber { get; set; }
        [XmlElement(ElementName = "RespBillPeriod")]
        public string RespBillPeriod { get; set; }
        [XmlElement(ElementName = "RespCustomerName")]
        public string RespCustomerName { get; set; }
        [XmlElement(ElementName = "RespDueDate")]
        public string RespDueDate { get; set; }
        [XmlElement(ElementName = "errorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
    }
    #endregion
}
