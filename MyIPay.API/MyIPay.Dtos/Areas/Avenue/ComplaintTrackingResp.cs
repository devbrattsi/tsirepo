﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.Avenue
{
    ///<summary>
    ///Response model for Complaint Tracking
    /// </summary>
    ///
    #region Response model for Complaint Tracking
    [XmlRoot(ElementName = "complaintTrackingResp")]
    public class ComplaintTrackingResp
    {
        [XmlElement(ElementName = "complaintAssigned")]
        public string ComplaintAssigned { get; set; }
        [XmlElement(ElementName = "complaintId")]
        public string ComplaintId { get; set; }
        [XmlElement(ElementName = "complaintStatus")]
        public string ComplaintStatus { get; set; }
        [XmlElement(ElementName = "responseCode")]
        public string RespCode { get; set; }
        [XmlElement(ElementName = "responseReason")]
        public string RespReason { get; set; }
        [XmlElement(ElementName = "errorInfo")]
        public ErrorInfo ErrorInfo { get; set; }
    }
    #endregion
}
