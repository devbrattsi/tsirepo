﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    //public class CustomerDto
    //{
    //    public CustomerDto()
    //    {
    //        Data = new GetCustomerData();
    //    }

    //    [JsonProperty("ReturnGetCustomer")]
    //    public GetCustomerData Data { get; set; }
    //}


    //public class GetCustomerData
    //{
    //    public GetCustomerData()
    //    {
    //        Customers = new Customers();
    //    }

    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //    public Customers Customers { get; set; }
    //}

    //public class Customers
    //{
    //    public Customers()
    //    {
    //        Customer = new Customer();
    //    }

    //    public Customer Customer { get; set; }
    //}

    //public class Customer
    //{
    //    public Customer()
    //    {
    //        Ids = new Ids();
    //        Mobile = new Mobile();
    //        YearlyLimitDetail = new YearlyLimitDetail();
    //    }

    //    public string CustomerId { get; set; }
    //    public string Dob { get; set; }
    //    public string Employer { get; set; }
    //    public string Gender { get; set; }
    //    public Ids Ids { get; set; }
    //    public Mobile Mobile { get; set; }
    //    public string Name { get; set; }
    //    public string Nationality { get; set; }
    //    public string State { get; set; }
    //    public YearlyLimitDetail YearlyLimitDetail { get; set; }
    //}


    //public class YearlyLimitDetail
    //{
    //    public string TotalNoTransaction { get; set; }
    //    public string AvailableNoTransaction { get; set; }
    //    public string TotalAmount { get; set; }
    //    public string AvailableAmount { get; set; }
    //}

    //public class Ids
    //{
    //    public Ids()
    //    {
    //        Id = new Id();
    //    }

    //    public Id Id { get; set; }
    //}

    //public class Id
    //{
    //    public string IdNumber { get; set; }
    //    public string IdType { get; set; }
    //}

    //public class Mobile
    //{
    //    [JsonProperty("string")]
    //    public string Number { get; set; }
    //}


    public class CustomerDto
    {
        public CustomerDto()
        {
            Customers = new List<Customer>();
        }

        public string Code { get; set; }
        public string Message { get; set; }
        public List<Customer> Customers { get; set; }
    }


    public class Customer
    {
        public Customer()
        {
            YearlyLimitDetail = new YearlyLimitDetail();
            //Ids = new List<Id>();
        }
        public object Address { get; set; }
        public object City { get; set; }
        public int CustomerId { get; set; }
        public object District { get; set; }
        public string Dob { get; set; }
        public string Employer { get; set; }
        public string Gender { get; set; }
        // public List<Id> Ids { get; set; }
        public object Ids { get; set; }
        public List<string> Mobile { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public string State { get; set; }
        public YearlyLimitDetail YearlyLimitDetail { get; set; }
    }


    public class Id
    {
        public string IdNumber { get; set; }
        public string IdType { get; set; }
    }

    public class YearlyLimitDetail
    {
        public int TotalNoTransaction { get; set; }
        public int AvailableNoTransaction { get; set; }
        public double TotalAmount { get; set; }
        public double AvailableAmount { get; set; }
    }


}
