﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    public class CancelTransactionDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string PinNo { get; set; }
    }
    //public class CancelTransactionDto
    //{
    //    public CancelTransactionDto()
    //    {
    //        Data = new ReturnCancelTransaction();
    //    }

    //    [JsonProperty("ReturnCancelTransaction")]
    //    public ReturnCancelTransaction Data { get; set; }
    //}

    //public class ReturnCancelTransaction
    //{
    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //    public string PinNo { get; set; }
    //}

}
