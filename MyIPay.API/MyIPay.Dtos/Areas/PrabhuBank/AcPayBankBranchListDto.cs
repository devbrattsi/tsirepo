﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.PrabhuBank
{

    public class AcPayBankBranchListDto
    {
        public AcPayBankBranchListDto()
        {
            Data = new ReturnAcPayBankBranchList();
        }

        [JsonProperty("ReturnAcPayBankBranchList")]
        public ReturnAcPayBankBranchList Data { get; set; }
    }

    public class ReturnAcPayBankBranchList
    {

        public ReturnAcPayBankBranchList()
        {
            BankBranches = new BankBranches();
        }
        public string Code { get; set; }
        public string Message { get; set; }
        public BankBranches BankBranches { get; set; }
    }

    public class BankBranches
    {
        public BankBranches()
        {
            BankBranch = new List<BankBranch>();
        }

        public List<BankBranch> BankBranch { get; set; }
    }

    public class BankBranch
    {
        public string BranchId { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string RoutingCode { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
    }




}
