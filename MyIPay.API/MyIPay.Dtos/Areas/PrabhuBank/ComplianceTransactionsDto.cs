﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.PrabhuBank
{

    public class ComplianceTransactionsDto
    {
        public ComplianceTransactionsDto()
        {
            Data = new ReturnComplianceTransactions();
        }

        [JsonProperty("ReturnComplianceTransactions")]
        public ReturnComplianceTransactions Data { get; set; }
    }

    public class ReturnComplianceTransactions
    {
        public ReturnComplianceTransactions()
        {
            Transactions = new ComplianceTransactions();
        }
        public string Code { get; set; }
        public string Message { get; set; }
        public ComplianceTransactions Transactions { get; set; }
    }

    public class ComplianceTransactions
    {
        public ComplianceTransactions()
        {
            TransactionList = new List<TransactionList>();
        }
        public List<TransactionList> TransactionList { get; set; }
    }

    public class TransactionList
    {
        public string PinNo { get; set; }
        public string PartnerPinNo { get; set; }
        public string Comment { get; set; }
    }



}
