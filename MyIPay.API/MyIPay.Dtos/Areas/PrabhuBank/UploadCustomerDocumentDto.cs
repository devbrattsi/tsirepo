﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    //public class UploadCustomerDocumentDto
    //{
    //    public UploadCustomerDocumentDto()
    //    {
    //        Data = new ReturnUploadCustomerDocument();
    //    }

    //    [JsonProperty("ReturnUploadCustomerDocument")]
    //    public ReturnUploadCustomerDocument Data { get; set; }
    //}
    //public class ReturnUploadCustomerDocument
    //{
    //    public ReturnUploadCustomerDocument()
    //    {
    //        Response = new Response();
    //    }
    //    public Response Response { get; set; }
    //}
    //public class Response
    //{
    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //}

    //public class UploadCustomerDocumentDto
    //{
    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //}

    public class UploadCustomerDocumentDto
    {
        public Response Response { get; set; }
    }

    public class Response
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }



}
