﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{

    public class ValidateBankAccountDto
    {
        public ValidateBankAccountDto()
        {
            Data = new ReturnValidateBankAccount();
        }

        [JsonProperty("ReturnValidateBankAccount")]
        public ReturnValidateBankAccount Data { get; set; }
    }

    public class ReturnValidateBankAccount
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
    }


}
