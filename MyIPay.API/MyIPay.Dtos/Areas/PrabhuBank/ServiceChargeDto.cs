﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    //public class ServiceChargeDto
    //{
    //    public ServiceChargeDto()
    //    {
    //        Data = new ReturnGetServiceCharge();
    //    }

    //    [JsonProperty("ReturnGetServiceCharge")]
    //    public ReturnGetServiceCharge Data { get; set; }
    //}


    //public class ReturnGetServiceCharge
    //{
    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //    public string CollectionAmount { get; set; }
    //    public string CollectionCurrency { get; set; }
    //    public string ServiceCharge { get; set; }
    //    public string TransferAmount { get; set; }
    //    public string ExchangeRate { get; set; }
    //    public string PayoutAmount { get; set; }
    //    public string PayoutCurrency { get; set; }
    //}

    public class ServiceChargeDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string CollectionAmount { get; set; }
        public string CollectionCurrency { get; set; }
        public string ServiceCharge { get; set; }
        public string TransferAmount { get; set; }
        public string ExchangeRate { get; set; }
        public string PayoutAmount { get; set; }
        public string PayoutCurrency { get; set; }
    }


}
