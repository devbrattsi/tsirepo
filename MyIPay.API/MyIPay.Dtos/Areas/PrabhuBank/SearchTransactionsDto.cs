﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace MyIPay.Dtos.Areas.PrabhuBank
{

    public class Transaction
    {
        public int TransactionId { get; set; }
        public string PinNo { get; set; }
        public string SenderName { get; set; }
        public string SenderGender { get; set; }
        public string SenderDoB { get; set; }
        public string SenderAddress { get; set; }
        public object SenderPhone { get; set; }
        public string SenderMobile { get; set; }
        public string SenderCity { get; set; }
        public string SenderDistrict { get; set; }
        public string SenderState { get; set; }
        public string SenderNationality { get; set; }
        public string Employer { get; set; }
        public string SenderIDType { get; set; }
        public string SenderIDNumber { get; set; }
        public object SenderIDExpiryDate { get; set; }
        public object SenderIDIssuedPlace { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverGender { get; set; }
        public string ReceiverAddress { get; set; }
        public object ReceiverPhone { get; set; }
        public string ReceiverMobile { get; set; }
        public object ReceiverCity { get; set; }
        public string ReceiverDistrict { get; set; }
        public object ReceiverState { get; set; }
        public object ReceiverIDType { get; set; }
        public object ReceiverIDNumber { get; set; }
        public object ReceiverIDExpiryDate { get; set; }
        public object ReceiverIDIssuedPlace { get; set; }
        public string SendCountry { get; set; }
        public string PayoutCountry { get; set; }
        public string PaymentMode { get; set; }
        public double CollectedAmount { get; set; }
        public double ServiceCharge { get; set; }
        public double SendAmount { get; set; }
        public string SendCurrency { get; set; }
        public double PayAmount { get; set; }
        public string PayCurrency { get; set; }
        public double ExchangeRate { get; set; }
        public object BankCode { get; set; }
        public string BankName { get; set; }
        public string BankBranchCode { get; set; }
        public string BankBranchName { get; set; }
        public string BankBranchDistrict { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public object NewAccountRequest { get; set; }
        public string PartnerPinNo { get; set; }
        public string IncomeSource { get; set; }
        public string RemittanceReason { get; set; }
        public string Relationship { get; set; }
        public string TxnStatus { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public object PaidDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public string SendBy { get; set; }
    }

    public class SearchTransactionsDto
    {
        public SearchTransactionsDto()
        {
            Transactions = new List<Transaction>();
        }
        public string Code { get; set; }
        public string Message { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
    //public class SearchTransactionsDto
    //{
    //    public SearchTransactionsDto()
    //    {
    //        Data = new ReturnSearchTransaction();
    //    }

    //    [JsonProperty("ReturnSearchTransaction")]
    //    public ReturnSearchTransaction Data { get; set; }
    //}

    //public class ReturnSearchTransaction
    //{
    //    public ReturnSearchTransaction()
    //    {
    //        Transactions = new Transactions();
    //    }

    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //    public Transactions Transactions { get; set; }
    //}

    //public class Transactions
    //{
    //    public Transactions()
    //    {
    //        Transaction = new List<Transaction>();
    //    }

    //    public List<Transaction> Transaction { get; set; }
    //}


    //public class Transaction
    //{
    //    public string TransactionId { get; set; }
    //    public string PinNo { get; set; }
    //    public string SenderName { get; set; }
    //    public string SenderGender { get; set; }
    //    public string SenderDoB { get; set; }
    //    public string SenderAddress { get; set; }
    //    public string SenderMobile { get; set; }
    //    public string SenderState { get; set; }
    //    public string SenderNationality { get; set; }
    //    public string Employer { get; set; }
    //    public string SenderIDType { get; set; }
    //    public string SenderIDNumber { get; set; }
    //    public string ReceiverName { get; set; }
    //    public string ReceiverGender { get; set; }
    //    public string ReceiverAddress { get; set; }
    //    public string ReceiverMobile { get; set; }
    //    public string SendCountry { get; set; }
    //    public string PayoutCountry { get; set; }
    //    public string PaymentMode { get; set; }
    //    public string CollectedAmount { get; set; }
    //    public string ServiceCharge { get; set; }
    //    public string SendAmount { get; set; }
    //    public string SendCurrency { get; set; }
    //    public string PayAmount { get; set; }
    //    public string PayCurrency { get; set; }
    //    public string ExchangeRate { get; set; }
    //    public string PartnerPinNo { get; set; }
    //    public string IncomeSource { get; set; }
    //    public string RemittanceReason { get; set; }
    //    public string Relationship { get; set; }
    //    public string TxnStatus { get; set; }
    //    public DateTime SendDate { get; set; }
    //    public DateTime VerifiedDate { get; set; }
    //    public string SendBy { get; set; }
    //    public DateTime? CancelDate { get; set; }
    //}

}
