﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    public class VerifyTransactionDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string PinNo { get; set; }
    }
}
