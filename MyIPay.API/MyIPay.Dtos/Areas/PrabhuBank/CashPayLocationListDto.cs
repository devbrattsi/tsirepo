﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    public class CashPayLocationListDto
    {
        public CashPayLocationListDto()
        {
            Data = new ReturnCashPayLocationList();
        }

        [JsonProperty("ReturnCashPayLocationList")]
        public ReturnCashPayLocationList Data { get; set; }
    }

    public class ReturnCashPayLocationList
    {
        public ReturnCashPayLocationList()
        {
            Locations = new Locations();
        }

        public object ExtensionData { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public Locations Locations { get; set; }
    }

    public class Locations
    {
        public Locations()
        {
            Location = new Location();
        }

        public Location Location { get; set; }
    }

    public class Location
    {
        public Location()
        {
            LocationId = new LocationId();
        }

        public object ExtensionData { get; set; }
        public LocationId LocationId { get; set; }
        public string LocationName { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class LocationId
    {

        [JsonProperty("@d4p1:nil")]
        public string Nil { get; set; }

        [JsonProperty("@xmlns:d4p1")]
        public string D4p1 { get; set; }
    }

}
