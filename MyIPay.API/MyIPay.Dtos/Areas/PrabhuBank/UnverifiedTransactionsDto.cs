﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    public class UnverifiedTransactionsDto
    {
        public UnverifiedTransactionsDto()
        {
            Transactions = new List<UnverifiedTransaction>();
        }
        public string Code { get; set; }
        public string Message { get; set; }
        public List<UnverifiedTransaction> Transactions { get; set; }
    }


    public class UnverifiedTransaction
    {
        public int TransactionId { get; set; }
        public string PinNo { get; set; }
        public string SenderName { get; set; }
        public string SenderGender { get; set; }
        public string SenderDoB { get; set; }
        public string SenderAddress { get; set; }
        public string SenderPhone { get; set; }
        public string SenderMobile { get; set; }
        public string SenderCity { get; set; }
        public string SenderDistrict { get; set; }
        public string SenderState { get; set; }
        public string SenderNationality { get; set; }
        public string Employer { get; set; }
        public string SenderIDType { get; set; }
        public string SenderIDNumber { get; set; }
        public string SenderIDExpiryDate { get; set; }
        public string SenderIDIssuedPlace { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverGender { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverMobile { get; set; }
        public string ReceiverCity { get; set; }
        public string ReceiverDistrict { get; set; }
        public string ReceiverState { get; set; }
        public string ReceiverIDType { get; set; }
        public string ReceiverIDNumber { get; set; }
        public string ReceiverIDExpiryDate { get; set; }
        public string ReceiverIDIssuedPlace { get; set; }
        public string SendCountry { get; set; }
        public string PayoutCountry { get; set; }
        public string PaymentMode { get; set; }
        public double CollectedAmount { get; set; }
        public double ServiceCharge { get; set; }
        public double SendAmount { get; set; }
        public string SendCurrency { get; set; }
        public double PayAmount { get; set; }
        public string PayCurrency { get; set; }
        public double ExchangeRate { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string BankBranchCode { get; set; }
        public string BankBranchName { get; set; }
        public string BankBranchDistrict { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string NewAccountRequest { get; set; }
        public string PartnerPinNo { get; set; }
        public string IncomeSource { get; set; }
        public string RemittanceReason { get; set; }
        public string Relationship { get; set; }
        public string TxnStatus { get; set; }
        public string SendDate { get; set; }
        public string VerifiedDate { get; set; }
        public string PaidDate { get; set; }
        public string CancelDate { get; set; }
        public string SendBy { get; set; }
    }

}
