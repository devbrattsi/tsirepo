﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    //public class SendTransactionDto
    //{
    //    public SendTransactionDto()
    //    {
    //        Data = new ReturnSendTransaction();
    //    }
    //    [JsonProperty("ReturnSendTransaction")]
    //    public ReturnSendTransaction Data { get; set; }
    //}

    //public class ReturnSendTransaction
    //{
    //    public string Code { get; set; }
    //    public string Message { get; set; }
    //    public string TrnsactionId { get; set; }
    //    public string PinNo { get; set; }
    //}

    public class SendTransactionDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string TrnsactionId { get; set; }
        public string PinNo { get; set; }
    }

}
