﻿using Newtonsoft.Json;
using System;

namespace MyIPay.Dtos.Areas.PrabhuBank
{
    public class SearchTransactionDto
    {
        public SearchTransactionDto()
        {
            Data = new ReturnSearchTxns();
        }

        [JsonProperty("ReturnSearchTransaction")]
        public ReturnSearchTxns Data { get; set; }
    }


    public class ReturnSearchTxns
    {
        public ReturnSearchTxns()
        {
            Transactions = new Txns();
        }
        public string Code { get; set; }
        public string Message { get; set; }
        public Txns Transactions { get; set; }
    }

    public class Txns
    {
        public Txns()
        {
            Transaction = new Txn();
        }
        public Txn Transaction { get; set; }
    }



    public class Txn
    {
        public string TransactionId { get; set; }
        public string PinNo { get; set; }
        public string SenderName { get; set; }
        public string SenderGender { get; set; }
        public string SenderDoB { get; set; }
        public string SenderAddress { get; set; }
        public string SenderMobile { get; set; }
        public string SenderState { get; set; }
        public string SenderNationality { get; set; }
        public string Employer { get; set; }
        public string SenderIDType { get; set; }
        public string SenderIDNumber { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverGender { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverMobile { get; set; }
        public string SendCountry { get; set; }
        public string PayoutCountry { get; set; }
        public string PaymentMode { get; set; }
        public string CollectedAmount { get; set; }
        public string ServiceCharge { get; set; }
        public string SendAmount { get; set; }
        public string SendCurrency { get; set; }
        public string PayAmount { get; set; }
        public string PayCurrency { get; set; }
        public string ExchangeRate { get; set; }
        public string PartnerPinNo { get; set; }
        public string IncomeSource { get; set; }
        public string RemittanceReason { get; set; }
        public string Relationship { get; set; }
        public string TxnStatus { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime CancelDate { get; set; }
        public string SendBy { get; set; }
    }

}
