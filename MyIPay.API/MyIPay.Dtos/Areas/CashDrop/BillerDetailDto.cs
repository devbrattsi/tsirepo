﻿using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.CashDrop
{
    public class BillerDetailDto
    {
        public BillerDetailMeta Meta { get; set; }
        public BillerDetailData Data { get; set; }
    }
    public class BillerDetailMeta
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }

    public class BillerDetailValidation
    {
        public string Max { get; set; }
        public string Min { get; set; }
        public string Regex { get; set; }
        public string Fixed { get; set; }

    }


    public class BillerDetailField
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string OrderId { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string IsEditable { get; set; }
        public string IsVisible { get; set; }
        public BillerDetailValidation Validation { get; set; }
        public string PostKey { get; set; }
        public string ActionUrl { get; set; }
        public string IsUniqueRef { get; set; }
    }

    public class BillerDetailData
    {
        public BillerDetailData()
        {
            Fields = new List<BillerDetailField>();
        }

        public string Id { get; set; }
        public string ActionUrl { get; set; }
        public string Action { get; set; }
        public List<BillerDetailField> Fields { get; set; }
    }
}
