﻿using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.CashDrop
{
    public class FetchDetailDto
    {
        public FetchDetailMeta Meta { get; set; }
        public FetchDetailData Data { get; set; }
    }

    public class FetchDetailMeta
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }


    public class FetchDetailData
    {
        public FetchDetailData()
        {
            Fields = new List<FetchDetailField>();
        }

        public string Id { get; set; }
        public string ActionUrl { get; set; }
        public string Action { get; set; }
        public List<FetchDetailField> Fields { get; set; }
    }


    public class FetchDetailField
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string OrderId { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string IsEditable { get; set; }
        public string IsVisible { get; set; }
        public FetchDetailValidation Validation { get; set; }
        public string PostKey { get; set; }
        public string ActionUrl { get; set; }
        public string SmsLabel { get; set; }
        public string IsUniqueRef { get; set; }
    }

    public class FetchDetailValidation
    {
        public string Max { get; set; }
        public string Min { get; set; }
        public string Regex { get; set; }
        public string MaxValue { get; set; }
        public string MinValue { get; set; }
    }


}
