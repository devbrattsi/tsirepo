﻿namespace MyIPay.Dtos.Areas.CashDrop
{

    public class TxnDetailDto
    {
        public TxnDetail TxnDetail { get; set; }
        public TxnDetailMeta Meta { get; set; }
    }

    public class TxnDetail
    {
        public string BillerName { get; set; }
        public string TxnId { get; set; }
        public string Amount { get; set; }
        public string PartnerTxnId { get; set; }
    }

    public class TxnDetailMeta
    {
        public string Description { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
    }
}
