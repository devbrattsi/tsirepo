﻿namespace MyIPay.Dtos.Areas.CashDrop
{
    public class PaymentDto
    {
        public PaymentTxnDetail Data { get; set; }
        public PaymentMeta Meta { get; set; }
    }

    public class PaymentTxnDetail
    {
        public string BillerName { get; set; }
        public string TxnId { get; set; }
        public string Amount { get; set; }
        public string PartnerTxnId { get; set; }
        public string Description { get; set; }
    }

    public class PaymentMeta
    {
        public string Description { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
    }

}
