﻿using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.CashDrop
{

    public class BillersDto
    {
        public Data Data { get; set; }
        public Meta Meta { get; set; }
    }
    public class Data
    {
        public Data()
        {
            Billers = new List<Biller>();
        }
        public List<Biller> Billers { get; set; }
    }

    public class Biller
    {
        public string Name { get; set; }
        public string FetchUrl { get; set; }
        public string Type { get; set; }
    }

    
    public class Meta
    {
        public string Description { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
    }

    
}
