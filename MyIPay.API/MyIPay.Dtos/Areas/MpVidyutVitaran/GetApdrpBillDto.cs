﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.MpVidyutVitaran
{
    public class GetApdrpBillDto
    {
        //[JsonProperty(PropertyName = "STATUS")]
        public string Status { get; set; }

        //[JsonProperty(PropertyName = "ORDER_ID")]
        public string OrderId { get; set; }

        //[JsonProperty(PropertyName = "CUSTOMERID")]
        public string CustomerId { get; set; }

        //[JsonProperty(PropertyName = "CUSTOMERNAME")]
        public string CustomerName { get; set; }

        //[JsonProperty(PropertyName = "DUEDATE")]
        public string DueDate { get; set; }

        //[JsonProperty(PropertyName = "AMOUNT")]
        public string Amount { get; set; }

        // [JsonProperty(PropertyName = "REASON")]
        public string Reason { get; set; }

        // [JsonProperty(PropertyName = "DC_CODE")]
        public string DcCode { get; set; }

        // [JsonProperty(PropertyName = "RAO_CODE")]
        public string RaoCode { get; set; }

        // [JsonProperty(PropertyName = "CITY")]
        public string City { get; set; }
    }
}
