﻿namespace MyIPay.Dtos.Areas.Forex
{
    public class Currency
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public string ExchangeRateDenominator { get; set; }
        public string FlagActive { get; set; }
    }
}
