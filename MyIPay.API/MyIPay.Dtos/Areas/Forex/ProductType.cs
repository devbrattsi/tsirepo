﻿namespace MyIPay.Dtos.Areas.Forex
{
    public class ProductType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public int CurrencyValueLimit { get; set; }
        public bool FlagActive { get; set; }
    }
}
