﻿namespace MyIPay.Dtos.Areas.Forex
{
    public class CurrencyProductRate
    {
        public double CurrencyExchangeRate { get; set; }
        public int CityId { get; set; }
        public int CurrencyId { get; set; }
        public int ProductTypeId { get; set; }
        public int TransTypeId { get; set; }
        public double MarginDiff { get; set; }
        public double MinLoadBalance { get; set; }
        public double MaxLoadBalance { get; set; }
        public int Denomination { get; set; }
        public int SalePlatform { get; set; }
        public int Id { get; set; }
    }
}
