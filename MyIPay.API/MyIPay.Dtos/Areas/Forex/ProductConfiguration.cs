﻿
namespace MyIPay.Dtos.Areas.Forex
{
    public class ProductConfiguration
    {
        public int CityId { get; set; }
        public int CurrencyId { get; set; }
        public int ProductTypeId { get; set; }
        public int TransTypeId { get; set; }
        public double MarginDiff { get; set; }
        public double MinLoadBalance { get; set; }
        public double MaxLoadBalance { get; set; }
        public int Denomination { get; set; }
        public string SalePlatform { get; set; }
        public int Id { get; set; }
        public bool FlagActive { get; set; }
    }
}
