﻿namespace MyIPay.Dtos.Areas.Forex
{
    public class PinCode
    {
        public int LocationId { get; set; }
        public string Pin { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
