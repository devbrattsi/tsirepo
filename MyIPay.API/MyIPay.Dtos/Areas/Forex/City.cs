﻿
namespace MyIPay.Dtos.Areas.Forex
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool FlagActive { get; set; }
        public string AllowedProductType { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string GstnNo { get; set; }
    }
}
