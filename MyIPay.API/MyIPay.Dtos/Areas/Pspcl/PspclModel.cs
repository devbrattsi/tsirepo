﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.Pspcl
{
    public class BILLDETAIL
    {
        public string BusinessPartner { get; set; }
        public string ContractAccount { get; set; }
        public string Contract { get; set; }
        public string ConsumerName { get; set; }
        public string OpenAmount { get; set; }
        public string SDO { get; set; }
        public string DueAmount { get; set; }
        public string DueDate { get; set; }
        public string CreditAmount { get; set; }
        public string ChequePayAllowed { get; set; }
        public string PrintDate { get; set; }
        public string PrintDocNo { get; set; }
        public string TotalAmount { get; set; }
        public string LatePaymChrg { get; set; }
        public string TARIFPLAN { get; set; }
        public string CURRENT_READING { get; set; }
        public string CURRENT_READING_DATE { get; set; }
        public string FINAL_MTR_READING_DATE { get; set; }
        public string FINAL_MTR_READING { get; set; }
        public string INITIAL_MTR_READING_DATE { get; set; }
        public string INITIAL_MTR_READING { get; set; }
        public string MessageID { get; set; }
        public string Proposed_LPC { get; set; }
        public string Proposed_LPC2 { get; set; }
        public string DueDate_for_LPC2 { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescr { get; set; }
    }

    public class Ns1MTGetBillDetailRes
    {
        [JsonProperty(PropertyName = "-xmlns:ns1")]
        public string _xmlns_ns1 { get; set; }
        public BILLDETAIL BILL_DETAIL { get; set; }
    }

    public class SOAPBody
    {
        [JsonProperty(PropertyName = "ns1:MT_GetBillDetail_Res")]
        public Ns1MTGetBillDetailRes ns1_MT_GetBillDetail_Res { get; set; }
    }

    public class SOAPEnvelope
    {
        [JsonProperty(PropertyName = "-xmlns:SOAP")]
        public string _xmlns_SOAP { get; set; }
        [JsonProperty(PropertyName = "SOAP:Body")]
        public SOAPBody SOAP_Body { get; set; }
    }

    public class PSPCLModel
    {
        [JsonProperty(PropertyName = "SOAP:Envelope")]
        public SOAPEnvelope SOAP_Envelope { get; set; }
    }
}
