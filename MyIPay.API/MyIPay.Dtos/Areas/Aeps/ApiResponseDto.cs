﻿namespace MyIPay.Dtos.Areas.Aeps
{
    public class ApiResponseDto
    {
        public bool HasError { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public string TsiTransactionId { get; set; }
        public object Data { get; set; }
    }
}
