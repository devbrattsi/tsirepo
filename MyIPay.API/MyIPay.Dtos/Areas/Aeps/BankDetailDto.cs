﻿using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.Aeps
{
    public class BankDetailDto
    {
        public BankDetailDto()
        {
            Data = new List<BankData>();
        }

        public bool Status { get; set; }
        public string Message { get; set; }
        public List<BankData> Data { get; set; }
        public int StatusCode { get; set; }
    }

    public class BankData
    {
        public int Id { get; set; }
        public int ActiveFlag { get; set; }
        public string BankName { get; set; }
        public string Details { get; set; }
        public string IINNo { get; set; }
        public string Remarks { get; set; }
        public string Timestamp { get; set; }
    }
}
