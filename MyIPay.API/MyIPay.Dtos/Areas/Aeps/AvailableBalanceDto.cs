﻿namespace MyIPay.Dtos.Areas.Aeps
{
    public class AvailableBalanceDto
    {
        public int StatusCode { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
    }
}
