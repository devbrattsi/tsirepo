﻿using System;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.Aeps
{
    public class CreateMerchantDto
    {
        public bool status { get; set; }
        public string message { get; set; }
        public MerchantData data { get; set; }
        public int statusCode { get; set; }
    }

    public class MerchantData
    {
        public string username { get; set; }
        public string password { get; set; }
        public string ipAddress { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public List<Merchant> merchants { get; set; }
        public int supermerchantId { get; set; }
        public DateTime timestamp { get; set; }
    }


    public class Merchant
    {
        public int merchantId { get; set; }
        public string merchantLoginId { get; set; }
        public string merchantLoginPin { get; set; }
        public string merchantName { get; set; }
        public MerchantAddress merchantAddress { get; set; }
        public string merchantPhoneNumber { get; set; }
        public int supermerchantId { get; set; }
        public string companyLegalName { get; set; }
        public string companyMarketingName { get; set; }
        public Kyc kyc { get; set; }
        public Settlement settlement { get; set; }
        public string merchantBranch { get; set; }
        public int activeFlag { get; set; }
        public string creationStatus { get; set; }
        public object remarks { get; set; }
        public string emailId { get; set; }
        public string shopAndPanImage { get; set; }
        public string cancellationCheckImages { get; set; }
        public string ekycDocuments { get; set; }
    }



    public class MerchantAddress
    {
        public string merchantAddress { get; set; }
        public string merchantState { get; set; }
    }

    public class Kyc
    {
        public string userPan { get; set; }
        public string aadhaarNumber { get; set; }
        public object gstinNumber { get; set; }
        public string companyOrShopPan { get; set; }
    }

    public class Settlement
    {
        public string companyBankAccountNumber { get; set; }
        public string bankIfscCode { get; set; }
        public string companyBankName { get; set; }
        public string bankBranchName { get; set; }
        public string bankAccountName { get; set; }
    }

    
}
