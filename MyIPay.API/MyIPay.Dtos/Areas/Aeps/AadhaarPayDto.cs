﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.Aeps
{
    public class AadhaarPayDto
    {
        [JsonProperty(PropertyName = "status")]
        public bool Status { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "data")]
        public AadhaarPayData Data { get; set; }

        [JsonProperty(PropertyName = "statusCode")]
        public long StatusCode { get; set; }
    }

    public class AadhaarPayData
    {

        [JsonProperty(PropertyName = "terminalId")]
        public string TerminalId { get; set; }

        [JsonProperty(PropertyName = "requestTransactionTime")]
        public string RequestTransactionTime { get; set; }

        [JsonProperty(PropertyName = "transactionAmount")]
        public double TransactionAmount { get; set; }

        [JsonProperty(PropertyName = "transactionStatus")]
        public string TransactionStatus { get; set; }

        [JsonProperty(PropertyName = "balanceAmount")]
        public double BalanceAmount { get; set; }

        [JsonProperty(PropertyName = "bankRRN")]
        public string BankRRN { get; set; }

        [JsonProperty(PropertyName = "transactionType")]
        public string TransactionType { get; set; }

        [JsonProperty(PropertyName = "fingpayTransactionId")]
        public string FingpayTransactionId { get; set; }

        [JsonProperty(PropertyName = "merchantTxnId")]
        public string MerchantTxnId { get; set; }
    }
}
