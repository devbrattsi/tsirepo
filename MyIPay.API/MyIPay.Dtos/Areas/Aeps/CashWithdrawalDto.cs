﻿namespace MyIPay.Dtos.Areas.Aeps
{
    public class CashWithdrawalDto
    {
        public bool status { get; set; }

        public string message { get; set; }

        public BalanceInquiryData data { get; set; }

        public long statusCode { get; set; }
    }

    public class CashWithdrawalData
    {

        public string terminalId { get; set; }

        public string requestTransactionTime { get; set; }

        public double transactionAmount { get; set; }

        public string transactionStatus { get; set; }

        public double balanceAmount { get; set; }

        public string bankRRN { get; set; }

        public string transactionType { get; set; }

        public string fpTransactionId { get; set; }

        public string merchantTxnId { get; set; }


        public string errorCode { get; set; }

        public string errorMessage { get; set; }

        public string merchantTransactionId { get; set; }

        public string bankAccountNumber { get; set; }

        public string ifscCode { get; set; }

    }

}
