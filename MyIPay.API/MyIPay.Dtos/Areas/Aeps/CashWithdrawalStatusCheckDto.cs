﻿using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.Aeps
{
    public class CashWithdrawalStatusCheckDto
    {
        public CashWithdrawalStatusCheckDto()
        {
            data = new List<Data>();
        }

        public bool apiStatus { get; set; }
        public string apiStatusMessage { get; set; }
        public List<Data> data { get; set; }
        public long apiStatusCode { get; set; }
    }

    public class Data
    {
        public string fingpayTransactionId { get; set; }
        public string stan { get; set; }
        public string bankRRN { get; set; }
        public string transactionTime { get; set; }
        public string merchantTranId { get; set; }
        public bool transactionStatus { get; set; }
        public double transactionAmount { get; set; }
        public string transactionStatusCode { get; set; }
        public string transactionStatusMessage { get; set; }
        public string remarks { get; set; }
    }
}
