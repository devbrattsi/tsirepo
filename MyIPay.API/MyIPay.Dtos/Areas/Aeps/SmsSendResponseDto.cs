﻿namespace MyIPay.Dtos.Areas.Aeps
{
    public class SmsSendResponseDto
    {
        public bool MessageSendSuccess { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
}
