﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.Aeps
{
    public class AepsDto
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("merchantRefNo")]
        public string MerchantRefNo { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
