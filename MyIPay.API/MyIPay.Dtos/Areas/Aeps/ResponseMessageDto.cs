﻿namespace MyIPay.Dtos.Areas.Aeps
{
    public class ResponseMessageDto
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
}
