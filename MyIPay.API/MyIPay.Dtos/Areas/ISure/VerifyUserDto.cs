﻿namespace MyIPay.Dtos.Areas.ISure
{
    public class VerifyUserDto
    {
        public string Client_Code { get; set; }
        // public string Client_VAccount_No1 { get; set; }

        //Here AgentCode is virtual account no
        public string AgentCode { get; set; }
        public long Amount { get; set; }
        public string Status { get; set; }
        public string Reject_Reason { get; set; }
    }
}
