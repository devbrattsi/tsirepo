﻿namespace MyIPay.Dtos.Areas.Yatra
{
    public class CheckStatusAsyncDto
    {
        public string ResponseCode { get; set; }
        public decimal Amount { get; set; }
        public string Signature { get; set; }
        public string Description { get; set; }
        public string TransactionId { get; set; }
        public string Status { get; set; }
        public bool IsSuccess { get; set; }
        public string OrderId { get; set; }
    }

}
