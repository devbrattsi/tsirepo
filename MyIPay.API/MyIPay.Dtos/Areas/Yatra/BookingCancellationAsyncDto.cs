﻿
namespace MyIPay.Dtos.Areas.Yatra
{
    public class BookingCancellationAsyncDto
    {
        public string RefundedOrderId { get; set; }
        public string ResponseCode { get; set; }
        public string Amount { get; set; }
        public string RefTxnId { get; set; }
        public string Signature { get; set; }
        public string Description { get; set; }
        public string PurOrderId { get; set; }
        public string Status { get; set; }
        public string OrderId { get; set; }
        //public string AgentId { get; set; }
        public bool IsSuccess { get; set; }
    }
}
