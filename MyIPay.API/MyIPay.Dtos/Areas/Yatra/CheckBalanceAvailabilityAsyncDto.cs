﻿
namespace MyIPay.Dtos.Areas.Yatra
{
    public class CheckBalanceAvailabilityAsyncDto
    {
        public bool IsSuccess { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
    }
}
