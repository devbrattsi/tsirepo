﻿namespace MyIPay.Dtos.Areas.Yatra
{
    public class VerifyTokenDto
    {
        public string ResponseCode { get; set; }
        public string ResponseType { get; set; }
        public string Description { get; set; }
        public bool IsAuthenticated { get; set; }
        public string ResponseMessage { get; set; }
        public string AgentId { get; set; }
        public string AgentEmail { get; set; }

    }
}
