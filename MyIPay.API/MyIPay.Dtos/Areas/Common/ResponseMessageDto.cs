﻿
namespace MyIPay.Dtos.Areas.Common
{
    public class ResponseMessageDto
    {
        public bool HasError { get; set; }
        public bool IsSuccess { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }
}
