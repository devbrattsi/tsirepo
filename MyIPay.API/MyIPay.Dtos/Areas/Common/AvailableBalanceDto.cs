﻿namespace MyIPay.Dtos.Areas.Common
{
    public class AvailableBalanceDto
    {
        public int StatusCode { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
    }
}
