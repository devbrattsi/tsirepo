﻿using System;

namespace MyIPay.Dtos.Areas.Common
{
    public class ApiResponseDto
    {
        public bool HasError { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public bool IsDuplicate { get; set; }
        public object Response { get; set; }
    }
}
