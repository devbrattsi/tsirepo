﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class WalnutRechargePlan
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "operator_id")]
        public string OperatorId { get; set; }
        [JsonProperty(PropertyName = "circle_id")]
        public string CircleId { get; set; }
        [JsonProperty(PropertyName = "recharge_amount")]
        public string RechargeAmount { get; set; }
        [JsonProperty(PropertyName = "recharge_talktime")]
        public string RechargeTalktime { get; set; }
        [JsonProperty(PropertyName = "recharge_validity")]
        public string RechargeValidity { get; set; }
        [JsonProperty(PropertyName = "recharge_short_desc")]
        public string RechargeShortDesc { get; set; }
        [JsonProperty(PropertyName = "recharge_long_desc")]
        public string RechargeLongDesc { get; set; }
        [JsonProperty(PropertyName = "recharge_type")]
        public string RechargeType { get; set; }
        [JsonProperty(PropertyName = "updated_at")]
        public string UpdatedAt { get; set; }
        [JsonProperty(PropertyName = "operatorid")]
        public string DthOperatorId { get; set; }
        [JsonProperty(PropertyName = "circleid")]
        public string DthCircleId { get; set; }
        [JsonProperty(PropertyName = "recharge_shortdesc")]
        public string DthRechargeShortDesc { get; set; }
        [JsonProperty(PropertyName = "recharge_longdesc")]
        public string DthRechargeLongDesc { get; set; }
    }

    public class Data
    {
        [JsonProperty(PropertyName = "data")]
        public List<WalnutRechargePlan> Datas { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
    }

    public class WalnutRechargePlanDto
    {
        [JsonProperty(PropertyName = "txnId")]
        public string ClientReferenceId { get; set; }
        [JsonProperty(PropertyName = "twTxnId")]
        public string WalnutTransactionId { get; set; }
        [JsonProperty(PropertyName = "responseCode")]
        public string ResponseCode { get; set; }
        [JsonProperty(PropertyName = "data")]
        public Data Data { get; set; }
    }
}
