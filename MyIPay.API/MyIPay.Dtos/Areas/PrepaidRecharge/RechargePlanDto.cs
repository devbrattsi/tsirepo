﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class RechargePlanDto
    {
        [JsonProperty(PropertyName = "status")]
        public int status { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }

        [JsonProperty(PropertyName = "api_reference_id")]
        public string api_reference_id { get; set; }

        [JsonProperty(PropertyName = "result")]
        public List<RechargePlanResponse> result { get; set; }
    }

    public class RechargePlanResponse
    {

        [JsonProperty(PropertyName = "plan_id")]
        public string plan_id { get; set; }

        [JsonProperty(PropertyName = "operator_code")]
        public string operator_code { get; set; }

        [JsonProperty(PropertyName = "operator_name")]
        public string operator_name { get; set; }

        [JsonProperty(PropertyName = "circle_code")]
        public string circle_code { get; set; }

        [JsonProperty(PropertyName = "circle_name")]
        public string circle_name { get; set; }

        [JsonProperty(PropertyName = "plan_type")]
        public string plan_type { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public string amount { get; set; }

        [JsonProperty(PropertyName = "talktime")]
        public string talktime { get; set; }

        [JsonProperty(PropertyName = "validity")]
        public string validity { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string description { get; set; }

        [JsonProperty(PropertyName = "created_on")]
        public string created_on { get; set; }

        [JsonProperty(PropertyName = "updated_on")]
        public string updated_on { get; set; }
    }
}
