﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class TransactionStatusDto
    {
        [JsonProperty(PropertyName = "trans_id")]
        public string TransactionId { get; set; }

        [JsonProperty(PropertyName = "client_trans_id")]
        public string ClientTransactionId { get; set; }

        [JsonProperty(PropertyName = "msisdn")]
        public long Msisdn { get; set; }

        [JsonProperty(PropertyName = "custid")]
        public long CustomerId { get; set; }

        [JsonProperty(PropertyName = "operator_code")]
        public int OperatorCode { get; set; }

        [JsonProperty(PropertyName = "operator_name")]
        public string OperatorName { get; set; }

        [JsonProperty(PropertyName = "trans_amount")]
        public int TransactionAmount { get; set; }

        [JsonProperty(PropertyName = "datetime")]
        public string TransactionDateTime { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string TransactionStatus { get; set; }

        [JsonProperty(PropertyName = "charged_amount")]
        public decimal ChargedAmount { get; set; }

        [JsonProperty(PropertyName = "response_code")]
        public string ResponseCode { get; set; }

        [JsonProperty(PropertyName = "opr_transid")]
        public string OperatorTransactionId { get; set; }

        [JsonProperty(PropertyName = "opening_balance")]
        public string OpeningBalance { get; set; }

        public string ErrorCode { get; set; }
        public string Message { get; set; }

    }

    
}
