﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class CircleInfoDto
    {
        [JsonProperty(PropertyName = "circle_name")]
        public string CircleName { get; set; }

        [JsonProperty(PropertyName = "circle_code")]
        public int CircleCode { get; set; }
    }

    
}
