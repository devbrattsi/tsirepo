﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class BalanceCheckDto
    {
        [JsonProperty(PropertyName = "goid")]
        public string GoId { get; set; }

        [JsonProperty(PropertyName = "balance")]
        public string Balance { get; set; }
    }
}
