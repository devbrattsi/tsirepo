﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class WalnutMsisdnInfoDto
    {
        [JsonProperty(PropertyName = "transTime")]
        public string TransactionDateTime { get; set; }
        [JsonProperty(PropertyName = "txnId")]
        public string ClientReferenceId { get; set; }
        [JsonProperty(PropertyName = "twTxnId")]
        public string WalnutTransactionId { get; set; }
        [JsonProperty(PropertyName = "ported")]
        public string Ported { get; set; }
        [JsonProperty(PropertyName = "responseCode")]
        public string ResponseCode { get; set; }
        [JsonProperty(PropertyName = "responseMsg")]
        public string ResponseMessage { get; set; }
        [JsonProperty(PropertyName = "operator")]
        public string Operator { get; set; }
        [JsonProperty(PropertyName = "circle")]
        public string Circle { get; set; }
    }
}
