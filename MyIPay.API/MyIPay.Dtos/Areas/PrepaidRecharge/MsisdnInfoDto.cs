﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class MsisdnInfoDto
    {
        [JsonProperty(PropertyName = "operator_code")]
        public string OperatorCode { get; set; }

        [JsonProperty(PropertyName = "circle_code")]
        public string CircleCode { get; set; }
    }
}
