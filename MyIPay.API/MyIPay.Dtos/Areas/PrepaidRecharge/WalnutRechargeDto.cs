﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.PrepaidRecharge
{
    public class WalnutRechargeDto
    {
        [JsonProperty(PropertyName = "ts")]
        public string TimeStamp { get; set; }
        [JsonProperty(PropertyName = "clientId")]
        public string ClientReferenceId { get; set; }
        [JsonProperty(PropertyName = "txnId")]
        public string TransactionId { get; set; }
        [JsonProperty(PropertyName = "optId")]
        public string ServiceProviderId { get; set; }
        [JsonProperty(PropertyName = "errCode")]
        public string ResponseCode { get; set; }
        [JsonProperty(PropertyName = "msg")]
        public string ResponseMessage { get; set; }
    }
}
