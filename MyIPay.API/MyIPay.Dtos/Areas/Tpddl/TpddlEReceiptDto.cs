﻿namespace MyIPay.Dtos.Areas.Tpddl
{
    public class TpddlEReceiptDto
    {
        public string ConsumerNumber { get; set; }
        public string CounterNumber { get; set; }
        public string ReceiptNumber { get; set; }
        public string ResponseMessage { get; set; }
    }
}
