﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Tpddl
{
    public class GetBillDetailDto
    {
        public string ConsumerNumber { get; set; }
        public string KNumber { get; set; }

        public string ConsumerName { get; set; }
        public string BillMonthYear { get; set; }
        public string Address { get; set; }
        public DateTime? BillDate { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? CurrentDemand { get; set; }
        public string BillNumbre { get; set; }
        public decimal? AmountPayableRounded { get; set; }
        public string ChequePaymentAllowed { get; set; }
        public string OrderId { get; set; }
        public string ReceiptId { get; set; }
    }
}
