﻿
using System;

namespace MyIPay.Dtos.Areas.Tpddl
{
    public class GetBillInfoDto
    {
        public string kNo { get; set; }
        public string caNo { get; set; }
        public string consumerName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string billNumber { get; set; }
        public string billMonth { get; set; }
        public string billYear { get; set; }
        public DateTime? billDate { get; set; }
        public DateTime? dueDate { get; set; }
        public string dueAmount { get; set; }
        public string amountPayable { get; set; }
        public string currentDemand { get; set; }
        public string chequeBounceHistory { get; set; }
        public int responseCode { get; set; }
        public string responseMessage { get; set; }
    }
}
