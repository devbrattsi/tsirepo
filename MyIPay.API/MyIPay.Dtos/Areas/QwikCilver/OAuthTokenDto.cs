﻿namespace MyIPay.Dtos.Areas.QwikCilver
{
    public class OAuthTokenDto
    {
        public string AOuthToken { get; set; }
        public string OAuthTokenSecret { get; set; }
    }
}
