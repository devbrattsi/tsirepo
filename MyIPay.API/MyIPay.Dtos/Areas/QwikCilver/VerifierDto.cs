﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.QwikCilver
{
    public class VerifierDto
    {
        //[JsonProperty("success")]
        public bool Success { get; set; }

        //[JsonProperty("verifier")]
        public string Verifier { get; set; }

        public string Message { get; set; }
    }
}
