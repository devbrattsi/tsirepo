﻿
namespace MyIPay.Dtos.Areas.QwikCilver
{
    public class AccessTokenDto
    {
        public string AauthToken { get; set; }
        public string OauthTokenSecret { get; set; }
    }
}
