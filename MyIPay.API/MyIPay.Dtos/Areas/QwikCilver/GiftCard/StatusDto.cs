﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class StatusDto
    {
        [JsonProperty("Process")]
        public bool process { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }

        [JsonProperty("error_code")]
        public string ErrorCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
