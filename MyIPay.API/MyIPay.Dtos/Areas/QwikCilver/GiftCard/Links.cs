﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class Links
    {
        [JsonProperty("self")]
        public Self Self { get; set; }
    }
}
