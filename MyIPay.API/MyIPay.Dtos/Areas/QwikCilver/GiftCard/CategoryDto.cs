﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{

    public class CategoryDto
    {

        [JsonProperty("root_category")]
        public RootCategory RootCategory { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
    public class RootCategory
    {
        public RootCategory()
        {
            CategoryList = new List<CategoryList>();
        }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("category_image")]
        public string CategoryImage { get; set; }

        [JsonProperty("category_list")]
        public List<CategoryList> CategoryList { get; set; }
    }

    public class CategoryList
    {
        public ParentCategory parent_category { get; set; }
    }

    public class ParentCategory
    {
        public ParentCategory()
        {
            ChildCategories = new List<ChildCategory>();
        }

        [JsonProperty("category_id")]
        public int CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("category_image")]
        public string CategoryImage { get; set; }

        [JsonProperty("child_category")]
        public List<ChildCategory> ChildCategories { get; set; }
    }

    public class ChildCategory
    {
        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("CategoryName")]
        public string category_name { get; set; }

        [JsonProperty("category_image")]
        public string CategoryImage { get; set; }
    }


    //public class Self
    //{
    //    public string href { get; set; }
    //}

    //public class Links
    //{
    //    public Self self { get; set; }
    //}


}
