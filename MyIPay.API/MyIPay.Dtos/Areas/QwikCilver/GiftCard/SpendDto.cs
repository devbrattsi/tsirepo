﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{

    public class SpendDto
    {
        public SpendDto()
        {
            CardDetails = new CardDetails();
        }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("error_code")]
        public string ErrorCode { get; set; }

        [JsonProperty("carddetails")]
        public CardDetails CardDetails { get; set; }

        [JsonProperty("available_balance")]
        public string AvailableBalance { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }

    public class TempDto
    {
        public TempDto()
        {
            CardDetails = new Dictionary<object, object>();
        }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("error_code")]
        public string ErrorCode { get; set; }

        [JsonProperty("carddetails")]
        public Dictionary<object, object> CardDetails { get; set; }

        [JsonProperty("available_balance")]
        public string AvailableBalance { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }

    public class CardDetails
    {
        public CardDetails()
        {
            EGiftCards = new List<EGiftCard>();
        }

        public List<EGiftCard> EGiftCards { get; set; }
    }

    public class EGiftCard
    {
        public string CardName { get; set; }

        [JsonProperty("card_price")]
        public string CardPrice { get; set; }

        [JsonProperty("expiry_date")]
        public string ExpiryDate { get; set; }

        [JsonProperty("cardnumber")]
        public string CardNumber { get; set; }

        [JsonProperty("pin_or_url")]
        public string PinOrUrl { get; set; }

        [JsonProperty("activation_code")]
        public string ActivationCode { get; set; }
    }


}
