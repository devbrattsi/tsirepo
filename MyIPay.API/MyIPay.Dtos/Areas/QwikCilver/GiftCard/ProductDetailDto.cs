﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class ProductDetailDto
    {
        public ProductDetailDto()
        {
            Themes = new Dictionary<object, object>();
        }
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonIgnore]
        public int CashBack { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("short_description")]
        public string ShortDescription { get; set; }

        [JsonProperty("price_type")]
        public string PriceType { get; set; }

        [JsonProperty("min_custom_price")]
        public string MinCustomPrice { get; set; }

        [JsonProperty("max_custom_price")]
        public string MaxCustomPrice { get; set; }

        [JsonProperty("custom_denominations")]
        public string CustomDenominations { get; set; }

        [JsonIgnore]
        public string[] Denominations { get; set; }

        [JsonProperty("product_type")]
        public string ProductType { get; set; }

        [JsonProperty("paywithamazon_disable")]
        public string PaywithamazonDisable { get; set; }

        [JsonProperty("images")]
        public string Images { get; set; }

        [JsonProperty("tnc_mobile")]
        public string TncMobile { get; set; }

        [JsonProperty("tnc_web")]
        public string TncWeb { get; set; }

        [JsonProperty("tnc_mail")]
        public string TncMail { get; set; }

        [JsonProperty("themes")]
        public Dictionary<object, object> Themes { get; set; }
        //public Themes Themes { get; set; }

        [JsonProperty("order_handling_charge")]
        public int OrderHandlingCharge { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }

}
