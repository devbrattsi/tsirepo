﻿
namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class ProductDto
    {
        public string Id { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string PriceType { get; set; }
        public string MinCustomPrice { get; set; }
        public string MaxCustomPrice { get; set; }
        public string CustomDenominations { get; set; }
        public string ProductType { get; set; }
        public string TncMobile { get; set; }
        public string TncWeb { get; set; }
        public string TncMail { get; set; }
        public int OrderHandlingCharge { get; set; }
        public string Image { get; set; }
        public string SmallImage { get; set; }
        public string BaseImage { get; set; }

        public string RequestId { get; set; }
    }
}
