﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    

    public class ResendDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("carddetails")]
        public Dictionary<object, object> CardDetails { get; set; }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonIgnore]
        public CardDetail CardDetail { get; set; }


        [JsonProperty("error_code")]
        public string ErrorCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class CardDetail
    {
        [JsonIgnore]
        public string CardName { get; set; }

        [JsonProperty("card_price")]
        public string CardPrice { get; set; }

        [JsonProperty("expiry_date")]
        public string ExpiryDate { get; set; }

        [JsonProperty("cardnumber")]
        public string CardNumber { get; set; }

        [JsonProperty("pin_or_url")]
        public string PinOrUrl { get; set; }

        [JsonProperty("activation_code")]
        public string ActivationCode { get; set; }
    }
}
