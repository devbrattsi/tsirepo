﻿using MyIPay.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class SettingDto
    {

        public SettingDto()
        {
            PaymentMethod = new List<PaymentMethod>();
            Links = new Links();
        }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("store_id")]
        public string StoreId { get; set; }

        [JsonProperty("config")]
        public Config Config { get; set; }

        [JsonProperty("payment_method")]
        [JsonConverter(typeof(SingleOrArrayConverter<PaymentMethod>))]
        public List<PaymentMethod> PaymentMethod { get; set; }

        [JsonProperty("terms_and_conditions")]
        public string TermsAndConditions { get; set; }

        [JsonProperty("faq")]
        public object Faq { get; set; }

        [JsonProperty("powered_by_url")]
        public string PoweredByUrl { get; set; }

        [JsonProperty("logo_url")]
        public string LogoUrl { get; set; }

        [JsonProperty("base_url")]
        public string BaseUrl { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }

    public class Config
    {
        [JsonProperty("product_max_qty_allow")]
        public string ProductMaxQtyAllow { get; set; }

        [JsonProperty("max_item_in_cart")]
        public string MaxItemInCart { get; set; }
    }

    public class PaymentMethod
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("info")]
        public string Info { get; set; }

        [JsonProperty("partial_allow")]
        public bool? PartialAllow { get; set; }

        [JsonProperty("show_captcha")]
        public bool? AhowCaptcha { get; set; }
    }
    
    //public class Links
    //{
    //    [JsonProperty("self")]
    //    public Self Self { get; set; }
    //}

    //public class Self
    //{
    //    [JsonProperty("href")]
    //    public string Href { get; set; }
    //}

}
