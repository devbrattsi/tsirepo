﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class CategoryDetailDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("navigation_urlpath")]
        public string NavigationUrlPath { get; set; }

        [JsonProperty("banner_image")]
        public string BannerImage { get; set; }

        [JsonProperty("page_title")]
        public object PageTitle { get; set; }

        [JsonProperty("meta_title")]
        public object MetaTitle { get; set; }

        [JsonProperty("meta_description")]
        public object MetaDescription { get; set; }

        [JsonProperty("meta_keywords")]
        public object MetaKeywords { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }

        [JsonProperty("_embedded")]
        public Embedded Embedded { get; set; }
    }

    public class Embedded
    {
        public Embedded()
        {
            Products = new List<Product>();
        }

        [JsonProperty("product")]
        public List<Product> Products { get; set; }
    }

    public class Images
    {
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("small_image")]
        public string SmallImage { get; set; }

        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("base_image ")]
        public string BaseImage { get; set; }
    }

    public class Product
    {
        [JsonIgnore]
        public string CategoryId { get; set; }

        [JsonIgnore]
        public string CategoryName { get; set; }

        [JsonIgnore]
        public string CategoryImage { get; set; }

        [JsonIgnore]
        public int CashBack { get; set; }

        [JsonProperty("id")]
        public string ProductId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("brand_id")]
        public string BrandId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("images")]
        public Images Images { get; set; }

        [JsonProperty("price_type")]
        public string PriceType { get; set; }

        [JsonProperty("navigation_apicall")]
        public string NavigationApiCall { get; set; }

        [JsonProperty("navigation_urlpath")]
        public string NavigationUrlPath { get; set; }

        [JsonProperty("min_custom_price")]
        public string MinCustomPrice { get; set; }

        [JsonProperty("max_custom_price")]
        public string MaxCustomPrice { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }

        [JsonProperty("custom_denominations")]
        public string CustomDenominations { get; set; }
    }



    //public class Self
    //{
    //    public string href { get; set; }
    //}

    //public class Links
    //{
    //    public Self self { get; set; }
    //}
}
