﻿using Newtonsoft.Json;

namespace MyIPay.Dtos.Areas.QwikCilver.GiftCard
{
    public class Self
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }
}
