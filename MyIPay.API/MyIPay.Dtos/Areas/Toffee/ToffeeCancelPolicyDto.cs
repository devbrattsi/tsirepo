﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Toffee
{
    public class ToffeeCancelPolicyDto : ToffeeDto
    {
        public CancelPolicy CancelPolicyData
        {
            get
            {
                return JsonConvert.DeserializeObject<CancelPolicy>(JsonConvert.SerializeObject(Data));
            }
            set { }
        }
    }

    public class CancelPolicy
    {
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
