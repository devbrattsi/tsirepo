﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Toffee
{
    public class ToffeeProductDto : ToffeeDto
    {
        public List<Product> Products
        {
            get
            {
                return JsonConvert.DeserializeObject<List<Product>>(JsonConvert.SerializeObject(Data));
            }
            set { }
        }
    }

    public class Product
    {
        [JsonProperty("product_name")]
        public string ProductName { get; set; }
        [JsonProperty("premium")]
        public string Premium { get; set; }
        [JsonProperty("gross_premium")]
        public string GrossPremium { get; set; }
        [JsonProperty("sum_insured")]
        public string SumInsured { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("duration")]
        public int Duration { get; set; }
        [JsonProperty("durationUnit")]
        public string DurationUnit { get; set; }
        [JsonProperty("insuranceProvider")]
        public string InsuranceProvider { get; set; }
        [JsonProperty("coverage_highlights")]
        public List<CoverageHighlight> CoverageHighlights { get; set; }
        [JsonProperty("nominee_relationships")]
        public object Nominee_Relationships { get; set; }
        public List<KeyValuePair<int, string>> NomineeRelationships
        {
            get
            {
                return JsonConvert.DeserializeObject<Dictionary<int, string>>(JsonConvert.SerializeObject(Nominee_Relationships)).OrderBy(o => o.Value).ToList();
            }
            set { }
        }
        [JsonProperty("coverages")]
        public List<Coverage> Coverages { get; set; }
    }

    public class CoverageHighlight
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("sub_title")]
        public string SubTitle { get; set; }
    }
    public class Coverage
    {
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("sub-text")]
        public List<object> SubText { get; set; }
    }
}
