﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Toffee
{
    public class InsurancePurchaseDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("locale")]
        public string Locale { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public Data Data { get; set; }
    }
    public class Data
    {
        [JsonProperty("toffee_id")]
        public string ToffeeId { get; set; }
        [JsonProperty("covernote")]
        public string CoverNote { get; set; }
        [JsonProperty("msg")]
        public string Message { get; set; }
    }
}
