﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Toffee
{
    public class ToffeeDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("locale")]
        public string Locale { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public object Data { get; set; }
    }
}
