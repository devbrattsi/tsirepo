﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Toffee
{
    public class ToffeeCityDto : ToffeeDto
    {
        public List<KeyValuePair<int, string>> CityData
        {
            get
            {
                return JsonConvert.DeserializeObject<Dictionary<int, string>>(JsonConvert.SerializeObject(Data)).OrderBy(o => o.Value).ToList();
            }
            set { }
        }
    }
}
