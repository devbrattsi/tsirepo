﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Dtos.Areas.Toffee
{
    public class ToffeeQueryPurchaseDto : ToffeeDto
    {
        public QueryData QueryDatas
        {
            get
            {
                return JsonConvert.DeserializeObject<QueryData>(JsonConvert.SerializeObject(Data));
            }
            set { }
        }
    }

    public class QueryData
    {
        [JsonProperty("toffee_id")]
        public string ToffeeId { get; set; }
        [JsonProperty("policy_number")]
        public string PolicyNumber { get; set; }
        [JsonProperty("policy_document")]
        public string PolicyDocument { get; set; }
        [JsonProperty("covernote")]
        public string CoverNote { get; set; }
    }
}
