﻿namespace MyIPay.Dtos.Areas.BankDeposit.ECollection
{
    public class BankDepositTransactionDto
    {
        public string Status { get; set; }
        public string BankRefNumber { get; set; }
        public string RejectReason { get; set; }
        public string TsiTrxnId { get; set; }
    }
}
