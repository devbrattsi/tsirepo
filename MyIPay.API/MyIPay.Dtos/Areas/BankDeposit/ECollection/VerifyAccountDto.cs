﻿using System;
using System.Xml.Serialization;

namespace MyIPay.Dtos.Areas.BankDeposit.ECollection
{
    [Serializable()]
    public class VerifyAccountDto
    {
        [XmlElement]
        public string ClientCode { get; set; }

        [XmlElement]
        public string VirtualAccountNumber { get; set; }

        [XmlElement]
        public string TransactionAmount { get; set; }

        [XmlElement]
        public string UtrNumber { get; set; }

        [XmlElement]
        public string SenderIfscCode { get; set; }

        [XmlElement]
        public string Status { get; set; }

        [XmlElement]
        public string RejectReason { get; set; }
    }
}
