﻿using AepsInterface = MyIPay.Services.Areas.Aeps.Interfaces;
using Aeps = MyIPay.Services.Areas.Aeps;
using MyIPay.Services.Areas.Avenue;
using MyIPay.Services.Areas.Avenue.Interface;
using MyIPay.Services.Areas.BillDesk;
using MyIPay.Services.Areas.BillDesk.Interfaces;
using MyIPay.Services.Areas.Common;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.Forex;
using MyIPay.Services.Areas.Forex.Interfaces;
using MyIPay.Services.Areas.ISure;
using MyIPay.Services.Areas.ISure.Interfaces;
using MyIPay.Services.Areas.MoneyTransfer;
using MyIPay.Services.Areas.MoneyTransfer.Airtel;
using MyIPay.Services.Areas.MoneyTransfer.Airtel.Interfaces;
using MyIPay.Services.Areas.MoneyTransfer.Interfaces;
using MyIPay.Services.Areas.MpVidyutVitaran;
using MyIPay.Services.Areas.MpVidyutVitaran.Interfaces;
using MyIPay.Services.Areas.PrabhuBank;
using MyIPay.Services.Areas.PrabhuBank.Interfaces;
using MyIPay.Services.Areas.PrepaidRecharge;
using MyIPay.Services.Areas.PrepaidRecharge.Interface;
using MyIPay.Services.Areas.Pspcl;
using MyIPay.Services.Areas.Pspcl.Interfaces;
using MyIPay.Services.Areas.Tpddl;
using MyIPay.Services.Areas.Tpddl.Interfaces;
using MyIPay.Services.Areas.Yatra;
using MyIPay.Services.Areas.Yatra.Interfaces;
using System.Web.Http;
using Unity;
using Unity.Lifetime;
using MyIPay.Services.Areas.BankDeposit.ECollection.Interfaces;
using MyIPay.Services.Areas.BankDeposit.ECollection;
using MyIPay.Services.Areas.Toffee;
using MyIPay.Services.Areas.Toffee.Interfaces;
using MyIPay.Services.Areas.QwikCilver.Interfaces;
using MyIPay.Services.Areas.QwikCilver;
using MyIPay.Services.Areas.CashDrop;
using MyIPay.Services.Areas.CashDrop.Interfaces;

namespace MyIPay.API.App_Start
{
    public class UnityConfig
    {
        public static void RegisterComponents(HttpConfiguration config)
        {
            //var container = new UnityContainer();
            //container.RegisterType<IISureService, ISureService>(new HierarchicalLifetimeManager());
            //container.RegisterType<ICommonService, CommonService>(new HierarchicalLifetimeManager());

            var container = BuildUnityContainer();
            config.DependencyResolver = new UnityResolver(container);

            // Other Web API configuration not shown.
        }

        //public static void RegisterComponents()
        //{
        //    //var container = BuildUnityContainer();
        //    //System.Web.Mvc.DependencyResolver.SetResolver(new UnityResolver(container));

        //    //GlobalConfiguration.Configuration.DependencyResolver = new UnityResolver(container);

        //}


        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //Register services
            container.RegisterType<IDomesticMoneyTransferService, DomesticMoneyTransferService>(new HierarchicalLifetimeManager());
            container.RegisterType<IForexSynchronizerService, ForexSynchronizerService>(new HierarchicalLifetimeManager());
            container.RegisterType<IForexApiManagerService, ForexApiManagerService>(new HierarchicalLifetimeManager());
            container.RegisterType<IISureService, ISureService>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommonService, CommonService>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommunicationService, CommunicationService>(new HierarchicalLifetimeManager());
            container.RegisterType<IPrepaidRechargeService, PrepaidRechargeService>(new HierarchicalLifetimeManager());
            container.RegisterType<IApiManagerService, ApiManagerService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAvenueService, AvenueService>(new HierarchicalLifetimeManager());
            container.RegisterType<IPspclService, PspclService>(new HierarchicalLifetimeManager());
            container.RegisterType<IYatraService, YatraService>(new HierarchicalLifetimeManager());
            container.RegisterType<IApiManagerService, ApiManagerService>(new HierarchicalLifetimeManager());
            container.RegisterType<ITransactionService<string>, TransactionService<string>>(new HierarchicalLifetimeManager());
            container.RegisterType<IAirtelBankService, AirtelBankService>(new HierarchicalLifetimeManager());
            container.RegisterType<ITpddlService, TpddlService>(new HierarchicalLifetimeManager());
            container.RegisterType<IBillDeskService, BillDeskService>(new HierarchicalLifetimeManager());
            container.RegisterType<IMpVidyutVitaranService, MpVidyutVitaranService>(new HierarchicalLifetimeManager());
            container.RegisterType<IECollectionService, ECollectionService>(new HierarchicalLifetimeManager());
            container.RegisterType<IInmtService, InmtService>(new HierarchicalLifetimeManager());
            container.RegisterType<AepsInterface.IAepsService, Aeps.AepsService>(new HierarchicalLifetimeManager());
            container.RegisterType<AepsInterface.IPaymentService, Aeps.PaymentService>(new HierarchicalLifetimeManager());
            container.RegisterType<IToffeeService, ToffeeService>(new HierarchicalLifetimeManager());
            container.RegisterType<IGiftCardService, GiftCardService>(new HierarchicalLifetimeManager());
            container.RegisterType<ICashDropService, CashDropService>(new HierarchicalLifetimeManager());

            //returning container
            return container;
        }
    }
}