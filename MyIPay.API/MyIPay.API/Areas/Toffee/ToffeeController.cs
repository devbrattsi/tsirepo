﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.Toffee;
using MyIPay.Services.Areas.Toffee.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Toffee
{
    public class ToffeeController : ApiController
    {
        private readonly IToffeeService _toffeeService;
        public ToffeeController(IToffeeService toffeeService)
        {
            _toffeeService = toffeeService;
        }

        [HttpGet]
        [Route("api/Toffee/GetTokenAsync")]
        public async Task<IHttpActionResult> GetTokenAsync()
        {
            try
            {
                return Ok(await _toffeeService.GetTokenAsync());
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetTokenAsync :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetTokenAsync :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetTokenAsync :: Error : " + ex.StackTrace);

                return BadRequest("error | 500 | Internal Server Error " + ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Toffee/GetRefreshTokenAsync")]
        public async Task<IHttpActionResult> GetRefreshTokenAsync(string refreshToken)
        {
            try
            {
                return Ok(await _toffeeService.GetRefreshTokenAsync(refreshToken));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetRefreshTokenAsync :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetRefreshTokenAsync :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetRefreshTokenAsync :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Toffee/PurchaseInsuranceAsync")]
        public async Task<IHttpActionResult> PurchaseInsuranceAsync(InsurancePurchaseModel model)
        {
            try
            {
                return Ok(await _toffeeService.PurchaseInsurance(model));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"PurchaseInsurance :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"PurchaseInsurance :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"PurchaseInsurance :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Toffee/GetState")]
        public async Task<IHttpActionResult> GetState(string token)
        {
            try
            {
                return Ok(await _toffeeService.State(token));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetState :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetState :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetState :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Toffee/GetCity")]
        public async Task<IHttpActionResult> GetCity(CityModel model)
        {
            try
            {
                return Ok(await _toffeeService.City(model));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetCity :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetCity :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetCity :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Toffee/GetProduct")]
        public async Task<IHttpActionResult> GetProduct(string token)
        {
            try
            {
                return Ok(await _toffeeService.Product(token));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetProduct :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetProduct :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"GetProduct :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Toffee/QueryPurchasePolicy")]
        public async Task<IHttpActionResult> QueryPurchasePolicy(QueryPurchaseModel model)
        {
            try
            {
                return Ok(await _toffeeService.QueryPurchasePolicy(model));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"QueryPurchaseOrder :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"QueryPurchaseOrder :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"QueryPurchaseOrder :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Toffee/CancelPolicy")]
        public async Task<IHttpActionResult> CancelPolicy(CancelPolicyModel model)
        {
            try
            {
                return Ok(await _toffeeService.CancelPolicy(model));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"CancelPolicy :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"CancelPolicy :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("Toffee_Error", $"CancelPolicy :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }
    }
}
