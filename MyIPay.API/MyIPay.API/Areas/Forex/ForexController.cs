﻿using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.Forex;
using MyIPay.Services.Areas.Forex.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Forex
{
    [RoutePrefix("api/Forex")]
    public class ForexController : ApiController
    {
        private readonly IForexApiManagerService _forexApiManagerService;// = new forexApiManagerService();
        public ForexController(IForexApiManagerService forexApiManagerService)
        {
            _forexApiManagerService = forexApiManagerService;
        }
        [HttpGet]
        public async Task<ApiResponseDto> GetCities()
        {
            return await _forexApiManagerService.GetAllCityAsync();
        }

        [Route("getCity/{id}")]
        [HttpGet]
        public async Task<City> GetCity(int id)
        {
            return await _forexApiManagerService.GetCityAsync(id);
        }

        [HttpGet]
        public async Task<ApiResponseDto> GetCurrencies()
        {
            return await _forexApiManagerService.GetAllCurrencyAsync();

        }

        [HttpGet]
        public async Task<ApiResponseDto> GetProductTypes()
        {
            return await _forexApiManagerService.GetAllProductTypeAsync();
        }

        [HttpGet]
        public async Task<ApiResponseDto> GetProductConfiguration(int id)
        {
            return await _forexApiManagerService.GetProductConfigurationAsync(id);
        }

        [HttpGet]
        public async Task<ApiResponseDto> GetForexCurrencyProductRates(string cityName, string currencyCode, string productName, string transType)
        {
            return await _forexApiManagerService.GetForexCurrencyRateAsync(cityName, currencyCode, productName, transType);
        }


    }
}
