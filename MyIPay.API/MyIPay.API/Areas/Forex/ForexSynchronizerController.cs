﻿using MyIPay.Services.Areas.Forex.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Forex
{
    [RoutePrefix("api/ForexSynchronizer")]
    public class ForexSynchronizerController : ApiController
    {
        // GET api/<controller>
        private readonly IForexSynchronizerService _synchronizerService;// = new ForexSynchronizerService();
        public ForexSynchronizerController(IForexSynchronizerService synchronizerService)
        {
            _synchronizerService = synchronizerService;
        }

        [HttpGet]
        public async Task<bool> SyncForexAPIResponse()
        {
            var isCitiesAdded = await _synchronizerService.AddCitiesAsync();
            var isCurrenciesAdded = await _synchronizerService.AddCurrenciesAsync();
            var ProductTypes = await _synchronizerService.AddProductTypesAsync();

            if (isCitiesAdded && isCurrenciesAdded && ProductTypes)
                return true;
            else
                return false;
        }

    }
}
