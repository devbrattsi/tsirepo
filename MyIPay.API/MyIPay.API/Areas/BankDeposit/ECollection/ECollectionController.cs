﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.BankDeposit.ECollection;
using MyIPay.Services.Areas.BankDeposit.ECollection.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;


namespace MyIPay.API.Areas.BankDeposit.ECollection
{

    [RoutePrefix("api/eCollection")]
    public class ECollectionController : ApiController
    {
        private readonly IECollectionService _eCollectionService;
        public ECollectionController(IECollectionService eCollectionService)
        {
            _eCollectionService = eCollectionService;
        }

        //[Route("VerifyAccountAsync")]
        //[HttpPost]
        //public async Task<IHttpActionResult> VerifyAccountAsync([FromBody]VerifyAccountModel model)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    Utility.SaveRequestAndResponseLog("ECollection", $"VerifyAccountAsync Request Log :- {JsonConvert.SerializeObject(model)}");


        //    if (model.ClientCode != AppSettings.ISureClientCode)
        //        return BadRequest(Constants.InvalidClientCode);


        //    var result = await _eCollectionService.VerifyAccountAsync(model);

        //    Utility.SaveRequestAndResponseLog("ECollection", $"VerifyAccountAsync Response Log :- {JsonConvert.SerializeObject(result)}");

        //    return Ok(result);
        //}

        [HttpPost]
        [Route("TransactionAsync")]
        public async Task<IHttpActionResult> BankDepositTransactionAsync(BankDepositTransactionModel model)//string data)
        {
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                if (Request.Headers.TryGetValues(Constants.Username, out IEnumerable<string> username)
                    && Request.Headers.TryGetValues(Constants.Password, out IEnumerable<string> password)
                    && Request.Headers.TryGetValues(Constants.Agency, out IEnumerable<string> agency)
                    && Request.Headers.TryGetValues(Constants.Subscriber, out IEnumerable<string> subscriber))
                {
                    dictionary.Add(Constants.Username, username.FirstOrDefault());
                    dictionary.Add(Constants.Password, password.FirstOrDefault());
                    dictionary.Add(Constants.Agency, agency.FirstOrDefault());
                    dictionary.Add(Constants.Subscriber, subscriber.FirstOrDefault());
                }
                else
                {
                    return BadRequest("Header missing.");
                }

                Utility.SaveRequestAndResponseLog("ECollection", $"Transaction Request Data Log :- {JsonConvert.SerializeObject(model)}");

                //XmlSerializer serializer = new XmlSerializer(typeof(BankDepositTransactionModel));

                //BankDepositTransactionModel model;
                //string xmlString = data.InnerXml;
                //using (TextReader reader = new StringReader(xmlString))
                //{
                //    model = (BankDepositTransactionModel)serializer.Deserialize(reader);
                //}


                var result = await _eCollectionService.BankDepositTransactionAsync(model, dictionary);

                Utility.SaveRequestAndResponseLog("ECollection", $"BankDepositTransaction Response Log :- {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("ECollection_Error", $"BankDepositTransaction Error :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("ECollection_Error", $"BankDepositTransaction Error :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("ECollection_Error", $"BankDepositTransaction Error :- {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }
    }
}