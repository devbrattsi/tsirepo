﻿//using MyIPay.Dtos.Areas.BankDeposit.ECollection;
//using MyIPay.Helpers;
//using MyIPay.Models.Areas.BankDeposit.ECollection;
//using MyIPay.Services.Areas.BankDeposit.ECollection;
//using MyIPay.Services.Areas.BankDeposit.ECollection.Interfaces;
//using Newtonsoft.Json;
//using System.Web.Script.Services;
//using System.Web.Services;

//namespace MyIPay.API.Areas.BankDeposit.ECollection
//{
//    /// <summary>
//    /// Summary description for ECollection
//    /// </summary>
//    [WebService(Namespace = "http://tempuri.org/")]
//    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//    //[WebServiceBinding(ConformsTo = WsiProfiles.None)]
//    [System.ComponentModel.ToolboxItem(false)]
//    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
//    [System.Web.Script.Services.ScriptService]
//    public class ECollection : System.Web.Services.WebService
//    {

//        private readonly IECollectionService _eCollectionService;
//        public ECollection() : this(new ECollectionService()) { }

//        public ECollection(IECollectionService eCollectionService)
//        {
//            _eCollectionService = eCollectionService;
//        }


//        [ScriptMethod(UseHttpGet = false)]
//        [WebMethod(MessageName = "VerifyAccount")]
//        public VerifyAccountDto VerifyAccount(VerifyAccountModel model)
//        {
//            Utility.SaveRequestAndResponseLog("ECollection", $"VerifyAccountAsync Request Log :- {JsonConvert.SerializeObject(model)}");

//            var result = _eCollectionService.VerifyAccount(model);

//            Utility.SaveRequestAndResponseLog("ECollection", $"VerifyAccountAsync Response Log :- {JsonConvert.SerializeObject(result)}");

//            return result;
//        }


//    }
//}
