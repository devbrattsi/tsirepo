﻿using MyIPay.Dtos.Areas.Tpddl;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Tpddl;
using MyIPay.Services.Areas.Tpddl.Interfaces;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Tpddl
{
    public class TpddlController : ApiController
    {

        private ITpddlService _tpddlService;
        public TpddlController(ITpddlService tpddlService)
        {
            _tpddlService = tpddlService;
        }

        [Route("api/Tpddl/GetBillDetailAsync")]
        public async Task<IHttpActionResult> GetBillDetailAsync(string caOrKaNumber)
        {
            try
            {

                if (string.IsNullOrEmpty(caOrKaNumber) || !long.TryParse(caOrKaNumber, out long consumerNumber))
                    return BadRequest(Constants.ParameterMissing);

                Utility.SaveRequestAndResponseLog("Tpddl", $"GetBillDetailAsync Request Log  --> caOrKaNumber : {caOrKaNumber}");

                var response = await _tpddlService.GetBillDetailAsync(Convert.ToString(consumerNumber));


                Utility.SaveRequestAndResponseLog("Tpddl", $"GetBillDetailAsync Response Log  --> {JsonConvert.SerializeObject(response)}");

                return Ok(response);

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Tpddl_Error", $"GetBillDetailAsync Error { ex.Message}");
                Utility.WriteLog("Tpddl_Error", $"GetBillDetailAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Tpddl_Error", $"GetBillDetailAsync Error {ex.StackTrace}");

                return BadRequest("error | 500 | Internal Server Error");
            }
        }

        [HttpPost]
        [Route("api/Tpddl/TpddlEReceiptAsync")]
        public async Task<IHttpActionResult> TpddlEReceiptAsync([FromBody]TpddlEReceiptModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                Utility.SaveRequestAndResponseLog("Tpddl", $"TpddlEReceiptAsync Request Log --> model :   {JsonConvert.SerializeObject(model)}");

                var response = await _tpddlService.TpddlEReceiptAsync(model);

                Utility.SaveRequestAndResponseLog("Tpddl", $"TpddlEReceiptAsync Response Log --> Dto :   {JsonConvert.SerializeObject(response)}");


                return Ok(response);

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Tpddl_Error", $"TpddlEReceiptAsync Error Log : {ex.Message}");
                Utility.WriteLog("Tpddl_Error", $"TpddlEReceiptAsync Error Log : {ex.InnerException}");
                Utility.WriteLog("Tpddl_Error", $"TpddlEReceiptAsync Error Log : {ex.StackTrace}");

                return BadRequest("error | 500 | Internal Server Error");
            }

        }

        [HttpGet]
        [Route("api/Tpddl/GetBillDetailsAsync")]
        public async Task<IHttpActionResult> GetBillDetailsAsync(string caOrKaNumber)
        {
            try
            {

                if (string.IsNullOrEmpty(caOrKaNumber) || !long.TryParse(caOrKaNumber, out long consumerNumber))
                    return BadRequest(Constants.ParameterMissing);

                Utility.SaveRequestAndResponseLog("Tpddl", $"GetBillDetailAsync Request Log  --> caOrKaNumber : {caOrKaNumber}");

                var response = await _tpddlService.GetBillDetailsAsync(Convert.ToString(consumerNumber));
                Utility.SaveRequestAndResponseLog("Tpddl", $"GetBillDetailAsync Response Log  --> {JsonConvert.SerializeObject(response)}");

                return Ok(response);

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Tpddl_Error", $"GetBillDetailAsync Error { ex.Message}");
                Utility.WriteLog("Tpddl_Error", $"GetBillDetailAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Tpddl_Error", $"GetBillDetailAsync Error {ex.StackTrace}");

                return BadRequest("error | 500 | Internal Server Error");
            }
        }

        [HttpPost]
        [Route("api/Tpddl/PayBillAsync")]
        public async Task<IHttpActionResult> PayBillAsync(TpddlPayModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok( await _tpddlService.PayBillAsync(model));
        }

        [HttpPost]
        [Route("api/Tpddl/TransactionStatus")]
        public async Task<IHttpActionResult> TransactionStatus(TpddlTransactionStatusModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(await _tpddlService.TransactionStatus(model));
        }
    }
}
