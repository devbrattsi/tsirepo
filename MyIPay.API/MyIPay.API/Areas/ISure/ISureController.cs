﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.ISure;
using MyIPay.Services.Areas.ISure.Interfaces;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.ISure
{
    public class ISureController : ApiController
    {
        private readonly IISureService _iSureService;
        public ISureController(IISureService iSureService)
        {
            _iSureService = iSureService;
        }
       
        [Route("api/ISure/VerifyUserAsync")]
        [HttpPost]
        public async Task<IHttpActionResult> VerifyUserAsync([FromBody]VerifyUserModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Helpers.Utility.SaveRequestAndResponseLog("ISure", $"VerifyUserAsync Request Log :- {JsonConvert.SerializeObject(model)}");


            if (model.Client_Code != AppSettings.ISureClientCode)
                return BadRequest(Constants.InvalidClientCode);

            if (model.UserId != AppSettings.ISureUserId || model.UserPwd != AppSettings.ISurePassword)
                return BadRequest(Constants.InvalidUserIdOrPassword);


            var result = await _iSureService.VerifyUserAsync(model);

            Helpers.Utility.SaveRequestAndResponseLog("ISure", $"VerifyUserAsync Response Log :- {JsonConvert.SerializeObject(result)}");

            return Ok(result);
        }

        [Route("api/ISure/InitiateTransactionAsync")]
        [HttpPost]
        public async Task<IHttpActionResult> InitiateTransactionAsync([FromBody]InitiateTransactionModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Helpers.Utility.SaveRequestAndResponseLog("ISure", $"InitiateTransactionAsync Request Log :- {JsonConvert.SerializeObject(model)}");


            if (model.Client_Code != AppSettings.ISureClientCode)
                return BadRequest(Constants.InvalidClientCode);

            if (model.UserId != AppSettings.ISureUserId || model.UserPwd != AppSettings.ISurePassword)
                return BadRequest(Constants.InvalidUserIdOrPassword);

            var result = await _iSureService.InitiateTransactionAsync(model);

            Helpers.Utility.SaveRequestAndResponseLog("ISure", $"InitiateTransactionAsync Response Log :- {JsonConvert.SerializeObject(result)}");


            return Ok(result);
        }
    }
}
