﻿using log4net;
using MyIPay.Helpers;
using MyIPay.Services.Areas.BillDesk.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.BillDesk
{
    [RoutePrefix("api/BillDesk")]
    public class BillDeskController : ApiController
    {

        private readonly IBillDeskService _billDeskService;
        public BillDeskController(IBillDeskService billDeskService)
        {
            _billDeskService = billDeskService;
        }

        #region Billers

        [HttpGet]
        [Route("GetBillersAsync")]
        public async Task<IHttpActionResult> GetBillersAsync()
        {
            try
            {
                //Utility.SaveRequestAndResponseLog("AirtelBank", $"CheckDistributorBalanceAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                //if (model == null || !ModelState.IsValid)
                //{
                //    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                //}

                var responseMsg = await _billDeskService.GetBillersAsync();

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("BillDesk", $"GetBillersAsync Response Log  --> {responseString}");

                //var dto = JsonConvert.DeserializeObject<object>(responseString);

                return ResponseMessage(responseMsg);//this.Request.CreateResponse(HttpStatusCode.OK, responseString);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("BillDesk_Error", $"GetBillersAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("BillDesk_Error", $"GetBillersAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("BillDesk_Error", $"GetBillersAsync Error Log  --> {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }

        #endregion


        #region Payments

        #endregion
    }
}
