﻿using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.QwikCilver;
using MyIPay.Services.Areas.QwikCilver.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;
using MyIPay.Helpers;
using MyIPay.Dtos.Areas.QwikCilver.GiftCard;
using MyIPay.Models.Areas.QwikCilver.GiftCard;
using System;

namespace MyIPay.API.Areas.QwikCilver
{
    [RoutePrefix("api")]
    public class GiftCardController : ApiController
    {
        private readonly IGiftCardService _giftCardService;
        public GiftCardController(IGiftCardService giftCardService)
        {
            _giftCardService = giftCardService;
        }

        #region Woohoo APIs

        //[HttpGet]
        //[Route("GiftCard/QwikCilver/GetOAuthTokenAsync")]
        //public async Task<IHttpActionResult> GetOAuthTokenAsync()
        //{

        //    OAuthTokenDto result = await _giftCardService.GetOAuthTokenAsync();

        //    return Ok(result);
        //}

        //[HttpGet]
        //[Route("GiftCard/QwikCilver/GetOauthVerifierAsync")]
        //public async Task<IHttpActionResult> GetOauthVerifierAsync(string oauthToken)
        //{
        //    if (string.IsNullOrEmpty(oauthToken))
        //        return BadRequest(Constants.ParameterMissing);

        //    VerifierDto result = await _giftCardService.GetOauthVerifierAsync(oauthToken);

        //    return Ok(result);
        //}

        //[HttpGet]
        //[Route("GiftCard/QwikCilver/GetOauthAccessTokenAsync")]
        //public async Task<IHttpActionResult> GetOauthAccessTokenAsync(string oauthToken, string oauthTokenSecret, string oauthVerifier)
        //{
        //    AccessTokenDto result = await _giftCardService.GetOauthAccessTokenAsync(oauthToken, oauthTokenSecret, oauthVerifier);

        //    return Ok(result);
        //}



        [HttpGet]
        [Route("GiftCard/QwikCilver/GetSttingsAsync")]
        public async Task<IHttpActionResult> GetSttingsAsync()
        {
            SettingDto result = await _giftCardService.GetSttingsAsync();

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/QwikCilver/GetCategoryAsync")]
        public async Task<IHttpActionResult> GetCategoryAsync()
        {
            CategoryDto result = await _giftCardService.GetCategoryAsync();

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/QwikCilver/GetCategoryDetailByIdAsync")]
        public async Task<IHttpActionResult> GetCategoryDetailByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest(Constants.ParameterMissing);

            CategoryDetailDto result = await _giftCardService.GetCategoryDetailByIdAsync(id);

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/QwikCilver/GetProductDetailByIdAsync")]
        public async Task<IHttpActionResult> GetProductDetailByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest(Constants.ParameterMissing);

            ProductDetailDto result = await _giftCardService.GetProductDetailByIdAsync(id);

            return Ok(result);
        }

        [HttpPost]
        [Route("GiftCard/QwikCilver/SpendAsync")]
        public async Task<IHttpActionResult> SpendAsync(SpendModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                SpendDto result = await _giftCardService.SpendAsync(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("GiftCard/QwikCilver/GetOrderStatusAsync")]
        public async Task<IHttpActionResult> GetOrderStatusAsync(string refNo)
        {
            if (string.IsNullOrEmpty(refNo))
                return BadRequest(Constants.ParameterMissing);

            StatusDto result = await _giftCardService.GetOrderStatusAsync(refNo);

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/QwikCilver/ResendAsync")]
        public async Task<IHttpActionResult> ResendAsync(string refNo)
        {
            if (string.IsNullOrEmpty(refNo))
                return BadRequest(Constants.ParameterMissing);

            ResendDto result = await _giftCardService.ResendAsync(refNo);

            return Ok(result);
        }

        #endregion


        #region Insert APIs

        [HttpGet]
        [Route("GiftCard/TSI/AddOauthAccessTokenAsync")]
        public async Task<IHttpActionResult> AddOauthAccessTokenAsync()
        {
            ApiResponseDto result = await _giftCardService.AddOauthAccessTokenAsync();

            return Ok(result);
        }


        /// <summary>
        /// Sync Oauth Access Token from MongoDb Server
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GiftCard/TSI/UpdateOauthAccessTokenAsync")]
        public async Task<IHttpActionResult> UpdateOauthAccessTokenAsync()
        {
            ApiResponseDto result = await _giftCardService.UpdateOauthAccessTokenAsync();

            return Ok(result);

        }

        [HttpGet]
        [Route("GiftCard/TSI/AddCategoryAsync")]
        public async Task<IHttpActionResult> AddCategoryAsync()
        {
            ApiResponseDto result = await _giftCardService.AddCategoryAsync();

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/TSI/AddCategoriesDetailAsync")]
        public async Task<IHttpActionResult> AddCategoriesDetailAsync()
        {
            ApiResponseDto result = await _giftCardService.AddCategoriesDetailAsync();

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/TSI/AddProductsDetailAsync")]
        public async Task<IHttpActionResult> AddProductsDetailAsync()
        {
            ApiResponseDto result = await _giftCardService.AddProductsDetailAsync();

            return Ok(result);
        }

        #endregion


        #region Customized APIs
        [HttpGet]
        [Route("GiftCard/TSI/GetProductAsync")]
        public async Task<IHttpActionResult> GetProductAsync()
        {
            var result = await _giftCardService.GetProductAsync();

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/TSI/GetProductsAsync")]
        public async Task<IHttpActionResult> GetProductsAsync()
        {
            var result = await _giftCardService.GetProductsAsync();

            if (result.Count == 0)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [Route("GiftCard/TSI/GetProductByIdAsync")]
        public async Task<IHttpActionResult> GetProductByIdAsync(string id, string agentId)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(agentId))
                return BadRequest(Constants.ParameterMissing);

            var result = await _giftCardService.GetProductByIdAsync(id, agentId);

            if (result == null)
                return NotFound();

            return Ok(result);
        }
        #endregion
    }
}
