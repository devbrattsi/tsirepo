﻿using MyIPay.Models.Areas.PrabhuBank;
using MyIPay.Services.Areas.PrabhuBank.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;
using MyIPay.Helpers;
using Newtonsoft.Json;
using System;

namespace MyIPay.API.Areas.PrabhuBank
{

    [RoutePrefix("api/Inmt")]
    public class InmtController : ApiController
    {
        private readonly IInmtService _inmtService;
        public InmtController(IInmtService inmtService)
        {
            _inmtService = inmtService;
        }


        [HttpGet]
        [Route("GetServiceChargeAsync")]
        public async Task<IHttpActionResult> GetServiceChargeAsync(string country,
              string paymentMode,
              string transferAmount = null,
              string payoutAmount = null,
              string bankBranchId = null,
              string isNewAccount = null
            )
        {

            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetServiceChargeAsync Request Log - country:{country},paymentMode:{paymentMode},transferAmount:{transferAmount},payoutAmount:{payoutAmount},bankBranchId:{bankBranchId},isNewAccount:{isNewAccount}");

                if (string.IsNullOrEmpty(country) || string.IsNullOrEmpty(paymentMode))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.GetServiceChargeAsync(country, paymentMode, transferAmount, payoutAmount, bankBranchId, isNewAccount);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetServiceChargeAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetServiceChargeAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetServiceChargeAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetServiceChargeAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }



        [HttpGet]
        [Route("GetServiceChargeByCollectionAsync")]
        public async Task<IHttpActionResult> GetServiceChargeByCollectionAsync(string country,
              string paymentMode,
              string collectionAmount = null,
              string payoutAmount = null,
              string bankBranchId = null,
              string isNewAccount = null
            )
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetServiceChargeByCollectionAsync Request Log - country:{country},paymentMode:{paymentMode},collectionAmount:{collectionAmount},payoutAmount:{payoutAmount},bankBranchId:{bankBranchId},isNewAccount:{isNewAccount}");

                if (string.IsNullOrEmpty(country) || string.IsNullOrEmpty(paymentMode))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.GetServiceChargeByCollectionAsync(country, paymentMode, collectionAmount, payoutAmount, bankBranchId, isNewAccount);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetServiceChargeByCollectionAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetServiceChargeByCollectionAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetServiceChargeByCollectionAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetServiceChargeByCollectionAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("GetCashPayLocationListAsync")]
        public async Task<IHttpActionResult> GetCashPayLocationListAsync(string country,
                                                                     string state = null,
                                                                     string district = null,
                                                                     string city = null,
                                                                     string locationName = null)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetCashPayLocationListAsync Request Log - country:{country},state:{state},district:{district},city:{city},locationName:{locationName}");

                if (string.IsNullOrEmpty(country))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.GetCashPayLocationListAsync(country, state, district, city, locationName);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetCashPayLocationListAsync Response Log - {JsonConvert.SerializeObject(result)}");


                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCashPayLocationListAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCashPayLocationListAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCashPayLocationListAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("GetAcPayBankBranchListAsync")]
        public async Task<IHttpActionResult> GetAcPayBankBranchListAsync(string country,
                                                                    string state = null,
                                                                    string district = null,
                                                                    string city = null,
                                                                    string bankName = null,
                                                                    string branchName = null)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetAcPayBankBranchListAsync Request Log - country:{country},state:{state},district:{district},city:{city},bankName:{bankName},branchName:{branchName}");

                if (string.IsNullOrEmpty(country))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.GetAcPayBankBranchListAsync(country, state, district, city, bankName, branchName);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetAcPayBankBranchListAsync Response Log - {JsonConvert.SerializeObject(result)}");


                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetAcPayBankBranchListAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetAcPayBankBranchListAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetAcPayBankBranchListAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        [Route("SendTransactionAsync")]
        public async Task<IHttpActionResult> SendTransactionAsync([FromBody]SendTransactionModel model)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SendTransactionAsync Request Log - {JsonConvert.SerializeObject(model)}");

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _inmtService.SendTransactionAsync(model);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SendTransactionAsync Response Log - {JsonConvert.SerializeObject(result)}");


                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SendTransactionAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SendTransactionAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SendTransactionAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        [Route("CancelTransactionAsync")]
        public async Task<IHttpActionResult> CancelTransactionAsync([FromBody]CancelTransactionModel model)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"CancelTransactionAsync Request Log - {JsonConvert.SerializeObject(model)}");

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _inmtService.CancelTransactionAsync(model);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"CancelTransactionAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"CancelTransactionAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"CancelTransactionAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"CancelTransactionAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("VerifyTransactionAsync")]
        public async Task<IHttpActionResult> VerifyTransactionAsync(string pinNo)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"VerifyTransactionAsync Request Log - pinNo:{pinNo}");

                if (string.IsNullOrEmpty(pinNo))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.VerifyTransactionAsync(pinNo);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"VerifyTransactionAsync Response Log - {JsonConvert.SerializeObject(result)}");


                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"VerifyTransactionAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"VerifyTransactionAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"VerifyTransactionAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("UnverifiedTransactionsAsync")]
        public async Task<IHttpActionResult> UnverifiedTransactionsAsync()
        {
            try
            {
                var result = await _inmtService.UnverifiedTransactionsAsync();

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"UnverifiedTransactionsAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"UnverifiedTransactionsAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"UnverifiedTransactionsAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"UnverifiedTransactionsAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("ComplianceTransactionsAsync")]
        public async Task<IHttpActionResult> ComplianceTransactionsAsync()
        {
            try
            {
                var result = await _inmtService.ComplianceTransactionsAsync();

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"ComplianceTransactionsAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"ComplianceTransactionsAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"ComplianceTransactionsAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"ComplianceTransactionsAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("SearchAllTransactionAsync")]
        public async Task<IHttpActionResult> SearchAllTransactionAsync()
        {
            try
            {
                var result = await _inmtService.SearchAllTransactionAsync();

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchAllTransactionAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchAllTransactionAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchAllTransactionAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchAllTransactionAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("SearchTransactionByDateRangeAsync")]
        public async Task<IHttpActionResult> SearchTransactionByDateRangeAsync(string fromDate, string toDate)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchTransactionByDateRangeAsync Request Log - fromDate:{fromDate},toDate:{toDate}");

                var result = await _inmtService.SearchTransactionByDateRangeAsync(fromDate,toDate);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchTransactionByDateRangeAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByDateRangeAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByDateRangeAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByDateRangeAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

      

        [HttpGet]
        [Route("SearchTransactionByPartnerPinNoAsync")]
        public async Task<IHttpActionResult> SearchTransactionByPartnerPinNoAsync(string partnerPinNo)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchTransactionByPartnerPinNoAsync Request Log - partnerPinNo:{partnerPinNo}");

                var result = await _inmtService.SearchTransactionByPartnerPinNoAsync(partnerPinNo);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchTransactionByPartnerPinNoAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByPartnerPinNoAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByPartnerPinNoAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByPartnerPinNoAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("SearchTransactionByPinNoAsync")]
        public async Task<IHttpActionResult> SearchTransactionByPinNoAsync(string pinNo)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchTransactionByPinNoAsync Request Log - pinNo:{pinNo}");

                var result = await _inmtService.SearchTransactionByPinNoAsync(pinNo);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"SearchTransactionByPinNoAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByPinNoAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByPinNoAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"SearchTransactionByPinNoAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("ValidateBankAccountAsync")]
        public async Task<IHttpActionResult> ValidateBankAccountAsync(string bankCode, string accountNumber)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"ValidateBankAccountAsync Request Log - bankCode:{bankCode},accountNumber:{accountNumber}");

                if (string.IsNullOrEmpty(bankCode) || string.IsNullOrEmpty(accountNumber))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.ValidateBankAccountAsync(bankCode, accountNumber);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"ValidateBankAccountAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"ValidateBankAccountAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"ValidateBankAccountAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"ValidateBankAccountAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("UploadCustomerDocumentAsync")]
        public async Task<IHttpActionResult> UploadCustomerDocumentAsync(UploadCustomerDocumentModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"UploadCustomerDocumentAsync Request Log - {JsonConvert.SerializeObject(model)}");

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _inmtService.UploadCustomerDocumentAsync(model);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"UploadCustomerDocumentAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"UploadCustomerDocumentAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"UploadCustomerDocumentAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"UploadCustomerDocumentAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetCustomerByMobileAsync")]
        public async Task<IHttpActionResult> GetCustomerByMobileAsync(string mobile)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetCustomerByMobileAsync Request Log - mobile:{mobile}");

                if (string.IsNullOrEmpty(mobile))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.GetCustomerByMobileAsync(mobile);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetCustomerByMobileAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCustomerByMobileAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCustomerByMobileAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCustomerByMobileAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetCustomerByIdNumberAsync")]
        public async Task<IHttpActionResult> GetCustomerByIdNumberAsync(string id)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetCustomerByIdNumberAsync Request Log - id:{id}");

                if (string.IsNullOrEmpty(id))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _inmtService.GetCustomerByIdNumberAsync(id);

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt", $"GetCustomerByIdNumberAsync Response Log - {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCustomerByIdNumberAsync Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCustomerByIdNumberAsync Error Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("PrabhuBankInmt_Error", $"GetCustomerByIdNumberAsync Error Log  --> {ex.StackTrace}");
                return BadRequest(ex.Message);
            }
        }
    }

}
