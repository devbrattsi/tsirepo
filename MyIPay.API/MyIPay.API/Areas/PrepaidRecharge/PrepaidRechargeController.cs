﻿using MyIPay.Models.Areas.PrepaidRecharge;
using MyIPay.Services.Areas.PrepaidRecharge.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.PrepaidRecharge
{
    [RoutePrefix("api/PrepaidRecharge")]
    public class PrepaidRechargeController : ApiController
    {
        private readonly IPrepaidRechargeService _prepaidRechargeService;


        public PrepaidRechargeController(IPrepaidRechargeService prepaidRechargeService)
        {
            _prepaidRechargeService = prepaidRechargeService;
        }

        #region Go Processing
        [HttpGet]
        public async Task<IHttpActionResult> GetServiceList() => Ok(await _prepaidRechargeService.ServiceList());

        [HttpGet]
        public async Task<IHttpActionResult> GetCircleInfoList() => Ok(await _prepaidRechargeService.CircleInfoList());

        [HttpGet]
        public async Task<IHttpActionResult> GetOperatotList(int id) => Ok(await _prepaidRechargeService.OperatorList(id));

        [HttpGet]
        public async Task<IHttpActionResult> GetAvailableBalance() => Ok(await _prepaidRechargeService.BalanceCheck());

        [HttpGet]
        public async Task<IHttpActionResult> GetMsisdnInfo(int serviceFamily, long number) => Ok(await _prepaidRechargeService.FindMsisdnInfo(serviceFamily, number));

        [HttpPost]
        public async Task<IHttpActionResult> GetRechargePlan(RechargePlanModel model) => Ok(await _prepaidRechargeService.RechargePlan(model));

        [HttpPost]
        public async Task<IHttpActionResult> Recharge(ServiceModel model) => Ok(await _prepaidRechargeService.ServiceRequest(model));


        [HttpPost]
        public async Task<IHttpActionResult> GetTransactionStatus(TransactionStatusModel model) => Ok(await _prepaidRechargeService.TransactionStatus(model));

        [HttpPost]
        public async Task<IHttpActionResult> GetBulkTransactionStatuses(BulkTransactionStatusesModel model) => Ok(await _prepaidRechargeService.BulkTransactionStatuses(model));
        #endregion

        #region Walnut
        [HttpGet]
        public async Task<IHttpActionResult> GetWalnutBalance() => Ok(await _prepaidRechargeService.WalnutBalance());

        [HttpGet]
        public async Task<IHttpActionResult> GetMsisdnInfoViaWalnut(string clientReferenceId, string number) => Ok(await _prepaidRechargeService.WalnutFindMsisdnInfo(clientReferenceId, number));

        [HttpPost]
        public async Task<IHttpActionResult> GetWalnutRechargePlan(WalnutRechargePlanModel model) => Ok(await _prepaidRechargeService.WalnutRechargePlan(model));

        [HttpPost]
        public async Task<IHttpActionResult> WalnutRecharge(WalnutRechargeModel model) => Ok(await _prepaidRechargeService.WalnutRecharge(model));

        [HttpGet]
        public async Task<IHttpActionResult> GetWalnutTransactionStatus(string clientReferenceId) => Ok(await _prepaidRechargeService.WalnutTransactionStatus(clientReferenceId));
        #endregion
    }
}
