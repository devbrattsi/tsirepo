﻿using MyIPay.Dtos.Areas.Yatra;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Yatra;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.Yatra.Interfaces;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Yatra
{
    public class YatraController : ApiController
    {
        private readonly IYatraService _yatraService;
        private readonly ICommonService _commonService;

        public YatraController(IYatraService yatraService,
                               ICommonService commonService)
        {
            _yatraService = yatraService;
            _commonService = commonService;
        }

        [HttpGet]
        [Route("api/TSI/GenerateToken")]
        [Route("api/TSI/GenerateTokenAsync")]
        public async Task<IHttpActionResult> GenerateTokenAsync(string agentId)
        {
            try
            {
                if (string.IsNullOrEmpty(agentId))
                    return BadRequest(Constants.ParameterMissing);

                //bool result = await _yatraService.IsUserExistAsync(agentId);

                //if (!result)
                //    return NotFound();

                //string timestamp = SecurityHelper.GenerateSecretKeyTimestamp();

                //string token = SecurityHelper.Encrypt($"{timestamp}_{agentId}");

                var result = await _yatraService.GenerateTokenAsync(agentId);

                Utility.SaveRequestAndResponseLog("Yatra", $"GenerateTokenAsync Response Log  --> {result}");

                if (string.IsNullOrEmpty(result))
                    return NotFound();


                return Ok(new { Token = result });
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Yatra_Error", $"GenerateTokenAsync Error { ex.Message}");
                Utility.WriteLog("Yatra_Error", $"GenerateTokenAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Yatra_Error", $"GenerateTokenAsync Error {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [Route("api/Yatra/VerifyToken")]
        [Route("api/Yatra/VerifyTokenAsync")]
        public async Task<IHttpActionResult> VerifyTokenAsync(VerifyTokenModel model)//string authenticationType, string key)
        {
            try
            {
                //var headers = Request.Headers;
                //if (!headers.Contains("key") || !headers.Contains("key"))
                //    return BadRequest(Constants.ParameterMissing);

                ////if (headers.Contains("key"))
                ////{
                ////    string token = headers.GetValues("key").FirstOrDefault();
                ////}

                Utility.SaveRequestAndResponseLog("Yatra", $"VerifyToken Request Log  --> model : {JsonConvert.SerializeObject(model)}");


                if (!ModelState.IsValid)
                    return BadRequest(ModelState);


                var response = await _yatraService.VerifyTokenAsync(model);


                Utility.SaveRequestAndResponseLog("Yatra", $"VerifyToken Response Log  --> {JsonConvert.SerializeObject(response)}");

                return Ok(response);

            }
            catch (Exception ex)
            {
                Utility.WriteLog("Yatra_Error", $"VerifyToken Error { ex.Message}");
                Utility.WriteLog("Yatra_Error", $"VerifyToken Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Yatra_Error", $"VerifyToken Error {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        //[HttpGet]
        //[Route("api/Yatra/GetMerchantBalanceAsync")]
        //public async Task<IHttpActionResult> GetMerchantBalanceAsync(string agentId, string orderId, string totalAmount)
        //{
        //    Utility.SaveRequestAndResponseLog("Yatra", $"GetMerchantBalanceAsync Request Log  --> agentId : {agentId} , orderId = {orderId}, totalAmount : {totalAmount}");

        //    if (string.IsNullOrEmpty(agentId)
        //        || string.IsNullOrEmpty(orderId)
        //        || string.IsNullOrEmpty(totalAmount)
        //        || !decimal.TryParse(totalAmount, out decimal amount))

        //        return BadRequest(Constants.ParameterMissing);

        //    var result = await _yatraService.GetMerchantBalanceAsync(agentId, orderId, amount);
        //    Utility.SaveRequestAndResponseLog("Yatra", $"GetMerchantBalanceAsync Response Log  --> {JsonConvert.SerializeObject(result)}");

        //    return Ok(result);
        //}

        [HttpGet]
        [Route("api/Yatra/GetMerchantAvailableBalanceAsync")]
        [Route("api/Yatra/CheckBalanceAvailabilityAsync")]
        public async Task<IHttpActionResult> CheckBalanceAvailabilityAsync(string amount, string agentId, string orderId)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("Yatra", $"CheckBalanceAvailabilityAsync Request Log  --> agentId : {agentId} , orderId = {orderId}, totalAmount : {amount}");

                if (string.IsNullOrEmpty(agentId)
                    || string.IsNullOrEmpty(orderId)
                    || string.IsNullOrEmpty(amount)
                    || !decimal.TryParse(amount, out decimal totalAmount))

                    return BadRequest(Constants.ParameterMissing);

                var result = await _yatraService.CheckBalanceAvailabilityAsync(agentId, orderId, totalAmount);
                Utility.SaveRequestAndResponseLog("Yatra", $"CheckBalanceAvailabilityAsync Response Log  --> {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Yatra_Error", $"CheckBalanceAvailabilityAsync Error { ex.Message}");
                Utility.WriteLog("Yatra_Error", $"CheckBalanceAvailabilityAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Yatra_Error", $"CheckBalanceAvailabilityAsync Error {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return merchant balance available or not</returns>
        [HttpPost]
        [Route("api/Yatra/DoPaymentAsync")]
        public async Task<IHttpActionResult> DoPaymentAsync(DoPaymentAsyncModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("Yatra", $"DoPaymentAsync Request Log  --> model : {JsonConvert.SerializeObject(model)}");

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _yatraService.DoPaymentAsync(model);
                Utility.SaveRequestAndResponseLog("Yatra", $"DoPaymentAsync Response Log  --> {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Yatra_Error", $"DoPaymentAsync Error { ex.Message}");
                Utility.WriteLog("Yatra_Error", $"DoPaymentAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Yatra_Error", $"DoPaymentAsync Error {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        ///Booking Confirmation & Payment Crosscheck API
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="amount"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Yatra/CrosscheckPaymentStatusAsync")]
        public async Task<IHttpActionResult> CheckPaymentStatusAsync(string orderId, decimal amount, string signature)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("Yatra", $"CheckPaymentStatusAsync Request Log  --> orderId : {orderId}, amount : {amount}, singnature : {signature}");

                if (string.IsNullOrEmpty(orderId) || amount <= 0 || string.IsNullOrEmpty(signature))
                    return BadRequest(Constants.ParameterMissing);



                var result = await _yatraService.CheckPaymentStatusAsync(orderId, amount, signature);

                Utility.SaveRequestAndResponseLog("Yatra", $"CheckPaymentStatusAsync Response Log  --> {JsonConvert.SerializeObject(result)}");

                return Ok(result);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Yatra_Error", $"CheckPaymentStatusAsync Error { ex.Message}");
                Utility.WriteLog("Yatra_Error", $"CheckPaymentStatusAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Yatra_Error", $"CheckPaymentStatusAsync Error {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Booking Cancellation & Refund API
        /// </summary>
        /// <param name="model"></param>
        ///Signature - ordered|amount|merchantId|salt this logic is encrypted by MD5
        /// <returns></returns>
        [HttpPost]
        //[AcceptVerbs("Get", "Post")]
        [Route("api/Yatra/BookingCancellationAsync")]
        public async Task<IHttpActionResult> BookingCancellationAsync(BookingCancellationAsyncModel model)//(string orderId, decimal amount, string refundOrderId, string singnature, string isPartialCal)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("Yatra", $"BookingCancellationAsync Request Log  --> model : {JsonConvert.SerializeObject(model)}");

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                BookingCancellationAsyncDto dto = new BookingCancellationAsyncDto();
                if (model.RefundCategory == Constants.Refund)
                {
                    dto = await _yatraService.BookingCancellationAsync(model);
                }
                else
                {
                    dto = await _yatraService.GiveCommissionAsync(model);
                }


                Utility.SaveRequestAndResponseLog("Yatra", $"BookingCancellationAsync Response Log  --> {JsonConvert.SerializeObject(dto)}");

                return Ok(dto);
            }
            catch (Exception ex)
            {
                Utility.WriteLog("Yatra_Error", $"BookingCancellationAsync Error { ex.Message}");
                Utility.WriteLog("Yatra_Error", $"BookingCancellationAsync Error { ex.InnerException.ToString()}");
                Utility.WriteLog("Yatra_Error", $"BookingCancellationAsync Error {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }



        //[HttpPost]
        //[Route("api/Yatra/DoYatraPaymentAsync")]
        //public async Task<IHttpActionResult> DoYatraPaymentAsync(DoYatraPaymentAsyncModel model)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    //if (model.Signature != "")
        //    //    return BadRequest(Constants.SignatureMismatch);


        //    var result = await _yatraService.DoYatraPaymentAsync(model);

        //    return Ok(result);
        //}


        ///// <summary>
        ///// Booking Cancellation & Refund API 
        ///// </summary>
        ///// <param name="orderId">Yatra reference Number </param>
        ///// <param name="amount"> Refund  Amount in rupees with  two decimal place </param>
        ///// <param name="refundOrderId">Yatra reference Number  for refund. </param>
        ///// <param name="signature"> ordered|amount|merchantId|salt  this logic is encrypted by MD5</param>
        ///// <param name="isPartialCal"> 0 for Full Refund and 1 for partial Cancellation </param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("api/Yatra/YatraBookingCancellationAsync")]
        //public async Task<IHttpActionResult> YatraBookingCancellationAsync(string orderId, decimal amount, string refundOrderId, string signature, int isPartialCal)
        //{
        //    if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(refundOrderId) || string.IsNullOrEmpty(signature))
        //        return BadRequest(Constants.ParameterMissing);


        //    var result = await _yatraService.YatraBookingCancellationAsync(orderId, amount, refundOrderId, signature, isPartialCal);

        //    return Ok(result);
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="orderId">Yatra reference Number</param>
        ///// <param name="amount">Refund  Amount in rupees with  two decimal place</param>
        ///// <param name="signature">ordered|amount|merchantId|salt  this logic is encrypted by MD5</param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("api/Yatra/CrossCheckYatraPaymentAsync")]
        //public async Task<IHttpActionResult> CrossCheckYatraPaymentAsync(string orderId, decimal amount, string signature)
        //{
        //    if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(signature))
        //        return BadRequest(Constants.ParameterMissing);

        //    //if (model.Signature != "")
        //    //    return BadRequest(Constants.SignatureMismatch);

        //    var result = await _yatraService.CrossCheckYatraPaymentAsync(orderId, amount, signature);

        //    return Ok(result);
        //}

    }
}
