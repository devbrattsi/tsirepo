﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.MpVidyutVitaran;
using MyIPay.Services.Areas.MpVidyutVitaran.Interfaces;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.MpVidyutVitaran
{
    [RoutePrefix("api/MpVidyutVitaran")]
    public class MpVidyutVitaranController : ApiController
    {
        private readonly IMpVidyutVitaranService _mpVidyutVitaranService;
        public MpVidyutVitaranController(IMpVidyutVitaranService mpVidyutVitaranService)
        {
            _mpVidyutVitaranService = mpVidyutVitaranService;
        }

        #region Apdrp Code

        [HttpGet]
        [Route("GetApdrpBillAsync")]
        public async Task<IHttpActionResult> GetApdrpBillAsync(string consumerNo)
        {
            try
            {
                if (string.IsNullOrEmpty(consumerNo))
                    return BadRequest(Constants.ParameterMissing);

                return Ok(await _mpVidyutVitaranService.GetApdrpBillAsync(consumerNo));
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"GetApdrpBillAsync :: Request : " + ex.Message);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"GetApdrpBillAsync :: Request : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"GetApdrpBillAsync :: Request : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        [Route("PostApdrpBillAsync")]
        public async Task<IHttpActionResult> PostApdrpBillAsync(PostApdrpBill model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"PostApdrpBillAsync :: Request : {JsonConvert.SerializeObject(model)} ");

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var response = await _mpVidyutVitaranService.PostApdrpBillAsync(model);

                return ResponseMessage(response);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"PostApdrpBillAsync :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"PostApdrpBillAsync :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"PostApdrpBillAsync :: Error : " + ex.StackTrace);

                return BadRequest(ex.Message);
            }
        }

        #endregion


        #region Rms Code

        [HttpGet]
        [Route("GetRmsBillInfoAsync")]
        public async Task<IHttpActionResult> GetRmsBillInfoAsync(string consumerNo)//, string agency)//, string TerminalId)
        {
            try
            {

                Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"GetRmsBillInfoAsync :: Request, ConsumerNo :-   { consumerNo}");

                if (string.IsNullOrEmpty(consumerNo))
                    return BadRequest(Constants.ParameterMissing);

                var result = await _mpVidyutVitaranService.GetRmsBillInfoAsync(consumerNo);

                string responseString = await result.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"GetRmsBillInfoAsync :: Response  {responseString}");

                return ResponseMessage(result);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"GetRmsBillInfoAsync :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"GetRmsBillInfoAsync :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"GetRmsBillInfoAsync :: Error : " + ex.StackTrace);

                return BadRequest("error | 500 | Internal Server Error " + ex.Message);
            }
        }

        [HttpPost]
        [Route("PayRmsBillAsync")]
        public async Task<IHttpActionResult> PayRmsBillAsync([FromBody]PayRmsBillModel model)
        {
            Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"PayRmsBillAsync :: Request  {JsonConvert.SerializeObject(model)}");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {

                var result = await _mpVidyutVitaranService.PayRmsBillAsync(model);


                string responseString = await result.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"GetRmsBillInfoAsync :: Response  {responseString}");

                return ResponseMessage(result);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"PayRmsBillAsync :: Error : " + ex.Message);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"PayRmsBillAsync :: Error : " + ex.InnerException);
                Utility.SaveRequestAndResponseLog("MpVidyutVitaran_Error", $"PayRmsBillAsync :: Error : " + ex.StackTrace);

                return BadRequest("error | 500 | Internal Server Error " + ex.Message);
            }

        }

        #endregion
    }
}
