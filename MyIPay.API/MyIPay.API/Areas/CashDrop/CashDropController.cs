﻿using MyIPay.Models.Areas.CashDrop;
using MyIPay.Services.Areas.CashDrop.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;
using MyIPay.Helpers;
using System.Collections.Generic;
using System.Net.Http;

namespace MyIPay.API.Areas.CashDrop
{

    [RoutePrefix("api/Airtel/CashDrop")]
    public class CashDropController : ApiController
    {
        private readonly ICashDropService _cashDropService;

        public CashDropController(ICashDropService cashDropService)
        {
            _cashDropService = cashDropService;
        }

        [Route("GetBillersAsync")]
        public async Task<IHttpActionResult> GetBillersAsync()
        {
            return Ok(await _cashDropService.GetBillersAsync());
        }

        [Route("GetBillerDetailAsync")]
        public async Task<IHttpActionResult> GetBillerDetailAsync(string url)
        {
            if (string.IsNullOrEmpty(url))
                return BadRequest(Constants.ParameterMissing);

            return Ok(await _cashDropService.GetBillerDetailAsync(url));
        }

        [Route("FetchDetailAsync")]
        public async Task<IHttpActionResult> FetchDetailAsync(Dictionary<string, string> keyValuePairs)//(FetchDetailModel model, string actionUrl)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return ResponseMessage(await _cashDropService.FetchDetailAsync(keyValuePairs));
        }


        [Route("PaymentAsync")]
        public async Task<IHttpActionResult> PaymentAsync(Dictionary<string, string> keyValuePairs)//(PaymentModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(await _cashDropService.PaymentAsync(keyValuePairs));
        }

        [Route("GetTxnDetailAsync")]
        public async Task<IHttpActionResult> GetTxnDetailAsync(string partnerTxnId)
        {
            if (string.IsNullOrEmpty(partnerTxnId))
                return BadRequest(Constants.ParameterMissing);

            return Ok(await _cashDropService.GetTxnDetailAsync(partnerTxnId));
        }

    }
}
