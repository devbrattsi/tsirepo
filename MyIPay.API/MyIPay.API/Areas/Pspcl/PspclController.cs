﻿using MyIPay.Services.Areas.Pspcl.Interfaces;
using Newtonsoft.Json.Linq;
using System.Web.Http;

namespace MyIPay.API.Areas.Pspcl
{
    [RoutePrefix("api/Pspcl")]
    public class PspclController : ApiController
    {
        private readonly IPspclService _pspclService;
        public PspclController(IPspclService pspclService)
        {
            _pspclService = pspclService;
        }

        [HttpGet]
        public IHttpActionResult GetBillDetails(string ContractAC)
        {
            return Ok(_pspclService.GetBillDetails(ContractAC));
        }
    }
}
