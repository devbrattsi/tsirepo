﻿using MyIPay.Models.Areas.Avenue;
using MyIPay.Services.Areas.Avenue.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Avenue
{
    [RoutePrefix("api/Avenue")]
    public class AvenueController : ApiController
    {

        private readonly IAvenueService _avenueService;

        public AvenueController(IAvenueService avenueService)
        {
            _avenueService = avenueService;
        }

        /// <summary>
        /// For Get All Biller List
        /// URL:- api/avenue/GetAllBillerList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>json</returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetAllBillerList() => Ok(await _avenueService.GetAllBillerList());


        /// <summary>
        /// Api for fetching bill details 
        /// URL:- api/avenue/FetchBill
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> FetchBill(BillFetchRequest model) => Ok(await _avenueService.FetchBill(model));


        /// <summary>
        /// this api is for paying bill
        /// URL:- api/avenue/PayBill
        /// </summary>
        /// <param name="obj">BillPaymentRequest Type</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> PayBill(BillPaymentRequest model) => Ok(await _avenueService.PayBill(model));

        /// <summary>
        /// Api is for getting transaction status
        /// URL:- api/avenue/GetTransactionStatus
        /// </summary>
        /// <param name="obj">TransactionStatusReq Type</param>
        /// <returns>json</returns>
        [HttpPost]
        public async Task<IHttpActionResult> GetTransactionStatus(TransactionStatusReq model) => Ok(await _avenueService.GetTransactionStatus(model));

        /// <summary>
        /// for registering new complaint
        /// URL:- api/avenue/RegisterComplaint
        /// </summary>
        /// <param name="obj">ComplaintRegistrationReq Type</param>
        /// <returns>json</returns>
        [HttpPost]
        public async Task<IHttpActionResult> RegisterComplaint(ComplaintRegistrationReq model) => Ok(await _avenueService.RegisterComplaint(model));

        /// <summary>
        /// For checking complaint status
        /// URL:- api/avenue/ComplaintStatus
        /// </summary>
        /// <param name="obj">ComplaintTrackingReq Type</param>
        /// <returns>json</returns>
        [HttpPost]
        public async Task<IHttpActionResult> ComplaintStatus(ComplaintTrackingReq model) => Ok(await _avenueService.ComplaintStatus(model));

        /// <summary>
        /// Deposit Enquiry with agent
        /// URL:- api/avenue/DepositEnquiryWithAgent
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>json</returns>
        [HttpPost]
        public async Task<IHttpActionResult> DepositEnquiryWithAgent(DepositDetailsRequestWithAgent model) => Ok(await _avenueService.DepositEnquiryWithAgent(model));


        /// <summary>
        /// Deposit Enquiry without agent
        /// URL:- api/avenue/DepositEnquiryWithoutAgent
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>json</returns>
        [HttpPost]
        public async Task<IHttpActionResult> DepositEnquiryWithoutAgent(DepositDetailsRequestWithoutAgent model) => Ok(await _avenueService.DepositEnquiryWithoutAgent(model));
    }
}
