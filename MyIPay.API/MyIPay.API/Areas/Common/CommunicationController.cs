﻿using MyIPay.Models.Areas.Common;
using MyIPay.Services.Areas.Common.Interfaces;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Common
{
    [RoutePrefix("api/Communication")]
    public class CommunicationController : ApiController
    {
        private readonly ICommunicationService _communicationService;

        public CommunicationController(ICommunicationService communicationService)
        {
            _communicationService = communicationService;
        }
        /// <summary>
        /// Api for Send SMS 
        /// URL:- api/SMS/SendSMS
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public JObject SendSms(SmsModel smsModel)
        {
            return JObject.Parse(_communicationService.SendSMS(smsModel));

        }

        [HttpPost]
        public JObject SendWebPortalEmail(SendEmailModel model)
        {
            return JObject.Parse(_communicationService.SendWebPortalEmail(model));

        }

        [HttpGet]
        public async Task<int> GetRemainingSmsCount()
        {
            return await _communicationService.GetRemainingSmsCountAsync();
        }
    }
}
