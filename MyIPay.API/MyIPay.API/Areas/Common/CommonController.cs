﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.Common;
using MyIPay.Services.Areas.Common.Interfaces;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.Common
{
    [RoutePrefix("api/Common")]
    public class CommonController : ApiController
    {
        private readonly ICommonService _commonService;
        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        // POST: api/Common/GetAvailableBalance
        [HttpPost]
        public async Task<IHttpActionResult> GetAvailableBalance([FromBody]AvailableBalanceModel model)
        {
            
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (model.SecretKeyTimeStamp != AppSettings.UatSecretKeyTimeStamp || model.SecretKey != AppSettings.UatSecretKey)
                return BadRequest(Constants.InvalidTimestampOrSecretKey);


            var result = await _commonService.GetAvailableBalanceAsync(model);

            return Ok(result);
        }

        public IHttpActionResult GetMerchantAvailableBalance(string agentId,string role,int distributorId)
        {
            if (string.IsNullOrEmpty(agentId))
                return BadRequest("Agent ID missing.");
            try
            {
                var result = _commonService.GetMerchantAvailableBalance(agentId,role,distributorId);
                return Ok(result);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        //public IHttpActionResult InitiateTransaction(InitiateTransactionModel model)
        //{

        //}

    }
}
