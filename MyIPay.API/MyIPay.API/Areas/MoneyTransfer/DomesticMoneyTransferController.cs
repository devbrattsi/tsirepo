﻿using log4net;
using MyIPay.Helpers;
using MyIPay.Models.Areas.MoneyTransfer;
using MyIPay.Models.Areas.MoneyTransfer.Airtel;
using MyIPay.Services.Areas.MoneyTransfer.Airtel.Interfaces;
using MyIPay.Services.Areas.MoneyTransfer.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;

namespace MyIPay.API.Areas.MoneyTransfer
{
    [RoutePrefix("api/DomesticMoneyTransfer")]
    public class DomesticMoneyTransferController : ApiController
    {
        private readonly IDomesticMoneyTransferService _dmtService;
        private readonly string _baseUrl;
        private readonly long _initiatorId;
        private readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string _mode = AppSettings.EkoDmtMode;
        private readonly IAirtelBankService _airtelBankService;

        public DomesticMoneyTransferController(IDomesticMoneyTransferService dmtService,
                                               IAirtelBankService airtelBankService)
        {
            _dmtService = dmtService;

            //Here mode L means Live and D means Demo
            _baseUrl = _mode == "L" ? AppSettings.EkoLiveApiBaseUrl : AppSettings.EkoUatApiBaseUrl;
            _initiatorId = _mode == "L" ? AppSettings.EkoLiveInitiatorId : AppSettings.EkoUatInitiatorId;
            _airtelBankService = airtelBankService;
        }
        // GET: api/DomesticMoneyTransfer


        #region EKO

        #region Customers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobileNumber">Customer Id</param>
        /// <param name="initiatorId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetCustomerInfo(string mobileNumber, string product = null)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetCustomerInfo Request Log --> mobileNumber = {mobileNumber},product={product}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetCustomerInfo BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");
                }



                string url = string.Empty;

                if (string.IsNullOrEmpty(product))
                    url = $"customers/mobile_number:{mobileNumber}?initiator_id={_initiatorId}";
                else
                    url = $"customers/mobile_number:{mobileNumber}/product:{product}?initiator_id={_initiatorId}";

                var response = await _dmtService.GetAsync(_baseUrl, url);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new HttpRequestException(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetCustomerInfo Response Log  --> {responseString}");

                var customerInfoDto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(customerInfoDto);
            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: GetCustomerInfo Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: GetCustomerInfo Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: GetCustomerInfo Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetCustomerInfo Error Log  --> {ex.Message}");
                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobileNumber">Here mobile no. is a customer_id</param>
        /// <param name="product">indonepal</param>
        /// <param name="value"></param>
        /// 

        [HttpPut]
        public async Task<IHttpActionResult> CreateCustomer([FromBody]CreateCustomerModel model, string mobileNumber)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateCustomer Request Log  -->model = {JsonConvert.SerializeObject(model)},mobileNumber = {mobileNumber}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateCustomer BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Mobile number is missing or Invalid.");

                }


                if (model == null || !ModelState.IsValid)
                    return BadRequest(ModelState);

                string url = $"customers/mobile_number:{mobileNumber}";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());

                //var json = JsonConvert.SerializeObject(model);
                var response = await _dmtService.PutAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateCustomer Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: CreateCustomer Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: CreateCustomer Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: CreateCustomer Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateCustomer Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }


        [HttpPut]
        public async Task<IHttpActionResult> CreateIndoNepalCustomer([FromBody]IndoNepalCreateCustomerModel model, string mobileNumber, string product)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer Request Log  -->model = {JsonConvert.SerializeObject(model)},mobileNumber = {mobileNumber},product={product}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile) || string.IsNullOrEmpty(product))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");

                }


                if (model == null || !ModelState.IsValid)
                    return BadRequest(ModelState);


                string url = $"customers/mobile_number:{mobileNumber}/product:{product}";

                PropertyInfo[] infos = model.GetType().GetProperties();

                MultipartFormDataContent formData = new MultipartFormDataContent
                {
                    { new ByteArrayContent(model.File1), "file1", DateTime.Now.ToString("ddMMyyyyHHmmssffff") + ".jpg" },
                    { new ByteArrayContent(model.File2), "file2", DateTime.Now.ToString("ddMMyyyyHHmmssffff") + ".jpg" }
                };

                string data = $"name={model.Name}&initiator_id=mobile_number:{_initiatorId}&id_proof_type_id={model.Id_Proof_Type_Id}&id_proof={model.Id_Proof}&gender={model.Gender}&dob={model.Dob}&address_line1={model.Address_Line1}&income_source={model.Income_source}&remittance_reason={model.Remittance_Reason}&nationality={model.Nationality}&sender_pincode={model.Pincode}&sender_state={model.State}&sender_city={model.City}&company_name={model.CompanyName}";
                //&customer_photo={model.Customer_Photo}

                formData.Add(new StringContent(data), "form-data");

                var response = await _dmtService.PutMultiFormDataAsync(_baseUrl, url, formData);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer Response StatusCode  --> {response.StatusCode}");


                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: CreateIndoNepalCustomer Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: CreateIndoNepalCustomer Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: CreateIndoNepalCustomer Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer Error Log  --> {ex.Message}");
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer InnerException Log  --> {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"CreateIndoNepalCustomer StackTrace Log  --> {ex.StackTrace}");

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="otp"></param>
        /// <param name="product">indonepal</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IHttpActionResult> VerifyCustomerIdentity([FromBody]VerifyCustomerIdentityModel model, string otp, string product = null)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId.ToString();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyCustomerIdentity Request Log  -->model = {JsonConvert.SerializeObject(model)},otp = {otp},product={product}");
                if (string.IsNullOrEmpty(otp)
                    || !int.TryParse(otp, out int oneTimePass))

                    return BadRequest("OTP is missing or Invalid.");

                if (model == null)//|| !ModelState.IsValid)
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyCustomerIdentity BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest(ModelState);

                }

                string url = string.Empty;
                if (string.IsNullOrEmpty(product))
                    url = $"customers/verification/otp:{otp}";
                else
                    url = $"customers/product:{product}/verification/otp:{otp}";


                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }
                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PutAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyCustomerIdentity Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: VerifyCustomerIdentity Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: VerifyCustomerIdentity Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: VerifyCustomerIdentity Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyCustomerIdentity Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobileNumber"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> ResendOtp(string mobileNumber, string product = null)
        {
            try
            {

                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId.ToString();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendOtp Request Log  -->mobileNumber = {mobileNumber},product={product}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile))
                {

                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendOtp BadRequest Log --> OTP is missing or Invalid.");
                    return BadRequest("OTP is missing or Invalid.");
                }


                string url = string.Empty;
                if (string.IsNullOrEmpty(product))
                    url = $"customers/mobile_number:{mobileNumber}/otp";
                else
                    url = $"customers/mobile_number:{mobileNumber}/product:{product}/otp";

                Dictionary<string, string> dictionary = new Dictionary<string, string>
                {
                    { "initiator_id", _initiatorId.ToString() }
                };



                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendOtp Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: ResendOtp Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: ResendOtp Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: ResendOtp Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendOtp Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion


        #region Recipients

        /// <summary>
        /// Get List of Recipients
        /// </summary>
        /// <param name="mobileNumber"></param>
        /// <param name="initiatorId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public async Task<IHttpActionResult> GetRecipients(string mobileNumber, string product = null)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipients Request Log  -->mobileNumber = {mobileNumber},initiatorId = {_initiatorId},product={product}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipients BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");
                }



                string url = string.Empty;
                if (string.IsNullOrEmpty(product))
                    url = $"customers/mobile_number:{mobileNumber}/recipients?initiator_id={_initiatorId}";
                else
                    url = $"customers/mobile_number:{mobileNumber}/product:{product}/recipients?initiator_id={_initiatorId}";

                var response = await _dmtService.GetAsync(_baseUrl, url);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipients Response Log  --> {responseString}");

                var result = JsonConvert.DeserializeObject<object>(responseString);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: GetRecipients Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: GetRecipients Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: GetRecipients Error --> " + ex.StackTrace);


                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipients Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Recipient Id</param>
        /// <param name="mobileNumber">Customer Id</param>
        /// <param name="initiatorId">Initiator Id</param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public async Task<IHttpActionResult> GetRecipientDetails(string id, string mobileNumber, string product = null)
        {

            try
            {

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipientDetails Request Log  -->id = {id},mobileNumber = {mobileNumber},initiatorId= {_initiatorId},product={product}");


                if (string.IsNullOrEmpty(id)
                    || !int.TryParse(id, out int recipientId)
                    || string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipientDetails BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");
                }

                string url = string.Empty;

                if (string.IsNullOrEmpty(product))
                    url = $"customers/mobile_number:{mobileNumber}/recipients/recipient_id:{id}?initiator_id={_initiatorId}";
                else
                    url = $"customers/mobile_number:{mobileNumber}/product:{product}/recipients/recipient_id:{id}?initiator_id={_initiatorId}";


                var response = await _dmtService.GetAsync(_baseUrl, url);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipientDetails Response Log  --> {responseString}");

                var result = JsonConvert.DeserializeObject<object>(responseString);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: GetRecipientDetails Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: GetRecipientDetails Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: GetRecipientDetails Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetRecipientDetails Error Log  --> {ex.Message}");
                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">value of the recipient_id</param>
        /// <param name="mobileNumber">Customer ID</param>
        /// <param name="recipientIdType">recipient_id_type can have the following parameters: a) acc_ifsc b) acc_bankcode (only applicable when IMPS service is available and IFSC is not required)</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IHttpActionResult> AddRecipient([FromBody]AddRecipientModel model, string mobileNumber, string recipientIdType)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"AddRecipient Request Log  -->model = {JsonConvert.SerializeObject(model)},mobileNumber = {mobileNumber},recipientIdType={recipientIdType}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile)
                    || string.IsNullOrEmpty(recipientIdType))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"AddRecipient BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest("Required parameter missing or Invalid.");
                }

                if (model == null || !ModelState.IsValid)
                    return BadRequest(ModelState);

                string url = $"customers/mobile_number:{mobileNumber}/recipients/{recipientIdType}";


                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PutAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"AddRecipient Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: AddRecipient Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: AddRecipient Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: AddRecipient Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"AddRecipient Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> RemoveRecipient(string mobileNumber, string RecipientId)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"RemoveRecipient Request Log  --> mobileNumber = {mobileNumber},RecipientId={RecipientId}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile)
                    || string.IsNullOrEmpty(RecipientId))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"RemoveRecipient BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest("Required parameter missing or Invalid.");
                }

                string url = $"customers/mobile_number:{mobile}/recipients/recipient_id:{RecipientId}";

                Dictionary<string, string> dictionary = new Dictionary<string, string>
                {
                    { "initiator_id", _initiatorId.ToString() }
                };


                var response = await _dmtService.DeleteAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"AddRecipient Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: AddRecipient Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: AddRecipient Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: AddRecipient Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"AddRecipient Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }



        [HttpPut]
        public async Task<IHttpActionResult> IndoNepalAddRecipient([FromBody]IndoNepalAddRecipientModel model, string mobileNumber, string product)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalAddRecipient Request Log  -->model = {JsonConvert.SerializeObject(model)},mobileNumber = {mobileNumber},product={product}");

                if (string.IsNullOrEmpty(mobileNumber)
                    || !long.TryParse(mobileNumber, out long mobile))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalAddRecipient BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest("Required parameter missing or Invalid.");
                }

                if (model == null || !ModelState.IsValid)
                    return BadRequest(ModelState);

                string url = string.Empty;
                if (model.PaymentMode == "Cash_Payout")
                    url = $"customers/mobile_number:{mobileNumber}/product:{product}/recipients/mobile_number:{model.Recipient_Mobile}";
                else
                    url = $"customers/mobile_number:{mobileNumber}/product:{product}/recipients/acc_indonepalbranch:{model.AccountNumber}_{model.Branch}";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    if (info.Name.ToLower() == "paymentmode" || info.Name.ToLower() == "cashpayoutlocation")
                        continue;
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PutAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalAddRecipient Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: IndoNepalAddRecipient Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: IndoNepalAddRecipient Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: IndoNepalAddRecipient Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalAddRecipient Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }


        // [HttpDelete]
        public async Task<IHttpActionResult> RemoveRecipient(RemoveRecipientModel model)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"RemoveRecipient Request Log  -->Model = {JsonConvert.SerializeObject(model)}");


                if (!ModelState.IsValid)
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"RemoveRecipient BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest("Required parameter missing or Invalid.");
                }


                string url = $"customers/mobile_number:{model.Mobile}/recipients/recipient_id:{model.RecipientId}";

                Dictionary<string, string> dictionary = new Dictionary<string, string>
                {
                    { "initiator_id", _initiatorId.ToString() }
                };


                var response = await _dmtService.DeleteAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"RemoveRecipient Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: RemoveRecipient Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: RemoveRecipient Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: RemoveRecipient Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"RemoveRecipient Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }
        #endregion


        #region Transactions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> InitiateTransaction([FromBody]InitiateTransactionModel model)
        {
            try
            {
                ////Assigning initiator id
                //model.initiator_id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"InitiateTransaction Request Log  -->model = {JsonConvert.SerializeObject(model)}");
                if (model == null || !ModelState.IsValid)
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"InitiateTransaction BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest(ModelState);

                }

                string url = "transactions";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"InitiateTransaction Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: InitiateTransaction Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: InitiateTransaction Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: InitiateTransaction Error --> " + ex.StackTrace);


                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"InitiateTransaction Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> IndoNepalInitiateTransaction([FromBody]IndoNepalInitiateTransactionModel model)
        {
            try
            {
                ////Assigning initiator id
                //model.initiator_id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalInitiateTransaction Request Log  -->model = {JsonConvert.SerializeObject(model)}");
                if (model == null || !ModelState.IsValid)
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalInitiateTransaction BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest(ModelState);

                }

                string url = "transactions";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalInitiateTransaction Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: IndoNepalInitiateTransaction Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: IndoNepalInitiateTransaction Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: IndoNepalInitiateTransaction Error --> " + ex.StackTrace);


                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalInitiateTransaction Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Transaction Id</param>
        /// <param name="recipientId">recipientId</param>
        /// <param name="initiatorId">initiatorId</param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public async Task<IHttpActionResult> GetTransactionStatus(string id)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetTransactionStatus Request Log --> id = {id},initiatorId ={_initiatorId}");

                if (string.IsNullOrEmpty(id))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetTransactionStatus BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");

                }

                string url = $"transactions/{id}?initiator_id={_initiatorId}";

                if (id.Contains("T"))
                {
                    url = $"transactions/client_ref_id:{id}?initiator_id={_initiatorId}";
                }


                var response = await _dmtService.GetAsync(_baseUrl, url);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetTransactionStatus Response Log  --> {responseString}");

                var result = JsonConvert.DeserializeObject<object>(responseString);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: GetTransactionStatus Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: GetTransactionStatus Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: GetTransactionStatus Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetTransactionStatus Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">can be Eko tid or partner's unique reference id.(transaction id  required)</param>
        /// <param name="client_Ref_Id">unique id of transaction on client platform</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Refund(string id, [FromBody]RefundModel model)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = Convert.ToInt64(_initiatorId);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"Refund Request Log  -->id = {id}, model = {JsonConvert.SerializeObject(model)}");

                if (string.IsNullOrEmpty(id))
                    return BadRequest("Required parameter missing or Invalid.");

                if (model == null || !ModelState.IsValid)
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"Refund BadRequest Log --> {ModelState}");

                    return BadRequest(ModelState);

                }

                string url = $"transactions/{id}/refund";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());

                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"Refund Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: Refund Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: Refund Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: Refund Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"Refund Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        //[HttpPost]
        //public async Task<IHttpActionResult> IndoNepalRefund([FromBody]IndoNepalRefundModel model)
        //{
        //    try
        //    {
        //        ////Assigning initiator id
        //        //model.Initiator_Id = Convert.ToInt64(_initiatorId);

        //        Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalRefund Request Log  -->model = {JsonConvert.SerializeObject(model)}");

        //        if (model == null || !ModelState.IsValid)
        //        {
        //            Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalRefund BadRequest Log --> {ModelState}");

        //            return BadRequest(ModelState);

        //        }

        //        string url = $"transactions/{model.TId}/refund";

        //        PropertyInfo[] infos = model.GetType().GetProperties();

        //        Dictionary<string, string> dictionary = new Dictionary<string, string>();

        //        foreach (PropertyInfo info in infos)
        //        {
        //            dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
        //        }

        //        dictionary.Add("initiator_id", _initiatorId.ToString());

        //        var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

        //        if (response.StatusCode != HttpStatusCode.OK)
        //        {
        //            throw new Exception(response.ReasonPhrase);
        //        }

        //        string responseString = await response.Content.ReadAsStringAsync();

        //        Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalRefund Response Log  --> {responseString}");

        //        var dto = JsonConvert.DeserializeObject<object>(responseString);
        //        return Ok(dto);

        //    }
        //    catch (Exception ex)
        //    {
        //        _log.Info("DomesticMoneyTransfer :: IndoNepalRefund Error --> " + ex.Message);
        //        _log.Info("DomesticMoneyTransfer :: IndoNepalRefund Error --> " + ex.InnerException);
        //        _log.Info("DomesticMoneyTransfer :: IndoNepalRefund Error --> " + ex.StackTrace);

        //        Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"IndoNepalRefund Error Log  --> {ex.Message}");

        //        return BadRequest(ex.Message);
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">id of the transaction for which refund needs to be initiated</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> ResendRefundOtp(string id)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendRefundOtp Request Log  -->id = {id}");


                if (string.IsNullOrEmpty(id) || !long.TryParse(id, out long tId))
                {

                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendRefundOtp BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");
                }


                string url = $"transactions/{id}/refund/otp";

                Dictionary<string, string> dictionary = new Dictionary<string, string>
                {
                    { "initiator_id", _initiatorId.ToString() }
                };


                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendRefundOtp Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: ResendRefundOtp Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: ResendRefundOtp Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: ResendRefundOtp Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"ResendRefundOtp Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion


        #region Banks



        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankCode">like SBIN and length should not be greater than 4</param>
        /// <param name="initiatorId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public async Task<IHttpActionResult> GetBankDetailsByBankCode(string bankCode)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByBankCode Request Log --> bankCode = {bankCode},initiatorId ={_initiatorId}");

                if (string.IsNullOrEmpty(bankCode))
                {

                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByBankCode BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");
                }

                string url = $"banks?bank_code={bankCode}&initiator_id={_initiatorId}";
                var response = await _dmtService.GetAsync(_baseUrl, url);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByBankCode Response Log  --> {responseString}");

                var result = JsonConvert.DeserializeObject<object>(responseString);

                return Ok(result);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByBankCode Error Log  --> {ex.Message}");

                _log.Info("DomesticMoneyTransfer :: GetBankDetailsByBankCode Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: GetBankDetailsByBankCode Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: GetBankDetailsByBankCode Error --> " + ex.StackTrace);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ifscCode">ifscCode</param>
        /// <param name="initiatorId">Int64</param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public async Task<IHttpActionResult> GetBankDetailsByIfscCode(string ifscCode)
        {

            try
            {
                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByIfscCode Request Log --> ifscCode = {ifscCode},initiatorId ={_initiatorId}");

                if (string.IsNullOrEmpty(ifscCode))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByIfscCode BadRequest Log --> Required parameter missing or Invalid.");

                    return BadRequest("Required parameter missing or Invalid.");
                }


                string url = $"banks?ifsc={ifscCode}&initiator_id={_initiatorId}";
                var response = await _dmtService.GetAsync(_baseUrl, url);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByIfscCode Response Log  --> {responseString}");

                var result = JsonConvert.DeserializeObject<object>(responseString);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: GetBankDetailsByIfscCode Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: GetBankDetailsByIfscCode Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: GetBankDetailsByIfscCode Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"GetBankDetailsByIfscCode Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id_Type">string bank_code,can have 2 values: ifsc or bank_code</param>
        /// <param name="Code">Bank Code or IFSC Code</param>
        /// <param name="Id">Integer, need to mention account number which needs to be verified</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> VerifyBankAccountDetails([FromBody]VerifyBankAccountDetailsModel model, string id_Type, string code, string id)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyBankAccountDetails Request Log  -->model = {JsonConvert.SerializeObject(model)},id_Type = {id_Type},code={code},id= {id}");

                if (string.IsNullOrEmpty(id_Type) || string.IsNullOrEmpty(code) || string.IsNullOrEmpty(id) || !long.TryParse(id, out long accountNo))
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyBankAccountDetails BadRequest Log --> Required parameter missing or Invalid.");
                    return BadRequest("Required parameter missing or Invalid.");
                }

                if (model == null || !ModelState.IsValid)
                    return BadRequest(ModelState);

                string url = $"banks/{id_Type}:{code}/accounts/{id}";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyBankAccountDetails Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: VerifyBankAccountDetails Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: VerifyBankAccountDetails Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: VerifyBankAccountDetails Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"VerifyBankAccountDetails Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> PanVerification([FromBody]PanVerificationModel model)
        {
            try
            {
                ////Assigning initiator id
                //model.Initiator_Id = _initiatorId;

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"PanVerification Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"PanVerification BadRequest Log --> {ModelState}");
                    return BadRequest(ModelState);
                }

                string url = $"pan/verify/";

                PropertyInfo[] infos = model.GetType().GetProperties();

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                foreach (PropertyInfo info in infos)
                {
                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
                }

                dictionary.Add("initiator_id", _initiatorId.ToString());


                var response = await _dmtService.PostAsync(_baseUrl, url, dictionary);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(response.ReasonPhrase);
                }

                string responseString = await response.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"PanVerification Response Log  --> {responseString}");

                var dto = JsonConvert.DeserializeObject<object>(responseString);
                return Ok(dto);

            }
            catch (Exception ex)
            {
                _log.Info("DomesticMoneyTransfer :: PanVerification Error --> " + ex.Message);
                _log.Info("DomesticMoneyTransfer :: PanVerification Error --> " + ex.InnerException);
                _log.Info("DomesticMoneyTransfer :: PanVerification Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("DomesticMoneyTransfer", $"PanVerification Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion

        #endregion


        #region Airtel Payment Bank

        #region Check Distributor Balance
        /// <summary>
        /// Check Distributor Balance
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 

        [HttpPost]
        [Route("airtel/checkDistributorBalanceAsync")]
        public async Task<IHttpActionResult> CheckDistributorBalanceAsync(CheckDistributorBalanceModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("AirtelBank", $"CheckDistributorBalanceAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                }

                var responseMsg = await _airtelBankService.CheckDistributorBalanceAsync(model);

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("AirtelBank", $"CheckDistributorBalanceAsync Response Log  --> {responseString}");

                //var dto = JsonConvert.DeserializeObject<object>(responseString);

                return ResponseMessage(responseMsg);//this.Request.CreateResponse(HttpStatusCode.OK, responseString);
            }
            catch (Exception ex)
            {
                _log.Info("AirtelBank_Error :: CheckDistributorBalanceAsync Error --> " + ex.Message);
                _log.Info("AirtelBank_Error :: CheckDistributorBalanceAsync Error --> " + ex.InnerException);
                _log.Info("AirtelBank_Error :: CheckDistributorBalanceAsync Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("AirtelBank_Error", $"CheckDistributorBalanceAsync Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region Check Walking Customer Limit
        /// <summary>
        /// Check Walking Customer Limit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("airtel/checkSenderLimitAsync")]
        public async Task<IHttpActionResult> CheckSenderLimitAsync(CheckSenderLimitModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("AirtelBank", $"CheckSenderLimitAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                }

                var responseMsg = await _airtelBankService.CheckSenderLimitAsync(model);

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("AirtelBank", $"CheckSenderLimitAsync Response Log  --> {responseString}");

                return ResponseMessage(responseMsg);

            }
            catch (Exception ex)
            {

                _log.Info("AirtelBank_Error :: CheckSenderLimitAsync Error --> " + ex.Message);
                _log.Info("AirtelBank_Error :: CheckSenderLimitAsync Error --> " + ex.InnerException);
                _log.Info("AirtelBank_Error :: CheckSenderLimitAsync Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("AirtelBank_Error", $"CheckSenderLimitAsync Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region ENQUIRY IMPS TRANSACTION
        /// <summary>
        /// ENQUIRY IMPS TRANSACTION
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("airtel/impsTransactionEnquiryAsync")]
        public async Task<IHttpActionResult> ImpsTransactionEnquiryAsync(ImpsTransactionEnquiryModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("AirtelBank", $"ImpsTransactionEnquiryAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                }


                //if (!Request.Headers.TryGetValues("X-Forwarded-For", out IEnumerable<string> xForwardedForValues))
                //{
                //    return BadRequest("Header missing.");

                //}

                //Utility.SaveRequestAndResponseLog("AirtelBank", $"X-Forwarded-For Header Log  --> {xForwardedForValues.FirstOrDefault()}");


                var responseMsg = await _airtelBankService.ImpsTransactionEnquiryAsync(model);//,xForwardedForValues);

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("AirtelBank", $"ImpsTransactionEnquiryAsync Response Log  --> {responseString}");

                return ResponseMessage(responseMsg);

            }
            catch (Exception ex)
            {

                _log.Info("AirtelBank_Error :: ImpsTransactionEnquiryAsync Error --> " + ex.Message);
                _log.Info("AirtelBank_Error :: ImpsTransactionEnquiryAsync Error --> " + ex.InnerException);
                _log.Info("AirtelBank_Error :: ImpsTransactionEnquiryAsync Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("AirtelBank_Error", $"ImpsTransactionEnquiryAsync Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region IMPS TRANSACTION

        /// <summary>
        ///  IMPS TRANSACTION
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("airtel/impsTransactionAsync")]
        public async Task<IHttpActionResult> ImpsTransactionAsync(ImpsTransactionModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("AirtelBank", $"ImpsTransactionAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                }


                //if (!Request.Headers.TryGetValues("X-Forwarded-For", out IEnumerable<string>  xForwardedForValues))
                //{
                //    return BadRequest("Header missing.");

                //}

                //Utility.SaveRequestAndResponseLog("AirtelBank", $"X-Forwarded-For Header Log  --> {xForwardedForValues.FirstOrDefault()}");


                var responseMsg = await _airtelBankService.ImpsTransactionAsync(model);//, xForwardedForValues);

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("AirtelBank", $"ImpsTransactionAsync Response Log  --> {responseString}");

                return ResponseMessage(responseMsg);

            }
            catch (Exception ex)
            {
                _log.Info("AirtelBank_Error :: ImpsTransactionAsync Error --> " + ex.Message);
                _log.Info("AirtelBank_Error :: ImpsTransactionAsync Error --> " + ex.InnerException);
                _log.Info("AirtelBank_Error :: ImpsTransactionAsync Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("AirtelBank_Error", $"ImpsTransactionAsync Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Fund Transfer
        ///// <summary>
        ///// Fund Transfer
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        ///
        //[HttpPost]
        //[Route("airtel/fundTransferAsync")]
        //public async Task FundTransferAsync()
        //{

        //} 

        #endregion

        #region IFSC

        /// <summary>
        /// Generate Token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("airtel/generateTokenAsync")]
        public async Task<IHttpActionResult> GenerateTokenAsync(GenerateTokenModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("AirtelBank", $"GenerateTokenAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                }

                var responseMsg = await _airtelBankService.GenerateTokenAsync(model);

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("AirtelBank", $"GenerateTokenAsync Response Log  --> {responseString}");

                return ResponseMessage(responseMsg);

            }
            catch (Exception ex)
            {
                _log.Info("AirtelBank_Error :: GenerateTokenAsync Error --> " + ex.Message);
                _log.Info("AirtelBank_Error :: GenerateTokenAsync Error --> " + ex.InnerException);
                _log.Info("AirtelBank_Error :: GenerateTokenAsync Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("AirtelBank_Error", $"GenerateTokenAsync Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("airtel/getBankByIfscCodeAsync")]
        public async Task<IHttpActionResult> GetBankByIfscCodeAsync(GetBankByIfscCodeModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("AirtelBank", $"GetBankByIfscCodeAsync Request Log  -->model = {JsonConvert.SerializeObject(model)}");

                if (model == null || !ModelState.IsValid)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
                }

                var responseMsg = await _airtelBankService.GetBankByIfscCodeAsync(model);

                string responseString = await responseMsg.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("AirtelBank", $"GetBankByIfscCodeAsync Response Log  --> {responseString}");

                return ResponseMessage(responseMsg);

            }
            catch (Exception ex)
            {
                _log.Info("AirtelBank_Error :: GetBankByIfscCodeAsync Error --> " + ex.Message);
                _log.Info("AirtelBank_Error :: GetBankByIfscCodeAsync Error --> " + ex.InnerException);
                _log.Info("AirtelBank_Error :: GetBankByIfscCodeAsync Error --> " + ex.StackTrace);

                Utility.SaveRequestAndResponseLog("AirtelBank_Error", $"GetBankByIfscCodeAsync Error Log  --> {ex.Message}");

                return BadRequest(ex.Message);
            }
        }

        #endregion






        #endregion

    }
}
