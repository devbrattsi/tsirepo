﻿//using MyIPay.Helpers;
//using MyIPay.Models.Areas.MoneyTransfer;
//using MyIPay.Services.Areas.MoneyTransfer.Interfaces;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Net;
//using System.Reflection;
//using System.Threading.Tasks;
//using System.Web.Http;

//namespace MyIPay.API.Areas.MoneyTransfer
//{

//    [RoutePrefix("api/Aeps")]
//    public class AepsController : ApiController
//    {

//        private readonly IDomesticMoneyTransferService _dmtService;
//        private readonly string _baseUrl;
//        private readonly long _initiatorId;
//        private readonly string _userCode;

//        private readonly string _mode = AppSettings.EkoDmtMode;

//        public AepsController(IDomesticMoneyTransferService dmtService)
//        {
//            _dmtService = dmtService;

//            //Here mode L means Live and D means Demo
//            _baseUrl = _mode == "L" ? AppSettings.EkoLiveApiBaseUrl : AppSettings.EkoUatApiBaseUrl;
//            _initiatorId = _mode == "L" ? AppSettings.EkoLiveInitiatorId : AppSettings.EkoUatInitiatorId;
//            _userCode = _mode == "L" ? AppSettings.EkoUatUserCode : AppSettings.EkoLiveUserCode;
//        }

//        [HttpGet]
//        public async Task<IHttpActionResult> GetEkoServices()
//        {
//            try
//            {
//                var response = await _dmtService.GetAsync(_baseUrl, "user/services?initiator_id=" + _initiatorId);

//                if (response.StatusCode != HttpStatusCode.OK)
//                {
//                    throw new Exception(response.ReasonPhrase);
//                }

//                string responseString = await response.Content.ReadAsStringAsync();

//                Utility.SaveRequestAndResponseLog("Aeps", $"GetEkoServices Response Log  --> {responseString}");

//                var dto = JsonConvert.DeserializeObject<object>(responseString);

//                return Ok(dto);
//            }
//            catch (Exception ex)
//            {
//                Utility.WriteLog("Aeps", $"GetEkoServices Error { ex.Message}");
//                Utility.WriteLog("Aeps", $"GetEkoServices Error { ex.InnerException.ToString()}");
//                Utility.WriteLog("Aeps", $"GetEkoServices Error {ex.StackTrace}");

//                return BadRequest(ex.Message);
//            }
//        }

//        [HttpPut]
//        public async Task<IHttpActionResult> ActivateEkycService([FromBody]ActivateEkycModel model)
//        {
//            try
//            {

//                Utility.SaveRequestAndResponseLog("Aeps", $"ActivateEkyc Request Log  -->model = {JsonConvert.SerializeObject(model)}");

//                if (model == null || !ModelState.IsValid)
//                    return BadRequest(ModelState);

//                string url = $"user/service/activate";

//                PropertyInfo[] infos = model.GetType().GetProperties();

//                Dictionary<string, string> dictionary = new Dictionary<string, string>();

//                foreach (PropertyInfo info in infos)
//                {
//                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
//                }

//                dictionary.Add("initiator_id", _initiatorId.ToString());
//                dictionary.Add("user_code", _userCode);


//                //var json = JsonConvert.SerializeObject(model);
//                var response = await _dmtService.PutAsync(_baseUrl, url, dictionary);

//                if (response.StatusCode != HttpStatusCode.OK)
//                {
//                    throw new Exception(response.ReasonPhrase);
//                }

//                string responseString = await response.Content.ReadAsStringAsync();

//                Utility.SaveRequestAndResponseLog("Aeps", $"ActivateEkyc Response Log  --> {responseString}");

//                var dto = JsonConvert.DeserializeObject<object>(responseString);

//                return Ok(dto);

//            }
//            catch (Exception ex)
//            {
//                Utility.WriteLog("Aeps", $"ActivateEkyc Error { ex.Message}");
//                Utility.WriteLog("Aeps", $"ActivateEkyc Error { ex.InnerException.ToString()}");
//                Utility.WriteLog("Aeps", $"ActivateEkyc Error {ex.StackTrace}");

//                return BadRequest(ex.Message);
//            }
//        }


//        [HttpPost]
//        public async Task<IHttpActionResult> DoEkyc([FromBody]DoEkycModel model)
//        {
//            try
//            {

//                Utility.SaveRequestAndResponseLog("Aeps", $"DoEkyc Request Log  -->model = {JsonConvert.SerializeObject(model)}");

//                if (model == null || !ModelState.IsValid)
//                    return BadRequest(ModelState);

//                string baseUrl = "https://stagegateway.eko.in/v2/";
//                string url = $"ekoekyc";

//                PropertyInfo[] infos = model.GetType().GetProperties();

//                Dictionary<string, string> dictionary = new Dictionary<string, string>();

//                foreach (PropertyInfo info in infos)
//                {
//                    dictionary.Add(info.Name.ToLower(), Convert.ToString(info.GetValue(model, null)));
//                }

//                dictionary.Add("initiator_id", _initiatorId.ToString());

//                //var json = JsonConvert.SerializeObject(model);
//                var response = await _dmtService.PostAsync(baseUrl, url, dictionary);

//                if (response.StatusCode != HttpStatusCode.OK)
//                {
//                    throw new Exception(response.ReasonPhrase);
//                }

//                string responseString = await response.Content.ReadAsStringAsync();

//                Utility.SaveRequestAndResponseLog("Aeps", $"DoEkyc Response Log  --> {responseString}");

//                var dto = JsonConvert.DeserializeObject<object>(responseString);

//                return Ok(dto);

//            }
//            catch (Exception ex)
//            {
//                Utility.WriteLog("Aeps", $"DoEkyc Error { ex.Message}");
//                Utility.WriteLog("Aeps", $"DoEkyc Error { ex.InnerException.ToString()}");
//                Utility.WriteLog("Aeps", $"DoEkyc Error {ex.StackTrace}");

//                return BadRequest(ex.Message);
//            }
//        }
//    }
//}
