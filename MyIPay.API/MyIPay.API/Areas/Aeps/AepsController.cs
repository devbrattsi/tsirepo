﻿using MyIPay.Dtos.Areas.Aeps;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Aeps;
using MyIPay.Services.Areas.Aeps.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace MyIPay.API.Areas.Aeps
{

    [RoutePrefix("api/Aeps")]
    public class AepsController : ApiController
    {
        private readonly IAepsService _aepsService;
        private readonly IPaymentService _paymentService;

        public AepsController(IAepsService aepsService, IPaymentService paymentService)
        {
            _aepsService = aepsService;
            _paymentService = paymentService;
        }

        [Route("api/Aeps/GetBankDetailsAsync")]
        [HttpGet]
        public async Task<IHttpActionResult> GetBankDetailsAsync()
        {
            try
            {

                var result = await _aepsService.GetBankDetailsAsync();

                string responseString = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    return BadRequest(responseString);
                }
                var json = JsonConvert.DeserializeObject<BankDetailDto>(responseString);

                return Ok(json);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("Aeps_Error", $"GetBankDetailsAsync Error Log :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"GetBankDetailsAsync Error Log :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"GetBankDetailsAsync Error Log :- {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        [Route("api/Aeps/CashWithdrawalAsync")]
        [HttpPost]
        public async Task<IHttpActionResult> CashWithdrawalAsync(CashWithdrawalModel model)
        {
            try
            {
                //if (!ModelState.IsValid)
                //    return BadRequest(ModelState);


                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                if (Request.Headers.TryGetValues(Constants.TransactionTimestamp, out IEnumerable<string> transactionTimestampValues)
                    && Request.Headers.TryGetValues(Constants.Hash, out IEnumerable<string> hashValues)
                    && Request.Headers.TryGetValues(Constants.DeviceIMEI, out IEnumerable<string> deviceIMEIValues)
                    && Request.Headers.TryGetValues(Constants.Eskey, out IEnumerable<string> eskeyValues)
                    && Request.Headers.TryGetValues(Constants.EncryptedJson, out IEnumerable<string> encryptedJsonValues)
                    )
                {
                    dictionary.Add(Constants.TransactionTimestamp, transactionTimestampValues.FirstOrDefault());
                    dictionary.Add(Constants.Hash, hashValues.FirstOrDefault());
                    dictionary.Add(Constants.DeviceIMEI, deviceIMEIValues.FirstOrDefault());
                    dictionary.Add(Constants.Eskey, eskeyValues.FirstOrDefault());
                    dictionary.Add(Constants.EncryptedJson, encryptedJsonValues.FirstOrDefault());

                    ////Logging request data
                    //Utility.SaveRequestAndResponseLog("Aeps", $"CashWithdrawalAsync Header Value Log, Timestamp, :- {transactionTimestampValues.FirstOrDefault()}");
                    //Utility.SaveRequestAndResponseLog("Aeps", $"CashWithdrawalAsync Header Value Log, DeviceIMEI :- {deviceIMEIValues.FirstOrDefault()}");

                }
                else
                {
                    return BadRequest("Header missing.");
                }


                var result = await _aepsService.CashWithdrawalAsync(model, dictionary);

                string responseString = await result.Content.ReadAsStringAsync();



                Utility.SaveRequestAndResponseLog("Aeps", $"CashWithdrawalAsync Response Log :- {responseString}");

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    return BadRequest(responseString);
                }

                var dto = JsonConvert.DeserializeObject<CashWithdrawalDto>(responseString);

                return Ok(dto);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("Aeps_Error", $"CashWithdrawalAsync Error Log :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"CashWithdrawalAsync Error Log :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"CashWithdrawalAsync Error Log :- {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        [Route("api/Aeps/BalanceInquiryAsync")]
        [HttpPost]
        public async Task<IHttpActionResult> BalanceInquiryAsync(BalanceInquiryModel model)
        {
            try
            {
                //if (!ModelState.IsValid)
                //    return BadRequest(ModelState);



                Dictionary<string, string> dictionary = new Dictionary<string, string>();


                if (Request.Headers.TryGetValues(Constants.TransactionTimestamp, out IEnumerable<string> transactionTimestampValues)
                    && Request.Headers.TryGetValues(Constants.Hash, out IEnumerable<string> hashValues)
                    && Request.Headers.TryGetValues(Constants.DeviceIMEI, out IEnumerable<string> deviceIMEIValues)
                    && Request.Headers.TryGetValues(Constants.Eskey, out IEnumerable<string> eskeyValues)
                    && Request.Headers.TryGetValues(Constants.EncryptedJson, out IEnumerable<string> encryptedJsonValues)
                    )
                {

                    dictionary.Add(Constants.TransactionTimestamp, transactionTimestampValues.FirstOrDefault());
                    dictionary.Add(Constants.Hash, hashValues.FirstOrDefault());
                    dictionary.Add(Constants.DeviceIMEI, deviceIMEIValues.FirstOrDefault());
                    dictionary.Add(Constants.Eskey, eskeyValues.FirstOrDefault());
                    dictionary.Add(Constants.EncryptedJson, encryptedJsonValues.FirstOrDefault());

                    ////Logging request data
                    //Utility.SaveRequestAndResponseLog("Aeps", $"BalanceInquiryAsync Header Value Log, Timestamp, :- {transactionTimestampValues.FirstOrDefault()}");
                    //Utility.SaveRequestAndResponseLog("Aeps", $"BalanceInquiryAsync Header Value Log, DeviceIMEI :- {deviceIMEIValues.FirstOrDefault()}");
                }
                else
                {
                    return BadRequest("Header missing.");
                }


                var result = await _aepsService.BalanceInquiryAsync(model, dictionary);

                string responseString = await result.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("Aeps", $"BalanceInquiryAsync Response Log :- {responseString}");

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    return BadRequest(responseString);
                }

                var dto = JsonConvert.DeserializeObject<BalanceInquiryDto>(responseString);

                return Ok(dto);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("Aeps_Error", $"BalanceInquiryAsync Error Log :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"BalanceInquiryAsync Error Log :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"BalanceInquiryAsync Error Log :- {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        [Route("api/Aeps/AadhaarPayAsync")]
        [HttpPost]
        public async Task<IHttpActionResult> AadhaarPayAsync(AadhaarPayModel model)
        {
            try
            {
                //if (!ModelState.IsValid)
                //    return BadRequest(ModelState);


                var result = await _aepsService.AadhaarPayAsync(model);

                string responseString = await result.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("Aeps", $"AadhaarPayAsync Response Log :- {responseString}");

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    return BadRequest(responseString);
                }

                var dto = JsonConvert.DeserializeObject<AadhaarPayDto>(responseString);

                return Ok(dto);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("Aeps_Error", $"AadhaarPayAsync Error Log :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"AadhaarPayAsync Error Log :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"AadhaarPayAsync Error Log :- {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [Route("api/Aeps/CashWithdrawalStatusCheckAsync")]
        public async Task<IHttpActionResult> CashWithdrawalStatusCheckAsync([FromBody]CashWithdrawalStatusCheckModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                Utility.SaveRequestAndResponseLog("Aeps", $"CashWithdrawalStatusCheckAsync Request Log :- {JsonConvert.SerializeObject(model)}");


                var result = await _aepsService.CashWithdrawalStatusCheckAsync(model);

                string responseString = await result.Content.ReadAsStringAsync();

                Utility.SaveRequestAndResponseLog("Aeps", $"CashWithdrawalStatusCheckAsync Response Log :- {responseString}");

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    return BadRequest(responseString);
                }

                var dto = JsonConvert.DeserializeObject<CashWithdrawalStatusCheckDto>(responseString);

                return Ok(dto);
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("Aeps_Error", $"CashWithdrawalStatusCheckAsync Error Log :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"CashWithdrawalStatusCheckAsync Error Log :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("Aeps_Error", $"CashWithdrawalStatusCheckAsync Error Log :- {ex.StackTrace}");

                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [Route("api/Tsi/DoPaymentAsync")]
        public async Task<IHttpActionResult> DoPaymentAsync(AepsTransactionModel model)
        {

            Utility.WriteLog("Aeps", $"DoPaymentAsync Request Json {JsonConvert.SerializeObject(model)}");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(await _paymentService.DoPaymentAsync(model));
        }

        //[HttpPost]
        //[Route("api/Tsi/CheckTransactionStatusAsync")]
        //public async Task<IHttpActionResult> CheckTransactionStatusAsync(CheckTransactionStatusModel model)
        //{

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    return Ok(await _paymentService.CheckTransactionStatusAsync(model));
        //}

        [HttpPost]
        [Route("api/Aeps/CreateMerchantAsync")]
        public async Task<IHttpActionResult> CreateMerchantAsync(CreateMerchantModel model)
        {
            Utility.WriteLog("Aeps", $"CreateMerchantAsync Request Json {JsonConvert.SerializeObject(model)}");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            HttpResponseMessage result = await _aepsService.CreateMerchantAsync(model);

            string responseString = await result.Content.ReadAsStringAsync();

            Utility.SaveRequestAndResponseLog("Aeps", $"CreateMerchantAsync Response Log :- {responseString}");

            if (result.StatusCode != HttpStatusCode.OK)
            {
                return BadRequest(responseString);
            }

            var dto = JsonConvert.DeserializeObject<CreateMerchantDto>(responseString);

            return Ok(dto);
        }
    }
}

