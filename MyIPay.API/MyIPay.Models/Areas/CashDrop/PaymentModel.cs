﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.CashDrop
{
    public class PaymentModel
    {
        [Required]
        public string Amount { get; set; }

        [Required]
        public string FeSessionId { get; set; }

        [Required]
        public string ContractNo { get; set; }

        [Required]
        public string Company { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string PartnerTxnId { get; set; }

        [Required]
        public string CustomerId { get; set; }

        [Required]
        public string AssistCustId { get; set; }

        [Required]
        public string Hash { get; set; }

        [Required]
        public string PartnerId { get; set; }
    }
}
