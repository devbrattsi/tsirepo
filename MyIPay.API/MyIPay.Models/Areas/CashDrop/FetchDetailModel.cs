﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.CashDrop
{
    public class FetchDetailModel
    {

        //[Required]
        public string MerchantName { get; set; }

        //[Required]
        public string FeSessionId { get; set; }

        //[Required]
        public string Hash { get; set; }

        //[Required]
        public string PartnerId { get; set; }
    }
}
