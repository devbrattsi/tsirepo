﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.PrabhuBank
{
    public class SendTransactionModel
    {
        public int? CustomerId { get; set; }

        [Required]
        [StringLength(100)]
        public string SenderName { get; set; }

        [Required]
        [StringLength(10)]
        public string SenderGender { get; set; }

        [Required]
        [StringLength(10)]
        [JsonProperty("SenderDoB")]
        public string SenderDob { get; set; }

        [Required]
        [StringLength(500)]
        public string SenderAddress { get; set; }

        public string SenderPhone { get; set; }

        [Required]
        public string SenderMobile { get; set; }

        [StringLength(100)]
        public string SenderCity { get; set; }

        [StringLength(100)]
        public string SenderDistrict { get; set; }

        [StringLength(100)]
        public string SenderState { get; set; }

        [Required]
        [StringLength(100)]
        public string SenderStateCode { get; set; }

        [Required]
        [StringLength(100)]
        public string SenderNationality { get; set; }

        [Required]
        [StringLength(100)]
        public string Employer { get; set; }

        [Required]
        [StringLength(50)]
        public string SenderIDType { get; set; }

        [Required]
        [StringLength(20)]
        public string SenderIDNumber { get; set; }

        //Format YYYY-MM-DD
        [StringLength(10)]
        [JsonProperty("SenderIDExpiryDate")]

        public string SenderIdExpiryDate { get; set; }

        [StringLength(50)]
        [JsonProperty("SenderIDIssuedPlace")]
        public string SenderIdIssuedPlace { get; set; }

        [Required]
        [StringLength(50)]
        public string ReceiverName { get; set; }

        [Required]
        [StringLength(10)]
        public string ReceiverGender { get; set; }

        [Required]
        [StringLength(200)]
        public string ReceiverAddress { get; set; }

        [StringLength(20)]
        public string ReceiverPhone { get; set; }

        [Required]
        [StringLength(20)]
        public string ReceiverMobile { get; set; }

        [StringLength(100)]
        public string ReceiverCity { get; set; }

        [StringLength(100)]
        public string ReceiverDistrict { get; set; }

        [StringLength(50)]
        public string ReceiverState { get; set; }

        [StringLength(50)]
        [JsonProperty("ReceiverIDType")]
        public string ReceiverIdType { get; set; }

        [StringLength(50)]
        [JsonProperty("ReceiverIDNumber")]
        public string ReceiverIdNumber { get; set; }

        [StringLength(10)]
        [JsonProperty("ReceiverIDExpiryDate")]
        public string ReceiverIdExpiryDate { get; set; }

        [StringLength(10)]
        [JsonProperty("ReceiverIDIssuedPlace")]
        public string ReceiverIdIssuedPlace { get; set; }

        // It’s Value will be Always India
        [Required]
        [StringLength(5)]
        public string SendCountry { get; set; }

        //It’s value will be Always Nepal
        [Required]
        [StringLength(5)]
        public string PayoutCountry { get; set; }

        [Required]
        [StringLength(50)]
        public string PaymentMode { get; set; }

        [Required]
        public string CollectedAmount { get; set; }

        [Required]
        public string ServiceCharge { get; set; }

        [Required]
        public string SendAmount { get; set; }

        //Its Value always “INR”
        [Required]
        [StringLength(3)]
        public string SendCurrency { get; set; }

        [Required]
        public string PayAmount { get; set; }

        //Its Value always “NPR”
        [Required]
        [StringLength(3)]
        public string PayCurrency { get; set; }

        //Its Value always 1.60 for now
        [Required]
        public string ExchangeRate { get; set; }
        public string BankBranchId { get; set; }

        [StringLength(22)]
        public string AccountNumber { get; set; }

        [StringLength(20)]
        public string AccountType { get; set; }

        [StringLength(1)]
        public string NewAccountRequest { get; set; }

        [Required]
        [StringLength(16)]
        public string PartnerPinNo { get; set; }

        [Required]
        [StringLength(20)]
        public string IncomeSource { get; set; }

        [Required]
        [StringLength(20)]
        public string RemittanceReason { get; set; }

        [Required]
        [StringLength(20)]
        public string Relationship { get; set; }

        [Required]
        [StringLength(2)]
        public string CSPStateCode { get; set; }

    }
}
