﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.PrabhuBank
{
    public class CancelTransactionModel
    {
        [Required]
        public string PinNo { get; set; }

        [Required]
        public string ReasonForCancellation { get; set; }
    }
}
