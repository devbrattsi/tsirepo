﻿using System.ComponentModel.DataAnnotations;
using System.IO;

namespace MyIPay.Models.Areas.PrabhuBank
{
    public class UploadCustomerDocumentModel
    {
        //Pin Number of Transaction for which you are sending transaction
        [Required]
        public string PinNo { get; set; }

        //Accepted File Extension Must be .jpg / .gif / .png / .pdf other will be rejected
        [Required]
        public string FileName { get; set; }

        //Accepted value must be Photo / ID card / KYC / Other
        [Required]
        public string DocumentType { get; set; }

        //Base64 Conversion of File data required to be uploaded
        [Required]
        public byte[] FileData { get; set; }
    }
}
