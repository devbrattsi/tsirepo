﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.PrabhuBank
{
    public class ServiceChargeModel
    {
        [Required]
        public string Country { get; set; }

        [Required]
        public string PaymentMode { get; set; }

        [Required]
        public string TransferAmount { get; set; }
        public string PayoutAmount { get; set; }
        public string BankBranchId { get; set; }
        public string IsNewAccount { get; set; }
    }
}
