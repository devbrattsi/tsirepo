﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.Toffee
{
    public class QueryPurchaseModel
    {
        public string Token { get; set; }
        [JsonProperty("toffee_id")]
        public string ToffeeId { get; set; }
    }
}
