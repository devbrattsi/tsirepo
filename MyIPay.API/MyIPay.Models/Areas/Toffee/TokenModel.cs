﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Toffee
{
    public class TokenModel
    {
        [Required]
        [JsonProperty(PropertyName = "grant_type")]
        public string GrantType { get; set; }
        [Required]
        [JsonProperty(PropertyName = "client_id")]
        public int ClientId { get; set; }
        [Required]
        [JsonProperty(PropertyName = "client_secret")]
        public string ClientSecret { get; set; }
        [Required]
        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }
        [Required]
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}
