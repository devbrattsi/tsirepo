﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.Toffee
{
    public class CityModel
    {
        [JsonProperty("state")]
        public int StateId { get; set; }
        public string Token { get; set; }
    }
}
