﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.Toffee
{
    public class InsurancePurchaseModel
    {
        [JsonProperty("insured_details")]
        public InsuredDetail InsuredDetails { get; set; }
        [JsonProperty("address")]
        public Address Addresses { get; set; }
        [JsonProperty("nominee")]
        public Nominee Nominees { get; set; }
        [JsonProperty("product_details")]
        public ProductDetail ProductDetails { get; set; }
        [JsonProperty("terms")]
        public int Terms { get; set; }
        [JsonProperty("payment_transaction_id")]
        public string PaymentTransactionId { get; set; }
        public string Token { get; set; }
    }
    public class InsuredDetail
    {
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("salutation")]
        public string Salutation { get; set; }
        [JsonProperty("date_of_birth")]
        public string DateOfBirth { get; set; }
    }

    public class Address
    {
        [JsonProperty("address_line_1")]
        public string AddressLine1 { get; set; }
        [JsonProperty("address_line_2")]
        public string AddressLine2 { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("pincode")]
        public int Pincode { get; set; }
    }

    public class Nominee
    {
        [JsonProperty("relationship_id")]
        public int RelationshipId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
    public class ProductDetail
    {
        [JsonProperty("product")]
        public string Product { get; set; }
    }
}
