﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Toffee
{
    public class RefreshTokenModel
    {
        [Required]
        [JsonProperty(PropertyName = "grant_type")]
        public string GrantType { get; set; }

        [Required]
        [JsonProperty(PropertyName = "client_id")]
        public int ClientId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "client_secret")]
        public string ClientSecret { get; set; }

        [Required]
        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }
    }
}
