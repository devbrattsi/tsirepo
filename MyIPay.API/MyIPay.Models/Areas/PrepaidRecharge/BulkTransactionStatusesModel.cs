﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class BulkTransactionStatusesModel : CommonModel
    {
        [JsonProperty(PropertyName = "service_family")]
        public int ServiceFamily { get; set; }

        [JsonProperty(PropertyName = "trans_ids")]
        public string TransactionIds { get; set; }

        [JsonProperty(PropertyName = "client_trans_id")]
        public string Client_Ref_Id { get; set; }
    }
}
