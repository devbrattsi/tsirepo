﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class ServiceDataResponseModel
    {
        [JsonProperty(PropertyName = "operator_name")]
        public string OperatorName { get; set; }

        [JsonProperty(PropertyName = "operator_code")]
        public int OperatorCode { get; set; }

        [JsonProperty(PropertyName = "margin")]
        public string Margin { get; set; }
    }
}
