﻿using Newtonsoft.Json;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class TransactionStatusModel:CommonModel
    {
        [JsonProperty(PropertyName = "service_family")]
        public int ServiceFamily { get; set; }

        [JsonProperty(PropertyName = "trans_id")]
        public string TransactionId { get; set; }

        [JsonProperty(PropertyName = "client_trans_id")]
        public string ClientRefId { get; set; }
    }
}
