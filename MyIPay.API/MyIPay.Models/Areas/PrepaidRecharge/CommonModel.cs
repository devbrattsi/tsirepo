﻿using MyIPay.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class CommonModel
    {
        [JsonProperty(PropertyName = "goid")]
        public long GoId { get; set; } = AppSettings.Goid;

        [JsonProperty(PropertyName = "apikey")]
        public string ApiKey { get; set; } = AppSettings.MobileApiKey;

        [JsonProperty(PropertyName = "rtype")]
        public string ReturnType { get; set; } = AppSettings.ReturnType;
    }
}
