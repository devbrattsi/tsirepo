﻿using Newtonsoft.Json;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class MsisdnInfoResponseModel
    {
        [JsonProperty(PropertyName = "operator_code")]
        public string OperatorCode { get; set; }

        [JsonProperty(PropertyName = "circle_code")]
        public string CircleCode { get; set; }
    }
}
