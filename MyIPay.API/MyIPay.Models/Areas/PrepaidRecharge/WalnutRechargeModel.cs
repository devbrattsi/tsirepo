﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class WalnutRechargeModel
    {
        [JsonProperty(PropertyName ="to")]
        public string MobileNumber { get; set; }
        [JsonProperty(PropertyName = "amount")]
        public int RechargeAmount { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string RechargeType { get; set; }
        [JsonProperty(PropertyName = "operator")]
        public int Operator { get; set; }
        [JsonProperty(PropertyName = "circle")]
        public int Circle { get; set; }
        [JsonProperty(PropertyName = "pid")]
        public string PosId { get; set; }
        [JsonProperty(PropertyName = "transId")]
        public string ClientReferenceId { get; set; }
        public string RechargeFor { get; set; }
    }
}
