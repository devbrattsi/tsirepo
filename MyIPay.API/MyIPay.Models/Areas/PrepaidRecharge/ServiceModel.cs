﻿using Newtonsoft.Json;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class ServiceModel : CommonModel
    {
        [JsonProperty(PropertyName = "operator_code")]
        public int OperatorCode { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public string Amount { get; set; }

        [JsonProperty(PropertyName = "custid")]
        public string CustomerId { get; set; }

        [JsonProperty(PropertyName = "msisdn")]
        public long Msisdn { get; set; }

        [JsonProperty(PropertyName = "client_trans_id")]
        public string ClientTransId { get; set; }

        [JsonProperty(PropertyName = "service_family")]
        public int ServiceFamily { get; set; }

        [JsonProperty(PropertyName = "apimode")]
        public string ApiMode { get; set; }
     
    }
}
