﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class WalnutRechargePlanModel
    {
        public string ClientReferenceId { get; set; }
        public int OperatorId { get; set; }
        public int CircleId { get; set; }
        public string RechargeType { get; set; }
    }
}
