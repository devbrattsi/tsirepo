﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class CircleInfoModel : CommonModel
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "circle";
    }

    public class OperatorListRequestModel : CommonModel
    {
        [JsonProperty(PropertyName = "service_family")]
        public int ServiceFamily { get; set; }
    }

    public class BalanceInfoRequestModel : CommonModel
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = "balance";
    }

    public class MsisdnInfoRequestModel : CommonModel
    {
        [JsonProperty(PropertyName = "msisdn")]
        public long Number { get; set; }

        [JsonProperty(PropertyName = "service_family")]
        public int ServiceFamily { get; set; }
    }
}
