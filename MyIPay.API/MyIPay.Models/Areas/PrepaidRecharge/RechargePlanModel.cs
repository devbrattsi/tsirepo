﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class RechargePlanModel : CommonModel
    {
        [JsonProperty(PropertyName = "service_family")]
        public string ServiceFamily { get; set; }

        [JsonProperty(PropertyName = "operator_code")]
        public string OperatorCode { get; set; }

        [JsonProperty(PropertyName = "circle_code")]
        public string CircleCode { get; set; }
    }
}
