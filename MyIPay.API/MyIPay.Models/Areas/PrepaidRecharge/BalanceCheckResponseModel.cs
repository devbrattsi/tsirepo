﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.PrepaidRecharge
{
    public class BalanceCheckResponseModel
    {
        [JsonProperty(PropertyName = "goid")]
        public string GoId { get; set; }

        [JsonProperty(PropertyName = "balance")]
        public string Balance { get; set; }
    }
}
