﻿using System.Collections.Generic;

namespace MyIPay.Models.Areas.Common
{
    public class SendEmailModel
    {
        public string toAddress { get; set; }
        public string messageBody { get; set; }
        public string emailSubject { get; set; }
        public List<string> attachFileName { get; set; }
        public List<byte[]> attachmentFile { get; set; }

        public List<string> ccList { get; set; }

        public List<string> bccList { get; set; }
    }
}
