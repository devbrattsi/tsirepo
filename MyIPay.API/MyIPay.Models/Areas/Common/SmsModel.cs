﻿using System.Collections.Generic;

namespace MyIPay.Models.Areas.Common
{
    public class SmsModel
    {
        public List<string> mobileNumber { get; set; }
        public string messageBody { get; set; }
    }
}
