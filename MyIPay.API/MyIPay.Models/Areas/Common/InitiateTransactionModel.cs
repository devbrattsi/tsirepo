﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Common
{

    public partial class InitiateTransactionModel
    {

        //  public int ID { get; set; }
        [Required]
        [JsonProperty("ApplicationMode")]
        public string Mode { get; set; }

        [Required]
        [JsonProperty("AgentId")]
        public string AGENTID { get; set; }

        [Required]
        [JsonProperty("UtilityAbbreviation")]
        public string USP { get; set; }

        [JsonProperty("ReceiptNo")]
        public int? RCPTNO { get; set; }

        [JsonProperty("ReceiptReference")]
        public string RCPTREF { get; set; }

        [JsonProperty("BatchNo")]
        public int? BATCHNO { get; set; }

        [Required]
        [JsonProperty("PaymentMode")]
        public string PAYMENTMODE { get; set; }

        [Required]
        [JsonProperty("PaymentDate")]
        [DataType(DataType.DateTime)]
        public DateTime PAYMENTDATE { get; set; }

        [Required]
        [JsonProperty("BillAmount")]
        public decimal BILLAMOUNT { get; set; }

        [Required]
        [JsonProperty("CollectionAmount")]
        public decimal COLLECTIONAMT { get; set; }

        //[Required]
        //[JsonProperty("BatchPayAmount")]
        //public decimal BATCHPAYMODEAMT { get; set; }

        [Required]
        [JsonProperty("Mobile")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(10)]
        public string mobileno { get; set; }

        [Required]
        [JsonProperty("Email")]
        [DataType(DataType.EmailAddress)]
        public string mailid { get; set; }

        [Required]
        [JsonProperty("Category")]
        public string REFERENCE1 { get; set; }

        [JsonProperty("BillerName")]
        public string REFERENCE2 { get; set; }

        [JsonProperty("BillerId")]
        public string REFERENCE3 { get; set; }

        [Required]
        //Customer reference with -
        [JsonProperty("CustomerReference")]
        public string REFERENCE4 { get; set; }

        //[JsonProperty("TransactionId")]
        //public string REFERENCE5 { get; set; }

        [Required]
        [JsonProperty("Ccf")]
        public string REFERENCE6 { get; set; }

        [Required]
        [JsonProperty("ClientRefId")]
        public string REFERENCE7 { get; set; }

        [JsonProperty("SessionId")]
        public string REFERENCE8 { get; set; }

        [JsonProperty("RemoteIpAddress")]
        public string REFERENCE9 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE10 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE11 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE12 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE13 { get; set; }

        [JsonProperty("TsiMargin")]
        public decimal REFERENCE14 { get; set; }

        //[JsonProperty("")]

        //public string REFERENCE15 { get; set; }

        [JsonProperty("AgentCashbackAmount")]
        public decimal REFERENCE16 { get; set; }

        [JsonProperty("AgentTdsAmount")]
        public string REFERENCE17 { get; set; }

        [JsonProperty("DistributorCashbackAmount")]
        public string REFERENCE18 { get; set; }

        [JsonProperty("DistributorTdsAmount")]
        public string REFERENCE19 { get; set; }

        [JsonProperty("CustomerFees")]
        public string REFERENCE20 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE21 { get; set; }

        [Required]
        [JsonProperty("PartnerName")]
        public string REFERENCE22 { get; set; }

        [Required]
        [JsonProperty("ApplicationType")]
        public string REFERENCE23 { get; set; }

        [Required]
        [JsonProperty("AppVersion")]
        public string REFERENCE24 { get; set; }

        [Required]
        [JsonProperty("DeviceInfo")]
        public string REFERENCE25 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE26 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE27 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE28 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE29 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE30 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE31 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE32 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE33 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE34 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE35 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE36 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE37 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE38 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE39 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE40 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE41 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE42 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE43 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE44 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE45 { get; set; }

        //[JsonProperty("")]
        //public string REFERENCE46 { get; set; }

        [JsonProperty("ChequeClearedOn")]
        public Nullable<System.DateTime> REFERENCEDATE1 { get; set; }

        //[JsonProperty("")]
        //public Nullable<System.DateTime> REFERENCEDATE2 { get; set; }

        //[JsonProperty("")]
        //public Nullable<long> REFERENCEINT1 { get; set; }

        //[JsonProperty("")]
        //public Nullable<long> REFERENCEINT2 { get; set; }

        [JsonProperty("AvailableBalance")]
        public Nullable<decimal> REFERENCEDECIMAL1 { get; set; }

        //[JsonProperty("")]
        //public Nullable<decimal> REFERENCEDECIMAL2 { get; set; }

        [JsonProperty("ChequeNo")]
        public string CHEQUENO { get; set; }

        [JsonProperty("ChequeDate")]
        public string CHEQUEDT { get; set; }

        [JsonProperty("MicrCode")]
        public string MICR { get; set; }

        [JsonProperty("BankCode")]
        public string BANKCODE { get; set; }

        //[JsonProperty("")]
        //public string TrType { get; set; }

        //[JsonProperty("")]
        //public string BR_CODE { get; set; }

        //[JsonProperty("")]
        //public string BankShortName { get; set; }

        //[JsonProperty("")]
        //public string BankName { get; set; }

        //[JsonProperty("")]
        //public string ACTYPE { get; set; }

        //[JsonProperty("")]
        //public Nullable<bool> PostDated { get; set; }

        //[JsonProperty("")]
        //public Nullable<bool> ChequeReturned { get; set; }

        //[JsonProperty("")]
        //public byte[] FrontImage { get; set; }

        //[JsonProperty("")]
        //public string ImageName { get; set; }

        //[JsonProperty("")]
        //public Nullable<decimal> ImageSize { get; set; }

        //[JsonProperty("")]
        //public byte[] RECEIPTIMAGE { get; set; }

        //[JsonProperty("")]
        //public string ReportStatus { get; set; }

        //[JsonProperty("")]
        //public string UPDATIONSTATUS { get; set; }

        //[JsonProperty("")]
        //public string UpdationMessage { get; set; }

        //[JsonProperty("")]
        //public Nullable<long> ReportBatchNo { get; set; }

        //[JsonProperty("")]
        //public Nullable<System.DateTime> CreatedOn { get; set; }

        //[JsonProperty("")]
        //public string CreatedBy { get; set; }

        //[JsonProperty("")]
        //public Nullable<System.DateTime> UpdatedOn { get; set; }

        //[JsonProperty("")]
        //public string UpdatedBy { get; set; }

        //[JsonProperty("")]
        //public string ENACCNO { get; set; }

        //[JsonProperty("")]
        //public string ENCOLLAMT { get; set; }

        //[JsonProperty("")]
        //public int update_token { get; set; }

        //[JsonProperty("")]
        //public string Ed { get; set; }

        //[JsonProperty("")]
        //public string Idf { get; set; }

        //[JsonProperty("")]
        //public string BillCyc { get; set; }

        //[JsonProperty("")]
        //public string BillGrp { get; set; }

        //[JsonProperty("")]
        //public string ApprovalCode { get; set; }

        //[JsonProperty("")]
        //public string CardNo { get; set; }

        //[JsonProperty("")]
        //public string ExpDate { get; set; }


        //[JsonProperty("")]
        //public string CardType { get; set; }

        //[JsonProperty("")]
        //public string InvoiceNo { get; set; }

        [JsonProperty("IsChequeCleared")]
        public bool? IsChequeCleared { get; set; }

        [Required]
        [JsonProperty("DistributorId")]
        public int DistributorId { get; set; }
    }

}
