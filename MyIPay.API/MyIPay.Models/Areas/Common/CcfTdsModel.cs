﻿namespace MyIPay.Models.Areas.Common
{
    public class CcfTdsModel
    {
        public int CcfModel { get; set; }
        public decimal CcfAmount { get; set; }
        public decimal DistributorCashbackAmount { get; set; }
        public decimal DistributorTdsAmount { get; set; }
        public decimal Margin { get; set; }
        public decimal TsiMargin { get; set; }
        public decimal AgentCashbackAmount { get; set; }
        public decimal AgentTdsAmount { get; set; }
        public decimal CustomerFees { get; set; }
    }
}
