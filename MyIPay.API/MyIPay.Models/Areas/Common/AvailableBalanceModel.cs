﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Common
{
    public class AvailableBalanceModel
    {
        [Required]
        [StringLength(10)]
        public string TransactionId { get; set; }

        [Required]
        public string AgentId { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public string SecretKeyTimeStamp { get; set; }

        [Required]
        public string SecretKey { get; set; }
    }
}
