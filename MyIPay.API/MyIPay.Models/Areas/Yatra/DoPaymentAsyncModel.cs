﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Yatra
{
    public class DoPaymentAsyncModel
    {
        [Required]
        //Yatra Account Number
        public string MerchantId { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        //Order id of yatra
        public string RefernceNo { get; set; }

        [Required]
        public string AgentId { get; set; }

        [Required]
        public string Signature { get; set; }

       // [Required]
        public string Product { get; set; }

    }
}
