﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Yatra
{
    public class VerifyTokenModel
    {
        [Required]
        public string AuthenticationType { get; set; }

        [Required]
        public string Key { get; set; }

    }
}
