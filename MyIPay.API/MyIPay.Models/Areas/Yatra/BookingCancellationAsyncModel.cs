﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Yatra
{
    public class BookingCancellationAsyncModel
    {
        [Required]
        public string OrderId { get; set; }

        //[Required]
        //public string AgentId { get; set; }

        [Required]
        public string Amount { get; set; }

        [Required]
        public string RefundedOrderId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Signnature")]
        public string Signature { get; set; }

        [Required]
        //0 for Full Refund and 1 for partial Cancellation
        public string IsPartialCal { get; set; }

        [Required]
        public string RefundCategory { get; set; }

        // [Required]
        //public string Product { get; set; }
    }
}
