﻿
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Yatra
{
    public class DoYatraPaymentAsyncModel
    {
        // Yatra Account Number
        [Required]
        public string MerchantId { get; set; }
        //Transaction Amount in rupees with  two decimal place
        [Required]
        public decimal Amount { get; set; }
        // Order Id of Yatra 
        [Required]
        public string RefernceNo { get; set; }
        // Tsi Retailer Unique ID
        [Required]
        public string AgentId { get; set; }

        [Required]
        public string Signature { get; set; }
        //[Required]
        //public string Salt { get; set; }

    }
}
