﻿
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Yatra
{
    public class CheckBalanceAvailabilityAsyncModel
    {
        [Required]
        public string AgentId { get; set; }

        [Required]
        public string OrderId { get; set; }

        [Required]
        public decimal Amount { get; set; }
    }
}
