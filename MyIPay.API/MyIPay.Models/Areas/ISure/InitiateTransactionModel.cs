﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.ISure
{
    public class InitiateTransactionModel
    {
        [Required]
        [StringLength(10)]
        public string Client_Code { get; set; }

        [Required]
        [StringLength(20)]
        // public string Client_VAccount_No1 { get; set; }
        //Here AgentCode is virtual account no
        public string AgentCode { get; set; }

        [Required]
        public long Amount { get; set; }

        [Required]
        public string Transaction_Date { get; set; }

        [Required]
        [StringLength(15)]
        //Unique Alphanumeric id generated for the transaction by ibank
        public string IBANK_Transaction_Id { get; set; }

        [Required]
        [StringLength(2)]
        public string Pay_Mode { get; set; }

        [Required]
        //Unique 10 digit id generated for the transaction by bank.To be used for reconciliation.This number is available as Slip Number for non Ibank instruments and available in the clearing report.
        public string ISure_ID { get; set; }
        //
        public long? MICR_CODE { get; set; }

        [StringLength(25)]
        public string Bank_Name { get; set; }

        [StringLength(50)]
        public string Branch_Name { get; set; }

        public long? Instrument_Number { get; set; }

        public string Instrument_Date { get; set; }
        //
        [Required]
        [StringLength(10)]
        public string UserId { get; set; }

        [Required]
        [StringLength(15)]
        public string UserPwd { get; set; }

    }
}
