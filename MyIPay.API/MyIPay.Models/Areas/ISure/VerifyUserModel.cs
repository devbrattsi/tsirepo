﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.ISure
{
    public class VerifyUserModel
    {
        [Required]
        [StringLength(10)]
        public string Client_Code { get; set; }

        [Required]
        [StringLength(20)]
       // public string Client_VAccount_No1 { get; set; }
       //Here AgentCode is virtual account no
        public string AgentCode { get; set; }

        [Required]
        [StringLength(10)]
        public string UserId { get; set; }

        [Required]
        [StringLength(15)]
        public string UserPwd { get; set; }

    }
}
