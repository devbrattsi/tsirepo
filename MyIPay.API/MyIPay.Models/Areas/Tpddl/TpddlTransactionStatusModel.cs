﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.Tpddl
{
    public class TpddlTransactionStatusModel
    {
        [Required]
        public string ReceiptNo { get; set; }
        [Required]
        public string CaNumber { get; set; }
    }
}
