﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.Tpddl
{
    public class TpddlPayModel
    {
        [Required]
        public string AgentId { get; set; }
        [Required]
        public string CaNumber { get; set; }
        //[Required]
        //public string BillNumber { get; set; }
        //[Required]
        //public string OrderId { get; set; }
        //[Required]
        //public string ReceiptId { get; set; }
        [Required]
        public string Amount { get; set; }
        [Required]
        public string TransactionId { get; set; }
        public string BankName { get; set; } 
      //  public string BankCode { get; set; } 
        [Required]
        public string InvoiceNo { get; set; }
        [Required]
        public string ReceiptNo { get; set; }
        public string TransactionStatus { get; set; }
        public string TransactionMessage { get; set; }
        [Required]
        public string IpAddress { get; set; }
        [Required]
        public string PaymentMode { get; set; }
    }
}
