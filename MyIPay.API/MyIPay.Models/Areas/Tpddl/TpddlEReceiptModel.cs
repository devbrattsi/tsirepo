﻿using Newtonsoft.Json;

namespace MyIPay.Models.Areas.Tpddl
{
    public class TpddlEReceiptModel
    {
        //[JsonProperty(PropertyName = "cano")]
        public string ConsumerNumber { get; set; }

        //[JsonProperty(PropertyName = "rcptno")]
        public string ReceiptNumber { get; set; }

        //[JsonProperty(PropertyName = "contactDetails")]
        public string ContactDetails { get; set; }

        //[JsonProperty(PropertyName = "cashierName")]
        public string CashierName { get; set; }

        //[JsonProperty(PropertyName = "counterNo")]
        public string CounterNumber { get; set; }

        //[JsonProperty(PropertyName = "mode")]
        public string Mode { get; set; }

        //[JsonProperty(PropertyName = "amount")]
        public string Amount { get; set; }

    }
}

