﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Aeps
{

    public class CardNumberOrUid
    {
        [Required]
        public string adhaarNumber { get; set; }

        [Required]
        public int indicatorforUID { get; set; }

        [Required]
        public string nationalBankIdentificationNumber { get; set; }
    }
}
