﻿using MyIPay.Models.Areas.Aeps;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Aeps
{
    public class AadhaarPayModel
    {

        [Required]
        [JsonProperty(PropertyName = "merchantTranId")]
        public string MerchantTranId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "captureResponse")]
        public CaptureResponse CaptureResponse { get; set; }

        [Required]
        [JsonProperty(PropertyName = "cardnumberORUID")]
        public CardNumberOrUid CardnumberOrUid { get; set; }

        [Required]
        [JsonProperty(PropertyName = "languageCode")]
        public string LanguageCode { get; set; }

        [Required]
        [JsonProperty(PropertyName = "latitude")]
        public double Latitude { get; set; }

        [Required]
        [JsonProperty(PropertyName = "longitude")]
        public double Longitude { get; set; }

        [Required]
        [JsonProperty(PropertyName = "mobileNumber")]
        public string MobileNumber { get; set; }

        [Required]
        [JsonProperty(PropertyName = "paymentType")]
        public string PaymentType { get; set; }

        //[Required]
        [JsonProperty(PropertyName = "requestRemarks")]
        public string RequestRemarks { get; set; }

        [Required]
        [JsonProperty(PropertyName = "timestamp")]
        public string Timestamp { get; set; }

        [Required]
        [JsonProperty(PropertyName = "transactionAmount")]
        public double TransactionAmount { get; set; }

        [Required]
        [JsonProperty(PropertyName = "transactionType")]
        public string TransactionType { get; set; }

        [Required]
        [JsonProperty(PropertyName = "merchantUserName")]
        public string MerchantUserName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "merchantPin")]
        public string MerchantPin { get; set; }

        [Required]
        [JsonProperty(PropertyName = "subMerchantId")]
        public string SubMerchantId { get; set; }
    }

    //public class CaptureResponse
    //{

    //    [Required]
    //    [JsonProperty(PropertyName = "PidDatatype")]
    //    public string PidDataType { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "Piddata")]
    //    public string PidData { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "ci")]
    //    public string Ci { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "dc")]
    //    public string Dc { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "dpID")]
    //    public string DpId { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "errCode")]
    //    public string ErrorCode { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "errInfo")]
    //    public string ErrorInfo { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "fCount")]
    //    public string FCount { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "fType")]
    //    public string FType { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "hmac")]
    //    public string Hmac { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "iCount")]
    //    public string ICount { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "mc")]
    //    public string Mc { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "mi")]
    //    public string Mi { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "nmPoints")]
    //    public string NmPoints { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "pCount")]
    //    public string PCount { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "pType")]
    //    public string PType { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "qScore")]
    //    public string QScore { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "rdsID")]
    //    public string RdsID { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "rdsVer")]
    //    public string RdsVer { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "sessionKey")]
    //    public string SessionKey { get; set; }
    //}


    //public class CardNumberOrUid
    //{
    //    [Required]
    //    [JsonProperty(PropertyName = "adhaarNumber")]
    //    public string AdhaarNumber { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "indicatorforUID")]
    //    public int IndicatorForUid { get; set; }

    //    [Required]
    //    [JsonProperty(PropertyName = "nationalBankIdentificationNumber")]
    //    public string NationalBankIdentificationNumber { get; set; }
    //}


}
