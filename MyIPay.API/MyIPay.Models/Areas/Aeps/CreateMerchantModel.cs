﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Aeps
{
    public class CreateMerchantModel
    {
        public CreateMerchantModel()
        {
            merchants = new List<MerchantModel>();
        }

        //This is username of  the super merchant
        public string username { get; set; }
        //This is password of  the  super merchant 
        public string password { get; set; }

        [Required]
        public DateTime timestamp { get; set; }

        [Required]
        public double latitude { get; set; }

        [Required]
        public double longitude { get; set; }
        public int supermerchantId { get; set; }

        [Required]
        public List<MerchantModel> merchants { get; set; }
    }

    public class MerchantModel
    {
        [Required]
        public string merchantLoginId { get; set; }
        public string merchantLoginPin { get; set; }
        public string merchantName { get; set; }
        public MerchantAddress merchantAddress { get; set; }
        public string merchantBranch { get; set; }
        public string merchantPhoneNumber { get; set; }
        public string companyLegalName { get; set; }
        public string companyMarketingName { get; set; }
        public Kyc kyc { get; set; }
        public Settlement settlement { get; set; }
        public string emailId { get; set; }
        public string shopAndPanImage { get; set; }
        public string cancellationCheckImages { get; set; }
        public string ekycDocuments { get; set; }
    }

    public class MerchantAddress
    {
        public string merchantAddress { get; set; }
        public string merchantState { get; set; }
    }
    public class Kyc
    {
        public string userPan { get; set; }
        public string aadhaarNumber { get; set; }
        public string gstInNumber { get; set; }
        public string companyOrShopPan { get; set; }
    }

    public class Settlement
    {
        public string companyBankAccountNumber { get; set; }
        public string bankIfscCode { get; set; }
        public string companyBankName { get; set; }
        public string bankBranchName { get; set; }
        public string bankAccountName { get; set; }
    }

}
