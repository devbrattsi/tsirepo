﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;


namespace MyIPay.Models.Areas.Aeps
{
    public class CaptureResponse
    {

        [Required]
        public string PidDatatype { get; set; }

        [Required]
        public string Piddata { get; set; }

        [Required]
        public string ci { get; set; }

        [Required]
        public string dc { get; set; }

        [Required]
        public string dpID { get; set; }

        [Required]
        public string errCode { get; set; }

        [Required]
        public string errInfo { get; set; }

        [Required]
        public string fCount { get; set; }

        [Required]
        public string fType { get; set; }

        [Required]
        public string hmac { get; set; }

        [Required]
        public string iCount { get; set; }

        [Required]
        public string mc { get; set; }

        [Required]
        public string mi { get; set; }

        [Required]
        public string nmPoints { get; set; }

        [Required]
        public string pCount { get; set; }

        [Required]
        public string pType { get; set; }

        [Required]
        public string qScore { get; set; }

        [Required]
        public string rdsID { get; set; }

        [Required]
        public string rdsVer { get; set; }

        [Required]
        public string sessionKey { get; set; }
    }

}
