﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Aeps
{
    public class CashWithdrawalStatusCheckModel
    {
        [Required]
        public string merchantLoginId { get; set; }

        [Required]
        public string merchantTranId { get; set; }

        [Required]
        public string hash { get; set; }
    }
}