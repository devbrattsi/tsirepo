﻿using MyIPay.Models.Areas.Aeps;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Aeps
{

    public class BalanceInquiryModel
    {

        [Required]
        // public string merchantTranId { get; set; }
        public string merchantTransactionId { get; set; }

        [Required]
        public CaptureResponse captureResponse { get; set; }

        [Required]
        public CardNumberOrUid cardnumberORUID { get; set; }

        [Required]
        public string languageCode { get; set; }

        [Required]
        public double latitude { get; set; }

        [Required]
        public double longitude { get; set; }

        [Required]
        public string mobileNumber { get; set; }

        [Required]
        public string paymentType { get; set; }

        public string requestRemarks { get; set; }

        [Required]
        public string timestamp { get; set; }

        [Required]
        public string transactionType { get; set; }

        [Required]
        public string merchantUserName { get; set; }

        [Required]
        public string merchantPin { get; set; }

        [Required]
        public string subMerchantId { get; set; }
    }
}
