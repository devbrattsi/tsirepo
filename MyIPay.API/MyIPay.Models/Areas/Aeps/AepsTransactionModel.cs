﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.Aeps
{
    public class AepsTransactionModel
    {
        [JsonProperty("ipaddress")]
        public string IpAddress { get; set; }

        [Required]
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [Required]
        [JsonProperty("transactionStatus")]
        public string TransactionStatus { get; set; }

        [JsonProperty("merchantRefNo")]
        public string MerchantRefNo { get; set; }

        [Required]
        [JsonProperty("fpTransactionId")]
        public string TransactionId { get; set; }

        [Required]
        [JsonProperty("aadhaarNumber")]
        public string AadhaarNumber { get; set; }

        [Required]
        [JsonProperty("typeOfTransaction")]
        public string TransactionType { get; set; }

        [JsonProperty("latitude")]
        public decimal Latitude { get; set; }

        [JsonProperty("longitude")]
        public decimal Longitude { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("bankRRN")]
        public string BankRRN { get; set; }

        [JsonProperty("merchantName")]
        public string MerchantName { get; set; }

        [JsonProperty("terminalID")]
        public string TerminalId { get; set; }

        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("requestedTimestamp")]
        public string RequestedTimestamp { get; set; }

        [Required]
        [JsonProperty("merchantID")]
        public string MerchantId { get; set; }

        [JsonProperty("deviceIMEI")]
        public string DeviceIMEI { get; set; }
    }
}
