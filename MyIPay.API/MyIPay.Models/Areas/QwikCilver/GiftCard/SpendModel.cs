﻿using MyIPay.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.QwikCilver.GiftCard
{
    public class SpendModel
    {
        public SpendModel()
        {
            PaymentMethod = new List<PaymentMethod>();
            Products = new List<Product>();
        }

        [Required]
        [JsonProperty("billing")]
        public Billing Billing { get; set; }

        [Required]
        [JsonProperty("shipping")]
        public Shipping Shipping { get; set; }

        [Required]
        [JsonProperty("payment_method")]
        [JsonConverter(typeof(SingleOrArrayConverter<PaymentMethod>))]
        public List<PaymentMethod> PaymentMethod { get; set; }

        [Required]
        [JsonProperty("refno")]
        public string RefNo { get; set; }

        [Required]
        [JsonProperty("delivery_mode")]
        public string DeliveryMode { get; set; }

        [Required]
        [JsonProperty("products")]
        [JsonConverter(typeof(SingleOrArrayConverter<Product>))]
        public List<Product> Products { get; set; }
    }

    public class Billing
    {
        [Required]
        [JsonProperty("firstname")]
        public string FirstName { get; set; }


        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [Required]
        [JsonProperty("line_1")]
        public string Line1 { get; set; }

        [Required]
        [JsonProperty("line_2")]
        public string Line2 { get; set; }

        [Required]
        [JsonProperty("city")]
        public string City { get; set; }

        [Required]
        [JsonProperty("region")]
        public string Region { get; set; }

        [Required]
        [JsonProperty("country_id")]
        public string CountryId { get; set; }

        [Required]
        [JsonProperty("postcode")]
        public string Postcode { get; set; }
    }

    public class Shipping
    {
        [Required]
        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [Required]
        [JsonProperty("line_1")]
        public string Line1 { get; set; }

        [Required]
        [JsonProperty("line_2")]
        public string Line2 { get; set; }

        [Required]
        [JsonProperty("city")]
        public string City { get; set; }

        [Required]
        [JsonProperty("region")]
        public string Region { get; set; }

        [Required]
        [JsonProperty("country_id")]
        public string CountryId { get; set; }

        [Required]
        [JsonProperty("postcode")]
        public string Postcode { get; set; }
    }

    public class PaymentMethod
    {

        [Required]
        [JsonProperty("method")]
        public string Method { get; set; }

        [Required]
        [JsonProperty("amount_to_redem")]
        public string AmountToRedem { get; set; }
    }

    public class Product
    {
        [Required]
        [JsonProperty("product_id")]
        public string ProductId { get; set; }

        [Required]
        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("Theme")]
        public string Theme { get; set; }

        [Required]
        [JsonProperty("qty")]
        public string Qty { get; set; }

        [JsonProperty("gift_message")]
        public string GiftMessage { get; set; }

        [JsonProperty("card")]
        public string Card { get; set; }
    }


}
