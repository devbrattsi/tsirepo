﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MpVidyutVitaran
{
    public class PayRmsBillModel
    {
        [Required]
        public string txn_no { get; set; }

        [Required]
        public string ivrs_no { get; set; }
        [Required]
        public string receipt_date { get; set; }
        [Required]
        public string bill_mon { get; set; }
        [Required]
        public string amount { get; set; }
        [Required]
        public string agency_id { get; set; }
        [Required]
        public string machine_id { get; set; }
        [Required]
        public string payment_mode_cd { get; set; }
        [Required]
        public string instru_no { get; set; }
        [Required]
        public string date_time { get; set; }
    }
}
