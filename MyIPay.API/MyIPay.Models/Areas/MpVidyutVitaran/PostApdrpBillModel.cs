﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MpVidyutVitaran
{
    public class PostApdrpBill
    {
        [Required]
        public string MerchantId { get; set; }

        [Required]
        public string OrderId { get; set; }

        [Required]
        public string TxnReferenceNo { get; set; }

        [Required]
        public string BankReferenceNo { get; set; }

        public string TxnAmount { get; set; }

        [Required]
        public string BankId { get; set; }

        [Required]
        public string BankMerchantId { get; set; }

        [Required]
        public string TxnType { get; set; }

        [Required]
        public string CurrencyName { get; set; }

        [Required]
        public string ItemCode { get; set; }

        [Required]
        public string SecurityType { get; set; }

        [Required]
        public string SecurityId { get; set; }

        [Required]
        public string SecurityPassword { get; set; }

        [Required]
        public string TxnDate { get; set; }

        [Required]
        public string AuthStatus { get; set; }

        [Required]
        public string SettlementType { get; set; }

        [Required]
        public string CustomerId { get; set; }

        [Required]
        public string RaoCode { get; set; }

        [Required]
        public string DcCode { get; set; }

        [Required]
        public string CustomerName { get; set; }

        [Required]
        public string DueDate { get; set; }

        [Required]
        public string AdditionalInfo1 { get; set; }

        [Required]
        public string AdditionalInfo2 { get; set; }

        [Required]
        public string AdditionalInfo3 { get; set; }

        [Required]
        public string TxnStatus { get; set; } 
      
    }
}
