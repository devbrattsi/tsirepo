﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class DoEkycModel
    {
        [Required]
        public string Mobile_Number { get; set; }
        [Required]
        public string Partner_Name { get; set; }
        [Required]
        public string Is_New_Window { get; set; }
    }
}
