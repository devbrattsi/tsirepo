﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class PanVerificationModel
    {
        [Required]
        public string Pan_Number { get; set; }

        [Required]
        public int Purpose { get; set; }

        [Required]
        public string Purpose_Desc { get; set; }

        [Required]
        public long Customer_Mobile { get; set; }

        //[Required]
        //public long Initiator_Id { get; set; }

    }
}
