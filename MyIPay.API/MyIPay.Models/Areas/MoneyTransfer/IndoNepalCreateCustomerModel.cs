﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class IndoNepalCreateCustomerModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Dob { get; set; }
        [Required]
        public int Gender { get; set; }
        [Required]
        public string Address_Line1 { get; set; }
        [Required]
        public string Nationality { get; set; }
        [Required]
        public string Id_Proof_Type_Id { get; set; }
        [Required]
        public string Id_Proof { get; set; }

        //public HttpPostedFileBase File1 { get; set; }
        //public HttpPostedFileBase File2 { get; set; }

        public byte[] File1 { get; set; }
        public byte[] File2 { get; set; }


        [Required]
        public string Income_source { get; set; }

        [Required]
        public string Remittance_Reason { get; set; }
        public byte[] Customer_Photo { get; set; }

        [Required]
        public string Pincode { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string City { get; set; }


    }
}
