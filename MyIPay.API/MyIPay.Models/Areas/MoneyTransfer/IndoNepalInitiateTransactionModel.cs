﻿

using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class IndoNepalInitiateTransactionModel
    {
        [Required]
        public long Recipient_Id { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        //timestamp (in TZ format)
        public string TimeStamp { get; set; }

        [Required]
        //INR
        public string Currency { get; set; }

        [Required]
        public long Customer_Id { get; set; }

        [Required]
        public string Client_Ref_Id { get; set; }

        [Required]
        public long Hold_Timeout { get; set; }
        [Required]
        public int State { get; set; }

        [Required]
        public string Product { get; set; }
    }
}
