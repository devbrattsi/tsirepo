﻿

using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class IndoNepalAddRecipientModel
    {

        [Required]
        public string Recipient_Name { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public string Address_Line1 { get; set; }

        [Required]
        public string Relationship_With_Sender { get; set; }

        [Required]
        public string District { get; set; }

        [Required]
        public string Recipient_Mobile { get; set; }

        public string Bank { get; set; }
        public string Branch { get; set; }
        public string AccountNumber { get; set; }
        public string CashPayoutLocation { get; set; }

        [Required]
        public string PaymentMode { get; set; }
    }
}
