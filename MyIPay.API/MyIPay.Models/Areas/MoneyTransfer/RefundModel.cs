﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class RefundModel
    {
        //[Required]
        //public long Initiator_Id { get; set; }

        [Required]
        public long Otp { get; set; }

        [Required]
        public int State { get; set; }

        public string Client_Ref_Id { get; set; }

    }
}
