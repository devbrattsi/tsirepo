﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class AddRecipientModel
    {
        //[Required]
        ////id of the customer; currently only mobile number of the customer can be used
        //public long Initiator_Id { get; set; }

        [Required]
        //a unique id for each bank has been allocated and that has to passed here
        public string Bank_Id { get; set; }

        [Required]
        //name of the recipient
        public string Recipient_Name { get; set; }

        [Required]
        public long Recipient_Mobile { get; set; }

        [Required]
        public int Recipient_Type { get; set; }

    }
}
