﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class VerifyCustomerIdentityModel
    {
        [Required]
        //Customer_Id_Type value is mobile_number
        public string Customer_Id_Type { get; set; }

        [Required]
        public long Customer_Id { get; set; }

        //[Required]
        //public string Initiator_Id { get; set; }
    }
}
