﻿using Newtonsoft.Json;

namespace MyIPay.Models.Areas.MoneyTransfer.Airtel
{
    public class ImpsTransactionModel
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("apiMode")]
        public string ApiMode { get; set; }

        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("beneAccNo")]
        public string BeneAccNo { get; set; }

        [JsonProperty("beneMobNo")]
        public string BeneMobNo { get; set; }

        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("customerId")]
        public string CustomerId { get; set; }

        [JsonProperty("externalRefNo")]
        public string ExternalRefNo { get; set; }

        [JsonProperty("feSessionId")]
        public string FeSessionId { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("ifsc")]
        public string Ifsc { get; set; }

        [JsonProperty("partnerId")]
        public string PartnerId { get; set; }

        [JsonProperty("reference1")]
        public string Reference1 { get; set; }

        [JsonProperty("reference2")]
        public string Reference2 { get; set; }

        [JsonProperty("reference3")]
        public string Reference3 { get; set; }

        [JsonProperty("reference4")]
        public string Reference4 { get; set; }

        [JsonProperty("reference5")]
        public string Reference5 { get; set; }

        [JsonProperty("ver")]
        public string Ver { get; set; }
    }

}
