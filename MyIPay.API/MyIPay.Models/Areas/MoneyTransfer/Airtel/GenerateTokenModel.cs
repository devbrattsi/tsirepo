﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Models.Areas.MoneyTransfer.Airtel
{
    public class GenerateTokenModel
    {
        public string ActorId { get; set; }
        public string ActorType { get; set; }
        public string FeSessionId { get; set; }
        public string JwtExpiry { get; set; }
        public string LanguageId { get; set; }
        public string Ver { get; set; }
    }
}
