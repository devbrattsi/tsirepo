﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer.Airtel
{
    public class CheckDistributorBalanceModel
    {
        [Required]
        [JsonProperty("channel")]
        public string Channel { get; set; }

        //[Required]
        [JsonProperty("customerId")]
        public string CustomerId { get; set; }

        [Required]
        [JsonProperty("feSessionId")]
        public string FeSessionId { get; set; }

        //[Required]
        [JsonProperty("hash")]
        public string Hash { get; set; }

        //[Required]
        [JsonProperty("partnerId")]
        public string PartnerId { get; set; }
    }
}
