﻿namespace MyIPay.Models.Areas.MoneyTransfer.Airtel
{
    public class GetBankByIfscCodeModel
    {
        public string IfscCode { get; set; }
        public string Token { get; set; }
    }

}
