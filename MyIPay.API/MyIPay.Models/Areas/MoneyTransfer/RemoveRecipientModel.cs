﻿

using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class RemoveRecipientModel
    {
        [Required]
        public string RecipientId { get; set; }
        [Required]
        public long Mobile { get; set; }
    }
}
