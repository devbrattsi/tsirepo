﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class InitiateTransactionModel
    {
        [Required]
        public long recipient_id { get; set; }

        [Required]
        public int amount { get; set; }

        [Required]
        public string timestamp { get; set; }

        [Required]
        //INR
        public string currency { get; set; }

        [Required]
        public long customer_id { get; set; }

        //[Required]
        //public long initiator_id { get; set; }

        [Required]
        //public long Client_Ref_Id { get; set; }
        public string client_ref_id { get; set; }


        public long hold_timeout { get; set; }
        [Required]
        public int state { get; set; }

        [Required]
        public int channel { get; set; }

        //1 - PAN, 2 - Aadhaar number
        public int merchant_document_id_type { get; set; }
        public string merchant_document_id { get; set; }
        public string latlong { get; set; }
        public int pincode { get; set; }
        public string user_pan { get; set; }


    }
}
