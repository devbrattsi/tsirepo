﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.MoneyTransfer
{
    public class ActivateEkycModel
    {
        //public string User_Code { get; set; }
        // public string Initiator_Id { get; set; }

        [Required]
        public string Service_Code { get; set; }
        public string Error_Redirect_Url { get; set; }
        public string Success_Redirect_Url { get; set; }
    }
}
