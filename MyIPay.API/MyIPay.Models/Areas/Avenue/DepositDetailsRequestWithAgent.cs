﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    /// <summary>
    /// DEPOSIT ENQUIRY REQUEST MODEL - FOR ANY SPECIFIC AGENT ID
    /// </summary>
    /// 
    #region DEPOSIT ENQUIRY REQUEST MODEL FOR ANY SPECIFIC AGENT ID
    [XmlRoot(ElementName = "agents")]
    public class Agents
    {
        [XmlElement(ElementName = "agentId")]
        public string AgentId { get; set; }
    }

    [XmlRoot(ElementName = "depositDetailsRequest")]
    public class DepositDetailsRequestWithAgent
    {
        [XmlElement(ElementName = "fromDate")]
        public string FromDate { get; set; }
        [XmlElement(ElementName = "toDate")]
        public string ToDate { get; set; }
        [XmlElement(ElementName = "transType")]
        public string TransType { get; set; }
        [XmlElement(ElementName = "agents")]
        public Agents Agents { get; set; }
    }
    #endregion
}
