﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    /// <summary>
    /// Requset Model for Complaint Registration Api
    /// </summary>
    #region Complaint Registration Requset Model
    [XmlRoot(ElementName = "complaintRegistrationReq")]
    public class ComplaintRegistrationReq
    {
        [XmlElement(ElementName = "complaintType")]
        public string ComplaintType { get; set; }
        [XmlElement(ElementName = "participationType")]
        public string ParticipationType { get; set; }
        [XmlElement(ElementName = "agentId")]
        public string AgentId { get; set; }
        [XmlElement(ElementName = "txnRefId")]
        public string TxnRefId { get; set; }
        [XmlElement(ElementName = "billerId")]
        public string BillerId { get; set; }
        [XmlElement(ElementName = "complaintDesc")]
        public string ComplaintDesc { get; set; }
        [XmlElement(ElementName = "servReason")]
        public string ServReason { get; set; }
        [XmlElement(ElementName = "complaintDisposition")]
        public string ComplaintDisposition { get; set; }
        public string RequestID { get; set; }
    }
    #endregion
}
