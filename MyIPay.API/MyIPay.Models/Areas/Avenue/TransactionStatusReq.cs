﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    /// <summary>
    /// Request Model for Transaction Status using TRANS_REF_ID or Mobile Number
    /// </summary>
    #region Transaction Status Request Model
    [XmlRoot(ElementName = "transactionStatusReq")]
    public class TransactionStatusReq
    {
        [XmlElement(ElementName = "trackType")]
        public string TrackType { get; set; }
        [XmlElement(ElementName = "trackValue")]
        public string TrackValue { get; set; }
        [XmlElement(ElementName = "fromDate")]
        public string FromDate { get; set; }
        [XmlElement(ElementName = "toDate")]
        public string ToDate { get; set; }
        public string RequestID { get; set; }
    }
    #endregion
}
