﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    /// <summary>
    /// Request Model for Bill Fetch Api
    /// </summary>
    /// 
    #region Bill Fetch Api Request Model

    [XmlRoot(ElementName = "agentDeviceInfo")]
    public class AgentDeviceInfo
    {
        [XmlElement(ElementName = "ip")]
        public string Ip { get; set; }
        [XmlElement(ElementName = "initChannel")]
        public string InitChannel { get; set; }
        [XmlElement(ElementName = "mac")]
        public string Mac { get; set; }
    }

    [XmlRoot(ElementName = "customerInfo")]
    public class CustomerInfo
    {
        [XmlElement(ElementName = "customerMobile")]
        public string CustomerMobile { get; set; }
        [XmlElement(ElementName = "customerEmail")]
        public string CustomerEmail { get; set; }
        [XmlElement(ElementName = "customerAdhaar")]
        public string CustomerAdhaar { get; set; }
        [XmlElement(ElementName = "customerPan")]
        public string CustomerPan { get; set; }
    }

    [XmlRoot(ElementName = "input")]
    public class Input
    {
        [XmlElement(ElementName = "paramName")]
        public string ParamName { get; set; }
        [XmlElement(ElementName = "paramValue")]
        public string ParamValue { get; set; }
    }

    [XmlRoot(ElementName = "inputParams")]
    public class InputParams
    {
        [XmlElement(ElementName = "input")]
        public List<Input> Input { get; set; }
    }

    [XmlRoot(ElementName = "billFetchRequest")]
    public class BillFetchRequest
    {
        [XmlElement(ElementName = "agentId")]
        public string AgentId { get; set; }
        [XmlElement(ElementName = "agentDeviceInfo")]
        public AgentDeviceInfo AgentDeviceInfo { get; set; }
        [XmlElement(ElementName = "customerInfo")]
        public CustomerInfo CustomerInfo { get; set; }
        [XmlElement(ElementName = "billerId")]
        public string BillerId { get; set; }
        [XmlElement(ElementName = "inputParams")]
        public InputParams InputParams { get; set; }
        public string RequestID { get; set; }
    }

    #endregion
}
