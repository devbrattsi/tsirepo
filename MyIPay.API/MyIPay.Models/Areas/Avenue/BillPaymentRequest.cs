﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    /// <summary>
    /// Request Model for Bill Payment Api
    /// </summary>
    /// 
    #region Bill Payment Api Request Model
    [XmlRoot(ElementName = "amountTags")]
    public class AmountTags
    {
        [XmlElement(ElementName = "amountTag")]
        public string AmountTag { get; set; }
        [XmlElement(ElementName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "amountInfo")]
    public class AmountInfo
    {
        [XmlElement(ElementName = "amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; }
        [XmlElement(ElementName = "custConvFee")]
        public string CustConvFee { get; set; }
        [XmlElement(ElementName = "amountTags")]
        public AmountTags AmountTags { get; set; }
    }

    [XmlRoot(ElementName = "paymentMethod")]
    public class PaymentMethod
    {
        [XmlElement(ElementName = "paymentMode")]
        public string PaymentMode { get; set; }
        [XmlElement(ElementName = "quickPay")]
        public string QuickPay { get; set; }
        [XmlElement(ElementName = "splitPay")]
        public string SplitPay { get; set; }
    }

    [XmlRoot(ElementName = "paymentInfo")]
    public class PaymentInfo
    {
        [XmlElement(ElementName = "info")]
        public Info Info { get; set; }
    }

    [XmlRoot(ElementName = "billPaymentRequest")]
    public class BillPaymentRequest
    {
        [XmlElement(ElementName = "agentId")]
        public string AgentId { get; set; }
        [XmlElement(ElementName = "billerAdhoc")]
        public string BillerAdhoc { get; set; }
        [XmlElement(ElementName = "agentDeviceInfo")]
        public AgentDeviceInfo AgentDeviceInfo { get; set; }
        [XmlElement(ElementName = "customerInfo")]
        public CustomerInfo CustomerInfo { get; set; }
        [XmlElement(ElementName = "billerId")]
        public string BillerId { get; set; }
        [XmlElement(ElementName = "inputParams")]
        public InputParams InputParams { get; set; }
        [XmlElement(ElementName = "billerResponse")]
        public BillerResponse BillerResponse { get; set; }
        [XmlElement(ElementName = "additionalInfo")]
        public AdditionalInfo AdditionalInfo { get; set; }
        [XmlElement(ElementName = "amountInfo")]
        public AmountInfo AmountInfo { get; set; }
        [XmlElement(ElementName = "paymentMethod")]
        public PaymentMethod PaymentMethod { get; set; }
        [XmlElement(ElementName = "paymentInfo")]
        public PaymentInfo PaymentInfo { get; set; }
        public string RequestID { get; set; }
    }

    [XmlRoot(ElementName = "additionalInfo")]
    public class AdditionalInfo
    {
        [XmlElement(ElementName = "info")]
        public List<Info> Info { get; set; }
    }
    [XmlRoot(ElementName = "info")]
    public class Info
    {
        [XmlElement(ElementName = "infoName")]
        public string InfoName { get; set; }
        [XmlElement(ElementName = "infoValue")]
        public string InfoValue { get; set; }
    }

    [XmlRoot(ElementName = "billerResponse")]
    public class BillerResponse
    {
        [XmlElement(ElementName = "amountOptions")]
        public AmountOptions AmountOptions { get; set; }
        [XmlElement(ElementName = "billAmount")]
        public string BillAmount { get; set; }
        [XmlElement(ElementName = "billDate")]
        public string BillDate { get; set; }
        [XmlElement(ElementName = "billNumber")]
        public string BillNumber { get; set; }
        [XmlElement(ElementName = "billPeriod")]
        public string BillPeriod { get; set; }
        [XmlElement(ElementName = "customerName")]
        public string CustomerName { get; set; }
        [XmlElement(ElementName = "dueDate")]
        public string DueDate { get; set; }
    }

    [XmlRoot(ElementName = "amountOptions")]
    public class AmountOptions
    {
        [XmlElement(ElementName = "option")]
        public List<Option> Option { get; set; }
    }

    [XmlRoot(ElementName = "option")]
    public class Option
    {
        [XmlElement(ElementName = "amountName")]
        public string AmountName { get; set; }
        [XmlElement(ElementName = "amountValue")]
        public string AmountValue { get; set; }
    }
    #endregion
}
