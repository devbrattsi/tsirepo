﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    ///<summary>
    ///Request model for Complaint Tracking
    ///</summary>
    ///
    #region Request model for Complaint Tracking
    [XmlRoot(ElementName = "complaintTrackingReq")]
    public class ComplaintTrackingReq
    {
        [XmlElement(ElementName = "complaintType")]
        public string ComplaintType { get; set; }
        [XmlElement(ElementName = "complaintId")]
        public string ComplaintId { get; set; }
        public string RequestID { get; set; }
    }
    #endregion
}
