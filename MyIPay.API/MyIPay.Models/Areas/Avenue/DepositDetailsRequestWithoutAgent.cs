﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.Avenue
{
    ///<summary>
    ///DEPOSIT ENQUIRY REQUEST - FOR ALL DEBIT TRANSACTIONS IN A DAY
    /// </summary>
    #region DEPOSIT ENQUIRY REQUEST - FOR ALL DEBIT TRANSACTIONS IN A DAY
    [XmlRoot(ElementName = "depositDetailsRequest")]
    public class DepositDetailsRequestWithoutAgent
    {
        [XmlElement(ElementName = "fromDate")]
        public string FromDate { get; set; }
        [XmlElement(ElementName = "toDate")]
        public string ToDate { get; set; }
        [XmlElement(ElementName = "transType")]
        public string TransType { get; set; }
        [XmlElement(ElementName = "agents")]
        public string Agents { get; set; }
    }
    #endregion
}
