﻿using System.ComponentModel.DataAnnotations;

namespace MyIPay.Models.Areas.BankDeposit.ECollection
{
    public class BankDepositTransactionModel
    {
        public string TravelagentCode { get; set; }

        public string CurrencyCode { get; set; }

        public string DepositAmount { get; set; }

        public string TransactionDate { get; set; }

        public string BankName { get; set; }

        public string InteBenefCustomer { get; set; }

        public string BranchName { get; set; }

        public string BankRefNumber { get; set; }

        public string BankProduct { get; set; }

        public string BankRefNo { get; set; }

        [Required]
        public string UtrRefNo { get; set; }

        public string VendorCode { get; set; }

        public string SenderName { get; set; }

        public string SenderToReceiverInfo { get; set; }

        public string PaymentId { get; set; }

        public string Remarks { get; set; }

        public string DebitAcctNumber { get; set; }

        public string DebitAccName { get; set; }

        public string SenderAcctNumber { get; set; }

        public string SendingIfscCode { get; set; }

        public string ChequeNumber { get; set; }

        public string ReasonCode { get; set; }

        public string ReturnReason { get; set; }
    }
}
