﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace MyIPay.Models.Areas.BankDeposit.ECollection
{
    
    public class VerifyAccountModel
    {
        [XmlElement(IsNullable = false)]
        [StringLength(4)]
        public string ClientCode { get; set; }

        [XmlElement(IsNullable = false)]
        [StringLength(20)]
        public string VirtualAccountNumber { get; set; }

        [XmlElement(IsNullable = false)]
        [StringLength(30)]
        public string TransactionAmount { get; set; }

        [XmlElement(IsNullable = false)]
        [StringLength(30)]
        public string UtrNumber { get; set; }

        [XmlElement(IsNullable = false)]
        [StringLength(30)]
        public string SenderIfscCode { get; set; }

        [XmlElement(IsNullable = false)]
        [StringLength(30)]
        public string ReceiverInfo { get; set; }
    }
}
