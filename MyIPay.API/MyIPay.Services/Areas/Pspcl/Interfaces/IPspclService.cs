﻿using MyIPay.Dtos.Areas.Common;

namespace MyIPay.Services.Areas.Pspcl.Interfaces
{
    public interface IPspclService
    {
        ResponseMessageDto GetBillDetails(string caNumber);
    }
}
