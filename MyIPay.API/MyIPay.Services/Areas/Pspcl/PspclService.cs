﻿using log4net;
using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.Pspcl;
using MyIPay.Helpers;
using MyIPay.Services.Areas.Pspcl.Interfaces;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace MyIPay.Services.Areas.Pspcl
{
    public class PspclService : IPspclService
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // Get PSPCL Bill
        public ResponseMessageDto GetBillDetails(string caNumber)
        {
            string requestBody = string.Empty;
            string result = string.Empty;
            XmlDocument doc = new XmlDocument();
            ResponseMessageDto responseObj = new ResponseMessageDto();
            try
            {
                requestBody = "<soapenv:Envelope xmlns:soapenv=\'http://schemas.xmlsoap.org/soap/envelope/\' xmlns:if0=\'http://pspcl.com/xi/WS/IF0006_GetBillDetail_100\'>";
                requestBody += "<soapenv:Header/>";
                requestBody += "<soapenv:Body>";
                requestBody += "<if0:MT_GetBillDetail_Req>";
                requestBody += "<BILL_DETAIL>";
                requestBody += "<BusinessPartner></BusinessPartner>";
                requestBody += "<ContractAccount>" + caNumber + "</ContractAccount>";
                requestBody += "<Contract>?</Contract>";
                requestBody += "</BILL_DETAIL>";
                requestBody += "</if0:MT_GetBillDetail_Req>";
                requestBody += "</soapenv:Body>";
                requestBody += "</soapenv:Envelope>";

                Utility.SaveRequestAndResponseLog("Pspcl", $"Pspcl Request :: GetBillDetails Request Log  --> xml = {requestBody}");

                string response = CallingAPIPSPCL(requestBody);
                Utility.SaveRequestAndResponseLog("Pspcl", $"Pspcl Response :: GetBillDetails Response Log  --> xml = {response}");
                doc.LoadXml(response);
                string serializeResponse = JsonConvert.SerializeXmlNode(doc);
                PSPCLModel root = JsonConvert.DeserializeObject<PSPCLModel>(serializeResponse);

                //var reader = new StringReader(response);
                //var serializer = new XmlSerializer(typeof(Envelope));
                //var root = (Envelope)serializer.Deserialize(reader);
                responseObj.IsSuccess = true;
                responseObj.Message = "Bill fetch successfully";
                responseObj.Response = root;

            }
            catch (Exception ex)
            {
                responseObj.IsSuccess = false;
                responseObj.Message = "Bill not fetch due to : " + ex.Message;
                responseObj.Response = null;

                Utility.SaveRequestAndResponseLog("Pspcl_Error", $"Pspcl Error :: GetBillDetails  {ex.Message}");
                Utility.SaveRequestAndResponseLog("Pspcl_Error", $"Pspcl Error :: GetBillDetails  {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("Pspcl_Error", $"Pspcl Error :: GetBillDetails  {ex.StackTrace}");
            }
            //      result = JsonConvert.SerializeObject(responseObj);
            return responseObj;
        }

        private static string CallingAPIPSPCL(string xmlRequest)
        {
            EnableHost.EnableTrustedHosts();
            string result = string.Empty;
            string username = AppSettings.PspclUsername;
            string password = AppSettings.PspclPassword;


            //This URL write in config file
            // string url = "http://pi.pspcl.in:55000/XISOAPAdapter/MessageServlet?senderParty=&senderService=WS_PID&receiverParty=&receiverService=&interface=MI_GetBillDetail_OB_SYNC&interfaceNamespace=http://pspcl.com/xi/WS/IF0006_GetBillDetail_100";
            //string url = "http://223.30.120.175:50000/XISOAPAdapter/MessageServlet?senderParty=&amp;senderService=WS_PRD&amp;receiverParty=&amp;receiverService=&amp;interface=MI_GetBillDetail_OB_SYNC&amp;interfaceNamespace=http://pspcl.com/xi/WS/IF0006_GetBillDetail_100";
            // string url = "http://223.30.120.175:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=WS_PRD&receiverParty=&receiverService=&interface=MI_GetBillDetail_OB_SYNC&interfaceNamespace=http://pspcl.com/xi/WS/IF0006_GetBillDetail_100";

            try
            {

                HttpWebRequest req = (HttpWebRequest)System.Net.HttpWebRequest.Create(AppSettings.PspclBaseUrl);
                req.Method = "POST";
                req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(username + ':' + password));
                req.ContentType = "text/xml;charset=UTF-8";
                System.Text.Encoding enc = System.Text.Encoding.ASCII;
                byte[] byteArray = enc.GetBytes(xmlRequest);

                req.ContentLength = xmlRequest.Length;
                string str = "http://sap.com/xi/WebService/soap1.1";
                req.Headers.Set("SOAPAction", '\"' + str + '\"');

                req.Timeout = 5 * 60 * 10000;
                req.UserAgent = "Jakarta Commons-HttpClient/3.1";
                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    streamWriter.Write(xmlRequest, 0, xmlRequest.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                using (var streamReader = new StreamReader(resp.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;

            }
            catch (Exception ex)
            {
                result = "Exception: " + ex.Message;
                return result;
            }
        }
    }

}
