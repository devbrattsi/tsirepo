﻿using MyIPay.Dtos.Areas.Toffee;
using MyIPay.Models.Areas.Toffee;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Toffee.Interfaces
{
    public interface IToffeeService
    {
        Task<TokenDto> GetTokenAsync();
        Task<TokenDto> GetRefreshTokenAsync(string refreshToken);
        Task<InsurancePurchaseDto> PurchaseInsurance(InsurancePurchaseModel model);
        Task<ToffeeStateDto> State(string token);
        Task<ToffeeCityDto> City(CityModel model);
        Task<ToffeeDto> Product(string token);
        Task<ToffeeDto> QueryPurchasePolicy(QueryPurchaseModel model);
        Task<ToffeeDto> CancelPolicy(CancelPolicyModel model);
    }
}
