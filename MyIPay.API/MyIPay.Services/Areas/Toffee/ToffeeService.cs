﻿using MyIPay.Dtos.Areas.Toffee;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Toffee;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.Toffee.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Toffee
{
    public class ToffeeService : IToffeeService
    {
        private IApiManagerService _apiManagerService;

        private readonly string _mode = AppSettings.ToffeeMode;

        private readonly string _baseUrl;
        private readonly int _clientId;
        private readonly string _clientSecret;
        private readonly string _username;
        private readonly string _password;
       
        public ToffeeService(IApiManagerService apiManagerService)
        {
            _apiManagerService = apiManagerService;

            _baseUrl = _mode == "L" ? AppSettings.ToffeeLiveBaseUrl : AppSettings.ToffeeUatBaseUrl;
            _clientId = _mode == "L" ? AppSettings.ToffeeLiveClientId : AppSettings.ToffeeUatClientId;
            _clientSecret = _mode == "L" ? AppSettings.ToffeeLiveClientSecret : AppSettings.ToffeeUatClientSecret;
            _username = _mode == "L" ? AppSettings.ToffeeLiveUsername : AppSettings.ToffeeUatUsername;
            _password = _mode == "L" ? AppSettings.ToffeeLivePassword : AppSettings.ToffeeUatPassword;
        }
        public async Task<TokenDto> GetTokenAsync()
        {
            TokenModel model = new TokenModel
            {
                ClientId = _clientId,
                ClientSecret = _clientSecret,
                GrantType = Constants.GrantTypePassword,
                UserName = _username,
                Password = _password
            };

            StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _apiManagerService.PostAsync(_baseUrl, "oauth/token", content);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TokenDto>(responseString);
        }

        public async Task<TokenDto> GetRefreshTokenAsync(string refreshToken)
        {
            RefreshTokenModel model = new RefreshTokenModel
            {
                ClientId = _clientId,
                ClientSecret = _clientSecret,
                GrantType = Constants.GrantTypeRefreshToken,
                RefreshToken = refreshToken
            };

            StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _apiManagerService.PostAsync(_baseUrl, "oauth/token", content);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TokenDto>(responseString);
        }

        public async Task<InsurancePurchaseDto> PurchaseInsurance(InsurancePurchaseModel model)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", model.Token);

            var response = await _apiManagerService.PostAsync(_baseUrl, "consumer/purchase", JsonConvert.SerializeObject(model), client);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<InsurancePurchaseDto>(responseString);
        }

        public async Task<ToffeeStateDto> State(string token)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await _apiManagerService.GetAsync(_baseUrl, "consumer/states", client);
            string responseString = await response.Content.ReadAsStringAsync();
            var tt = JsonConvert.DeserializeObject<ToffeeStateDto>(responseString);
            return tt;
        }

        public async Task<ToffeeCityDto> City(CityModel model)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", model.Token);

            var response = await _apiManagerService.PostAsync(_baseUrl, "consumer/cities", JsonConvert.SerializeObject(model), client);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ToffeeCityDto>(responseString);
        }

        public async Task<ToffeeDto> Product(string token)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await _apiManagerService.GetAsync(_baseUrl, "consumer/products", client);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ToffeeProductDto>(responseString);
        }

        public async Task<ToffeeDto> QueryPurchasePolicy(QueryPurchaseModel model)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", model.Token);

            var response = await _apiManagerService.PostAsync(_baseUrl, "consumer/query", JsonConvert.SerializeObject(model), client);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ToffeeQueryPurchaseDto>(responseString);
        }

        public async Task<ToffeeDto> CancelPolicy(CancelPolicyModel model)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", model.Token);

            var response = await _apiManagerService.PostAsync(_baseUrl, "consumer/cancel", JsonConvert.SerializeObject(model), client);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ToffeeCancelPolicyDto>(responseString);
        }
    }
}
