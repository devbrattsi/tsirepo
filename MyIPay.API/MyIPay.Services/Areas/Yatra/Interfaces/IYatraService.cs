﻿
using MyIPay.Dtos.Areas.Yatra;
using MyIPay.Models.Areas.Yatra;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Yatra.Interfaces
{
    public interface IYatraService
    {

        Task<string> GenerateTokenAsync(string agentId);
        //Task<bool> IsUserExistAsync(string agentId);
        Task<VerifyTokenDto> VerifyTokenAsync(VerifyTokenModel model);//string authenticationType, string key);

        Task<CheckBalanceAvailabilityAsyncDto> CheckBalanceAvailabilityAsync(string agentId, string orderId, decimal amount);
        Task<DoPaymentAsyncDto> DoPaymentAsync(DoPaymentAsyncModel model);

        Task<CheckStatusAsyncDto> CheckPaymentStatusAsync(string orderId, decimal amount, string signature);

        Task<BookingCancellationAsyncDto> BookingCancellationAsync(BookingCancellationAsyncModel model);
        Task<BookingCancellationAsyncDto> GiveCommissionAsync(BookingCancellationAsyncModel model);
        //Task<HttpResponseMessage> DoYatraPaymentAsync(DoYatraPaymentAsyncModel model);

        //Task<HttpResponseMessage> YatraBookingCancellationAsync(string orderId, decimal amount, string refundOrderId, string signature, int isPartialCal);

        //Task<HttpResponseMessage> CrossCheckYatraPaymentAsync(string orderId, decimal amount, string signature);
    }
}
