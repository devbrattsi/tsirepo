﻿using MyIPay.DataLayer.EF;
using MyIPay.Dtos.Areas.Yatra;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Yatra;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.Yatra.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Yatra
{
    public class YatraService : IYatraService
    {
        private readonly IApiManagerService _apiManagerService;
        private readonly string timestamp = SecurityHelper.GenerateSecretKeyTimestamp();
        private readonly DateTime _dateTimeUtcNow = DateTime.UtcNow;
        private readonly DateTime _dateTimeNow = DateTime.Now;


        private readonly ITransactionService<string> _transactionService;
        private readonly MyIPayEntities _myIPayEntities;

        public YatraService(IApiManagerService apiManagerService,
                            ITransactionService<string> transactionService,
                            MyIPayEntities myIPayEntities)
        {
            _apiManagerService = apiManagerService;
            _transactionService = transactionService;
            _myIPayEntities = myIPayEntities;

        }

        public async Task<string> GenerateTokenAsync(string agentId)
        {
            bool result = await IsUserExistAsync(agentId);

            if (!result)
                return null;

            string timestamp = SecurityHelper.GenerateSecretKeyTimestamp();

            string token = SecurityHelper.Encrypt($"{timestamp}_{agentId}");

            return token;

        }
        private async Task<bool> IsUserExistAsync(string agentId)
        {
            if (await _myIPayEntities.AspNetUsers.AnyAsync(x => x.AgentId.ToUpper() == agentId.ToUpper()))
                return true;
            else
                return false;
        }

        public async Task<VerifyTokenDto> VerifyTokenAsync(VerifyTokenModel model)//string authenticationType, string key)
        {
            try
            {
                if (string.IsNullOrEmpty(model.AuthenticationType) || string.IsNullOrEmpty(model.Key) || model.AuthenticationType != Constants.AuthenticationType)
                {
                    return new VerifyTokenDto
                    {
                        ResponseCode = Constants.ParameterMissingOrInvalidResponsecode,
                        ResponseType = Constants.Invalid_User,
                        Description = Constants.ParameterMissing,
                        IsAuthenticated = false,
                        ResponseMessage = Constants.RequiredDataMismatch
                    };

                }

                model.Key = model.Key.Replace(" ", "+");

                //Decrypt access token
                string decryptKey = SecurityHelper.Decrypt(model.Key);





                if (!ValidateToken(decryptKey))
                {
                    return new VerifyTokenDto
                    {
                        ResponseCode = Constants.InvalidTokenResponsecode,
                        ResponseType = Constants.UnAuthrize_User,
                        Description = Constants.InvalidToken,
                        IsAuthenticated = false,
                        ResponseMessage = Constants.UnAuthorizeUser
                    };
                }

                string[] decryptKeyArray = decryptKey.Split('_');

                double unixTimeMilliseconds = Convert.ToDouble(decryptKeyArray[0]);
                string agentId = decryptKeyArray[1];

                //Converting Unix time Milliseconds to DateTime
                DateTime tokenDateTime = SecurityHelper.UnixTimeMillisecondsToDateTime(unixTimeMilliseconds);

                Helpers.Utility.WriteLog("YatraService", $"VerifyToken UTC tokenDateTime :- {tokenDateTime} , UTC DateTimeNow :- {_dateTimeUtcNow}");

                //if (_dateTimeUtcNow > tokenDateTime.AddMinutes(5))
                if (_dateTimeUtcNow > tokenDateTime.AddHours(24))
                {
                    return new VerifyTokenDto
                    {
                        ResponseCode = Constants.ExpireTokenResponsecode,
                        ResponseType = Constants.UnAuthrize_User,
                        Description = Constants.TokenExpire,
                        IsAuthenticated = false,
                        ResponseMessage = Constants.UnAuthorizeUser
                    };
                }

                //Getting user's detail
                var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == agentId.ToUpper());
                if (userDetail == null)
                {
                    return new VerifyTokenDto
                    {
                        ResponseCode = Constants.InvalidTokenResponsecode,
                        ResponseType = Constants.UnAuthrize_User,
                        Description = Constants.InvalidToken,
                        IsAuthenticated = false,
                        ResponseMessage = Constants.UnAuthorizeUser
                    };
                }

                return new VerifyTokenDto
                {
                    ResponseCode = Constants.SuccessTokenResponsecode,
                    ResponseType = Constants.Authrize_User,
                    Description = Constants.ValidToken,
                    IsAuthenticated = true,
                    ResponseMessage = Constants.AuthorizeUser,
                    AgentId = agentId,
                    AgentEmail = userDetail.Role.ToUpper() == ClaimConstants.Admin.ToUpper() || userDetail.Role.ToUpper() == ClaimConstants.SuperAdmin.ToUpper() ? "sarika@tsiplc.com" : userDetail.Email
                };
            }
            catch (Exception ex)
            {
                Helpers.Utility.WriteLog("YatraService", $"VerifyToken Error :- {ex.Message}");
                Helpers.Utility.WriteLog("YatraService", $"VerifyToken Error :- {ex.InnerException}");
                Helpers.Utility.WriteLog("YatraService", $"VerifyToken Error :- {ex.StackTrace}");

                return new VerifyTokenDto
                {
                    ResponseCode = Constants.TechnicalFailureResponsecode,
                    ResponseType = Constants.Error,
                    Description = Constants.TechnicalFailure,
                    IsAuthenticated = false,
                    ResponseMessage = Constants.ErrorInApi,
                };
            }
        }


        public async Task<CheckBalanceAvailabilityAsyncDto> CheckBalanceAvailabilityAsync(string agentId, string orderId, decimal amount)
        {
            try
            {
                if (amount <= 0)
                    return new CheckBalanceAvailabilityAsyncDto
                    {
                        IsSuccess = false,
                        Remarks = Constants.RejectRemarks,
                        Description = Constants.InvalidAmount
                    };


                using (var db = new MyIPayEntities())
                {
                    var userDetail = await db.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToLower() == agentId.ToLower());

                    if (userDetail == null)
                    {
                        return new CheckBalanceAvailabilityAsyncDto
                        {
                            IsSuccess = false,
                            Remarks = Constants.RejectRemarks,
                            Description = Constants.AgentIdNotExist
                        };
                    }

                    var availableBalance = db.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();

                    //if (availableBalance.HasValue && availableBalance.Value >= amount)
                    //{
                    //    //Checking transaction made successfully or not
                    //    if (!int.TryParse(await _transactionService.SaveTransactionAsync(agentId, orderId, amount, availableBalance.Value), out int Id))
                    //        return new CheckBalanceAvailabilityAsyncDto
                    //        {
                    //            IsSuccess = false,
                    //            Remarks = Constants.RejectRemarks,
                    //            Description = Constants.TechnicalFailure
                    //        };
                    //}

                    return new CheckBalanceAvailabilityAsyncDto
                    {
                        IsSuccess = availableBalance.HasValue && availableBalance.Value >= (amount + userDetail.MinLimit) ? true : false,
                        Remarks = availableBalance.HasValue && availableBalance.Value >= (amount + userDetail.MinLimit) ? Constants.AcceptRemarks : Constants.RejectRemarks,
                        Description = availableBalance.HasValue && availableBalance.Value >= amount ? Constants.BalanceAvailable : Constants.InsufficientBalance
                    };

                }
            }
            catch (Exception ex)
            {

                Helpers.Utility.WriteLog("YatraService", $"GetMerchantBalanceAsync Error :- {ex.Message}");
                Helpers.Utility.WriteLog("YatraService", $"GetMerchantBalanceAsync Error :- {ex.InnerException}");
                Helpers.Utility.WriteLog("YatraService", $"GetMerchantBalanceAsync Error :- {ex.StackTrace}");

                return new CheckBalanceAvailabilityAsyncDto
                {
                    IsSuccess = false,
                    Remarks = Constants.RejectRemarks,
                    Description = Constants.TechnicalFailure// ex.Message
                };
            }


        }


        public async Task<DoPaymentAsyncDto> DoPaymentAsync(DoPaymentAsyncModel model)
        {
            try
            {

                if (model.MerchantId != AppSettings.YatraMerchantId)
                {
                    return new DoPaymentAsyncDto
                    {
                        TransactionTime = _dateTimeNow,
                        ResponseCode = Constants.InvalidMerchantIdResponseCode,
                        Amount = model.Amount,
                        OrderId = model.RefernceNo,
                        MerchantId = model.MerchantId,
                        Signature = model.Signature,
                        Description = Constants.InvalidMerchantId,
                        Status = Constants.Failed,
                        IsSuccess = false,
                        //Remarks = Constants.RejectRemarks

                    };
                }

                string md5 = SecurityHelper.MD5Hash($"{model.RefernceNo}|{model.Amount}|{model.MerchantId}|{AppSettings.SaltForYatra}");

                //Comparing/Validating signature
                if (model.Signature != md5)
                {
                    return new DoPaymentAsyncDto
                    {
                        TransactionTime = _dateTimeNow,
                        ResponseCode = "08",
                        Amount = model.Amount,
                        OrderId = model.RefernceNo,
                        MerchantId = model.MerchantId,
                        Signature = model.Signature,
                        Description = Constants.SignatureMismatch,
                        Status = Constants.Failed,
                        IsSuccess = false,
                        //Remarks = Constants.RejectRemarks

                    };
                }

                if (model.Amount <= 0)
                {
                    return new DoPaymentAsyncDto
                    {
                        TransactionTime = _dateTimeNow,
                        ResponseCode = Constants.ParameterMissingOrInvalidResponsecode,
                        Amount = model.Amount,
                        OrderId = model.RefernceNo,
                        MerchantId = model.MerchantId,
                        Signature = model.Signature,
                        Description = Constants.InvalidAmount,
                        Status = Constants.Failed,
                        IsSuccess = false,
                        //Remarks = Constants.RejectRemarks

                    };
                }

                //var transactionResult = await _myIPayEntities.TransactionsDBs
                //                                             .FirstOrDefaultAsync(x => x.USP == USP.IRCTC.ToString()
                //                                                                  && x.AGENTID == model.AgentId
                //                                                                  && x.REFERENCE5 == model.RefernceNo
                //                                                                  && x.Mode == Mode.L.ToString());

                //if (transactionResult == null)
                //{
                //    return new DoPaymentAsyncDto
                //    {
                //        TransactionTime = _dateTimeNow,
                //        ResponseCode = "06",
                //        Amount = model.Amount,
                //        OrderId = model.RefernceNo,
                //        MerchantId = model.MerchantId,
                //        Signature = model.Signature,
                //        Description = "Problem To Parse the Request Data in payment deduction api.",
                //        Status = Constants.Failed,
                //        IsSuccess = false,
                //        //Remarks = Constants.RejectRemarks

                //    };
                //}

                using (var db = new MyIPayEntities())
                {
                    var userDetail = await db.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToLower() == model.AgentId.ToLower());

                    if (userDetail == null)
                    {
                        return new DoPaymentAsyncDto
                        {
                            TransactionTime = _dateTimeNow,
                            ResponseCode = "03",
                            Amount = model.Amount,
                            OrderId = model.RefernceNo,
                            MerchantId = model.MerchantId,
                            Signature = model.Signature,
                            Description = Constants.AgentIdNotExist,
                            Status = Constants.Failed,
                            IsSuccess = false,
                            //Remarks = Constants.RejectRemarks

                        };
                    }

                    if (await _myIPayEntities.TransactionsDBs.AnyAsync(x => x.USP == USP.IRCTC.ToString() && x.REFERENCE5 == model.RefernceNo))
                    {

                        return new DoPaymentAsyncDto
                        {
                            TransactionTime = _dateTimeNow,
                            ResponseCode = "03",
                            Amount = model.Amount,
                            OrderId = model.RefernceNo,
                            MerchantId = model.MerchantId,
                            Signature = model.Signature,
                            Description = Constants.DuplicateOrderId,
                            Status = Constants.Failed,
                            IsSuccess = false,
                            //Remarks = Constants.RejectRemarks

                        };
                    }

                    var availableBalance = db.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();

                    if (!availableBalance.HasValue || availableBalance.Value < (model.Amount + userDetail.MinLimit))
                    {

                        return new DoPaymentAsyncDto
                        {
                            TransactionTime = _dateTimeNow,
                            ResponseCode = "04",
                            Amount = model.Amount,
                            OrderId = model.RefernceNo,
                            MerchantId = model.MerchantId,
                            Signature = model.Signature,
                            Description = Constants.InsufficientBalance,
                            Status = Constants.Failed,
                            IsSuccess = false,
                            //Remarks = Constants.RejectRemarks

                        };

                    }

                    //Checking transaction made successfully or not
                    string transactionId = await _transactionService.SaveTransactionAsync(model, availableBalance.Value);//(model.AgentId, model.RefernceNo, model.Amount, availableBalance.Value)
                    if (!int.TryParse(transactionId, out int id))
                    {
                        return new DoPaymentAsyncDto
                        {
                            TransactionTime = _dateTimeNow,
                            ResponseCode = "05",
                            Amount = model.Amount,
                            OrderId = model.RefernceNo,
                            MerchantId = model.MerchantId,
                            Signature = model.Signature,
                            Description = Constants.TransactionFailed,
                            Status = Constants.Failed,
                            IsSuccess = false,
                            //Remarks = Constants.RejectRemarks

                        };
                    }


                    //successful transaction response
                    return new DoPaymentAsyncDto
                    {
                        TransactionTime = _dateTimeNow,
                        ResponseCode = "00",
                        Amount = model.Amount,
                        OrderId = model.RefernceNo,
                        MerchantId = model.MerchantId,
                        Signature = model.Signature,
                        Description = Constants.TransactionSuccessful,
                        TransactionId = transactionId,
                        Status = Constants.SuccessMsg,
                        IsSuccess = true,

                    };

                }
            }
            catch (Exception ex)
            {

                Helpers.Utility.WriteLog("YatraService", $"DoPaymentAsync Error :- {ex.Message}");
                Helpers.Utility.WriteLog("YatraService", $"DoPaymentAsync Error :- {ex.InnerException}");
                Helpers.Utility.WriteLog("YatraService", $"DoPaymentAsync Error :- {ex.StackTrace}");

                return new DoPaymentAsyncDto
                {
                    TransactionTime = _dateTimeNow,
                    ResponseCode = "06",
                    Amount = model.Amount,
                    OrderId = model.RefernceNo,
                    MerchantId = model.MerchantId,
                    Signature = model.Signature,
                    Description = Constants.ProblemToParseRequestData,
                    Status = Constants.Failed,
                    IsSuccess = false,
                    //Remarks = Constants.RejectRemarks
                };
            }


        }


        public async Task<CheckStatusAsyncDto> CheckPaymentStatusAsync(string orderId, decimal amount, string signature)
        {

            try
            {
                string md5 = SecurityHelper.MD5Hash($"{orderId}|{amount}|{AppSettings.YatraMerchantId}|{AppSettings.SaltForYatra}");

                //Comparing/Validating signature
                if (md5 != signature)
                {
                    return new CheckStatusAsyncDto
                    {
                        ResponseCode = "03",
                        Amount = amount,
                        Signature = signature,
                        Description = Constants.SignatureMismatch,
                        //TransactionId
                        Status = Constants.Failed,
                        IsSuccess = false,
                        OrderId = orderId,
                    };
                }

                int count = await _myIPayEntities.TransactionsDBs.CountAsync(x => x.USP == USP.IRCTC.ToString() && x.REFERENCE5 == orderId);

                if (count == 0)
                {
                    return new CheckStatusAsyncDto
                    {
                        ResponseCode = "01",
                        Amount = amount,
                        Signature = signature,
                        Description = Constants.InvalidOrderId,
                        //TransactionId
                        Status = Constants.Failed,
                        IsSuccess = false,
                        OrderId = orderId

                    };
                }

                else if (count == 1)
                {



                    if (await _myIPayEntities.TransactionsDBs.AnyAsync(x => x.USP == USP.IRCTC.ToString() && x.REFERENCE5 == orderId && x.BILLAMOUNT == amount))
                    {

                        var orderDetail = await _myIPayEntities.TransactionsDBs
                                                         .Select(x => new
                                                         {
                                                             x.ID,
                                                             x.USP,
                                                             x.REFERENCE5,
                                                             x.BILLAMOUNT
                                                         })
                                                         .FirstOrDefaultAsync(x => x.USP == USP.IRCTC.ToString()
                                                                                   && x.REFERENCE5 == orderId
                                                                                   && x.BILLAMOUNT == amount);

                        return new CheckStatusAsyncDto
                        {
                            ResponseCode = "00",
                            Amount = amount,
                            Signature = signature,
                            Description = Constants.TransactionSuccessful,
                            TransactionId = orderDetail.ID.ToString(),
                            Status = Constants.SuccessMsg,
                            IsSuccess = true,
                            OrderId = orderId
                        };
                    }
                    else
                    {
                        return new CheckStatusAsyncDto
                        {
                            ResponseCode = "02",
                            Amount = amount,
                            Signature = signature,
                            Description = Constants.TransactionAmountMisMatch,
                            Status = Constants.Failed,
                            IsSuccess = false,
                            OrderId = orderId
                        };

                    }
                }

                else
                {

                    return new CheckStatusAsyncDto
                    {
                        ResponseCode = "04",
                        Amount = amount,
                        Signature = signature,
                        Description = Constants.DuplicateOrderId,
                        Status = Constants.Failed,
                        IsSuccess = false,
                        OrderId = orderId
                    };


                }
            }
            catch (Exception ex)
            {

                Helpers.Utility.WriteLog("YatraService", $"CheckPaymentStatusAsync Error :- {ex.Message}");
                Helpers.Utility.WriteLog("YatraService", $"CheckPaymentStatusAsync Error :- {ex.InnerException}");
                Helpers.Utility.WriteLog("YatraService", $"CheckPaymentStatusAsync Error :- {ex.StackTrace}");


                return new CheckStatusAsyncDto
                {
                    ResponseCode = "10",
                    Amount = amount,
                    Signature = signature,
                    Description = "Problem To Parse the Request Data in paymentCrossCheckApi.",
                    Status = Constants.Failed,
                    IsSuccess = false,
                    OrderId = orderId

                };
            }

        }



        public async Task<BookingCancellationAsyncDto> BookingCancellationAsync(BookingCancellationAsyncModel model)
        {
            try
            {



                string md5 = SecurityHelper.MD5Hash($"{model.OrderId}|{model.Amount}|{AppSettings.YatraMerchantId}|{AppSettings.SaltForYatra}");

                //Comparing/Validating signature
                if (model.Signature != md5)
                {
                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        // AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = Constants.SignatureMismatch,
                        Signature = model.Signature,
                        ResponseCode = Constants.SignatureMismatchResponseCode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }

                if (model.IsPartialCal != "0" && model.IsPartialCal != "1")
                {
                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        // AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = "Invalid IsPartialCal",
                        Signature = model.Signature,
                        ResponseCode = Constants.ParameterMissingOrInvalidResponsecode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""

                    };
                }

                //Getting transaction details
                var transactionDetail = await _myIPayEntities.TransactionsDBs
                                                             .Where(x => x.REFERENCE5.ToUpper() == model.RefundedOrderId.ToUpper())
                                                             .ToListAsync();


                if (transactionDetail.Count == 0)
                {

                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = Constants.InvalidOrderId,
                        Signature = model.Signature,
                        ResponseCode = Constants.ParameterMissingOrInvalidResponsecode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""

                    };
                }
                else if (transactionDetail.Any(x => x.Mode == Mode.F.ToString()))
                {

                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = Constants.OrderCanNotCancelled,
                        Signature = model.Signature,
                        ResponseCode = Constants.OrderCanNotCancelledResponseCode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }
                else if (transactionDetail.Any(x => x.Mode == Mode.C.ToString())
                        && transactionDetail.FirstOrDefault(x => x.Mode == Mode.L.ToString()).BILLAMOUNT - (-transactionDetail.Where(x => x.Mode == Mode.C.ToString()).Sum(x => x.BILLAMOUNT)) == 0)
                {

                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = model.IsPartialCal == "0" ? Constants.AlreadyCancelledAndRefunded : Constants.OrderIdAlreadyFullyRefunded,
                        Signature = model.Signature,
                        ResponseCode = model.IsPartialCal == "0" ? Constants.AlreadyCancelledAndRefundedResponsecode : Constants.OrderIdAlreadyFullyRefundedResponseCode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }

                //else if (model.Amount > transactionDetail.FirstOrDefault(x => x.Mode == Mode.L.ToString()).BILLAMOUNT
                //        || (transactionDetail.FirstOrDefault(x => x.Mode == Mode.L.ToString()).BILLAMOUNT - (transactionDetail.Where(x => x.Mode == Mode.C.ToString()).Sum(x => x.BILLAMOUNT) + model.Amount)) < 0)
                else if (transactionDetail.FirstOrDefault(x => x.Mode == Mode.L.ToString()).BILLAMOUNT < Convert.ToDecimal(model.Amount)
                         || transactionDetail.FirstOrDefault(x => x.Mode == Mode.L.ToString()).BILLAMOUNT < (-transactionDetail.Where(x => x.Mode == Mode.C.ToString()).Sum(x => x.BILLAMOUNT) + Convert.ToDecimal(model.Amount)))
                {

                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = Constants.RefundAmountIsLargerThanReamingAmount,
                        Signature = model.Signature,
                        ResponseCode = Constants.RefundAmountIsLargerThanReamingAmountResponseCode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }
                else
                {

                    string result = await _transactionService.RefundAsync(model);// (model.OrderId, model.Amount, model.RefundedOrderId);


                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = true,
                        Description = Constants.BookingSuccessfullyCancelled,
                        Signature = model.Signature,
                        ResponseCode = Constants.BookingSuccessfullyCancelledResponseCode,
                        Status = Constants.SuccessMsg,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }
            }
            catch (Exception ex)
            {

                Helpers.Utility.WriteLog("YatraService", $"BookingCancellationAsync Error :- {ex.Message}");
                Helpers.Utility.WriteLog("YatraService", $"BookingCancellationAsync Error :- {ex.InnerException}");
                Helpers.Utility.WriteLog("YatraService", $"BookingCancellationAsync Error :- {ex.StackTrace}");

                return new BookingCancellationAsyncDto
                {
                    OrderId = model.OrderId,
                    //AgentId = model.AgentId,
                    Amount = model.Amount,
                    RefundedOrderId = model.RefundedOrderId,
                    IsSuccess = false,
                    Description = Constants.ErrorInBookingCancellationApi,
                    Signature = model.Signature,
                    ResponseCode = Constants.ErrorInBookingCancellationApiResponseCode,
                    Status = Constants.Failed,
                    PurOrderId = "",
                    RefTxnId = ""
                };
            }
        }


        public async Task<BookingCancellationAsyncDto> GiveCommissionAsync(BookingCancellationAsyncModel model)
        {

            try
            {
                string md5 = SecurityHelper.MD5Hash($"{model.OrderId}|{model.Amount}|{AppSettings.YatraMerchantId}|{AppSettings.SaltForYatra}");

                //Comparing/Validating signature
                if (model.Signature != md5)
                {
                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        // AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = Constants.SignatureMismatch,
                        Signature = model.Signature,
                        ResponseCode = Constants.SignatureMismatchResponseCode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }

                if (model.IsPartialCal != "1" && model.RefundCategory != "C")
                {
                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        // AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = model.IsPartialCal != "1" ? "Invalid IsPartialCal" : "Invalid RefundCategory",
                        Signature = model.Signature,
                        ResponseCode = Constants.ParameterMissingOrInvalidResponsecode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""

                    };
                }
                //Getting transaction details
                var transactionDetail = await _myIPayEntities.TransactionsDBs
                                                             .Where(x => x.USP == USP.IRCTC.ToString()
                                                                 //&& x.AGENTID.ToLower() == model.AgentId.ToLower()
                                                                 && x.REFERENCE5.ToUpper() == model.RefundedOrderId.ToUpper())
                                                             .ToListAsync();


                if (transactionDetail.Count == 0)
                {

                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = false,
                        Description = Constants.InvalidOrderId,
                        Signature = model.Signature,
                        ResponseCode = Constants.ParameterMissingOrInvalidResponsecode,
                        Status = Constants.Failed,
                        PurOrderId = "",
                        RefTxnId = ""

                    };
                }
                else
                {
                    string result = await _transactionService.GiveCommissionAsync(model);

                    return new BookingCancellationAsyncDto
                    {
                        OrderId = model.OrderId,
                        //AgentId = model.AgentId,
                        Amount = model.Amount,
                        RefundedOrderId = model.RefundedOrderId,
                        IsSuccess = true,
                        Description = Constants.AgentCommissionCreditedSuccessfully,
                        Signature = model.Signature,
                        ResponseCode = Constants.BookingSuccessfullyCancelledResponseCode,
                        Status = Constants.SuccessMsg,
                        PurOrderId = "",
                        RefTxnId = ""
                    };
                }
            }
            catch (Exception ex)
            {

                Helpers.Utility.WriteLog("YatraService", $"GiveCommissionAsync Error :- {ex.Message}");
                Helpers.Utility.WriteLog("YatraService", $"GiveCommissionAsync Error :- {ex.InnerException}");
                Helpers.Utility.WriteLog("YatraService", $"GiveCommissionAsync Error :- {ex.StackTrace}");

                return new BookingCancellationAsyncDto
                {
                    OrderId = model.OrderId,
                    //AgentId = model.AgentId,
                    Amount = model.Amount,
                    RefundedOrderId = model.RefundedOrderId,
                    IsSuccess = false,
                    Description = Constants.ErrorInBookingCancellationApi,
                    Signature = model.Signature,
                    ResponseCode = Constants.ErrorInBookingCancellationApiResponseCode,
                    Status = Constants.Failed,
                    PurOrderId = "",
                    RefTxnId = ""
                };
            }
        }



        //public async Task<HttpResponseMessage> DoYatraPaymentAsync(DoYatraPaymentAsyncModel model)
        //{
        //    string json = JsonConvert.SerializeObject(model);
        //    Helpers.Utility.SaveRequestAndResponseLog("YatraService", $"DoYatraPaymentAsync Request Log model :- {json}");
        //    var content = new StringContent(json, Encoding.UTF8, "application/json");

        //    //Getting API Response
        //    var response = await _apiManagerService.PostAsync(AppSettings.YatraAesApiBaseUrl, "TBO/services/yatraRequest/payment", content);

        //    Helpers.Utility.SaveRequestAndResponseLog("YatraService", $"DoYatraPaymentAsync Response Log :- {response.Content.ReadAsStringAsync()}");

        //    return response;
        //}

        //public async Task<HttpResponseMessage> YatraBookingCancellationAsync(string orderId, decimal amount, string refundOrderId, string signature, int isPartialCal)
        //{
        //    Helpers.Utility.SaveRequestAndResponseLog("YatraService", $"YatraBookingCancellationAsync Request Log :- orderId = {orderId}, amount = {amount}, refundOrderId = {refundOrderId}, signature = {signature}, isPartialCal = {isPartialCal}");

        //    string url = $"TBO/services/yatra/bookingCancellation?orderId={orderId}&amount={amount}&refundOrderId={refundOrderId}&singnature={signature}&isPartialCal={isPartialCal}";

        //    var response = await _apiManagerService.GetAsync(AppSettings.YatraAesApiBaseUrl, url);

        //    Helpers.Utility.SaveRequestAndResponseLog("YatraService", $"YatraBookingCancellationAsync Response Log :- {response.Content.ReadAsStringAsync()}");

        //    return response;

        //}

        //public async Task<HttpResponseMessage> CrossCheckYatraPaymentAsync(string orderId, decimal amount, string signature)
        //{
        //    Helpers.Utility.SaveRequestAndResponseLog("YatraService", $"CrossCheckYatraPaymentAsync Request Log :- orderId = {orderId}, amount = {amount}, signature = {signature}");

        //    string url = $"TBO/services/yatraCrossCheck/payment?orderId={orderId}&amount={amount}&signature={signature}";

        //    var response = await _apiManagerService.GetAsync(AppSettings.YatraAesApiBaseUrl, url);

        //    Helpers.Utility.SaveRequestAndResponseLog("YatraService", $"CrossCheckYatraPaymentAsync Response Log :- {response.Content.ReadAsStringAsync()}");

        //    return response;

        //}

        #region Private Method
        private bool ValidateToken(string decryptKey)
        {
            string[] decryptKeyArray = decryptKey.Split('_');
            if (decryptKeyArray.Length != 2)
                return false;


            string token = decryptKeyArray[0];
            string agentId = decryptKeyArray[1];

            if (!double.TryParse(token, out double unixTimeMilliseconds))
                return false;

            //Converting UnixTime Milliseconds To DateTime
            DateTime tokenDateTime = SecurityHelper.UnixTimeMillisecondsToDateTime(unixTimeMilliseconds);
            if (_dateTimeUtcNow < tokenDateTime)
                return false;


            return true;

        }





        #endregion
    }
}
