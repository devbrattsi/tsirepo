﻿using MyIPay.Dtos.Areas.PrabhuBank;
using MyIPay.Helpers;
using MyIPay.Models.Areas.PrabhuBank;
using MyIPay.Services.Areas.PrabhuBank.Interfaces;
using InmtUatService = MyIPay.Services.InmtServiceReference;
using InmtLiveService = MyIPay.Services.InmtLiveServiceReference;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml;


namespace MyIPay.Services.Areas.PrabhuBank
{
    public class InmtService : IInmtService
    {
        private readonly InmtUatService.ISend _sendUat = new InmtUatService.SendClient();
        private readonly InmtLiveService.ISend _sendLive = new InmtLiveService.SendClient();

        private readonly string _mode = AppSettings.PrabhuBankMode;
        private readonly string _username;
        private readonly string _password;

        public InmtService()
        {
            _username = _mode == Mode.L.ToString() ? AppSettings.PrabhuBankLiveApiUsername : AppSettings.PrabhuBankUatApiUsername;
            _password = _mode == Mode.L.ToString() ? AppSettings.PrabhuBankLiveApiPassword : AppSettings.PrabhuBankUatApiPassword;

        }

        public async Task<ServiceChargeDto> GetServiceChargeAsync(string country,
        string paymentMode,
        string transferAmount = null,
        string payoutAmount = null,
        string bankBranchId = null,
        string isNewAccount = null)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.GetServiceChargeAsync(new InmtLiveService.InputGetServiceCharge
                {
                    UserName = _username,
                    Password = _password,
                    Country = country,
                    PaymentMode = paymentMode,
                    TransferAmount = transferAmount,
                    PayoutAmount = payoutAmount,
                    BankBranchId = bankBranchId,
                    IsNewAccount = isNewAccount
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.GetServiceChargeAsync(new InmtUatService.InputGetServiceCharge
                {
                    UserName = _username,
                    Password = _password,
                    Country = country,
                    PaymentMode = paymentMode,
                    TransferAmount = transferAmount,
                    PayoutAmount = payoutAmount,
                    BankBranchId = bankBranchId,
                    IsNewAccount = isNewAccount
                });

                jsonText = JsonConvert.SerializeObject(response);
            }


            //// To convert an XML node contained in string xml into a JSON string   
            //XmlDocument doc = new XmlDocument();
            //var xml = Converter.XmlSerializer(response);
            //doc.LoadXml(xml);

            //string jsonText = JsonConvert.SerializeXmlNode(doc);

            ServiceChargeDto dto = JsonConvert.DeserializeObject<ServiceChargeDto>(jsonText);


            return dto;
        }

        public async Task<ServiceChargeDto> GetServiceChargeByCollectionAsync(string country,
                                                                              string paymentMode,
                                                                              string collectionAmount = null,
                                                                              string payoutAmount = null,
                                                                              string bankBranchId = null,
                                                                              string isNewAccount = null)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.GetServiceChargeByCollectionAsync(new InmtLiveService.InputGetServiceChargeByCollection
                {
                    UserName = _username,
                    Password = _password,
                    Country = country,
                    PaymentMode = paymentMode,
                    CollectionAmount = collectionAmount,
                    PayoutAmount = payoutAmount,
                    BankBranchId = bankBranchId,
                    IsNewAccount = isNewAccount
                });

                jsonText = JsonConvert.SerializeObject(response);

            }
            else
            {
                var response = await _sendUat.GetServiceChargeByCollectionAsync(new InmtUatService.InputGetServiceChargeByCollection
                {
                    UserName = _username,
                    Password = _password,
                    Country = country,
                    PaymentMode = paymentMode,
                    CollectionAmount = collectionAmount,
                    PayoutAmount = payoutAmount,
                    BankBranchId = bankBranchId,
                    IsNewAccount = isNewAccount
                });

                jsonText = JsonConvert.SerializeObject(response);
            }


            //// To convert an XML node contained in string xml into a JSON string   
            //XmlDocument doc = new XmlDocument();
            //var xml = Converter.XmlSerializer(response);
            //doc.LoadXml(xml);

            //string jsonText = JsonConvert.SerializeXmlNode(doc);


            ServiceChargeDto dto = JsonConvert.DeserializeObject<ServiceChargeDto>(jsonText);


            return dto;
        }



        public async Task<CashPayLocationListDto> GetCashPayLocationListAsync(string country,
                                                                     string state,
                                                                     string district,
                                                                     string city,
                                                                     string locationName)
        {
            var response = await _sendLive.CashPayLocationListAsync(new InmtLiveService.InputCashPayLocationList
            {
                UserName = _username,
                Password = _password,
                Country = country,
                State = state,
                District = district,
                City = city,
                LocationName = locationName
            });

            // To convert an XML node contained in string xml into a JSON string   
            XmlDocument doc = new XmlDocument();
            var xml = Converter.XmlSerializer(response);
            doc.LoadXml(xml);

            string jsonText = JsonConvert.SerializeXmlNode(doc);

            CashPayLocationListDto dto = JsonConvert.DeserializeObject<CashPayLocationListDto>(jsonText);


            return dto;
        }



        public async Task<AcPayBankBranchListDto> GetAcPayBankBranchListAsync(string country,
                                                                     string state,
                                                                     string district,
                                                                     string city,
                                                                     string bankName,
                                                                     string branchName)
        {
            var response = await _sendLive.AcPayBankBranchListAsync(new InmtLiveService.InputAcPayBankBranchList
            {
                UserName = _username,
                Password = _password,
                Country = country,
                State = state,
                District = district,
                City = city,
                BankName = bankName,
                BranchName = branchName
            });

            // To convert an XML node contained in string xml into a JSON string   
            XmlDocument doc = new XmlDocument();
            var xml = Converter.XmlSerializer(response);
            doc.LoadXml(xml);

            string jsonText = JsonConvert.SerializeXmlNode(doc);

            AcPayBankBranchListDto dto = JsonConvert.DeserializeObject<AcPayBankBranchListDto>(jsonText);

            return dto;
        }


        public async Task<SendTransactionDto> SendTransactionAsync(SendTransactionModel model)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                InmtLiveService.InputSendTransaction inputSendTransaction = Extensions.CopyFrom<SendTransactionModel, InmtLiveService.InputSendTransaction>(new InmtLiveService.InputSendTransaction(), model);

                inputSendTransaction.UserName = _username;
                inputSendTransaction.Password = _password;

                var response = await _sendLive.SendTransactionAsync(inputSendTransaction);

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                InmtUatService.InputSendTransaction inputSendTransaction = Extensions.CopyFrom<SendTransactionModel, InmtUatService.InputSendTransaction>(new InmtUatService.InputSendTransaction(), model);

                inputSendTransaction.UserName = _username;
                inputSendTransaction.Password = _password;

                var response = await _sendUat.SendTransactionAsync(inputSendTransaction);

                jsonText = JsonConvert.SerializeObject(response);

            }

            var dto = JsonConvert.DeserializeObject<SendTransactionDto>(jsonText);

            //if (dto.Code == "000" && !string.IsNullOrEmpty(dto.PinNo))
            //{
            //    await VerifyTransactionAsync(dto.PinNo);
            //}

            return dto;
        }

        public async Task<CancelTransactionDto> CancelTransactionAsync(CancelTransactionModel model)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {


                var response = await _sendLive.CancelTransactionAsync(new InmtLiveService.InputCancelTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = model.PinNo,
                    ReasonForCancellation = model.ReasonForCancellation
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {


                var response = await _sendUat.CancelTransactionAsync(new InmtUatService.InputCancelTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = model.PinNo,
                    ReasonForCancellation = model.ReasonForCancellation
                });

                jsonText = JsonConvert.SerializeObject(response);

            }

            CancelTransactionDto dto = JsonConvert.DeserializeObject<CancelTransactionDto>(jsonText);


            return dto;
        }


        public async Task<VerifyTransactionDto> VerifyTransactionAsync(string pinNo)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.VerifyTransactionAsync(new InmtLiveService.InputVerifyTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = pinNo
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.VerifyTransactionAsync(new InmtUatService.InputVerifyTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = pinNo
                });

                jsonText = JsonConvert.SerializeObject(response);
            }

            var dto = JsonConvert.DeserializeObject<VerifyTransactionDto>(jsonText);

            return dto;
        }


        public async Task<UnverifiedTransactionsDto> UnverifiedTransactionsAsync()
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.UnverifiedTransactionsAsync(new InmtLiveService.InputUnverifiedTransactions
                {
                    UserName = _username,
                    Password = _password
                });
              
                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.UnverifiedTransactionsAsync(new InmtUatService.InputUnverifiedTransactions
                {
                    UserName = _username,
                    Password = _password
                });

                //// To convert an XML node contained in string xml into a JSON string   
                //XmlDocument doc = new XmlDocument();
                //var xml = Converter.XmlSerializer(response);
                //doc.LoadXml(xml);

                //jsonText = JsonConvert.SerializeXmlNode(doc);
                jsonText = JsonConvert.SerializeObject(response);

            }

            var dto = JsonConvert.DeserializeObject<UnverifiedTransactionsDto>(jsonText);

            return dto;
        }

        public async Task<ComplianceTransactionsDto> ComplianceTransactionsAsync()
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.ComplianceTransactionsAsync(new InmtLiveService.InputComplianceTransactions
                {
                    UserName = _username,
                    Password = _password
                });

                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument doc = new XmlDocument();
                var xml = Converter.XmlSerializer(response);
                doc.LoadXml(xml);

                jsonText = JsonConvert.SerializeXmlNode(doc);
            }
            else
            {
                var response = await _sendUat.ComplianceTransactionsAsync(new InmtUatService.InputComplianceTransactions
                {
                    UserName = _username,
                    Password = _password
                });

                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument doc = new XmlDocument();
                var xml = Converter.XmlSerializer(response);
                doc.LoadXml(xml);

                jsonText = JsonConvert.SerializeXmlNode(doc);

            }

            var dto = JsonConvert.DeserializeObject<ComplianceTransactionsDto>(jsonText);

            return dto;
        }

        public async Task<SearchTransactionsDto> SearchAllTransactionAsync()
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.SearchTransactionAsync(new InmtLiveService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.SearchTransactionAsync(new InmtUatService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password
                });

                jsonText = JsonConvert.SerializeObject(response);

            }

            SearchTransactionsDto dto = JsonConvert.DeserializeObject<SearchTransactionsDto>(jsonText);

            return dto;
        }

        public async Task<SearchTransactionsDto> SearchTransactionByDateRangeAsync(string fromDate, string toDate)
        {

            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.SearchTransactionAsync(new InmtLiveService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password,
                    FromDate = fromDate,
                    ToDate = toDate
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.SearchTransactionAsync(new InmtUatService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password,
                    FromDate = fromDate,
                    ToDate = toDate
                });

                jsonText = JsonConvert.SerializeObject(response);

            }

            SearchTransactionsDto dto = JsonConvert.DeserializeObject<SearchTransactionsDto>(jsonText);

            return dto;
        }

        public async Task<SearchTransactionsDto> SearchTransactionByPinNoAsync(string pinNo)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.SearchTransactionAsync(new InmtLiveService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = pinNo
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.SearchTransactionAsync(new InmtUatService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = pinNo
                });

                jsonText = JsonConvert.SerializeObject(response);

            }

            SearchTransactionsDto dto = JsonConvert.DeserializeObject<SearchTransactionsDto>(jsonText);

            return dto;
        }

        public async Task<SearchTransactionsDto> SearchTransactionByPartnerPinNoAsync(string partnerPinNo)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.SearchTransactionAsync(new InmtLiveService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PartnerPinNo = partnerPinNo
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.SearchTransactionAsync(new InmtUatService.InputSearchTransaction
                {
                    UserName = _username,
                    Password = _password,
                    PartnerPinNo = partnerPinNo
                });

                jsonText = JsonConvert.SerializeObject(response);

            }

            SearchTransactionsDto dto = JsonConvert.DeserializeObject<SearchTransactionsDto>(jsonText);

            return dto;
        }
        public async Task<object> ValidateCardNumberAsync(string cardNumber)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.ValidateCardNumberAsync(new InmtLiveService.InputValidateCardNumber
                {
                    UserName = _username,
                    Password = _password,
                    CardNumber = cardNumber
                });

                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument doc = new XmlDocument();
                var xml = Converter.XmlSerializer(response);
                doc.LoadXml(xml);

                jsonText = JsonConvert.SerializeXmlNode(doc);
            }
            else
            {
                var response = await _sendUat.ValidateCardNumberAsync(new InmtUatService.InputValidateCardNumber
                {
                    UserName = _username,
                    Password = _password,
                    CardNumber = cardNumber
                });

                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument doc = new XmlDocument();
                var xml = Converter.XmlSerializer(response);
                doc.LoadXml(xml);

                jsonText = JsonConvert.SerializeXmlNode(doc);
            }

            var dto = JsonConvert.DeserializeObject<object>(jsonText);

            return dto;
        }


        public async Task<ValidateBankAccountDto> ValidateBankAccountAsync(string bankCode, string accountNumber)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.ValidateBankAccountAsync(new InmtLiveService.InputValidateBankAccount
                {
                    UserName = _username,
                    Password = _password,
                    BankCode = bankCode,
                    AccountNumber = accountNumber
                });

                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument doc = new XmlDocument();
                var xml = Converter.XmlSerializer(response);
                doc.LoadXml(xml);

                jsonText = JsonConvert.SerializeXmlNode(doc);
            }
            else
            {
                var response = await _sendUat.ValidateBankAccountAsync(new InmtUatService.InputValidateBankAccount
                {
                    UserName = _username,
                    Password = _password,
                    BankCode = bankCode,
                    AccountNumber = accountNumber
                });

                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument doc = new XmlDocument();
                var xml = Converter.XmlSerializer(response);
                doc.LoadXml(xml);

                jsonText = JsonConvert.SerializeXmlNode(doc);

            }


            var dto = JsonConvert.DeserializeObject<ValidateBankAccountDto>(jsonText);

            return dto;
        }


        public async Task<UploadCustomerDocumentDto> UploadCustomerDocumentAsync(UploadCustomerDocumentModel model)
        {
            //// convert string to stream
            //byte[] byteArray = Encoding.UTF8.GetBytes(contents);
            ////byte[] byteArray = Encoding.ASCII.GetBytes(contents);
            //MemoryStream stream = new MemoryStream(byteArray);

            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.UploadCustomerDocumentAsync(new InmtLiveService.InputUploadCustomerDocument
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = model.PinNo,
                    FileName = model.FileName,
                    DocumentType = model.DocumentType,
                    FileData = new MemoryStream(model.FileData)
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.UploadCustomerDocumentAsync(new InmtUatService.InputUploadCustomerDocument
                {
                    UserName = _username,
                    Password = _password,
                    PinNo = model.PinNo,
                    FileName = model.FileName,
                    DocumentType = model.DocumentType,
                    FileData = new MemoryStream(model.FileData)
                });

                jsonText = JsonConvert.SerializeObject(response);

            }

            var dto = JsonConvert.DeserializeObject<UploadCustomerDocumentDto>(jsonText);

            return dto;
        }

        public async Task<CustomerDto> GetCustomerByMobileAsync(string mobile)
        {

            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.GetCustomerByMobileAsync(new InmtLiveService.InputGetCustomerByMobile
                {
                    UserName = _username,
                    Password = _password,
                    CustomerMobile = mobile
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.GetCustomerByMobileAsync(new InmtUatService.InputGetCustomerByMobile
                {
                    UserName = _username,
                    Password = _password,
                    CustomerMobile = mobile
                });

                jsonText = JsonConvert.SerializeObject(response);
            }

            CustomerDto dto = JsonConvert.DeserializeObject<CustomerDto>(jsonText);

            return dto;
        }

        public async Task<CustomerDto> GetCustomerByIdNumberAsync(string id)
        {
            string jsonText;
            if (_mode == Mode.L.ToString())
            {
                var response = await _sendLive.GetCustomerByIdNumberAsync(new InmtLiveService.InputGetCustomerByIdNumber
                {
                    UserName = _username,
                    Password = _password,
                    CustomerIdNo = id
                });

                jsonText = JsonConvert.SerializeObject(response);
            }
            else
            {
                var response = await _sendUat.GetCustomerByIdNumberAsync(new InmtUatService.InputGetCustomerByIdNumber
                {
                    UserName = _username,
                    Password = _password,
                    CustomerIdNo = id
                });

                jsonText = JsonConvert.SerializeObject(response);
            }

            CustomerDto dto = JsonConvert.DeserializeObject<CustomerDto>(jsonText);

            return dto;
        }
    }
}
