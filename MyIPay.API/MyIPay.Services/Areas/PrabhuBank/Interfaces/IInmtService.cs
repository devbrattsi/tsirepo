﻿using MyIPay.Dtos.Areas.PrabhuBank;
using MyIPay.Models.Areas.PrabhuBank;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.PrabhuBank.Interfaces
{
    public interface IInmtService
    {
        Task<ServiceChargeDto> GetServiceChargeAsync(string country, string paymentMode, string transferAmount, string payoutAmount, string bankBranchId, string isNewAccount);


        Task<ServiceChargeDto> GetServiceChargeByCollectionAsync(string country, string paymentMode, string collectionAmount, string payoutAmount, string bankBranchId, string isNewAccount);

        Task<CashPayLocationListDto> GetCashPayLocationListAsync(string country, string state, string district, string city, string locationName);

        Task<AcPayBankBranchListDto> GetAcPayBankBranchListAsync(string country, string state, string district, string city, string bankName, string branchName);

        Task<SendTransactionDto> SendTransactionAsync(SendTransactionModel model);

        Task<CancelTransactionDto> CancelTransactionAsync(CancelTransactionModel model);

        Task<VerifyTransactionDto> VerifyTransactionAsync(string pinNo);

        Task<UnverifiedTransactionsDto> UnverifiedTransactionsAsync();

        Task<ComplianceTransactionsDto> ComplianceTransactionsAsync();

        Task<SearchTransactionsDto> SearchAllTransactionAsync();
        Task<SearchTransactionsDto> SearchTransactionByDateRangeAsync(string fromDate, string toDate);

        Task<SearchTransactionsDto> SearchTransactionByPinNoAsync(string pinNo);
        Task<SearchTransactionsDto> SearchTransactionByPartnerPinNoAsync(string partnerPinNo);
        Task<object> ValidateCardNumberAsync(string cardNumber);

        Task<ValidateBankAccountDto> ValidateBankAccountAsync(string bankCode, string accountNumber);
        Task<UploadCustomerDocumentDto> UploadCustomerDocumentAsync(UploadCustomerDocumentModel model);
        Task<CustomerDto> GetCustomerByMobileAsync(string mobile);
        Task<CustomerDto> GetCustomerByIdNumberAsync(string id);

    }
}
