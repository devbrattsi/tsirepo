﻿using log4net;
using MyIPay.Dtos.Areas.Common;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Common;
using MyIPay.Services.Areas.Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;

namespace MyIPay.Services.Areas.Common
{
    public class CommunicationService : ICommunicationService
    {

        private readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string URL = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
        public string SendSMS(SmsModel model)
        {
            _log.Info("SMS Data Received --> " + JsonConvert.SerializeObject(model));
            string result = string.Empty;
            string response = string.Empty;
            ResponseMessageDto responseObj = new ResponseMessageDto();
            try
            {
                string profileId = ConfigurationManager.AppSettings["ProfileID"];
                string password = ConfigurationManager.AppSettings["Password"];
                string senderId = ConfigurationManager.AppSettings["SenderID"];
                string mobileNumber = string.Empty;

                foreach (var number in model.mobileNumber)
                {
                    mobileNumber += number.Trim() + ",";
                }

                mobileNumber = mobileNumber.Remove(mobileNumber.Length - 1);
                URL += "user=" + profileId + "&pwd=" + password + "&senderid=" + senderId + "&mobileno=" + mobileNumber + "&msgtext=" + model.messageBody + "&smstype=0";
                response = SendRequest(URL);
                SmsSendResponseDto sms_Response = JsonConvert.DeserializeObject<SmsSendResponseDto>(response);
                if (sms_Response.MessageSendSuccess)
                {
                    responseObj.IsSuccess = true;
                    responseObj.Message = "SMS Send";
                    responseObj.Response = "SMS Send Successfully";
                }
                else
                {
                    responseObj.IsSuccess = false;
                    responseObj.Message = "SMS not Send !!!";
                    responseObj.Response = "SMS not Send !!!";
                }

                result = JsonConvert.SerializeObject(responseObj);
            }

            catch (Exception ex)
            {
                responseObj.IsSuccess = false;
                responseObj.Message = "SMS not Send !!!";
                responseObj.Response = ex.Message;
                result = JsonConvert.SerializeObject(responseObj);
                _log.Info("SMS Error --> " + ex.Message);
                _log.Info("SMS Error --> " + ex.InnerException);
                _log.Info("SMS Error --> " + ex.StackTrace);
            }
            finally
            {
                result = JsonConvert.SerializeObject(responseObj);
            }
            _log.Info("SMS Data Response --> " + result);
            return result;
        }


        public string SendWebPortalEmail(SendEmailModel model)
        {
            _log.Info("Email Data Received --> " + JsonConvert.SerializeObject(model));
            Random rnd = new Random();
            string filePath = string.Empty;
            ResponseMessageDto responseObj = new ResponseMessageDto();
            MailMessage message = null;
            Attachment attachment = null;
            string[] toAddressList = model.toAddress.Split(';');
            string result = string.Empty;
            try
            {
                if (model.attachmentFile != null)
                {
                    filePath = HttpContext.Current.Server.MapPath("~") + "SendEmailFolder";
                    if (!Directory.Exists(filePath))
                        Directory.CreateDirectory(filePath);

                    if (model.attachFileName.Count > 1)
                    {
                        char[] delimiterChars = { '-', ':', '.' };
                        string fileName = Convert.ToString(DateTime.Now);
                        string name = string.Empty;
                        string[] splitWord = fileName.Split(delimiterChars);
                        foreach (string Name in splitWord)
                        {
                            name += Name;
                        }
                        filePath = filePath + "\\Mulit_File_Folder" + name; // Creating Folder
                        Directory.CreateDirectory(filePath);

                        for (int i = 0; i < model.attachmentFile.Count; i++)
                        {
                            string newFileName = filePath + "\\File_" + rnd.Next(999) + "_" + model.attachFileName[i];
                            File.WriteAllBytes(newFileName, model.attachmentFile[i]);
                        }

                        string zipPath = filePath + ".zip";
                        ZipFile.CreateFromDirectory(filePath, zipPath);
                        DeleteFolder(filePath);
                        filePath = filePath + ".zip";
                    }
                    else
                    {
                        for (int i = 0; i < model.attachmentFile.Count; i++)
                        {
                            filePath = filePath + "\\Receipt_" + rnd.Next(999) + model.attachFileName[i];
                            File.WriteAllBytes(filePath, model.attachmentFile[i]);
                        }
                    }

                }

                string fromAddress = ConfigurationManager.AppSettings["FromWebPortalAddress"];
                string fromPassword = ConfigurationManager.AppSettings["FromWebPortalPassword"];
                int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_Port_WebPortal"]);
                MailAddress fromEmail = new MailAddress(fromAddress, "i-Pay");

                string subject = model.emailSubject;

                string body = model.messageBody;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = smtpPort,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromEmail.Address, fromPassword)

                };

                foreach (var toEmailAddress in toAddressList)
                {
                    message = new MailMessage(fromEmail.Address, toEmailAddress.Trim())
                    {
                        Subject = subject,
                        // Body = body,
                        IsBodyHtml = true,
                        Priority = MailPriority.Normal,
                        BodyEncoding = System.Text.Encoding.GetEncoding("utf-8")

                    };

                    System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(body, @"< (.|\n) *?>", string.Empty), null, "text/plain");
                    System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, null, "text/html");

                    message.AlternateViews.Add(plainView);
                    message.AlternateViews.Add(htmlView);


                    if (filePath.Length > 0)
                    {
                        attachment = new Attachment(filePath, MediaTypeNames.Application.Octet);
                        ContentDisposition disposition = attachment.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(filePath);
                        disposition.ModificationDate = File.GetLastWriteTime(filePath);
                        disposition.ReadDate = File.GetLastAccessTime(filePath);
                        disposition.FileName = Path.GetFileName(filePath);
                        disposition.Size = new FileInfo(filePath).Length;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        message.Attachments.Add(attachment);
                    }

                    if (model.ccList != null)
                    {
                        foreach (var item in model.ccList)
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }

                    if (model.bccList != null)
                    {
                        foreach (var item in model.bccList)
                        {
                            message.Bcc.Add(new MailAddress(item));
                        }
                    }

                    smtp.Send(message);
                }

                //File.Delete(filePath);

                responseObj.IsSuccess = true;
                responseObj.Message = "Email Sent Successfully !!!";
                responseObj.Response = "Email Sent Successfully !!!";
                result = JsonConvert.SerializeObject(responseObj);

            }
            catch (Exception ex)
            {
                responseObj.IsSuccess = false;
                responseObj.Message = "Email not Send !!!";
                responseObj.Response = ex.Message;
                _log.Info("Email Error --> " + ex.Message);
                _log.Info("Email Error --> " + ex.InnerException);
                _log.Info("Email Error --> " + ex.StackTrace);
                result = JsonConvert.SerializeObject(responseObj);
            }
            finally
            {
                result = JsonConvert.SerializeObject(responseObj);
            }

            _log.Info("Email Data Response --> " + result);
            return result;
        }

        public async Task<int> GetRemainingSmsCountAsync()
        {


            EnableHost.EnableTrustedHosts();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri("http://bulksmsindia.mobi/");

                client.DefaultRequestHeaders.Clear();

                //make sure to use TLS 1.2 first before trying other version
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");


                try
                {
                    string userId = ConfigurationManager.AppSettings["ProfileID"].ToString();
                    string password = ConfigurationManager.AppSettings["Password"].ToString();
                    string url = $"balance.asp?user={userId}&pwd={password}";

                    var response = await client.GetAsync(url);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                    string stringResponse = await response.Content.ReadAsStringAsync();
                    double amount = Convert.ToDouble(stringResponse.Trim(new char[] { ',', ' ' }));
                    int reminingSmsCount = (int)(amount / 0.14);
                    return reminingSmsCount;
                }
                catch (Exception ex)
                {
                    _log.Info("GetReminingSmsCountAsync Error --> " + ex.Message);
                    _log.Info("GetReminingSmsCountAsync Error --> " + ex.InnerException);
                    _log.Info("GetReminingSmsCountAsync Error --> " + ex.StackTrace);
                    return -1;
                }
            }
        }

        private static string SendRequest(string url)
        {

            EnableHost.EnableTrustedHosts();
            string result = string.Empty;
            string sms_API_Response = string.Empty;
            SmsSendResponseDto res = new SmsSendResponseDto();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                httpWebRequest.Timeout = 10000;
                var httpresponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpresponse.GetResponseStream()))
                {
                    var output = streamReader.ReadToEnd();
                    sms_API_Response = output;
                }

                res.MessageSendSuccess = true;
                res.Message = sms_API_Response;
                res.Response = sms_API_Response;
                result = JsonConvert.SerializeObject(res);
                return result;
            }
            catch (Exception ex)
            {
                res.MessageSendSuccess = false;
                res.Message = "Exception: " + ex.Message; ;
                res.Response = "Exception: " + ex.Message; ;
                result = JsonConvert.SerializeObject(res);
                return result;
            }
        }

        private void DeleteFolder(string path)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo f in di.GetFiles())
                {
                    f.Delete();
                }
                Directory.Delete(path);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
