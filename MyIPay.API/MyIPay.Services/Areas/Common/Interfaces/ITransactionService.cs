﻿using MyIPay.Models.Areas.Yatra;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common.Interfaces
{
    public interface ITransactionService<TResult> where TResult : class
    {
        Task<TResult> SaveTransactionAsync(DoPaymentAsyncModel model, decimal availableBalance);

        Task<TResult> RefundAsync(BookingCancellationAsyncModel model);// (string orderId, decimal amount, string refundedOrderId);

        Task<TResult> GiveCommissionAsync(BookingCancellationAsyncModel model);
    }
}
