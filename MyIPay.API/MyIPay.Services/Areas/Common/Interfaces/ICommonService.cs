﻿
using MyIPay.Dtos.Areas.Common;
using MyIPay.Models.Areas.Common;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common.Interfaces
{
    public interface ICommonService
    {
        Task<AvailableBalanceDto> GetAvailableBalanceAsync(AvailableBalanceModel model);
        decimal? GetMerchantAvailableBalance(string agentId, string role, int distributorId);
       // Task<decimal?> GetRecptNumber();
        //Task<string> GetReceiptNumber();
        Task<decimal> GetReceiptNumber();
        Task<string> GetReceiptRefNumber();
        Task<decimal> GetBatchNumber();
    }
}
