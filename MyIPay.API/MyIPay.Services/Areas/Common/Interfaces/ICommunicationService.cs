﻿using MyIPay.Models.Areas.Common;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common.Interfaces
{
    public interface ICommunicationService
    {
        string SendSMS(SmsModel smsModel);
        string SendWebPortalEmail(SendEmailModel model);
        Task<int> GetRemainingSmsCountAsync();
    }
}
