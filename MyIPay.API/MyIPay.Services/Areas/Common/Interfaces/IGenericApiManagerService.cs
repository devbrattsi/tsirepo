﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common.Interfaces
{
    public interface IGenericApiManagerService<TResult> where TResult : class
    {
        Task<TResult> GetAsync(string baseUrl, string url);
        Task<TResult> PutAsync(string baseUrl, string url, string json);
        Task<TResult> PostAsync(string baseUrl, string url, string json);
        Task<TResult> DeleteAsync(string baseUrl, string url);
    }
}
