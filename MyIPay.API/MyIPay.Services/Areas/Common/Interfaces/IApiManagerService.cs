﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common.Interfaces
{
    public interface IApiManagerService
    {
        Task<HttpResponseMessage> GetAsync(string baseUrl, string url);
        Task<HttpResponseMessage> GetAsync(string baseUrl, string url, HttpClient client);
        Task<HttpResponseMessage> PutAsync(string baseUrl, string url, StringContent stringContent);
        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, StringContent stringContent);
        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, string json);
        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, string json, HttpClient client);
        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, IDictionary<string, string> keyValuePairs, HttpClient client);
        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, StringContent stringContent, HttpClient client);
        Task<HttpResponseMessage> DeleteAsync(string baseUrl, string url);
    }
}
