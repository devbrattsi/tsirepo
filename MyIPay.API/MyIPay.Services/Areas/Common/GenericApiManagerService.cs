﻿using MyIPay.Services.Areas.Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common
{
    public class GenericApiManagerService<TResult> : IGenericApiManagerService<TResult> where TResult : class
    {
        private readonly IApiManagerService _apiManagerService;
        public GenericApiManagerService(IApiManagerService apiManagerService)
        {
            _apiManagerService = apiManagerService;
        }

        public async Task<TResult> GetAsync(string baseUrl, string url)
        {

            //Getting API Response
            var response = await _apiManagerService.GetAsync(baseUrl, url);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(response.ReasonPhrase);
            }

            //Reading response as string
            string responseString = await response.Content.ReadAsStringAsync();

            //Deserializing response content string
            var result = JsonConvert.DeserializeObject<TResult>(responseString);

            //Returing result
            return result;
        }

        public async Task<TResult> PutAsync(string baseUrl, string url, string json)
        {
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            //Getting API Response
            var response = await _apiManagerService.PutAsync(baseUrl, url, content);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(response.ReasonPhrase);
            }

            //Reading response content as string
            string responseString = await response.Content.ReadAsStringAsync();

            //Deserializing response content string
            var result = JsonConvert.DeserializeObject<TResult>(responseString);

            //Returing result
            return result;

        }

        public async Task<TResult> PostAsync(string baseUrl, string url, string json)
        {
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            //Getting API Response
            var response = await _apiManagerService.PostAsync(baseUrl, url, content);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(response.ReasonPhrase);
            }

            //Reading response content as string
            string responseString = await response.Content.ReadAsStringAsync();

            //Deserializing response content string
            var result = JsonConvert.DeserializeObject<TResult>(responseString);

            //Returing result
            return result;

        }

        public async Task<TResult> DeleteAsync(string baseUrl, string url)
        {

            //Getting API Response
            var response = await _apiManagerService.DeleteAsync(baseUrl, url);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(response.ReasonPhrase);
            }

            //Reading response as string
            string responseString = await response.Content.ReadAsStringAsync();

            //Deserializing response content string
            var result = JsonConvert.DeserializeObject<TResult>(responseString);

            //Returing result
            return result;
        }
    }
}
