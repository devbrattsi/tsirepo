﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MyIPay.DataLayer.EF;
using MyIPay.Dtos.Areas.Common;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Common;
using MyIPay.Services.Areas.Common.Interfaces;

namespace MyIPay.Services.Areas.Common
{
    public class CommonService : ICommonService
    {
        private readonly MyIPayEntities _myIPayEntities;

        public CommonService(MyIPayEntities myIPayEntities)
        {
            _myIPayEntities = myIPayEntities;
        }
        public async Task<AvailableBalanceDto> GetAvailableBalanceAsync(AvailableBalanceModel model)
        {
            using (var db = new MyIPayEntities())
            {
                var userDetail = await db.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToLower() == model.AgentId.ToLower());

                if (userDetail == null)
                {
                    return new AvailableBalanceDto
                    {
                        StatusCode = Constants.FailureStatusCode,
                        Remarks = Constants.RejectRemarks,
                        Description = Constants.AgentIdNotExist
                    };

                }


                var availableBalance = db.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();

                return new AvailableBalanceDto
                {
                    StatusCode = Constants.SuccessStatusCode,
                    Remarks = availableBalance.HasValue && availableBalance.Value >= model.Amount ? Constants.AcceptRemarks : Constants.RejectRemarks,
                    Description = availableBalance.HasValue && availableBalance.Value >= model.Amount ? Constants.BalanceAvailable : Constants.InsufficientBalance
                };

            }
        }

        public decimal? GetMerchantAvailableBalance(string agentId, string role, int distributorId)
        {
            using (var db = new MyIPayEntities())
            {
                return db.sp_GET_AVAILABLE_LIMIT(agentId, role, distributorId).FirstOrDefault();
            }

        }

        public async Task<decimal> GetReceiptNumber()
        {
            decimal? id = 0;
            try
            {
                var cnt = await _myIPayEntities.TransactionsDBs.CountAsync();
                if (cnt > 0)
                {
                    id = await _myIPayEntities.TransactionsDBs.DefaultIfEmpty().MaxAsync(m => m.RCPTNO) + 1;
                }
                else
                {
                    id = 1;
                }
            }
            catch (Exception ex)
            {
                Helpers.Utility.WriteLog("CommonService :: GetReceiptNumber Error --> " + ex.Message);
                Helpers.Utility.WriteLog("CommonService :: GetReceiptNumber Error --> " + ex.InnerException);
                Helpers.Utility.WriteLog("CommonService :: GetReceiptNumber Error --> " + ex.StackTrace);

            }
            return id.Value;
        }

        public async Task<string> GetReceiptRefNumber()
        {
            string receiptNumber = string.Empty;
            try
            {
                int id = 0;
                var cnt = await _myIPayEntities.TransactionsDBs.CountAsync();
                if (cnt > 0)
                {

                    id = await _myIPayEntities.TransactionsDBs.DefaultIfEmpty().MaxAsync(m => m.ID);
                }
                receiptNumber = "TSI" + (id + 1).ToString().PadLeft(6, '0');

            }
            catch (Exception ex)
            {
                Helpers.Utility.WriteLog("CommonService :: GetReceiptRefNumber Error --> " + ex.Message);
                Helpers.Utility.WriteLog("CommonService :: GetReceiptRefNumber Error --> " + ex.InnerException);
                Helpers.Utility.WriteLog("CommonService :: GetReceiptRefNumber Error --> " + ex.StackTrace);
            }
            return receiptNumber;
        }

        public async Task<decimal> GetBatchNumber()
        {
            decimal? batchNo = 1;
            try
            {
                batchNo = await _myIPayEntities.TransactionsDBs.DefaultIfEmpty().MaxAsync(mx => mx.BATCHNO);
                if (batchNo.HasValue)
                    batchNo += 1;
                else
                    batchNo = 1;

            }
            catch (Exception ex)
            {
                Helpers.Utility.WriteLog("CommonService :: GetBatchNumber Error --> " + ex.Message);
                Helpers.Utility.WriteLog("CommonService :: GetBatchNumber Error --> " + ex.InnerException);
                Helpers.Utility.WriteLog("CommonService :: GetBatchNumber Error --> " + ex.StackTrace);
            }
            return batchNo.Value;
        }


    }
}
