﻿using MyIPay.DataLayer.EF;
using MyIPay.DataLayer.EF.DefaultContext.Entity.Procedure;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Common;
using MyIPay.Models.Areas.Yatra;
using MyIPay.Services.Areas.Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common
{
    public class TransactionService<TResult> : ITransactionService<TResult> where TResult : class
    {
        private readonly MyIPayEntities _myIPayEntities;
        private readonly ICommonService _commonService;
        public TransactionService(MyIPayEntities myIPayEntities, ICommonService commonService)
        {
            _myIPayEntities = myIPayEntities;
            _commonService = commonService;
        }

        public async Task<TResult> SaveTransactionAsync(DoPaymentAsyncModel model, decimal availableBalance)
        {

            TResult result = default(TResult);

            try
            {
                DateTime dateTimeNow = DateTime.Now;

                decimal? batchNo = await _myIPayEntities.TransactionsDBs.MaxAsync(mx => mx.BATCHNO);

                if (batchNo != null)
                    batchNo += 1;
                else
                    batchNo = 1;
                string transID = string.Empty;

                decimal? rcpt = await _commonService.GetReceiptNumber();
                string receiptNumber = await _commonService.GetReceiptRefNumber();


                var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == model.AgentId.ToUpper());


                if (userDetail != null)
                {

                    string usp = model.Product.ToUpper() == "FLIGHT" ? USP.AT.ToString()
                                                                     : model.Product == "HOTEL"
                                                                     ? USP.HTL.ToString()
                                                                     : model.Product == "IRCTC"
                                                                     ? USP.IRCTC.ToString()
                                                                     : model.Product;

                    SpGetYatraCcfByUsp ccfResult = new SpGetYatraCcfByUsp()
                    {
                        AgentId = userDetail.AgentId,
                        Role = userDetail.Role
                    };

                    if (usp == USP.AT.ToString() || usp == USP.HTL.ToString())
                    {
                        SqlParameter[] parameter = new SqlParameter[]
                        {
                                 new SqlParameter
                                 {
                                     ParameterName = "@agentId",
                                     SqlDbType = SqlDbType.VarChar,
                                     Direction = ParameterDirection.Input,
                                     Value = userDetail.AgentId
                                 },
                                 new SqlParameter
                                 {
                                     ParameterName = "@usp",
                                     SqlDbType = SqlDbType.VarChar,
                                     Direction = ParameterDirection.Input,
                                     Value = usp
                                 },
                                 new SqlParameter
                                 {
                                     ParameterName = "@amount",
                                     SqlDbType = SqlDbType.Decimal,
                                     Direction = ParameterDirection.Input,
                                     Value = model.Amount
                                 }
                        };

                        ccfResult = await _myIPayEntities.Database
                                                         .SqlQuery<SpGetYatraCcfByUsp>(SqlQueryConstants.SpGetYatraCcfByUsp, parameter)
                                                         .FirstOrDefaultAsync();

                    }

                    //decimal cashback = 0;
                    decimal ccf = ccfResult.Role.ToUpper() == Constants.Distributor ? ccfResult.DistributorCcf : ccfResult.AgentCcf;//decimal ccf = 0;

                    TransactionsDB transactionsDb = new TransactionsDB()
                    {
                        Mode = AppSettings.Mode,
                        AGENTID = model.AgentId,
                        USP = usp,
                        RCPTNO = rcpt,
                        RCPTREF = receiptNumber,
                        BATCHNO = (decimal)batchNo,
                        PAYMENTMODE = Mode.C.ToString(),

                        BATCHPAYMODEAMT = usp == USP.IRCTC.ToString() ? model.Amount + Convert.ToDecimal(ccf) : ccfResult.CollectionAmount,//model.Amount + Convert.ToDecimal(ccf),


                        PAYMENTDATE = dateTimeNow,
                        BILLAMOUNT = model.Amount,
                        COLLECTIONAMT = usp == USP.IRCTC.ToString() ? model.Amount + Convert.ToDecimal(ccf) : ccfResult.CollectionAmount,//model.Amount + Convert.ToDecimal(ccf),

                        REFERENCE1 = usp,
                        REFERENCE2 = usp,
                        REFERENCE3 = usp,
                        REFERENCE4 = model.RefernceNo,

                        REFERENCE5 = model.RefernceNo,
                        REFERENCE6 = Convert.ToString(ccf),


                        REFERENCE12 = JsonConvert.SerializeObject(model),
                        REFERENCE13 = Convert.ToString(ccfResult.DistributorMargin),//Convert.ToString(cashback),
                        REFERENCE14 = Convert.ToString(ccfResult.TsiMargin),//"0.00",
                        REFERENCE16 = Convert.ToString(ccfResult.AgentCashback),//"0.00",
                        REFERENCE17 = Convert.ToString(ccfResult.AgentTds),//"0.00",
                        REFERENCE18 = Convert.ToString(ccfResult.DistributorCashback),//"0.00",
                        REFERENCE19 = Convert.ToString(ccfResult.DistributorTds),//"0.00",
                        REFERENCE20 = Convert.ToString(ccfResult.CustomerFee),// "0.00",
                        REFERENCE22 = Partner.Yatra.ToString(),
                        REFERENCE26 = Convert.ToString((int)TxStatus.Success),
                        REFERENCE27 = Constants.Success,
                        REFERENCE28 = AppSettings.HostServer,
                        CreatedOn = dateTimeNow,
                        ReportBatchNo = 0,

                        update_token = (int)TransactionStatus.Success,
                        UPDATIONSTATUS = TransactionStatus.Success.ToString(),
                        ENACCNO = "null",
                        ENCOLLAMT = "null",
                        //mobileno = ,
                        //IsChequeCleared = null,
                        UpdationMessage = "",
                        UpdatedBy = "Yatra - Server to Server Call.",
                        DistributorId = userDetail.DistributorId,

                        REFERENCEDECIMAL1 = availableBalance - (model.Amount + Convert.ToInt32(ccf))
                    };

                    _myIPayEntities.TransactionsDBs.Add(transactionsDb);

                    //Save changes
                    await _myIPayEntities.SaveChangesAsync();

                    result = (TResult)Convert.ChangeType(transactionsDb.ID, typeof(TResult));

                }

            }
            catch (Exception ex)
            {
                result = default(TResult);

                Helpers.Utility.WriteLog("TransactionService :: SaveTransaction Error --> " + ex.Message);
                Helpers.Utility.WriteLog("TransactionService :: SaveTransaction Error --> " + ex.InnerException);
                Helpers.Utility.WriteLog("TransactionService :: SaveTransaction Error --> " + ex.StackTrace);
            }

            return result;
        }
        public async Task<TResult> RefundAsync(BookingCancellationAsyncModel model)//string orderId, decimal amount, string refundedOrderId)
        {

            TResult result = default(TResult);

            try
            {
                DateTime dateTimeNow = DateTime.Now;

                var transactionData = await _myIPayEntities.TransactionsDBs
                                                           .FirstOrDefaultAsync(x => x.REFERENCE5.ToUpper() == model.RefundedOrderId.ToUpper()
                                                                                && x.Mode == Mode.L.ToString());

                if (transactionData != null)
                {

                    transactionData.Mode = Mode.C.ToString(); // for Cancellation
                    transactionData.PAYMENTDATE = dateTimeNow;//transactionData.PAYMENTDATE;
                    transactionData.BILLAMOUNT = 0 - Convert.ToDecimal(model.Amount);//transactionData.BILLAMOUNT;
                    transactionData.COLLECTIONAMT = 0 - Convert.ToDecimal(model.Amount);//transactionData.COLLECTIONAMT;
                    transactionData.BATCHPAYMODEAMT = 0 - Convert.ToDecimal(model.Amount); //transactionData.BATCHPAYMODEAMT;

                    transactionData.REFERENCE4 = model.RefundedOrderId;

                    transactionData.REFERENCE5 = model.RefundedOrderId;
                    decimal.TryParse(transactionData.REFERENCE6, out decimal ref6);
                    transactionData.REFERENCE6 = "0";// (0 - Convert.ToDecimal(ref6)).ToString();

                    transactionData.REFERENCE7 = model.OrderId;
                    transactionData.REFERENCE11 = Partner.Yatra.ToString();

                    transactionData.REFERENCE12 = JsonConvert.SerializeObject(model);

                    decimal.TryParse(transactionData.REFERENCE13, out decimal ref13);
                    transactionData.REFERENCE13 = "0"; (0 - Convert.ToDecimal(ref13)).ToString();

                    transactionData.REFERENCE14 = "0.00";
                    transactionData.REFERENCE16 = "0.00";
                    transactionData.REFERENCE17 = "0.00";
                    transactionData.REFERENCE18 = "0.00";
                    transactionData.REFERENCE19 = "0.00";
                    transactionData.REFERENCE20 = "0.00";
                    transactionData.REFERENCE22 = Partner.Yatra.ToString();

                    transactionData.REFERENCE26 = Convert.ToString((int)TxStatus.Refunded);
                    transactionData.REFERENCE27 = Constants.Refunded;
                    transactionData.REFERENCE28 = AppSettings.HostServer;
                    transactionData.update_token = (int)TransactionStatus.Refunded;
                    transactionData.UPDATIONSTATUS = TransactionStatus.Refunded.ToString();

                    transactionData.UpdatedBy = "Yatra - Server to Server Call.";
                    transactionData.CreatedOn = dateTimeNow;


                    _myIPayEntities.Set<TransactionsDB>().Add(transactionData);

                    await _myIPayEntities.SaveChangesAsync();

                    result = (TResult)Convert.ChangeType(Constants.SuccessMsg, typeof(TResult));

                }

            }
            catch (Exception ex)
            {
                result = default(TResult);

                Helpers.Utility.WriteLog("TransactionService :: RefundAsync Error --> " + ex.Message);
                Helpers.Utility.WriteLog("TransactionService :: RefundAsync Error --> " + ex.InnerException);
                Helpers.Utility.WriteLog("TransactionService :: RefundAsync Error --> " + ex.StackTrace);
            }

            return result;
        }
        public async Task<TResult> GiveCommissionAsync(BookingCancellationAsyncModel model)
        {
            TResult result = default(TResult);

            try
            {
                //DateTime dateTimeNow = DateTime.Now;

                var transactionData = await _myIPayEntities.TransactionsDBs
                                                           .FirstOrDefaultAsync(x => x.USP == USP.IRCTC.ToString()
                                                                                && x.REFERENCE5.ToUpper() == model.RefundedOrderId.ToUpper()
                                                                                && x.Mode == Mode.L.ToString());

                if (transactionData != null)
                {

                    //var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == transactionData.AGENTID.ToUpper());

                    //if (userDetail.Role.ToUpper() == Constants.Distributor.ToUpper())
                    //{
                    //    transactionData.REFERENCE18 = model.Amount.ToString();
                    //}
                    //else
                    //{
                    //    transactionData.REFERENCE16 = model.Amount.ToString();
                    //}

                    var calculateCcfAndCashback = await CalculateCcfAndCashback(model, transactionData.AGENTID.ToUpper(), transactionData);

                    transactionData.REFERENCE11 = JsonConvert.SerializeObject(model);
                    transactionData.REFERENCE14 = calculateCcfAndCashback.TsiMargin.ToString();
                    transactionData.REFERENCE16 = calculateCcfAndCashback.AgentCashbackAmount.ToString();
                    transactionData.REFERENCE17 = calculateCcfAndCashback.AgentTdsAmount.ToString();
                    transactionData.REFERENCE18 = calculateCcfAndCashback.DistributorCashbackAmount.ToString();
                    transactionData.REFERENCE19 = calculateCcfAndCashback.DistributorTdsAmount.ToString();
                    transactionData.UpdatedOn = DateTime.Now;

                    _myIPayEntities.Entry<TransactionsDB>(transactionData).State = EntityState.Modified;

                    await _myIPayEntities.SaveChangesAsync();

                    result = (TResult)Convert.ChangeType(Constants.SuccessMsg, typeof(TResult));

                }

            }
            catch (Exception ex)
            {
                result = default(TResult);

                Helpers.Utility.WriteLog("TransactionService :: GiveCommissionAsync Error --> " + ex.Message);
                Helpers.Utility.WriteLog("TransactionService :: GiveCommissionAsync Error --> " + ex.InnerException);
                Helpers.Utility.WriteLog("TransactionService :: GiveCommissionAsync Error --> " + ex.StackTrace);
            }

            return result;
        }
        private async Task<CcfTdsModel> CalculateCcfAndCashback(BookingCancellationAsyncModel model, string agentId, TransactionsDB transactionsDb)
        {

            var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == agentId.ToUpper());


            CcfTdsModel ccfTdsModel = new CcfTdsModel();

            decimal pgCharges, gst;

            if (transactionsDb.BILLAMOUNT.Value <= 2000)
            {
                pgCharges = transactionsDb.BILLAMOUNT.Value * (decimal).0075;
            }
            else
            {
                pgCharges = transactionsDb.BILLAMOUNT.Value * (decimal).01;
            }
            gst = (decimal)18 / (decimal)100 * pgCharges;

            //decimal pgCons = (transactionsDb.BILLAMOUNT.Value * (decimal).0075);
            //decimal pgCharges = transactionsDb.BILLAMOUNT <= 2000 ? (pgCons + (transactionsDb.BILLAMOUNT.Value * (decimal).0075) * (decimal).18) / 2 : ((transactionsDb.BILLAMOUNT.Value * (decimal).01) + (transactionsDb.BILLAMOUNT.Value * (decimal).01) * (decimal).18) / 2;

            if (userDetail.Role.ToUpper() == Constants.Distributor.ToUpper()) // Code for Distributor
            {
                DistributorCcfMaster distibutorCcf = null;
                if (Convert.ToDecimal(model.Amount) < 20)
                {
                    distibutorCcf = await _myIPayEntities.DistributorCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.Utility == USP.IRCTCNONAC.ToString());
                }
                else
                {
                    distibutorCcf = await _myIPayEntities.DistributorCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.Utility == USP.IRCTCAC.ToString());
                }


                ccfTdsModel.DistributorTdsAmount = distibutorCcf == null ? 0 : (5 * distibutorCcf.DistributorCashBackPercentage.Value) / 100;
                ccfTdsModel.DistributorCashbackAmount = distibutorCcf == null ? 0 : (distibutorCcf.DistributorCashBackPercentage.Value - ccfTdsModel.DistributorTdsAmount);

                ccfTdsModel.TsiMargin = Convert.ToDecimal(model.Amount) - distibutorCcf.DistributorCashBackPercentage.Value + pgCharges + gst;

            }
            else  // Code for Agent
            {
                AgentCcfMaster agentCcfMaster = null;

                if (Convert.ToDecimal(model.Amount) < 20)
                    agentCcfMaster = await _myIPayEntities.AgentCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.Utility == USP.IRCTCNONAC.ToString());
                else
                    agentCcfMaster = await _myIPayEntities.AgentCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.Utility == USP.IRCTCAC.ToString());

                if (userDetail.DistributorId == 11) // Code for Direct Agent
                {
                    ccfTdsModel.AgentTdsAmount = agentCcfMaster == null ? 0 : (5 * agentCcfMaster.CashBackPercentage) / 100;
                    ccfTdsModel.AgentCashbackAmount = agentCcfMaster == null ? 0 : (agentCcfMaster.CashBackPercentage - ccfTdsModel.AgentTdsAmount);
                    ccfTdsModel.TsiMargin = Convert.ToDecimal(model.Amount) - agentCcfMaster.CashBackPercentage + pgCharges + gst;

                }
                else // Code for Distributor Agent
                {
                    ccfTdsModel.AgentTdsAmount = agentCcfMaster == null ? 0 : (5 * agentCcfMaster.CashBackPercentage) / 100;
                    ccfTdsModel.AgentCashbackAmount = agentCcfMaster == null ? 0 : (agentCcfMaster.CashBackPercentage - ccfTdsModel.AgentTdsAmount);

                    decimal distributorCashback = agentCcfMaster == null ? 0 : agentCcfMaster.DistributorCashBackPercentage.Value - agentCcfMaster.CashBackPercentage;
                    ccfTdsModel.DistributorTdsAmount = (5 * distributorCashback) / 100;
                    ccfTdsModel.DistributorCashbackAmount = distributorCashback - ccfTdsModel.DistributorTdsAmount;
                    ccfTdsModel.TsiMargin = Convert.ToDecimal(model.Amount) - agentCcfMaster.DistributorCashBackPercentage.Value + pgCharges + gst;

                }
            }

            return ccfTdsModel;
        }
    }
}
