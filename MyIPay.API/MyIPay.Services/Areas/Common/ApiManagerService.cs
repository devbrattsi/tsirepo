﻿using MyIPay.Helpers;
using MyIPay.Services.Areas.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Common
{
    public class ApiManagerService : IApiManagerService
    {
        public async Task<HttpResponseMessage> GetAsync(string baseUrl, string url)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                EnableHost.EnableTrustedHosts();
                ////make sure to use TLS 1.2 first before trying other version
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var response = await client.GetAsync(url);
                return response;
            }
        }
        public async Task<HttpResponseMessage> GetAsync(string baseUrl, string url, HttpClient client)
        {
            //Passing service base url  
            client.BaseAddress = new Uri(baseUrl);
            //client.DefaultRequestHeaders.Clear();

            EnableHost.EnableTrustedHosts();
            ////make sure to use TLS 1.2 first before trying other version
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //set Accept headers
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

            //set User agent
            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

            var response = await client.GetAsync(url);
            return response;

        }
        public async Task<HttpResponseMessage> PutAsync(string baseUrl, string url, StringContent stringContent)
        {

            using (var client = new HttpClient())
            {

                EnableHost.EnableTrustedHosts();
                ////make sure to use TLS 1.2 first before trying other version
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var response = await client.PutAsync(url, stringContent);

                return response;
            }
        }
        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, StringContent stringContent)
        {
            using (var client = new HttpClient())
            {
                EnableHost.EnableTrustedHosts();

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var response = await client.PostAsync(url, stringContent);
                return response;
            }
        }
        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, string json)
        {
            using (var client = new HttpClient())
            {
                EnableHost.EnableTrustedHosts();

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var content = new StringContent(json);

                var response = await client.PostAsync(url, content);
                return response;
            }
        }
        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, string json, HttpClient client)
        {
            EnableHost.EnableTrustedHosts();
            client.BaseAddress = new Uri(baseUrl);
            ////set Accept headers
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

            //set User agent
            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, stringContent);
            return response;

        }

        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, IDictionary<string, string> keyValuePairs, HttpClient client)
        {
            EnableHost.EnableTrustedHosts();
            client.BaseAddress = new Uri(baseUrl);
            ////set Accept headers
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

            //set User agent
            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");
            client.DefaultRequestHeaders.TryAddWithoutValidation("content-type", "application/x-www-form-urlencoded");

            var content = new FormUrlEncodedContent(keyValuePairs);

            var response = await client.PostAsync(url, content);

            return response;

        }


        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, StringContent stringContent, HttpClient client)
        {
            EnableHost.EnableTrustedHosts();
            //make sure to use TLS 1.2 first before trying other version

            client.Timeout = new TimeSpan(0, 5, 0);
            var response = await client.PostAsync(baseUrl + url, stringContent);
            return response;
        }
        public async Task<HttpResponseMessage> DeleteAsync(string baseUrl, string url)
        {
            using (var client = new HttpClient())
            {
                EnableHost.EnableTrustedHosts();
                ////make sure to use TLS 1.2 first before trying other version
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                var response = await client.DeleteAsync(url);
                return response;
            }
        }
    }
}
