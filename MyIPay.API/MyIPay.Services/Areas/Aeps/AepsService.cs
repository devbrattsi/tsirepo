﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.Aeps;
using MyIPay.Services.Areas.Aeps.Interfaces;
using MyIPay.Services.Areas.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace MyIPay.Services.Areas.Aeps
{
    public class AepsService : IAepsService
    {
        private readonly IApiManagerService _apiManagerService;

        public AepsService(IApiManagerService apiManagerService)
        {
            _apiManagerService = apiManagerService;
        }

        public async Task<HttpResponseMessage> GetBankDetailsAsync()
        {
            string url = "fingpay/getBankDetailsMasterData";
            var response = await _apiManagerService.GetAsync(AppSettings.AepsBaseUrl, url);
            return response;
        }
        public async Task<HttpResponseMessage> CashWithdrawalAsync(CashWithdrawalModel model, Dictionary<string, string> dictionary)
        {
            string json = Converter.JsonSerializer(model);// JsonConvert.SerializeObject(model);

            string url = "fpaepsservice/api/cashWithdrawal/merchant/withdrawal";
            var client = new HttpClient();


            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.TransactionTimestamp, dictionary[Constants.TransactionTimestamp]);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Hash, dictionary[Constants.Hash]);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.DeviceIMEI, dictionary[Constants.DeviceIMEI]);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Eskey, dictionary[Constants.Eskey]);

            var response = await _apiManagerService.PostAsync(AppSettings.AepsBaseUrl, url, dictionary[Constants.EncryptedJson], client);

            return response;
        }

        public async Task<HttpResponseMessage> BalanceInquiryAsync(BalanceInquiryModel model, Dictionary<string, string> dictionary)
        {

            string json = Converter.JsonSerializer(model);

            string url = "fpaepsservice/api/balanceInquiry/merchant/getBalance";

            var client = new HttpClient();

            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.TransactionTimestamp, dictionary[Constants.TransactionTimestamp]);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Hash, dictionary[Constants.Hash]);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.DeviceIMEI, dictionary[Constants.DeviceIMEI]);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Eskey, dictionary[Constants.Eskey]);

            var response = await _apiManagerService.PostAsync(AppSettings.AepsBaseUrl, url, dictionary[Constants.EncryptedJson], client);

            return response;
        }

        public async Task<HttpResponseMessage> AadhaarPayAsync(AadhaarPayModel model)
        {

            string json = Converter.JsonSerializer(model);// JsonConvert.SerializeObject(model);

            string url = "fpaepsservice/api/aadhaarPay/merchant/pay";
            var response = await _apiManagerService.PostAsync(AppSettings.AepsBaseUrl, url, json);

            return response;
        }

        public async Task<HttpResponseMessage> CashWithdrawalStatusCheckAsync(CashWithdrawalStatusCheckModel model)
        {

            string json = Converter.JsonSerializer(model);

            //string url = "fpaepsweb/api/auth/merchantInfo/statusCheckV2";
            string url = "fpaepsweb/api/auth/merchantInfo/statusCheckV2/merchantLoginId/cashWithdrawal";

            var response = await _apiManagerService.PostAsync(AppSettings.AepsBaseUrl, url, json);

            return response;
        }

        public async Task<HttpResponseMessage> CreateMerchantAsync(CreateMerchantModel model)
        {

            model.username = AppSettings.AepsUsername;
            model.password = AppSettings.AepsPassword;
            model.supermerchantId = AppSettings.AepsSuperMerchantId;
            model.latitude = Constants.TsiHeadOfficeLat;
            model.longitude = Constants.TsiHeadOfficeLong;

            string json = Converter.JsonSerializer(model);

            //var tt = HttpContext.Current.Server.MapPath("~/Content/VendorCertificate/" + AppSettings.AepsCertificatePath);
            //Getting certificate path 
            string certificatePath = Path.Combine(HostingEnvironment.MapPath("~/Content/VendorCertificate"), AppSettings.AepsCertificatePath);

            EncrypterReg encrypterReg = new EncrypterReg(@certificatePath);


            //Generating session key
            string sessionKey = encrypterReg.GenerateSessionKey();

            //Generating Sha256Hash
            var josnSha256Hash = encrypterReg.GenerateSha256Hash(Encoding.Default.GetBytes(json));


            //Generating eskey
            string esKey = encrypterReg.EncryptUsingPublicKey(Convert.FromBase64String(sessionKey));

            string encryptedJson = encrypterReg.EncryptUsingSessionKey(Convert.FromBase64String(sessionKey), Encoding.UTF8.GetBytes(json));
            //


            var client = new HttpClient();


            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.TransactionTimestamp, model.timestamp.ToString());
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Hash, josnSha256Hash);
            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Eskey, esKey);

            string url = "fpaepsweb/api/onboarding/merchant/creation";

            var response = await _apiManagerService.PostAsync(AppSettings.AepsBaseUrl, url, encryptedJson, client);

            return response;
        }
    }
}
