﻿//using MyIPay.DataLayer.EF;
//using MyIPay.Helpers;
//using MyIPay.Services.Areas.Aeps.Interfaces;
//using MyIPay.Services.Areas.Common.Interfaces;
//using Newtonsoft.Json;
//using System;
//using System.Data.Entity;
//using System.Threading.Tasks;

//namespace MyIPay.Services.Areas.Aeps
//{
//    public class TransactionService<TResult> : ITransactionService<TResult> where TResult : class
//    {
//        private readonly MyIPayEntities _myIPayEntities;
//        private readonly ICommonService _commonService;
//        public TransactionService(MyIPayEntities myIPayEntities, ICommonService commonService)
//        {
//            _myIPayEntities = myIPayEntities;
//            _commonService = commonService;
//        }

//        public async Task<TResult> SaveTransactionAsync(string agentId, string orderId, decimal amount, decimal availableBalance)//(object model, object response, string agentId, string templateName, string role, int distributorId, decimal ccf, decimal cashback, decimal availableBalance)
//        {

//            TResult result = default(TResult);

//            try
//            {
//                DateTime dateTimeNow = DateTime.Now;

//                decimal? batchNo = await _myIPayEntities.TransactionsDBs.MaxAsync(mx => mx.BATCHNO);

//                if (batchNo != null)
//                    batchNo += 1;
//                else
//                    batchNo = 1;
//                string transID = string.Empty;

//                decimal receiptNumber = await _commonService.GetReceiptNumber();
//                string receiptRefNumber = await _commonService.GetReceiptRefNumber();
//                decimal ccf = 0;
//                decimal cashback = 0;

//                var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == agentId.ToUpper());


//                if (userDetail != null)
//                {

//                    TransactionsDB transactionsDb = new TransactionsDB()
//                    {
//                        Mode = AppSettings.Mode,
//                        AGENTID = agentId,
//                        USP = USP.IRCTC.ToString(),
//                        RCPTNO = receiptNumber,
//                        RCPTREF = receiptRefNumber,
//                        BATCHNO = (decimal)batchNo,
//                        PAYMENTMODE = Mode.C.ToString(),
//                        //REFERENCE4 = ,
//                        BATCHPAYMODEAMT = amount + Convert.ToDecimal(ccf),
//                        //REFERENCE1 = ,
//                        //REFERENCE2 = ,
//                        PAYMENTDATE = dateTimeNow,
//                        BILLAMOUNT = amount,
//                        COLLECTIONAMT = amount + Convert.ToDecimal(ccf),
//                        REFERENCE5 = orderId,
//                        REFERENCE6 = Convert.ToString(ccf),
//                        //REFERENCE10 =,
//                        REFERENCE12 = $"agentId = {agentId}, orderId = {orderId}, amount = {amount}",
//                        CreatedOn = dateTimeNow,
//                        ReportBatchNo = 0,
//                        update_token = (int)TransactionStatus.Initiated,
//                        UPDATIONSTATUS = TransactionStatus.Initiated.ToString(),
//                        ENACCNO = "null",
//                        ENCOLLAMT = "null",
//                        //mobileno = ,
//                        //IsChequeCleared = null,
//                        UpdationMessage = "",
//                        DistributorId = userDetail.DistributorId,
//                        REFERENCE13 = Convert.ToString(cashback),
//                        REFERENCEDECIMAL1 = availableBalance - (amount + Convert.ToInt32(ccf))
//                    };

//                    _myIPayEntities.TransactionsDBs.Add(transactionsDb);

//                    //Save changes
//                    await _myIPayEntities.SaveChangesAsync();

//                    result = (TResult)Convert.ChangeType(transactionsDb.ID, typeof(TResult));

//                }

//            }
//            catch (Exception ex)
//            {
//                result = default(TResult);

//                Helpers.Utility.WriteLog("TransactionService :: SaveTransaction Error --> " + ex.Message);
//                Helpers.Utility.WriteLog("TransactionService :: SaveTransaction Error --> " + ex.InnerException);
//                Helpers.Utility.WriteLog("TransactionService :: SaveTransaction Error --> " + ex.StackTrace);
//            }

//            return result;
//        }


//        public async Task<TResult> RefundAsync(string orderId, decimal amount, string refundedOrderId)
//        {

//            TResult result = default(TResult);

//            try
//            {
//                DateTime dateTimeNow = DateTime.Now;

//                var transactionData = await _myIPayEntities.TransactionsDBs
//                                                             .FirstOrDefaultAsync(x => x.REFERENCE5 == orderId
//                                                                                  && x.Mode == Mode.L.ToString());

//                if (transactionData != null)
//                {

//                    transactionData.Mode = Mode.C.ToString(); // for Cancellation
//                    transactionData.PAYMENTDATE = dateTimeNow;//transactionData.PAYMENTDATE;
//                    transactionData.BILLAMOUNT = 0 - amount;//transactionData.BILLAMOUNT;
//                    transactionData.COLLECTIONAMT = 0 - amount;//transactionData.COLLECTIONAMT;
//                    transactionData.BATCHPAYMODEAMT = 0 - amount; //transactionData.BATCHPAYMODEAMT;

//                    decimal.TryParse(transactionData.REFERENCE6, out decimal ref6);
//                    transactionData.REFERENCE6 = "0";// (0 - Convert.ToDecimal(ref6)).ToString();

//                    transactionData.REFERENCE7 = refundedOrderId;


//                    decimal.TryParse(transactionData.REFERENCE13, out decimal ref13);
//                    transactionData.REFERENCE13 = "0"; (0 - Convert.ToDecimal(ref13)).ToString();

//                    transactionData.update_token = (int)TransactionStatus.Refunded;
//                    transactionData.UPDATIONSTATUS = TransactionStatus.Refunded.ToString();

//                    transactionData.UpdatedBy = "Yatra - Server to Server Call.";
//                    transactionData.CreatedOn = dateTimeNow;
//                    transactionData.REFERENCE12 = JsonConvert.SerializeObject(new { OrderId = orderId, Amount = amount, RefundedOrderId = refundedOrderId });

//                    _myIPayEntities.Set<TransactionsDB>().Add(transactionData);

//                    await _myIPayEntities.SaveChangesAsync();

//                    result = (TResult)Convert.ChangeType(Constants.SuccessMsg, typeof(TResult));

//                }

//            }
//            catch (Exception ex)
//            {
//                result = default(TResult);

//                Helpers.Utility.WriteLog("TransactionService :: RefundAsync Error --> " + ex.Message);
//                Helpers.Utility.WriteLog("TransactionService :: RefundAsync Error --> " + ex.InnerException);
//                Helpers.Utility.WriteLog("TransactionService :: RefundAsync Error --> " + ex.StackTrace);
//            }

//            return result;
//        }

//    }
//}

