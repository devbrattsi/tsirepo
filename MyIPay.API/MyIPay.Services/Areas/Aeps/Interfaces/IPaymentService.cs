﻿using MyIPay.Dtos.Areas.Aeps;
using MyIPay.Models.Areas.Aeps;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Aeps.Interfaces
{
    public interface IPaymentService
    {
        Task<ApiResponseDto> DoPaymentAsync(InitiateTransactionModel model);
        Task<AepsDto> DoPaymentAsync(AepsTransactionModel model);

        Task<ApiResponseDto> CheckTransactionStatusAsync(CheckTransactionStatusModel model);
    }
}
