﻿using MyIPay.Models.Areas.Aeps;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Aeps.Interfaces
{
    public interface IAepsService
    {

        Task<HttpResponseMessage> GetBankDetailsAsync();
        Task<HttpResponseMessage> CashWithdrawalAsync(CashWithdrawalModel model, Dictionary<string, string> dictionary);
        Task<HttpResponseMessage> BalanceInquiryAsync(BalanceInquiryModel model, Dictionary<string, string> dictionary);
        Task<HttpResponseMessage> AadhaarPayAsync(AadhaarPayModel model);

        Task<HttpResponseMessage> CashWithdrawalStatusCheckAsync(CashWithdrawalStatusCheckModel model);
        Task<HttpResponseMessage> CreateMerchantAsync(CreateMerchantModel model);
    }
}
