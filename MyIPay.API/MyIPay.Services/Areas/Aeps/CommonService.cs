﻿//using MyIPay.DataLayer.EF;
//using MyIPay.Services.Areas.Aeps.Interfaces;
//using System;
//using System.Data.Entity;
//using System.Linq;
//using System.Threading.Tasks;

//namespace MyIPay.Services.Areas.Aeps
//{
//    public class CommonService : ICommonService
//    {
//        private readonly MyIPayEntities _myIPayEntities;

//        public CommonService(MyIPayEntities myIPayEntities)
//        {
//            _myIPayEntities = myIPayEntities;
//        }





//        public async Task<decimal> GetReceiptNumber()
//        {
//            decimal? id = 0;
//            try
//            {
//                var cnt = await _myIPayEntities.TransactionsDBs.CountAsync();
//                if (cnt > 0)
//                {
//                    id = await _myIPayEntities.TransactionsDBs.DefaultIfEmpty().MaxAsync(m => m.RCPTNO) + 1;
//                }
//                else
//                {
//                    id = 1;
//                }
//            }
//            catch (Exception ex)
//            {
//                Helpers.Utility.WriteLog("CommonService :: GetReceiptNumber Error --> " + ex.Message);
//                Helpers.Utility.WriteLog("CommonService :: GetReceiptNumber Error --> " + ex.InnerException);
//                Helpers.Utility.WriteLog("CommonService :: GetReceiptNumber Error --> " + ex.StackTrace);

//            }
//            return id.Value;
//        }

//        public async Task<string> GetReceiptRefNumber()
//        {
//            string receiptNumber = string.Empty;
//            try
//            {
//                int id = 0;
//                var cnt = await _myIPayEntities.TransactionsDBs.CountAsync();
//                if (cnt > 0)
//                {

//                    id = await _myIPayEntities.TransactionsDBs.DefaultIfEmpty().MaxAsync(m => m.ID);
//                }
//                receiptNumber = "TSI" + (id + 1).ToString().PadLeft(6, '0');

//            }
//            catch (Exception ex)
//            {
//                Helpers.Utility.WriteLog("CommonService :: GetReceiptRefNumber Error --> " + ex.Message);
//                Helpers.Utility.WriteLog("CommonService :: GetReceiptRefNumber Error --> " + ex.InnerException);
//                Helpers.Utility.WriteLog("CommonService :: GetReceiptRefNumber Error --> " + ex.StackTrace);
//            }
//            return receiptNumber;
//        }

//        public async Task<decimal> GetBatchNumber()
//        {
//            decimal? batchNo = 1;
//            try
//            {
//                batchNo = await _myIPayEntities.TransactionsDBs.DefaultIfEmpty().MaxAsync(mx => mx.BATCHNO);
//                if (batchNo.HasValue)
//                    batchNo += 1;
//                else
//                    batchNo = 1;

//            }
//            catch (Exception ex)
//            {
//                Helpers.Utility.WriteLog("CommonService :: GetBatchNumber Error --> " + ex.Message);
//                Helpers.Utility.WriteLog("CommonService :: GetBatchNumber Error --> " + ex.InnerException);
//                Helpers.Utility.WriteLog("CommonService :: GetBatchNumber Error --> " + ex.StackTrace);
//            }
//            return batchNo.Value;
//        }



//    }
//}
