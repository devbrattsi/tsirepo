﻿using MyIPay.DataLayer.EF;
using MyIPay.Dtos.Areas.Aeps;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Aeps;
using MyIPay.Services.Areas.Aeps.Interfaces;
using MyIPay.Services.Areas.Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Utility = MyIPay.Helpers.Utility;

namespace MyIPay.Services.Areas.Aeps
{
    public class PaymentService : IPaymentService
    {

        private readonly MyIPayEntities _myIPayEntities;
        private readonly ICommonService _commonService;

        public PaymentService(MyIPayEntities myIPayEntities,
            ICommonService commonService)
        {
            _myIPayEntities = myIPayEntities;
            _commonService = commonService;
        }

        // By Sushil Sir
        public async Task<ApiResponseDto> DoPaymentAsync(InitiateTransactionModel model)
        {
            try
            {

                if (!await _myIPayEntities.Vw_GetUserDetail.AnyAsync(x => x.AgentId.ToUpper() == model.AGENTID.ToUpper()))
                {
                    return new ApiResponseDto
                    {
                        Message = Constants.AgentIdNotExist,
                        Data = model
                    };
                }
                else if (model.USP != USP.AEPS.ToString()
                        || (model.FingPayData?.TransactionType != Constants.CW && model.FingPayData?.TransactionType != Constants.BE)
                        || !model.REFERENCE22.Equals(Partner.FingPay.ToString(), StringComparison.OrdinalIgnoreCase)
                        || !model.REFERENCE2.Contains(" - ")
                        || !model.REFERENCE23.Equals(Platform.Mobile.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    return new ApiResponseDto
                    {
                        Message = Constants.InvalidRequest,
                        Data = model
                    };
                }
                else if (!await _myIPayEntities.TransactionsDBs.AnyAsync(x => x.REFERENCE5 == model.FingPayData.FpTransactionId))
                {

                    var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == model.AGENTID.ToUpper());

                    var availableBalance = _myIPayEntities.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();


                    CcfTdsModel ccfTdsModel = await CalculateCcfAndCashback(model, userDetail.AgentId, userDetail.Role, userDetail.DistributorId);


                    //Map request data
                    TransactionsDB transaction = new TransactionsDB
                    {
                        Mode = model.IsSuccess == true ? "L" : "F",
                        AGENTID = model.AGENTID,
                        USP = model.USP,
                        RCPTNO = await _commonService.GetReceiptNumber(),
                        RCPTREF = await _commonService.GetReceiptRefNumber(),
                        BATCHNO = await _commonService.GetBatchNumber(),
                        PAYMENTMODE = "C",
                        PAYMENTDATE = model.FingPayData.RequestTransactionTime,
                        BILLAMOUNT = model.FingPayData.TransactionAmount,
                        COLLECTIONAMT = model.FingPayData.TransactionAmount,
                        BATCHPAYMODEAMT = model.FingPayData.TransactionAmount + ccfTdsModel.CustomerFees,
                        mobileno = long.TryParse(model.Mobileno, out long mobile) ? mobile : default(long?),
                        mailid = userDetail.Email,
                        //model.FingPayData.TransactionType != Constants.CW || model.FingPayData.TransactionType == Constants.BE
                        REFERENCE1 = model.FingPayData.TransactionType.Equals(Constants.CW) ? Constants.CashWithdrawal : model.FingPayData.TransactionType.Equals(Constants.BE) ? Constants.BalanceEnquiry : model.FingPayData.TransactionType,
                        REFERENCE2 = model.REFERENCE2,
                        REFERENCE3 = "AEPS",
                        REFERENCE4 = $"Aadhaar No - {model.REFERENCE4}",
                        REFERENCE5 = model.FingPayData.FpTransactionId,
                        REFERENCE6 = ccfTdsModel.CcfAmount.ToString(),
                        REFERENCE7 = SecurityHelper.GenerateClientReferenceId(Constants.AppUserName),
                        REFERENCE8 = model.REFERENCE8,
                        REFERENCE9 = model.REFERENCE9,
                        REFERENCE12 = JsonConvert.SerializeObject(model),
                        REFERENCE13 = Convert.ToString(ccfTdsModel.Margin),
                        REFERENCE14 = ccfTdsModel.TsiMargin.ToString(),
                        REFERENCE16 = ccfTdsModel.AgentCashbackAmount.ToString(),
                        REFERENCE17 = ccfTdsModel.AgentTdsAmount.ToString(),
                        REFERENCE18 = ccfTdsModel.DistributorCashbackAmount.ToString(),
                        REFERENCE19 = ccfTdsModel.DistributorTdsAmount.ToString(),
                        REFERENCE20 = ccfTdsModel.CustomerFees.ToString(),
                        REFERENCE22 = model.REFERENCE22,
                        REFERENCE23 = model.REFERENCE23,
                        REFERENCE24 = model.REFERENCE24,
                        REFERENCE25 = model.REFERENCE25,
                        REFERENCE28 = AppSettings.HostServer,

                        REFERENCEDECIMAL1 = availableBalance + (model.FingPayData.TransactionAmount + ccfTdsModel.CcfAmount + (userDetail.Role.ToUpper() == Constants.Distributor.ToUpper() ? ccfTdsModel.DistributorTdsAmount : ccfTdsModel.AgentTdsAmount)),

                        UPDATIONSTATUS = model.FingPayData.TransactionStatus,
                        UpdationMessage = model.UpdationMessage,
                        ReportBatchNo = 0,
                        CreatedOn = DateTime.Now,
                        UpdatedBy = $"{Partner.FingPay}-Server to Server Call",
                        ENACCNO = "null",
                        ENCOLLAMT = "null",
                        update_token = int.TryParse(model.update_token, out int statusCode) ? statusCode : (int)TransactionStatus.Initiated,
                        DistributorId = userDetail.DistributorId
                    };


                    _myIPayEntities.TransactionsDBs.Add(transaction);

                    await _myIPayEntities.SaveChangesAsync();

                    return new ApiResponseDto
                    {
                        IsSuccess = true,
                        Message = Constants.SuccessMsg,
                        TsiTransactionId = transaction.REFERENCE7,
                        Data = model
                    };

                }
                else
                {
                    return new ApiResponseDto
                    {
                        Message = Constants.TransactionIdExist,
                        Data = model
                    };
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("PaymentService_Error", $"DoPaymentAsync Error {ex.Message}");
                Utility.WriteLog("PaymentService_Error", $"DoPaymentAsync Error {ex.InnerException}");
                Utility.WriteLog("PaymentService_Error", $"DoPaymentAsync Error {ex.StackTrace}");

                return new ApiResponseDto
                {
                    IsSuccess = false,
                    HasError = true,
                    Message = ex.Message,
                    Data = model
                };

            }

        }

        // By Rajesh
        public async Task<AepsDto> DoPaymentAsync(AepsTransactionModel model)
        {
            try
            {
                Utility.WriteLog("AepsPaymentService", $"Aeps Request -->  {JsonConvert.SerializeObject(model)}");

                if (!await _myIPayEntities.TransactionsDBs.AnyAsync(a => a.REFERENCE7 == model.TransactionId))
                {
                    var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == model.MerchantId.ToUpper());
                    string merchantRefNo = SecurityHelper.GenerateClientReferenceId(Constants.AppUserName);

                    // If User Exists
                    if (userDetail != null)
                    {
                        CcfTdsModel ccfTdsModel = await CalculateCcfAndCashback(model, userDetail.AgentId, userDetail.Role, userDetail.DistributorId);

                        //Map request data
                        TransactionsDB transaction = new TransactionsDB
                        {
                            Mode = model.TransactionStatus.ToUpper() == "S" ? Mode.L.ToString() : model.TransactionStatus.ToUpper() == "I" ? model.TransactionStatus.ToUpper() : Mode.F.ToString(),
                            AGENTID = model.MerchantId.ToUpper(),
                            USP = USP.AEPS.ToString(),
                            RCPTNO = await _commonService.GetReceiptNumber(),
                            RCPTREF = await _commonService.GetReceiptRefNumber(),
                            BATCHNO = await _commonService.GetBatchNumber(),
                            PAYMENTMODE = "M", // M for Mobile
                            PAYMENTDATE = DateTime.Now,
                            BILLAMOUNT = model.Amount,
                            COLLECTIONAMT = model.Amount,
                            BATCHPAYMODEAMT = model.Amount + ccfTdsModel.CustomerFees,
                            mobileno = long.TryParse(model.Mobile, out long mobile) ? mobile : default(long?),
                            mailid = userDetail.Email,
                            REFERENCE1 = model.TransactionType.Equals(Constants.CW) ? Constants.CashWithdrawal : Constants.BalanceEnquiry,
                            REFERENCE2 = $"{model.BankName}-{merchantRefNo}",
                            REFERENCE3 = "AEPS",
                            REFERENCE4 = $"Aadhaar No - {model.AadhaarNumber}",
                            REFERENCE5 = model.BankRRN == null ? merchantRefNo : model.BankRRN,
                            REFERENCE6 = ccfTdsModel.CcfAmount.ToString(),
                            REFERENCE7 = model.TransactionId,
                            REFERENCE8 = model.DeviceIMEI,
                            REFERENCE9 = model.IpAddress,
                            REFERENCE10 = JsonConvert.SerializeObject(model),
                            REFERENCE13 = Convert.ToString(ccfTdsModel.Margin),
                            REFERENCE14 = ccfTdsModel.TsiMargin.ToString(),
                            REFERENCE16 = ccfTdsModel.AgentCashbackAmount.ToString(),
                            REFERENCE17 = ccfTdsModel.AgentTdsAmount.ToString(),
                            REFERENCE18 = ccfTdsModel.DistributorCashbackAmount.ToString(),
                            REFERENCE19 = ccfTdsModel.DistributorTdsAmount.ToString(),
                            REFERENCE20 = ccfTdsModel.CustomerFees.ToString(),
                            REFERENCE22 = Partner.FingPay.ToString(),
                            REFERENCE23 = model.RequestedTimestamp,
                            REFERENCE24 = "AEPS APP",
                            REFERENCE25 = $"{model.Latitude},{model.Longitude}",

                            REFERENCE26 = model.TransactionStatus.ToUpper() == "S" ? Convert.ToString((int)CustomResponseStatusCode.Success)
                                         : model.TransactionStatus.ToUpper() == "I" ? Convert.ToString((int)CustomResponseStatusCode.Initiated)
                                                                                    : Convert.ToString((int)CustomResponseStatusCode.Failure),


                            REFERENCE27 = model.TransactionStatus.ToUpper() == "S" ? Constants.Success
                                         : model.TransactionStatus.ToUpper() == "I" ? Constants.Initiated
                                                                                    : Constants.Failure,

                            REFERENCE28 = AppSettings.HostServer,



                            UPDATIONSTATUS = model.ErrorMessage == "" ? TransactionStatus.Initiated.ToString() : model.ErrorMessage,
                            UpdationMessage = string.Empty,
                            ReportBatchNo = 0,
                            CreatedOn = DateTime.Now,
                            CreatedBy = $"{Partner.FingPay}-Server to Server Call",
                            ENACCNO = "null",
                            ENCOLLAMT = "null",
                            update_token = model.ErrorMessage == "" ? (int)TransactionStatus.Initiated : model.ErrorMessage.ToUpper().Equals(TransactionStatus.Success.ToString().ToUpper()) ? (int)TransactionStatus.Success : (int)TransactionStatus.Fail,
                            DistributorId = userDetail.DistributorId

                        };

                        _myIPayEntities.TransactionsDBs.Add(transaction);
                        await _myIPayEntities.SaveChangesAsync();
                    }
                    else
                    {
                        //Map request data
                        TransactionsDB transaction = new TransactionsDB
                        {
                            Mode = model.TransactionStatus.ToUpper() == "S" ? Mode.L.ToString() : model.TransactionStatus.ToUpper() == "I" ? model.TransactionStatus.ToUpper() : Mode.F.ToString(),
                            AGENTID = model.MerchantId.ToUpper(),
                            USP = USP.AEPS.ToString(),
                            RCPTNO = await _commonService.GetReceiptNumber(),
                            RCPTREF = await _commonService.GetReceiptRefNumber(),
                            BATCHNO = await _commonService.GetBatchNumber(),
                            PAYMENTMODE = "M", // M for Mobile
                            PAYMENTDATE = DateTime.Now,
                            BILLAMOUNT = model.Amount,
                            COLLECTIONAMT = model.Amount,
                            BATCHPAYMODEAMT = model.Amount,
                            mobileno = long.TryParse(model.Mobile, out long mobile) ? mobile : default(long?),
                            mailid = "",
                            REFERENCE1 = model.TransactionType.Equals(Constants.CW) ? Constants.CashWithdrawal : Constants.BalanceEnquiry,
                            REFERENCE2 = $"{model.BankName}-{merchantRefNo}",
                            REFERENCE3 = "AEPS",
                            REFERENCE4 = $"Aadhaar No - {model.AadhaarNumber}",
                            REFERENCE5 = model.BankRRN == null ? merchantRefNo : model.BankRRN,
                            REFERENCE6 = "0",
                            REFERENCE7 = model.TransactionId,
                            REFERENCE8 = model.DeviceIMEI,
                            REFERENCE9 = model.IpAddress,
                            REFERENCE10 = JsonConvert.SerializeObject(model),
                            REFERENCE13 = "0",
                            REFERENCE14 = "0",
                            REFERENCE16 = "0",
                            REFERENCE17 = "0",
                            REFERENCE18 = "0",
                            REFERENCE19 = "0",
                            REFERENCE20 = "0",
                            REFERENCE22 = Partner.FingPay.ToString(),
                            REFERENCE23 = model.RequestedTimestamp,
                            REFERENCE24 = "AEPS APP",
                            REFERENCE25 = $"{model.Latitude},{model.Longitude}",

                            REFERENCE26 = model.TransactionStatus.ToUpper() == "S" ? Convert.ToString((int)CustomResponseStatusCode.Success)
                                         : model.TransactionStatus.ToUpper() == "I" ? Convert.ToString((int)CustomResponseStatusCode.Initiated)
                                                                                    : Convert.ToString((int)CustomResponseStatusCode.Failure),


                            REFERENCE27 = model.TransactionStatus.ToUpper() == "S" ? Constants.Success
                                         : model.TransactionStatus.ToUpper() == "I" ? Constants.Initiated
                                                                                    : Constants.Failure,

                            REFERENCE28 = AppSettings.HostServer,
                            UPDATIONSTATUS = model.ErrorMessage == "" ? TransactionStatus.Initiated.ToString() : model.ErrorMessage,
                            UpdationMessage = string.Empty,
                            ReportBatchNo = 0,
                            CreatedOn = DateTime.Now,
                            CreatedBy = $"{Partner.FingPay}-Server to Server Call",
                            ENACCNO = "null",
                            ENCOLLAMT = "null",
                            update_token = model.ErrorMessage == "" ? (int)TransactionStatus.Initiated : model.ErrorMessage.ToUpper().Equals(TransactionStatus.Success.ToString().ToUpper()) ? (int)TransactionStatus.Success : (int)TransactionStatus.Fail,
                            DistributorId = -1
                        };

                        _myIPayEntities.TransactionsDBs.Add(transaction);
                        await _myIPayEntities.SaveChangesAsync();
                    }

                    return new AepsDto
                    {
                        Status = true,
                        MerchantRefNo = merchantRefNo,
                        Message = "CWTransactionInitiated."
                    };
                }
                else
                {
                    var transaction = await _myIPayEntities.TransactionsDBs.FirstOrDefaultAsync(f => f.REFERENCE7 == model.TransactionId);

                    model.MerchantRefNo = transaction.REFERENCE5;
                    transaction.Mode = model.TransactionStatus.ToUpper() == "S" ? Mode.L.ToString() : Mode.F.ToString();
                    transaction.REFERENCE5 = model.BankRRN;
                    transaction.REFERENCE12 = JsonConvert.SerializeObject(model);
                    transaction.UPDATIONSTATUS = model.ErrorMessage;
                    transaction.UpdationMessage = string.Empty;
                    transaction.update_token = model.ErrorMessage.ToUpper().Equals(TransactionStatus.Success.ToString().ToUpper()) ? (int)TransactionStatus.Success : (int)TransactionStatus.Fail;
                    transaction.UpdatedBy = $"{Partner.FingPay}-Server to Server Call";
                    transaction.UpdatedOn = DateTime.Now;

                    transaction.REFERENCE26 = model.TransactionStatus.ToUpper() == "S" ? Convert.ToString((int)CustomResponseStatusCode.Success)
                                      : model.TransactionStatus.ToUpper() == "I" ? Convert.ToString((int)CustomResponseStatusCode.Initiated)
                                                                                 : Convert.ToString((int)CustomResponseStatusCode.Failure);


                    transaction.REFERENCE27 = model.TransactionStatus.ToUpper() == "S" ? Constants.Success
                                         : model.TransactionStatus.ToUpper() == "I" ? Constants.Initiated
                                                                                    : Constants.Failure;

                    transaction.REFERENCE28 = AppSettings.HostServer;

                    var userDetail = await _myIPayEntities.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.AgentId.ToUpper() == model.MerchantId.ToUpper());

                    if (userDetail != null)
                    {
                        var availableBalance = _myIPayEntities.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();
                        CcfTdsModel ccfTdsModel = await CalculateCcfAndCashback(model, userDetail.AgentId, userDetail.Role, userDetail.DistributorId);
                        transaction.REFERENCEDECIMAL1 = availableBalance + (model.Amount + ccfTdsModel.CcfAmount + (userDetail.Role.ToUpper() == Constants.Distributor.ToUpper() ? ccfTdsModel.DistributorTdsAmount : ccfTdsModel.AgentTdsAmount));
                    }

                    await _myIPayEntities.SaveChangesAsync();

                    return new AepsDto
                    {
                        Status = true,
                        MerchantRefNo = model.MerchantRefNo,
                        Message = "CWTransactionSuccess."
                    };
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("AepsPaymentService_Error", $"DoPaymentAsync Error {ex.Message}");
                Utility.WriteLog("AepsPaymentService_Error", $"DoPaymentAsync Error {ex.InnerException}");
                Utility.WriteLog("AepsPaymentService_Error", $"DoPaymentAsync Error {ex.StackTrace}");
                return new AepsDto
                {
                    Status = false,
                    MerchantRefNo = model.MerchantRefNo,
                    Message = $"Error :- {ex.Message}"
                };
            }
        }

        public async Task<ApiResponseDto> CheckTransactionStatusAsync(CheckTransactionStatusModel model)
        {
            if (!await _myIPayEntities.TransactionsDBs.AnyAsync(x => x.REFERENCE5 == model.TransactionId))
            {
                return new ApiResponseDto
                {

                };
            }
            else
            {
                return new ApiResponseDto
                {
                    IsSuccess = false,
                    Message = ""
                };
            }

        }

        // By Sushil Sir
        private async Task<CcfTdsModel> CalculateCcfAndCashback(InitiateTransactionModel model, string agentId, string role, int distributorId)
        {

            CcfTdsModel ccfTdsModel = new CcfTdsModel();
            double margin = (Convert.ToDouble(model.FingPayData.TransactionAmount) * 0.005) * 0.80;
            if (role.ToUpper() == Constants.Distributor.ToUpper())
            {
                var distibutorCcf = await _myIPayEntities.DistributorCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.StartRange <= model.FingPayData.TransactionAmount && f.EndRange >= model.FingPayData.TransactionAmount && f.Utility == USP.AEPS.ToString());

                ccfTdsModel.DistributorTdsAmount = distibutorCcf == null ? 0 : (5 * distibutorCcf.DistributorCashBackPercentage.Value) / 100;
                ccfTdsModel.CcfAmount = distibutorCcf == null ? 0 : distibutorCcf.CCFAmount;
                ccfTdsModel.DistributorCashbackAmount = distibutorCcf == null ? 0 : (distibutorCcf.DistributorCashBackPercentage.Value - ccfTdsModel.DistributorTdsAmount);
                ccfTdsModel.CcfModel = distibutorCcf == null ? 0 : distibutorCcf.Model.Value;
                ccfTdsModel.TsiMargin = (margin < 15 ? Convert.ToDecimal(margin) : 15) - (distibutorCcf == null ? 0 : distibutorCcf.DistributorCashBackPercentage.Value);
                ccfTdsModel.CustomerFees = 0;
            }
            else
            {
                var agentCcfMaster = await _myIPayEntities.AgentCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.StartRange <= model.FingPayData.TransactionAmount && f.EndRange >= model.FingPayData.TransactionAmount && f.Utility == USP.AEPS.ToString());

                ccfTdsModel.CcfModel = agentCcfMaster == null ? 0 : agentCcfMaster.Model.Value;
                ccfTdsModel.CustomerFees = 0;
                ccfTdsModel.CcfAmount = agentCcfMaster == null ? 0 : agentCcfMaster.CCFAmount;
                ccfTdsModel.AgentTdsAmount = agentCcfMaster == null ? 0 : (5 * agentCcfMaster.CashBackPercentage) / 100;
                ccfTdsModel.AgentCashbackAmount = agentCcfMaster == null ? 0 : (agentCcfMaster.CashBackPercentage - ccfTdsModel.AgentTdsAmount);
                ccfTdsModel.TsiMargin = (margin < 15 ? Convert.ToDecimal(margin) : 15) - (agentCcfMaster == null ? 0 : agentCcfMaster.DistributorCashBackPercentage.Value);

                decimal distributorCashback = agentCcfMaster == null ? 0 : agentCcfMaster.DistributorCashBackPercentage.Value - agentCcfMaster.CashBackPercentage;
                ccfTdsModel.DistributorCashbackAmount = distributorCashback - ((5 * distributorCashback) / 100);
                ccfTdsModel.DistributorTdsAmount = ((5 * distributorCashback) / 100);
            }

            return ccfTdsModel;
        }

        // By Rajesh
        private async Task<CcfTdsModel> CalculateCcfAndCashback(AepsTransactionModel model, string agentId, string role, int distributorId)
        {

            CcfTdsModel ccfTdsModel = new CcfTdsModel();
            double margin = (Convert.ToDouble(model.Amount) * 0.005) * 0.80;
            if (role.ToUpper() == Constants.Distributor.ToUpper())
            {
                var distibutorCcf = await _myIPayEntities.DistributorCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.StartRange <= model.Amount && f.EndRange >= model.Amount && f.Utility == USP.AEPS.ToString());

                ccfTdsModel.DistributorTdsAmount = distibutorCcf == null ? 0 : (5 * distibutorCcf.DistributorCashBackPercentage.Value) / 100;
                ccfTdsModel.CcfAmount = distibutorCcf == null ? 0 : distibutorCcf.CCFAmount;
                ccfTdsModel.DistributorCashbackAmount = distibutorCcf == null ? 0 : (distibutorCcf.DistributorCashBackPercentage.Value - ccfTdsModel.DistributorTdsAmount);
                ccfTdsModel.CcfModel = distibutorCcf == null ? 0 : distibutorCcf.Model.Value;
                ccfTdsModel.TsiMargin = (margin < 15 ? Convert.ToDecimal(margin) : 15) - (distibutorCcf == null ? 0 : distibutorCcf.DistributorCashBackPercentage.Value);
                ccfTdsModel.CustomerFees = 0;
            }
            else
            {
                var agentCcfMaster = await _myIPayEntities.AgentCcfMasters.FirstOrDefaultAsync(f => f.AgentId.ToUpper() == agentId.ToUpper() && f.StartRange <= model.Amount && f.EndRange >= model.Amount && f.Utility == USP.AEPS.ToString());

                ccfTdsModel.CcfModel = agentCcfMaster == null ? 0 : agentCcfMaster.Model.Value;
                ccfTdsModel.CustomerFees = 0;
                ccfTdsModel.CcfAmount = agentCcfMaster == null ? 0 : agentCcfMaster.CCFAmount;
                ccfTdsModel.AgentTdsAmount = agentCcfMaster == null ? 0 : (5 * agentCcfMaster.CashBackPercentage) / 100;
                ccfTdsModel.AgentCashbackAmount = agentCcfMaster == null ? 0 : (agentCcfMaster.CashBackPercentage - ccfTdsModel.AgentTdsAmount);
                ccfTdsModel.TsiMargin = (margin < 15 ? Convert.ToDecimal(margin) : 15) - (agentCcfMaster == null ? 0 : agentCcfMaster.DistributorCashBackPercentage.Value);

                decimal distributorCashback = agentCcfMaster == null ? 0 : agentCcfMaster.DistributorCashBackPercentage.Value - agentCcfMaster.CashBackPercentage;
                ccfTdsModel.DistributorCashbackAmount = distributorCashback - ((5 * distributorCashback) / 100);
                ccfTdsModel.DistributorTdsAmount = ((5 * distributorCashback) / 100);
            }

            return ccfTdsModel;
        }
    }
}
