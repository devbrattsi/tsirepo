﻿using MyIPay.Dtos.Areas.MpVidyutVitaran;
using MyIPay.Models.Areas.MpVidyutVitaran;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.MpVidyutVitaran.Interfaces
{
    public interface IMpVidyutVitaranService
    {
        Task<GetApdrpBillDto> GetApdrpBillAsync(string consumerNo);
        Task<HttpResponseMessage> PostApdrpBillAsync(PostApdrpBill model);
        Task<HttpResponseMessage> GetRmsBillInfoAsync(string ivrs);
        Task<HttpResponseMessage> PayRmsBillAsync(PayRmsBillModel model);
    }
}
