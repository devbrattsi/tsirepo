﻿using com.billdesk.pgidsk;
using MyIPay.Dtos.Areas.MpVidyutVitaran;
using MyIPay.Helpers;
using MyIPay.Models.Areas.MpVidyutVitaran;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.MpVidyutVitaran.Interfaces;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MyIPay.Services.Areas.MpVidyutVitaran
{
    public class MpVidyutVitaranService : IMpVidyutVitaranService
    {
        private readonly string _mode = AppSettings.MpVidyutVitaranMode;
        //private readonly string _checkSumKey = AppSettings.MpVidyutVitaranCheckSumKey;
        private readonly string _apdrpBaseUrl;
        private readonly string _rmsBaseUrl;
        private readonly string _gateWayType = AppSettings.MpVidyutVitaranGateWayType;
        private readonly string _rmsAgency = AppSettings.MpVidyutVitaranRmsAgency;
        private IApiManagerService _apiManagerService;
        public MpVidyutVitaranService(IApiManagerService apiManagerService)
        {
            _apdrpBaseUrl = _mode == Mode.L.ToString() ? AppSettings.MpVidyutVitaranApdrpLiveApiBaseUrl : AppSettings.MpVidyutVitaranApdrpUatApiBaseUrl;
            _rmsBaseUrl = _mode == Mode.L.ToString() ? AppSettings.MpVidyutVitaranRmsLiveApiBaseUrl : AppSettings.MpVidyutVitaranRmsUatApiBaseUrl;
            _apiManagerService = apiManagerService;
        }

        #region Apdrp Code
        public async Task<GetApdrpBillDto> GetApdrpBillAsync(string consumerNo)
        {
            string url = $"ConsumerValidatorjbl?consumerNo={consumerNo}&gateWayType={_gateWayType}";

            var response = await _apiManagerService.GetAsync(_apdrpBaseUrl, url);

            string responseString = await response.Content.ReadAsStringAsync();

            Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"GetApdrpBillAsync :: Response : {responseString} ");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(responseString);
            XmlNodeList nodelist = doc.GetElementsByTagName("MPPKVVCL-Jabalpur");//doc.DocumentElement;
            foreach (XmlNode node in nodelist)
            {
                if (node.FirstChild.InnerText == "N")
                {
                    return new GetApdrpBillDto
                    {
                        Status = node["STATUS"].InnerText,
                        CustomerId = node["CUSTOMERID"].InnerText,
                        CustomerName = "NA",
                        DueDate = "NA",
                        Amount = "NA",
                        Reason = "Invalid Consumer No.",
                        City = "NA",
                        DcCode = "NA",
                        OrderId = "NA",
                        RaoCode = "NA"
                    };
                }
                else
                {
                    return new GetApdrpBillDto
                    {
                        Status = node["STATUS"].InnerText,
                        CustomerId = node["CUSTOMERID"].InnerText,
                        CustomerName = node["CUSTOMERNAME"].InnerText,
                        DueDate = node["DUEDATE"].InnerText,
                        Amount = node["AMOUNT"].InnerText,
                        Reason = node["REASON"].InnerText,
                        City = node["CITY"].InnerText,
                        DcCode = node["DC_CODE"].InnerText,
                        OrderId = node["ORDER_ID"].InnerText,
                        RaoCode = node["RAO_CODE"].InnerText
                    };
                }
            }

            return new GetApdrpBillDto();
        }


        public async Task<HttpResponseMessage> PostApdrpBillAsync(PostApdrpBill model)
        {
            string checkSumKey = AppSettings.MpVidyutVitaranCheckSumKey;

            string message = $"{model.MerchantId}|{model.OrderId}|{model.TxnReferenceNo}|{model.BankReferenceNo}|{model.TxnAmount}|{model.BankId}|{model.BankMerchantId}|{model.TxnType}|{model.CurrencyName}|{model.ItemCode}|{model.SecurityType}|{model.SecurityId}|{model.SecurityPassword}|{model.TxnDate}|{model.AuthStatus}|{model.SettlementType}|{model.CustomerId}|{model.RaoCode}|{model.DcCode}|{model.CustomerName}|{model.DueDate}|{model.AdditionalInfo1}|{model.AdditionalInfo2}|{model.AdditionalInfo3}|{model.TxnStatus}";

            string chksm = GenerateCheckSum(checkSumKey, message);

            message += "|" + chksm;

            string url = $"ServerRespPaymentAck?msg={message}";

            var response = await _apiManagerService.GetAsync(_apdrpBaseUrl, url);

            var responseStr = await response.Content.ReadAsStringAsync();

            Utility.SaveRequestAndResponseLog("MpVidyutVitaran", $"PostApdrpBillAsync :: Response : { responseStr} ");

            return response;
        }

        private static string GenerateCheckSum(string key, string msg)
        {
            return PGIUtil.doDigest(msg, key);
        }

        #endregion

        #region Rms Code
        public async Task<HttpResponseMessage> GetRmsBillInfoAsync(string consumerNo)
        {
            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(_rmsBaseUrl);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", SecurityHelper.Base64Encode($"{AppSettings.MpVidyutVitaranRmsApiUsername}:{AppSettings.MpVidyutVitaranRmsApiPassword}"));

                EnableHost.EnableTrustedHosts();

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");

                string url = $"query?ivrs={consumerNo}&agency={_rmsAgency}";

                //Getting client API response
                var response = await client.GetAsync(url);

                return response;
            }
        }

        public async Task<HttpResponseMessage> PayRmsBillAsync(PayRmsBillModel model)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_rmsBaseUrl);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", SecurityHelper.Base64Encode($"{AppSettings.MpVidyutVitaranRmsApiUsername}:{AppSettings.MpVidyutVitaranRmsApiPassword}"));

                EnableHost.EnableTrustedHosts();

                string xmlData = Converter.XmlSerializer<PayRmsBillModel>(model);

                xmlData = xmlData.Replace("PayAsyncModel", "payData").Trim();

                var content = new StringContent(xmlData, Encoding.UTF32, "application/xml");
                //UAT URL
                // var response = await client.PostAsync("censvr/rest/pay", content);
                //LIVE URL
                var response = await client.PostAsync("pay", content);
                return response;
            }
        }

        #endregion
    }
}
