﻿using MyIPay.Dtos.Areas.CashDrop;
using MyIPay.Models.Areas.CashDrop;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.CashDrop.Interfaces
{
    public interface ICashDropService
    {
        Task<BillersDto> GetBillersAsync();

        Task<BillerDetailDto> GetBillerDetailAsync(string url);

        Task<HttpResponseMessage> FetchDetailAsync(Dictionary<string, string> keyValuePairs);

        Task<PaymentDto> PaymentAsync(Dictionary<string, string> keyValuePairs);

        Task<TxnDetailDto> GetTxnDetailAsync(string partnerTxnId);
    }
}
