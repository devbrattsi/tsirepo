﻿using MyIPay.Dtos.Areas.Aeps;
using MyIPay.Dtos.Areas.CashDrop;
using MyIPay.Helpers;
using MyIPay.Models.Areas.CashDrop;
using MyIPay.Services.Areas.CashDrop.Interfaces;
using MyIPay.Services.Areas.Common.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.CashDrop
{
    public class CashDropService : ICashDropService
    {
        private readonly string _mode = AppSettings.AirtelCashDropMode;
        private readonly string _baseUrl;
        private readonly string _partnerId;
        private readonly string _salt;
        private readonly string _username;
        private readonly string _password;

        private IApiManagerService _apiManagerService;

        public CashDropService(IApiManagerService apiManagerService)
        {
            _baseUrl = _mode == Mode.L.ToString() ? AppSettings.AirtelCashDropLiveBaseUrl : AppSettings.AirtelCashDropUatBaseUrl;
            _partnerId = _mode == Mode.L.ToString() ? AppSettings.AirtelCashDropLivePartnerId : AppSettings.AirtelCashDropUatPartnerId;
            _salt = _mode == Mode.L.ToString() ? AppSettings.AirtelCashDropLiveSalt : AppSettings.AirtelCashDropUatSalt;
            _username = _mode == Mode.L.ToString() ? AppSettings.AirtelCashDropLiveUsername : AppSettings.AirtelCashDropUatUsername;
            _password = _mode == Mode.L.ToString() ? AppSettings.AirtelCashDropLivePassword : AppSettings.AirtelCashDropUatPassword;

            _apiManagerService = apiManagerService;
        }

        public async Task<BillersDto> GetBillersAsync()
        {

            try
            {

                string url = "billers/";


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Basic, SecurityHelper.Base64Encode($"{_username}:{_password}"));


                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("CashDrop", $"GetBillersAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    return JsonConvert.DeserializeObject<BillersDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CashDrop_Error", $"GetBillersAsync Error : {ex.Message}");
                Utility.WriteLog("CashDrop_Error", $"GetBillersAsync Error : {ex.InnerException}");
                Utility.WriteLog("CashDrop_Error", $"GetBillersAsync Error : {ex.StackTrace}");

                throw;
            }

        }

        public async Task<BillerDetailDto> GetBillerDetailAsync(string url)
        {

            try
            {

                url = url.TrimStart('/');
                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Basic, SecurityHelper.Base64Encode($"{_username}:{_password}"));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("CashDrop", $"GetBillerDetailAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    return JsonConvert.DeserializeObject<BillerDetailDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CashDrop_Error", $"GetBillerDetailAsync Error : {ex.Message}");
                Utility.WriteLog("CashDrop_Error", $"GetBillerDetailAsync Error : {ex.InnerException}");
                Utility.WriteLog("CashDrop_Error", $"GetBillerDetailAsync Error : {ex.StackTrace}");

                throw;
            }

        }
        public async Task<HttpResponseMessage> FetchDetailAsync(Dictionary<string, string> keyValuePairs)//, FetchDetailModel model, string actionUrl)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Getting relative request url 
                    string url = keyValuePairs.FirstOrDefault(x => x.Key == "ActionUrl").Value.TrimStart('/');


                    //Removing request url from Dictionary
                    keyValuePairs.Remove("ActionUrl");

                    client.DefaultRequestHeaders.Accept.Clear();

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Basic, SecurityHelper.Base64Encode($"{_username}:{_password}"));


                    string requestJson = JsonConvert.SerializeObject(keyValuePairs);

                    Utility.WriteLog("CashDrop", $"FetchDetailAsync Request : {requestJson}");

                    StringContent content = new StringContent(requestJson, Encoding.UTF8, Constants.ApplicationJson);

                    HttpResponseMessage httpResponse  = await _apiManagerService.PostAsync(_baseUrl, url, content, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("CashDrop", $"FetchDetailAsync Response : {json}");

                    return httpResponse;

                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CashDrop_Error", $"FetchDetailAsync Error : {ex.Message}");
                Utility.WriteLog("CashDrop_Error", $"FetchDetailAsync Error : {ex.InnerException}");
                Utility.WriteLog("CashDrop_Error", $"FetchDetailAsync Error : {ex.StackTrace}");

                throw;
            }

        }

        public async Task<PaymentDto> PaymentAsync(Dictionary<string, string> keyValuePairs)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Getting relative request url 
                    string url = keyValuePairs.FirstOrDefault(x => x.Key == "ActionUrl").Value.TrimStart('/');


                    //Removing request url from Dictionary
                    keyValuePairs.Remove(Constants.ActionUrl);

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Basic, SecurityHelper.Base64Encode($"{_username}:{_password}"));

                    string requestJson = JsonConvert.SerializeObject(keyValuePairs);

                    Utility.WriteLog("CashDrop", $"PaymentAsync Request : {requestJson}");

                    StringContent content = new StringContent(requestJson, Encoding.UTF8, Constants.ApplicationJson);

                    HttpResponseMessage httpResponse = await _apiManagerService.PostAsync(_baseUrl, url, content,client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("CashDrop", $"PaymentAsync Response : {json}");

                    return JsonConvert.DeserializeObject<PaymentDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CashDrop_Error", $"PaymentAsync Error : {ex.Message}");
                Utility.WriteLog("CashDrop_Error", $"PaymentAsync Error : {ex.InnerException}");
                Utility.WriteLog("CashDrop_Error", $"PaymentAsync Error : {ex.StackTrace}");

                throw;
            }

        }

        public async Task<TxnDetailDto> GetTxnDetailAsync(string partnerTxnId)
        {
            try
            {
                string url = $"/txnDetail/{partnerTxnId}";

                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Basic, SecurityHelper.Base64Encode($"{_username}:{_password}"));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("CashDrop", $"GetTxnDetailAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    return JsonConvert.DeserializeObject<TxnDetailDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("CashDrop_Error", $"GetTxnDetailAsync Error : {ex.Message}");
                Utility.WriteLog("CashDrop_Error", $"GetTxnDetailAsync Error : {ex.InnerException}");
                Utility.WriteLog("CashDrop_Error", $"GetTxnDetailAsync Error : {ex.StackTrace}");

                throw;
            }

        }
    }
}
