﻿using log4net;
using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.Forex;
using MyIPay.Helpers;
using MyIPay.Services.Areas.Forex.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Forex
{
    public class ForexApiManagerService : IForexApiManagerService
    {

        private readonly string _baseUrl = AppSettings.ForexApiBaseUrl;
        private readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<ApiResponseDto> GetAllCityAsync()
        {

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllCities using HttpClient  
                HttpResponseMessage response = await client.GetAsync($"api/Forex/GetCity/-1");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the City list

                        var deserializedData = JsonConvert.DeserializeObject<List<City>>(result);
                        return new ApiResponseDto
                        {
                            HasError = false,
                            Message = Constants.SuccessMsg,
                            Response = deserializedData
                        };
                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetCitiesAsync Error --> " + ex.Message);
                        _log.Info("GetCitiesAsync Error --> " + ex.InnerException);
                        _log.Info("GetCitiesAsync Error --> " + ex.StackTrace);

                        return new ApiResponseDto
                        {
                            HasError = true,
                            Message = ex.Message,
                            Response = null
                        };
                    }
                }
                return new ApiResponseDto
                {
                    HasError = false,
                    Message = response.StatusCode.ToString(),
                    Response = null
                };
            }
        }

        public async Task<City> GetCityAsync(int cityId)
        {

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetCityAsync using HttpClient  
                HttpResponseMessage response = await client.GetAsync($"api/Forex/GetCity/{cityId}");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the City list

                        return JsonConvert.DeserializeObject<City>(result);

                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetCitiesAsync Error --> " + ex.Message);
                        _log.Info("GetCitiesAsync Error --> " + ex.InnerException);
                        _log.Info("GetCitiesAsync Error --> " + ex.StackTrace);

                        return null;
                    }
                }
                return null;

            }
        }

        public async Task<ApiResponseDto> GetAllCurrencyAsync()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetCurrencies using HttpClient  
                HttpResponseMessage response = await client.GetAsync("api/Forex/GetCurrency/-1");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the Currency list  
                        var deserializedData = JsonConvert.DeserializeObject<List<Currency>>(result);
                        return new ApiResponseDto
                        {
                            HasError = false,
                            Message = Constants.SuccessMsg,
                            Response = deserializedData
                        };
                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetCurrenciesAsync Error --> " + ex.Message);
                        _log.Info("GetCurrenciesAsync Error --> " + ex.InnerException);
                        _log.Info("GetCurrenciesAsync Error --> " + ex.StackTrace);

                        return new ApiResponseDto
                        {
                            HasError = true,
                            Message = ex.Message,
                            Response = null
                        };
                    }
                }

                return new ApiResponseDto
                {
                    HasError = false,
                    Message = response.StatusCode.ToString(),
                    Response = null
                };
            }
        }

        public async Task<ApiResponseDto> GetAllProductTypeAsync()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetProductType using HttpClient  
                HttpResponseMessage response = await client.GetAsync("api/Forex/GetProductType/-1");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the ProductType list  
                        var deserializedData = JsonConvert.DeserializeObject<List<ProductType>>(result);

                        return new ApiResponseDto
                        {
                            HasError = false,
                            Message = Constants.SuccessMsg,
                            Response = deserializedData
                        };
                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetProductTypesAsync Error --> " + ex.Message);
                        _log.Info("GetProductTypesAsync Error --> " + ex.InnerException);
                        _log.Info("GetProductTypesAsync Error --> " + ex.StackTrace);

                        return new ApiResponseDto
                        {
                            HasError = true,
                            Message = ex.Message,
                            Response = null
                        };
                    }
                }

                return new ApiResponseDto
                {
                    HasError = false,
                    Message = response.StatusCode.ToString(),
                    Response = null
                };
            }
        }


        public async Task<ApiResponseDto> GetProductConfigurationAsync(int id)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetProductType using HttpClient  
                HttpResponseMessage response = await client.GetAsync($"api/Forex/GetProductConfiguration?CityId={id}");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the ProductType list  
                        var deserializedData = JsonConvert.DeserializeObject<List<ProductConfiguration>>(result);

                        return new ApiResponseDto
                        {
                            HasError = false,
                            Message = Constants.SuccessMsg,
                            Response = deserializedData
                        };
                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetProductTypesAsync Error --> " + ex.Message);
                        _log.Info("GetProductTypesAsync Error --> " + ex.InnerException);
                        _log.Info("GetProductTypesAsync Error --> " + ex.StackTrace);

                        return new ApiResponseDto
                        {
                            HasError = true,
                            Message = ex.Message,
                            Response = new List<ProductConfiguration>()
                        };
                    }
                }

                return new ApiResponseDto
                {
                    HasError = false,
                    Message = response.StatusCode.ToString(),
                    Response = new List<ProductConfiguration>()
                };
            }
        }

        public async Task<ApiResponseDto> GetForexCurrencyRateAsync(string cityName, string currencyCode, string productName, string transType)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetProductType using HttpClient  
                HttpResponseMessage response = await client.GetAsync($"api/Forex/GetForexCurrencyProductRates?City={cityName}&CurrencyCode={currencyCode}&TransType={transType}&productType={productName}");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the CurrencyProductRate list  
                        var deserializedData = JsonConvert.DeserializeObject<List<CurrencyProductRate>>(result);

                        return new ApiResponseDto
                        {
                            HasError = false,
                            Message = Constants.SuccessMsg,
                            Response = deserializedData
                        };

                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetForexCurrencyRateAsync Error --> " + ex.Message);
                        _log.Info("GetForexCurrencyRateAsync Error --> " + ex.InnerException);
                        _log.Info("GetForexCurrencyRateAsync Error --> " + ex.StackTrace);

                        return new ApiResponseDto
                        {
                            HasError = true,
                            Message = ex.Message,
                            Response = new List<CurrencyProductRate>()
                        };
                    }

                }
                return new ApiResponseDto
                {
                    HasError = false,
                    Message = response.StatusCode.ToString(),
                    Response = new List<CurrencyProductRate>()

                };
            }
        }

        public async Task<ApiResponseDto> GetAreaWiseMappedBranchLocation(string pinCode)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetProductType using HttpClient  
                HttpResponseMessage response = await client.GetAsync($"api/Master/GetAreaWiseMappedBranchLocation?strPinCode={pinCode}");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the CurrencyProductRate list  
                        var deserializedData = JsonConvert.DeserializeObject<List<PinCode>>(result);

                        return new ApiResponseDto
                        {
                            HasError = false,
                            Message = Constants.SuccessMsg,
                            Response = deserializedData
                        };

                    }
                    catch (Exception ex)
                    {
                        _log.Info("GetAreaWiseMappedBranchLocation Error --> " + ex.Message);
                        _log.Info("GetAreaWiseMappedBranchLocation Error --> " + ex.InnerException);
                        _log.Info("GetAreaWiseMappedBranchLocation Error --> " + ex.StackTrace);

                        return new ApiResponseDto
                        {
                            HasError = true,
                            Message = ex.Message,
                            Response = new List<PinCode>()
                        };
                    }

                }
                return new ApiResponseDto
                {
                    HasError = false,
                    Message = response.StatusCode.ToString(),
                    Response = new List<PinCode>()
                };
            }
        }
    }
}
