﻿using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.Forex;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Forex.Interfaces
{
    public interface IForexApiManagerService
    {
        Task<ApiResponseDto> GetAllCityAsync();
        Task<City> GetCityAsync(int cityId);
        Task<ApiResponseDto> GetAllCurrencyAsync();
        Task<ApiResponseDto> GetAllProductTypeAsync();
        Task<ApiResponseDto> GetProductConfigurationAsync(int id);
        Task<ApiResponseDto> GetForexCurrencyRateAsync(string cityName, string currencyCode, string productName, string transType);
    }
}
