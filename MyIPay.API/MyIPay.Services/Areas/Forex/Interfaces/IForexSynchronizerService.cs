﻿using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Forex.Interfaces
{
    public interface IForexSynchronizerService
    {
        Task<bool> AddCitiesAsync();
        Task<bool> AddCurrenciesAsync();
        Task<bool> AddProductTypesAsync();

    }
}
