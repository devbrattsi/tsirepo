﻿using log4net;
using MyIPay.DataLayer.EF;
using MyIPay.Helpers;
using MyIPay.Services.Areas.Forex.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Forex
{
    public class ForexSynchronizerService : IForexSynchronizerService
    {
        private readonly string _baseUrl = AppSettings.ForexApiBaseUrl;
        private readonly MyIPayEntities db = new MyIPayEntities();
        private readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<bool> AddCitiesAsync()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllCities using HttpClient  
                HttpResponseMessage response = await client.GetAsync("api/Forex/GetCity/-1");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the City list  
                        var deserializedData = JsonConvert.DeserializeObject<List<CityMaster>>(result);
                        if (deserializedData.Count > 0)
                        {
                            await db.Database.ExecuteSqlCommandAsync($"TRUNCATE TABLE CityMaster");
                            db.Set<CityMaster>().AddRange(deserializedData);
                            await db.SaveChangesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Info("AddCitiesAsync Error --> " + ex.Message);
                        _log.Info("AddCitiesAsync Error --> " + ex.InnerException);
                        _log.Info("AddCitiesAsync Error --> " + ex.StackTrace);
                        return false;
                    }
                }
                return true;

            }
        }

        public async Task<bool> AddCurrenciesAsync()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetCurrencies using HttpClient  
                HttpResponseMessage response = await client.GetAsync("api/Forex/GetCurrency/-1");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the Currency list  
                        var deserializedData = JsonConvert.DeserializeObject<List<CurrencyMaster>>(result);
                        if (deserializedData.Count > 0)
                        {
                            await db.Database.ExecuteSqlCommandAsync($"TRUNCATE TABLE CurrencyMaster");
                            db.Set<CurrencyMaster>().AddRange(deserializedData);
                            await db.SaveChangesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Info("AddCurrenciesAsync Error --> " + ex.Message);
                        _log.Info("AddCurrenciesAsync Error --> " + ex.InnerException);
                        _log.Info("AddCurrenciesAsync Error --> " + ex.StackTrace);
                        return false;
                    }
                }
                return true;
            }
        }

        public async Task<bool> AddProductTypesAsync()
        {


            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetProductType using HttpClient  
                HttpResponseMessage response = await client.GetAsync("api/Forex/GetProductType/-1");

                //Checking the response is successful or not which is sent using HttpClient  
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        //Storing the response details recieved from web api   
                        var result = await response.Content.ReadAsStringAsync();

                        //Deserializing the response recieved from web api and storing into the ProductType list  
                        var deserializedData = JsonConvert.DeserializeObject<List<ProductTypeMaster>>(result);
                        if (deserializedData.Count > 0)
                        {
                            await db.Database.ExecuteSqlCommandAsync($"TRUNCATE TABLE ProductTypeMaster");
                            db.Set<ProductTypeMaster>().AddRange(deserializedData);
                            await db.SaveChangesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Info("AddProductTypesAsync Error --> " + ex.Message);
                        _log.Info("AddProductTypesAsync Error --> " + ex.InnerException);
                        _log.Info("AddProductTypesAsync Error --> " + ex.StackTrace);
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
