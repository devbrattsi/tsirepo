﻿using MyIPay.Dtos.Areas.BankDeposit.ECollection;
using MyIPay.Models.Areas.BankDeposit.ECollection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.BankDeposit.ECollection.Interfaces
{

    public interface IECollectionService
    {
        Task<VerifyAccountDto> VerifyAccountAsync(VerifyAccountModel model);
        VerifyAccountDto VerifyAccount(VerifyAccountModel model);
        Task<BankDepositTransactionDto> BankDepositTransactionAsync(BankDepositTransactionModel model, Dictionary<string, string> dictionary);
    }
}
