﻿using MyIPay.DataLayer.EF;
using MyIPay.Dtos.Areas.BankDeposit.ECollection;
using MyIPay.Helpers;
using MyIPay.Models.Areas.BankDeposit.ECollection;
using MyIPay.Services.Areas.BankDeposit.ECollection.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Utility = MyIPay.Helpers.Utility;

namespace MyIPay.Services.Areas.BankDeposit.ECollection
{
    public class ECollectionService : IECollectionService
    {
        private readonly DateTime dateTimeNow = DateTime.Now;

        public async Task<VerifyAccountDto> VerifyAccountAsync(VerifyAccountModel model)
        {
            try
            {
                using (var db = new MyIPayEntities())
                {
                    var userDetail = await db.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.VirtualAccountNumber.ToLower() == model.VirtualAccountNumber.ToLower());

                    if (userDetail == null)
                    {
                        return new VerifyAccountDto
                        {
                            ClientCode = model.ClientCode,
                            VirtualAccountNumber = model.VirtualAccountNumber,
                            TransactionAmount = model.TransactionAmount,
                            UtrNumber = model.UtrNumber,
                            SenderIfscCode = model.SenderIfscCode,
                            Status = Constants.Reject,
                            RejectReason = Constants.InvalidVirtualAccountNumber
                        };

                    }

                    if (!decimal.TryParse(model.TransactionAmount, out decimal transactionAmount) || transactionAmount <= 0)
                    {
                        return new VerifyAccountDto
                        {
                            ClientCode = model.ClientCode,
                            VirtualAccountNumber = model.VirtualAccountNumber,
                            TransactionAmount = model.TransactionAmount,
                            UtrNumber = model.UtrNumber,
                            SenderIfscCode = model.SenderIfscCode,
                            Status = Constants.Reject,
                            RejectReason = Constants.InvalidTransactionAmount
                        };

                    }

                    //Getting user's available balance
                    var availableBalance = db.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();


                    //Removing white space and adding prefix
                    string transactionId = $"TP/{model.UtrNumber.Trim()}";

                    if (await db.DistributorMasters.AnyAsync(x => x.Transaction_Id.ToUpper() == transactionId.ToUpper())
                        || await db.AgentMasters.AnyAsync(x => x.Transaction_Id.ToUpper() == transactionId.ToUpper()))
                    {

                        return new VerifyAccountDto
                        {
                            ClientCode = model.ClientCode,
                            VirtualAccountNumber = model.VirtualAccountNumber,
                            TransactionAmount = model.TransactionAmount,
                            UtrNumber = model.UtrNumber,
                            SenderIfscCode = model.SenderIfscCode,
                            Status = Constants.Reject,
                            RejectReason = Constants.DuplicateUtrNumber
                        };
                    }




                    //Save topup data
                    if (userDetail.Role.ToLower() == ClaimConstants.Distributor.ToLower())
                    {
                        var distributorMaster = new DistributorMaster()
                        {
                            Agent_Id = userDetail.AgentId.ToUpper(),
                            AvailableLimit = availableBalance.Value + transactionAmount,
                            Bill_Amount = 0,
                            Cash_Back = 0,
                            CCF = 0,
                            Remarks = "ICICI",
                            TopBy = "E-Collection",
                            UpdatedBy = "E-Collection",
                            Top_Up_Amount = transactionAmount,
                            Transaction_Date_Time = dateTimeNow,
                            Transaction_Id = transactionId,
                            IMPS_NEFT_NO = model.UtrNumber,
                            DistributorId = userDetail.DistributorId,
                            ChequeNo = $"{model.SenderIfscCode}|{model.ReceiverInfo}"
                        };

                        db.DistributorMasters.Add(distributorMaster);

                    }
                    else
                    {
                        var agentMaster = new AgentMaster
                        {
                            Agent_Id = userDetail.AgentId.ToUpper(),
                            AvailableLimit = availableBalance.Value + transactionAmount,
                            Bill_Amount = 0,
                            Cash_Back = 0,
                            CCF = 0,
                            Remarks = "ICICI",
                            TopBy = "E-Collection",
                            UpdatedBy = "E-Collection",
                            Top_Up_Amount = transactionAmount,
                            Transaction_Date_Time = dateTimeNow,
                            Transaction_Id = transactionId,
                            IMPS_NEFT_NO = model.UtrNumber.ToString(),
                            DistributorId = userDetail.DistributorId,
                            ChequeNo = $"{model.SenderIfscCode}|{model.ReceiverInfo}"
                        };

                        db.AgentMasters.Add(agentMaster);
                    }

                    //Save changes
                    await db.SaveChangesAsync();

                    return new VerifyAccountDto
                    {
                        ClientCode = model.ClientCode,
                        VirtualAccountNumber = model.VirtualAccountNumber,
                        TransactionAmount = model.TransactionAmount,
                        UtrNumber = model.UtrNumber,
                        SenderIfscCode = model.SenderIfscCode,
                        Status = Constants.Accept
                    };
                }
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"VerifyAccountAsync Error :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"VerifyAccountAsync Error :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"VerifyAccountAsync Error :- {ex.StackTrace}");

                return new VerifyAccountDto
                {
                    ClientCode = model.ClientCode,
                    VirtualAccountNumber = model.VirtualAccountNumber,
                    TransactionAmount = model.TransactionAmount,
                    UtrNumber = model.UtrNumber,
                    SenderIfscCode = model.SenderIfscCode,
                    Status = Constants.Reject,
                    RejectReason = Constants.TechnicalFailure
                };
            }
        }


        public VerifyAccountDto VerifyAccount(VerifyAccountModel model)
        {
            try
            {
                if (model.ClientCode != AppSettings.ISureClientCode)
                {
                    return new VerifyAccountDto
                    {
                        ClientCode = model.ClientCode,
                        VirtualAccountNumber = model.VirtualAccountNumber,
                        TransactionAmount = model.TransactionAmount,
                        UtrNumber = model.UtrNumber,
                        SenderIfscCode = model.SenderIfscCode,
                        Status = Constants.Reject,
                        RejectReason = Constants.UnAuthrize_User
                    };
                }
                using (var db = new MyIPayEntities())
                {
                    var userDetail = db.Vw_GetUserDetail.FirstOrDefault(x => x.VirtualAccountNumber.ToLower() == model.VirtualAccountNumber.ToLower());

                    if (userDetail == null)
                    {
                        return new VerifyAccountDto
                        {
                            ClientCode = model.ClientCode,
                            VirtualAccountNumber = model.VirtualAccountNumber,
                            TransactionAmount = model.TransactionAmount,
                            UtrNumber = model.UtrNumber,
                            SenderIfscCode = model.SenderIfscCode,
                            Status = Constants.Reject,
                            RejectReason = Constants.InvalidVirtualAccountNumber
                        };

                    }

                    if (!decimal.TryParse(model.TransactionAmount, out decimal transactionAmount) || transactionAmount <= 0)
                    {
                        return new VerifyAccountDto
                        {
                            ClientCode = model.ClientCode,
                            VirtualAccountNumber = model.VirtualAccountNumber,
                            TransactionAmount = model.TransactionAmount,
                            UtrNumber = model.UtrNumber,
                            SenderIfscCode = model.SenderIfscCode,
                            Status = Constants.Reject,
                            RejectReason = Constants.InvalidTransactionAmount
                        };

                    }

                    //Getting user's available balance
                    var availableBalance = db.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();


                    //Removing white space and adding prefix
                    string transactionId = $"TP/{model.UtrNumber.Trim()}";

                    if (db.DistributorMasters.Any(x => x.Transaction_Id.ToUpper() == transactionId.ToUpper())
                        || db.AgentMasters.Any(x => x.Transaction_Id.ToUpper() == transactionId.ToUpper()))
                    {

                        return new VerifyAccountDto
                        {
                            ClientCode = model.ClientCode,
                            VirtualAccountNumber = model.VirtualAccountNumber,
                            TransactionAmount = model.TransactionAmount,
                            UtrNumber = model.UtrNumber,
                            SenderIfscCode = model.SenderIfscCode,
                            Status = Constants.Reject,
                            RejectReason = Constants.DuplicateUtrNumber
                        };
                    }




                    //Save topup data
                    if (userDetail.Role.ToLower() == ClaimConstants.Distributor.ToLower())
                    {
                        var distributorMaster = new DistributorMaster()
                        {
                            Agent_Id = userDetail.AgentId.ToUpper(),
                            AvailableLimit = availableBalance.Value + transactionAmount,
                            Bill_Amount = 0,
                            Cash_Back = 0,
                            CCF = 0,
                            Remarks = "ICICI",
                            TopBy = "E-Collection",
                            UpdatedBy = "E-Collection",
                            Top_Up_Amount = transactionAmount,
                            Transaction_Date_Time = dateTimeNow,
                            Transaction_Id = transactionId,
                            IMPS_NEFT_NO = model.UtrNumber,
                            DistributorId = userDetail.DistributorId,
                            ChequeNo = $"{model.SenderIfscCode}|{model.ReceiverInfo}"
                        };

                        db.DistributorMasters.Add(distributorMaster);

                    }
                    else
                    {
                        var agentMaster = new AgentMaster
                        {
                            Agent_Id = userDetail.AgentId.ToUpper(),
                            AvailableLimit = availableBalance.Value + transactionAmount,
                            Bill_Amount = 0,
                            Cash_Back = 0,
                            CCF = 0,
                            Remarks = "ICICI",
                            TopBy = "E-Collection",
                            UpdatedBy = "E-Collection",
                            Top_Up_Amount = transactionAmount,
                            Transaction_Date_Time = dateTimeNow,
                            Transaction_Id = transactionId,
                            IMPS_NEFT_NO = model.UtrNumber.ToString(),
                            DistributorId = userDetail.DistributorId,
                            ChequeNo = $"{model.SenderIfscCode}|{model.ReceiverInfo}"
                        };

                        db.AgentMasters.Add(agentMaster);
                    }

                    //Save changes
                    db.SaveChanges();

                    return new VerifyAccountDto
                    {
                        ClientCode = model.ClientCode,
                        VirtualAccountNumber = model.VirtualAccountNumber,
                        TransactionAmount = model.TransactionAmount,
                        UtrNumber = model.UtrNumber,
                        SenderIfscCode = model.SenderIfscCode,
                        Status = Constants.Accept
                    };
                }
            }
            catch (Exception ex)
            {

                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"VerifyAccountAsync Error :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"VerifyAccountAsync Error :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"VerifyAccountAsync Error :- {ex.StackTrace}");

                return new VerifyAccountDto
                {
                    ClientCode = model.ClientCode,
                    VirtualAccountNumber = model.VirtualAccountNumber,
                    TransactionAmount = model.TransactionAmount,
                    UtrNumber = model.UtrNumber,
                    SenderIfscCode = model.SenderIfscCode,
                    Status = Constants.Reject,
                    RejectReason = Constants.TechnicalFailure
                };
            }
        }




        public async Task<BankDepositTransactionDto> BankDepositTransactionAsync(BankDepositTransactionModel model, Dictionary<string, string> dictionary)
        {
            try
            {

                using (var db = new MyIPayEntities())
                {
                    if (await db.BankDepositTransactions.AnyAsync(x => x.UtrRefNo.ToUpper() == model.UtrRefNo))
                    {

                        return new BankDepositTransactionDto
                        {
                            Status = Constants.Reject,
                            BankRefNumber = model.BankRefNo,
                            RejectReason = Constants.DuplicateUtrNumber
                        };
                    }
                    else
                    {
                        //Map request data
                        BankDepositTransaction entity = new BankDepositTransaction().CopyFrom(model);

                        entity.CreatedOn = DateTime.Now;
                        entity.RequestLog = JsonConvert.SerializeObject(model);//Converter.XmlSerializer(model);

                        entity.Username = dictionary.FirstOrDefault(x => x.Key == Constants.Username).Value;
                        entity.Password = SecurityHelper.Encrypt(dictionary.FirstOrDefault(x => x.Key == Constants.Password).Value);
                        entity.Agency = dictionary.FirstOrDefault(x => x.Key == Constants.Agency).Value;
                        entity.Subscriber = dictionary.FirstOrDefault(x => x.Key == Constants.Subscriber).Value;


                        entity.TsiTrxnId = SecurityHelper.GenerateClientReferenceId(Constants.AppUserName);

                        db.BankDepositTransactions.Add(entity);

                        db.SaveChanges();

                        return new BankDepositTransactionDto
                        {
                            Status = Constants.Success,
                            BankRefNumber = model.BankRefNo,
                            TsiTrxnId = entity.TsiTrxnId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"BankDepositTransaction Error :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"BankDepositTransaction Error :- {ex.InnerException}");
                Utility.SaveRequestAndResponseLog("ECollectionService_Error", $"BankDepositTransaction Error :- {ex.StackTrace}");

                return new BankDepositTransactionDto
                {
                    Status = Constants.Reject,
                    BankRefNumber = model.BankRefNo,
                    RejectReason = Constants.ErrorInApi
                };
            }

        }

    }
}
