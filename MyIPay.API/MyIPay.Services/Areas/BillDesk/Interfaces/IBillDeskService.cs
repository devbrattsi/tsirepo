﻿using MyIPay.Dtos.Areas.Common;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.BillDesk.Interfaces
{
    public interface IBillDeskService
    {
        Task<HttpResponseMessage> GetBillersAsync();
    }
}
