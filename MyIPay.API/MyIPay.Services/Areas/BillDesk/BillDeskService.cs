﻿using MyIPay.Helpers;
using MyIPay.Services.Areas.BillDesk.Interfaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.BillDesk
{
    public class BillDeskService : IBillDeskService
    {
        private readonly string _baseUrl;
        private readonly string _sourceId;
        private readonly string _clientId;
        private readonly string _secretKey;
        private readonly string _timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
        private readonly string _traceId = SecurityHelper.GenerateClientReferenceId();
        private const string _contentType = "application/json";
        private const string _accept = "application/json";
        private const string _httpMethodTypePost = "POST";
        private const string _httpMethodTypeGet = "GET";


        public BillDeskService()
        {
            _baseUrl = AppSettings.BillDeskMode == "L" ? AppSettings.BillDeskLiveApiBaseUrl : AppSettings.BillDeskUatApiBaseUrl;
            _sourceId = AppSettings.BillDeskMode == "L" ? AppSettings.BillDeskSourceId : "TSIPL";
            _clientId = AppSettings.BillDeskMode == "L" ? AppSettings.BillDeskClientId : "tsipluat";
            _secretKey = AppSettings.BillDeskMode == "L" ? AppSettings.BillDeskSecretKey : "8FF613EE2FC8A9A";
        }

        #region Payments
        public async Task<HttpResponseMessage> GetBillersAsync()
        {
            using (var client = new HttpClient())
            {
                string url = $"hgpay/v2_1/{_sourceId}/billpay/billers";

                string authorization = GenerateAuthorizationKey(_httpMethodTypeGet, url, _traceId, _timestamp);

                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.BillDeskTraceId, _traceId);
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.BillDeskTimestamp, _timestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, authorization);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Authorization, authorization);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorization);
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.ContentType, _contentType);
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Accept, _accept);

                //Sending request to find web api REST service resource to get all billers using HttpClient  
                HttpResponseMessage response = await client.GetAsync(url);

                return response;
            }
        }

        #endregion




        #region Payments
        public async Task<HttpResponseMessage> DoPaymentAsync()
        {
            using (var client = new HttpClient())
            {
                string url = $"/hgpay/v2_1/<sourceid>/customers/<customerid>/billpay/payments";

                string authorization = GenerateAuthorizationKey("post", url, _traceId, _timestamp);

                //Passing service base url  
                client.BaseAddress = new Uri(_baseUrl);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.BillDeskTraceId, _traceId);
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.BillDeskTimestamp, _timestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, authorization);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Constants.Authorization, "Your Oauth token");
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.ContentType, "application/json");
                client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Accept, "application/json");

                //Sending request to find web api REST service resource to get all billers using HttpClient  
                HttpResponseMessage response = await client.GetAsync(url);

                return response;
            }
        }
        #endregion

        #region Private Functions

        private string GenerateAuthorizationKey(string httpMethod,
                                                string url,
                                                string traceId,
                                                string timestamp,
                                                string requestBody = null)
        {
            string requestBodyHash;
            string hmacString;
            //if (!string.IsNullOrEmpty(requestBody))
            //{
            //    requestBodyHash = SecurityHelper.GenerateSha256Hash(requestBody);
            //    hmacString = $"{httpMethod.ToUpper()}|/{url}/|{_contentType}|{_accept}|{traceId}|{timestamp}|{requestBodyHash}".Trim();
            //}
            //else
            //{
            //    hmacString = $"{httpMethod.ToUpper()}|/{url}/|{_contentType}|{_accept}|{traceId}|{timestamp}".Trim();

            //}
            if ("POST".Equals(httpMethod, StringComparison.OrdinalIgnoreCase))
            {
                requestBodyHash = SecurityHelper.GenerateSha256Hash(requestBody);
                hmacString = $"{httpMethod.ToUpper()}|{url}|{_contentType}|{_accept}|{traceId}|{timestamp}|{requestBodyHash}".Trim();
            }
            else
            {
                hmacString = $"{httpMethod.ToUpper()}|{url}|{_contentType}|{_accept}|{traceId}|{timestamp}".Trim();

            }

            var hmac = SecurityHelper.GenerateHmacSha256Hash(hmacString, _secretKey);

            string authorization = $"HMACSignature {_clientId}:{hmac}";

            return authorization;
        }


        #endregion
    }
}
