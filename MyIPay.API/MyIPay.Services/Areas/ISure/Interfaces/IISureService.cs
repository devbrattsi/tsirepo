﻿
using MyIPay.Dtos.Areas.ISure;
using MyIPay.Models.Areas.ISure;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.ISure.Interfaces
{
    public interface IISureService
    {
        Task<VerifyUserDto> VerifyUserAsync(VerifyUserModel model);
        Task<InitiateTransactionDto> InitiateTransactionAsync(InitiateTransactionModel model);
    }
}
