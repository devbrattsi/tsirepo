﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MyIPay.DataLayer.EF;
using MyIPay.Dtos.Areas.ISure;
using MyIPay.Helpers;
using MyIPay.Models.Areas.ISure;
using MyIPay.Services.Areas.ISure.Interfaces;

namespace MyIPay.Services.Areas.ISure
{
    public class ISureService : IISureService
    {
        private readonly DateTime dateTimeNow = DateTime.Now;

        public async Task<VerifyUserDto> VerifyUserAsync(VerifyUserModel model)
        {
            try
            {
                using (var db = new MyIPayEntities())
                {
                    var userDetail = await db.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.VirtualAccountNumber.ToLower() == model.AgentCode.ToLower());

                    if (userDetail == null)
                    {
                        return new VerifyUserDto
                        {
                            Client_Code = model.Client_Code,
                            AgentCode = model.AgentCode,
                            Amount = Constants.EditableAmountCode,
                            Status = Constants.Reject,
                            Reject_Reason = Constants.InvalidAgentCode
                        };

                    }

                    return new VerifyUserDto
                    {
                        Client_Code = model.Client_Code,
                        AgentCode = model.AgentCode,
                        Amount = Constants.EditableAmountCode,
                        Status = Constants.Accept,
                        Reject_Reason = string.Empty
                    };


                }
            }
            catch (Exception ex)
            {

                Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"VerifyUserAsync Error :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"VerifyUserAsync Error :- {ex.InnerException}");
                Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"VerifyUserAsync Error :- {ex.StackTrace}");

                return new VerifyUserDto
                {
                    Client_Code = model.Client_Code,
                    AgentCode = model.AgentCode,
                    Amount = Constants.EditableAmountCode,
                    Status = Constants.Reject,
                    Reject_Reason = Constants.TechnicalFailure
                };
            }
        }

        public async Task<InitiateTransactionDto> InitiateTransactionAsync(InitiateTransactionModel model)
        {
            try
            {
                using (var db = new MyIPayEntities())
                {

                    var userDetail = await db.Vw_GetUserDetail.FirstOrDefaultAsync(x => x.VirtualAccountNumber.ToLower() == model.AgentCode.ToLower());

                    if (userDetail == null)
                    {
                        return new InitiateTransactionDto
                        {
                            Client_Code = model.Client_Code,
                            AgentCode = model.AgentCode,
                            Status = Constants.Reject,
                            Reject_Reason = Constants.InvalidAgentCode
                        };

                        //return Ok(failureRseponse);
                    }



                    //Getting user's available balance
                    var availableBalance = db.sp_GET_AVAILABLE_LIMIT(userDetail.AgentId, userDetail.Role, userDetail.DistributorId).FirstOrDefault();

                    DateTime transactionDateTime;
                    try
                    {
                        //Converting transaction date from string to datetime type
                        transactionDateTime = DateTime.ParseExact(model.Transaction_Date.Replace('-', '/'), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //if (!DateTime.TryParse(model.Transaction_Date, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateResult) || dateResult != dateTimeNow.Date)
                    }
                    catch (FormatException fex)
                    {
                        Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"InitiateTransactionAsync Error :- {fex.Message}");
                        Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"InitiateTransactionAsync Error :- {fex.InnerException}");
                        Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"InitiateTransactionAsync Error :- {fex.StackTrace}");

                        return new InitiateTransactionDto
                        {
                            Client_Code = model.Client_Code,
                            AgentCode = model.AgentCode,
                            Status = Constants.Reject,
                            Reject_Reason = "Invalid transaction datetime format."
                        };
                    }

                    //if (transactionDateTime != dateTimeNow.Date)
                    //{
                    //    return new InitiateTransactionDto
                    //    {
                    //        Client_Code = model.Client_Code,
                    //        AgentCode = model.AgentCode,
                    //        Status = Constants.Reject,
                    //        Reject_Reason = "Invalid transaction date."
                    //    };

                    //}


                    //Removing white space and adding prefix
                    model.ISure_ID = $"TP/{model.ISure_ID.Trim()}";

                    if (db.DistributorMasters.Any(x => x.Transaction_Id.ToUpper() == model.ISure_ID.ToUpper())
                        || db.AgentMasters.Any(x => x.Transaction_Id.ToUpper() == model.ISure_ID.ToUpper()))
                    {
                        return new InitiateTransactionDto
                        {
                            Client_Code = model.Client_Code,
                            AgentCode = model.AgentCode,
                            Status = Constants.Reject,
                            Reject_Reason = Constants.DuplicateISurePayId
                        };

                    }

                    //if (db.DistributorMasters.Any(x => x.IMPS_NEFT_NO.ToUpper() == model.IBANK_Transaction_Id)
                    //   || db.AgentMasters.Any(x => x.IMPS_NEFT_NO.ToUpper() == model.IBANK_Transaction_Id))
                    //{
                    //    return new InitiateTransactionDto
                    //    {
                    //        Client_Code = model.Client_Code,
                    //        Client_VAccount_No1 = model.Client_VAccount_No1,
                    //        Status = Constants.Reject,
                    //        Reject_Reason = Constants.DuplicateISureId
                    //    };

                    //}



                    //Save topup data
                    if (userDetail.Role.ToLower() == ClaimConstants.Distributor.ToLower())
                    {

                        var distributorMaster = new DistributorMaster()
                        {
                            Agent_Id = userDetail.AgentId.ToUpper(),
                            AvailableLimit = availableBalance.Value + model.Amount,
                            Bill_Amount = 0,
                            Cash_Back = 0,
                            CCF = 0,
                            Remarks = "I-Sure",
                            TopBy = "I-Sure",
                            Top_Up_Amount = model.Amount,
                            Transaction_Date_Time = dateTimeNow,//transactionDateTime,
                            UpdatedBy = "I-Sure",
                            Transaction_Id = model.ISure_ID.ToString(),
                            IMPS_NEFT_NO = model.IBANK_Transaction_Id,
                            DistributorId = userDetail.DistributorId,
                            ChequeNo = $"{model.Instrument_Number}|{model.MICR_CODE}|{model.Bank_Name}|{model.Branch_Name}"
                        };

                        db.DistributorMasters.Add(distributorMaster);

                        await db.SaveChangesAsync();

                    }
                    else
                    {

                        var agentMaster = new AgentMaster
                        {
                            Agent_Id = userDetail.AgentId.ToUpper(),
                            AvailableLimit = availableBalance.Value + model.Amount,
                            Bill_Amount = 0,
                            Cash_Back = 0,
                            CCF = 0,
                            Remarks = "I-Sure",
                            TopBy = "I-Sure",
                            Top_Up_Amount = model.Amount,
                            Transaction_Date_Time = dateTimeNow,//transactionDateTime,
                            UpdatedBy = "I-Sure",
                            Transaction_Id = model.ISure_ID.ToString(),
                            IMPS_NEFT_NO = model.IBANK_Transaction_Id.ToString(),
                            DistributorId = userDetail.DistributorId,
                            ChequeNo = $"{model.Instrument_Number}|{model.MICR_CODE}|{model.Bank_Name}|{model.Branch_Name}"
                        };


                        db.AgentMasters.Add(agentMaster);

                        await db.SaveChangesAsync();
                    }



                    return new InitiateTransactionDto
                    {
                        Client_Code = model.Client_Code,
                        AgentCode = model.AgentCode,
                        Status = Constants.Accept,
                        Reject_Reason = string.Empty
                    };

                }
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"InitiateTransactionAsync Error :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"InitiateTransactionAsync Error :- {ex.InnerException}");
                Helpers.Utility.SaveRequestAndResponseLog("ISureService_Error", $"InitiateTransactionAsync Error :- {ex.StackTrace}");

                return new InitiateTransactionDto
                {
                    Client_Code = model.Client_Code,
                    AgentCode = model.AgentCode,
                    Status = Constants.Reject,
                    Reject_Reason = Constants.TechnicalFailure
                };
            }
        }
    }
}
