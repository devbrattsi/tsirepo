﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using MyIPay.Dtos.Areas.QwikCilver;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.QwikCilver.Interfaces;
using Newtonsoft.Json;
using MyIPay.Helpers;
using MyIPay.Dtos.Areas.Common;
using System.Data.SqlClient;
using MyIPay.DataLayer.EF;
using Utility = MyIPay.Helpers.Utility;
using System.Net.Http.Headers;
using System.Data;
using System.Data.Entity;
using RootCategory = MyIPay.DataLayer.EF.RootCategory;
using ParentCategory = MyIPay.DataLayer.EF.ParentCategory;
using ChildCategory = MyIPay.DataLayer.EF.ChildCategory;
using Product = MyIPay.DataLayer.EF.Product;
using Newtonsoft.Json.Linq;
using MyIPay.Dtos.Areas.QwikCilver.GiftCard;
using MyIPay.Models.Areas.QwikCilver.GiftCard;
using MyIPay.DataLayer.EF.DefaultContext.Entity.Procedure;
using MyIPay.DataLayer.EF.MongoDb;
using MongoDB.Driver;
using MyIPay.DataLayer.EF.MongoDb.Entity;
using MongoDB.Driver.Builders;
using System.Reflection;

namespace MyIPay.Services.Areas.QwikCilver
{
    public class GiftCardService : OAuthBase, IGiftCardService
    {
        private readonly string _mode = AppSettings.QwikCilverMode;
        private readonly string _baseUrl;
        private readonly string _oauthConsumerKey;
        private readonly string _oauthConsumerSecret;
        private readonly string _username;
        private readonly string _password;
        private readonly string _timestamp;

        private IApiManagerService _apiManagerService;
        private MyIPayEntities _myIPayEntities;
        private readonly ICommonService _commonService;



        public GiftCardService(IApiManagerService apiManagerService,
                               MyIPayEntities myIPayEntities,
                               ICommonService commonService)
        {
            _apiManagerService = apiManagerService;
            _myIPayEntities = myIPayEntities;

            _baseUrl = _mode == Mode.L.ToString() ? AppSettings.QwikCilverLiveBaseUrl : AppSettings.QwikCilverUatBaseUrl;
            _oauthConsumerKey = _mode == Mode.L.ToString() ? AppSettings.QwikCilverLiveConsumerKey : AppSettings.QwikCilverUatConsumerKey;
            _oauthConsumerSecret = _mode == Mode.L.ToString() ? AppSettings.QwikCilverLiveConsumerSecret : AppSettings.QwikCilverUatConsumerSecret;
            _username = _mode == Mode.L.ToString() ? AppSettings.QwikCilverLiveUsername : AppSettings.QwikCilverUatUsername;
            _password = _mode == Mode.L.ToString() ? AppSettings.QwikCilverLivePassword : AppSettings.QwikCilverUatPassword;

            _timestamp = GenerateTimeStamp();

            _commonService = commonService;
        }

        #region Woohoo APIs
        public async Task<OAuthTokenDto> GetOAuthTokenAsync()
        {

            try
            {
                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthCallback, Constants.Oob},
                    {  Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce,GetNonce() },
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue }
                };

                var sigString = string.Join("&", valuePairs.Union(valuePairs)
                                      .Select(kvp => string.Format("{0}={1}", Uri.EscapeDataString(kvp.Key), Uri.EscapeDataString(kvp.Value)))
                                      .OrderBy(s => s));

                string signature = GenerateSignature($"{_baseUrl}oauth/initiate", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, "");

                string url = $"oauth/initiate?{sigString}&oauth_signature={signature}";


                HttpResponseMessage httpResponseMessage = await _apiManagerService.GetAsync(_baseUrl, url);

                string jsonOauthToken = await httpResponseMessage.Content.ReadAsStringAsync();

                Utility.WriteLog("GiftCardService", $"GetOAuthTokenAsync Response : {jsonOauthToken}");

                OAuthTokenDto oAuthTokenDto = new OAuthTokenDto();

                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    throw new HttpRequestException(httpResponseMessage.StatusCode.ToString());
                }

                if (!string.IsNullOrEmpty(jsonOauthToken))
                {
                    string[] array = jsonOauthToken.Split(new char[1] { '&' });
                    for (int i = 0; i < array.Length; i++)
                    {
                        string[] array2 = array[i].Split(new char[1] { '=' });
                        string a = array2[0];
                        if (!(a == Constants.OAuthToken))
                        {
                            if (a == Constants.OAuthTokenSecret)
                            {
                                oAuthTokenDto.OAuthTokenSecret = array2[1];
                            }
                        }
                        else
                        {
                            oAuthTokenDto.AOuthToken = array2[1];
                        }
                    }
                }


                return oAuthTokenDto;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetOAuthTokenAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetOAuthTokenAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetOAuthTokenAsync Error : {ex.StackTrace}");

                throw;
            }
        }


        public async Task<VerifierDto> GetOauthVerifierAsync(string oauthToken)
        {
            try
            {
                string url = $"oauth/authorize/customerVerifier?oauth_token={oauthToken}&username={_username}&password={_password}";

                HttpResponseMessage httpResponseMessage = await _apiManagerService.GetAsync(_baseUrl, url);

                string json = await httpResponseMessage.Content.ReadAsStringAsync();

                Utility.WriteLog("GiftCardService", $"GetOauthVerifierAsync Response : {json}");


                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    throw new HttpRequestException(httpResponseMessage.StatusCode.ToString());
                }

                VerifierDto dto = JsonConvert.DeserializeObject<VerifierDto>(json);
                return dto;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetOauthVerifierAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetOauthVerifierAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetOauthVerifierAsync Error : {ex.StackTrace}");

                throw;
            }
        }


        public async Task<AccessTokenDto> GetOauthAccessTokenAsync(string oauthToken, string oauthTokenSecret, string oauthVerifier)
        {

            try
            {
                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken, oauthToken },
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVerifier, oauthVerifier},
                    { Constants.OAuthVersion, Constants.OAuthVersionValue }
                };

                string signature = GenerateSignature($"{_baseUrl}oauth/token", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, oauthTokenSecret);

                var sigString = string.Join("&", valuePairs.Union(valuePairs)
                                      .Select(kvp => string.Format("{0}={1}",
                                                     Uri.EscapeDataString(kvp.Key),
                                                     Uri.EscapeDataString(kvp.Value)))
                                      .OrderBy(s => s));


                string url = $"{_baseUrl}oauth/token?{sigString}&oauth_signature={signature}";

                AccessTokenDto dto = new AccessTokenDto();
                HttpResponseMessage httpResponseMessage = await _apiManagerService.GetAsync(_baseUrl, url);

                string result = await httpResponseMessage.Content.ReadAsStringAsync();

                Utility.WriteLog("GiftCardService", $"GetOauthAccessTokenAsync Response : {result}");

                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    throw new HttpRequestException(httpResponseMessage.StatusCode.ToString());
                }

                if (!string.IsNullOrEmpty(result))
                {
                    string[] array = result.Split(new char[1] { '&' });
                    for (int i = 0; i < array.Length; i++)
                    {
                        string[] array2 = array[i].Split(new char[1] { '=' });
                        string a = array2[0];
                        if (!(a == Constants.OAuthToken))
                        {
                            if (a == Constants.OAuthTokenSecret)
                            {
                                dto.OauthTokenSecret = array2[1];
                            }
                        }
                        else
                        {
                            dto.AauthToken = array2[1];
                        }
                    }
                }

                return dto;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetOauthAccessToken Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetOauthAccessToken Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetOauthAccessToken Error : {ex.StackTrace}");

                throw;
            }
        }



        public async Task<SettingDto> GetSttingsAsync()
        {

            try
            {
                OauthToken woohooToken = _myIPayEntities.OauthTokens
                                                              .OrderByDescending(x => x.CreatedOn)
                                                              .FirstOrDefault(x => x.ConsumerKey == _oauthConsumerKey);

                if (woohooToken == null)
                {
                    //return new ApiResponseDto
                    //{
                    //    Message = Constants.TokenMissing
                    //};

                    throw new HttpRequestException(Constants.TokenMissing);
                }

                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = "rest/settings";

                string signature = GenerateSignature($"{_baseUrl}{url}", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, woohooToken.TokenSecret);


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.Token, signature));

                    //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "OAuth oauth_consumer_key=\"" + _oauthConsumerKey + "\",oauth_token=\"" + woohooToken.OauthToken + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + GetTimestamp() + "\",oauth_nonce=\"" + GetNonce() + "\",oauth_version=\"1.0\",oauth_signature=\"" + signature + "\"");

                    //HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, settingUrl);
                    //requestMessage.Headers.Add("Authorization", "OAuth oauth_consumer_key=\"" + oauth_consumer_key + "\",oauth_token=\"" + oauth_token + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + GetTimestamp() + "\",oauth_nonce=\"" + GetNonce() + "\",oauth_version=\"1.0\",oauth_signature=\"" + signature + "\"");
                    //HttpResponseMessage httpResponseMessage = await client.SendAsync(requestMessage);
                    //httpStatusCode = httpResponseMessage.StatusCode;

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftCardService", $"GetSttingsAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    return JsonConvert.DeserializeObject<SettingDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetSttingsAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetSttingsAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetSttingsAsync Error : {ex.StackTrace}");

                throw;

                //return new ApiResponseDto
                //{
                //    HasError = true,
                //    Message = ex.Message,
                //    //Response = Constants.Failed
                //};
            }

        }


        public async Task<CategoryDto> GetCategoryAsync()
        {

            try
            {
                OauthToken woohooToken = _myIPayEntities.OauthTokens
                                                               .OrderByDescending(x => x.CreatedOn)
                                                               .FirstOrDefault(x => x.ConsumerKey == _oauthConsumerKey);

                if (woohooToken == null)
                {
                    throw new HttpRequestException(Constants.TokenMissing);
                }

                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = "rest/category";

                string signature = GenerateSignature($"{_baseUrl}{url}", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, woohooToken.TokenSecret);


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.Token, signature));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftCardService", $"GetCategoryAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    return JsonConvert.DeserializeObject<CategoryDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetCategoryAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetCategoryAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetCategoryAsync Error : {ex.StackTrace}");

                throw;
            }

        }

        public async Task<CategoryDetailDto> GetCategoryDetailByIdAsync(string id)
        {

            try
            {
                OauthToken woohooToken = _myIPayEntities.OauthTokens
                                                              .OrderByDescending(x => x.CreatedOn)
                                                              .FirstOrDefault(x => x.ConsumerKey == _oauthConsumerKey);

                if (woohooToken == null)
                {
                    //return new ApiResponseDto
                    //{
                    //    Message = Constants.TokenMissing
                    //};

                    throw new HttpRequestException(Constants.TokenMissing);
                }

                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = $"rest/category/{id}";

                string signature = GenerateSignature($"{_baseUrl}{url}", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, woohooToken.TokenSecret);


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.Token, signature));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftCardService", $"GetCategoryDetailByIdAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    return JsonConvert.DeserializeObject<CategoryDetailDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetCategoryDetailByIdAsync Error, Category ID : {id} : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetCategoryDetailByIdAsync Error, Category ID : {id} : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetCategoryDetailByIdAsync Error, Category ID : {id} : {ex.StackTrace}");

                throw;

                //return new ApiResponseDto
                //{
                //    HasError = true,
                //    Message = ex.Message,
                //    //Response = Constants.Failed
                //};
            }

        }


        public async Task<ProductDetailDto> GetProductDetailByIdAsync(string id)
        {

            try
            {
                OauthToken woohooToken = _myIPayEntities.OauthTokens
                                                        .OrderByDescending(x => x.CreatedOn)
                                                        .FirstOrDefault(x => x.ConsumerKey == _oauthConsumerKey);

                if (woohooToken == null)
                {
                    //return new ApiResponseDto
                    //{
                    //    Message = Constants.TokenMissing
                    //};

                    throw new HttpRequestException(Constants.TokenMissing);
                }

                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = $"rest/product/{id}";

                string signature = GenerateSignature($"{_baseUrl}{url}", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, woohooToken.TokenSecret);


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.Token, signature));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftCardService", $"GetProductDetailByIdAsync Response : {json}");


                    if (httpResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    ProductDetailDto result = JsonConvert.DeserializeObject<ProductDetailDto>(json);

                    return result;
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetProductDetailByIdAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetProductDetailByIdAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetProductDetailByIdAsync Error : {ex.StackTrace}");

                throw;

                //return new ApiResponseDto
                //{
                //    HasError = true,
                //    Message = ex.Message,
                //    //Response = Constants.Failed
                //};
            }

        }

        //public async Task<ApiResponseDto> SpendAsync(SpendModel model)
        //{

        //    try
        //    {
        //        WoohooOauthToken woohooToken = _myIPayEntities.WoohooOauthTokens
        //                                                      .OrderByDescending(x => x.CreatedOn)
        //                                                      .FirstOrDefault(x => x.OauthConsumerKey == _oauthConsumerKey);

        //        if (woohooToken == null)
        //        {
        //            return new ApiResponseDto
        //            {
        //                Message = Constants.TokenMissing
        //            };
        //        }

        //        Dictionary<string, string> valuePairs = new Dictionary<string, string>
        //        {
        //            { Constants.OAuthConsumerKey, _oauthConsumerKey},
        //            { Constants.OAuthNonce, GetNonce() },
        //            { Constants.OAuthTimestamp, GetTimestamp() },
        //            { Constants.OAuthToken,woohooToken.OauthToken},
        //            { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
        //            { Constants.OAuthVersion, Constants.OAuthVersionValue },
        //        };

        //        string url = $"rest/product/spend";

        //        string signature = GenerateSignature($"{_baseUrl}{url}", valuePairs, _oauthConsumerSecret, woohooToken.OauthTokenSecret);


        //        using (var client = new HttpClient())
        //        {

        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.BaseAddress = new Uri(_baseUrl);

        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

        //            client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.OauthToken, signature));

        //            string json = JsonConvert.SerializeObject(model);

        //            Utility.WriteLog("GiftCardService", $"SpendAsync Request Log SpendModel : {json}");

        //            HttpResponseMessage httpResponse = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

        //            string jsonResponse = await httpResponse.Content.ReadAsStringAsync();

        //            Utility.WriteLog("GiftCardService", $"SpendAsync Response : {jsonResponse}");


        //            if (httpResponse.StatusCode != HttpStatusCode.OK)
        //            {
        //                throw new HttpRequestException(httpResponse.StatusCode.ToString());
        //            }

        //            return new ApiResponseDto
        //            {
        //                IsSuceess = true,
        //                Message = Constants.Success,
        //                Response = JsonConvert.DeserializeObject<ProductDetailDto>(jsonResponse)
        //            };
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.Message}");
        //        Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.InnerException}");
        //        Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.StackTrace}");

        //        //throw;

        //        return new ApiResponseDto
        //        {
        //            HasError = true,
        //            Message = ex.Message,
        //            //Response = Constants.Failed
        //        };
        //    }

        //}

        public async Task<SpendDto> SpendAsync(SpendModel model)
         {

            try
            {
                OauthToken woohooToken = await _myIPayEntities.OauthTokens
                                                          .OrderByDescending(x => x.CreatedOn)
                                                          .FirstOrDefaultAsync(x => x.ConsumerKey == _oauthConsumerKey);


                if (woohooToken == null)
                {
                    //return new ApiResponseDto
                    //{
                    //    Message = Constants.TokenMissing
                    //};

                    throw new HttpRequestException(Constants.TokenMissing);
                }

                SortedDictionary<string, string> valuePairs = new SortedDictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce,_timestamp},
                    { Constants.OAuthTimestamp,_timestamp },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = $"rest/spend";
                string fullurl = $"{_baseUrl}{url}";



                //Generating signature
                string signature = GenerateSignature(new Uri(fullurl),
                                                     _oauthConsumerKey,
                                                     _oauthConsumerSecret,
                                                     woohooToken.Token,
                                                     woohooToken.TokenSecret,
                                                     Constants.HttpMethodPost,
                                                     _timestamp,
                                                     _timestamp,
                                                     out string normalizedUrl,
                                                     out string normalizedReqParams,
                                                     out string authHeader);



                //Generating Authorization Header
                string authorizationHeader = GenerateAuthorizationHeader(signature,
                                                                        _timestamp,
                                                                        _timestamp,
                                                                        Constants.OAuthSignatureMethodType,
                                                                        _oauthConsumerKey,
                                                                        woohooToken.Token,
                                                                        Constants.OAuthVersionValue);
                //var dictionary = model.ToKeyValue();

                using (var client = new HttpClient())
                {

                    client.Timeout = TimeSpan.FromSeconds(40);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    client.BaseAddress = new Uri(_baseUrl);
                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, authHeader);

                    string json = JsonConvert.SerializeObject(model);

                    Utility.WriteLog("GiftCardService", $"SpendAsync Request Log SpendModel : {json}");

                    HttpResponseMessage httpResponse = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

                    string jsonResponse = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftCardService", $"SpendAsync Response : {jsonResponse}");

                    if (httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    }

                    TempDto tempDto = JsonConvert.DeserializeObject<TempDto>(jsonResponse);

                    SpendDto result = JsonConvert.DeserializeObject<SpendDto>(jsonResponse);

                    if (tempDto.CardDetails != null && tempDto.CardDetails.Count > 0)
                    {
                        EGiftCard giftCard = JsonConvert.DeserializeObject<EGiftCard>(tempDto.CardDetails
                                                                                            .ElementAtOrDefault(0)
                                                                                            .Value
                                                                                            .ToString()
                                                                                            .Replace("[", "").Replace("]", ""));

                        giftCard.CardName = tempDto.CardDetails.ElementAtOrDefault(0).Key.ToString();

                        if (giftCard != null)
                            result.CardDetails.EGiftCards.Add(giftCard);
                    }

                    return result;
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"SpendAsync Error : {ex.StackTrace}");

                //throw;

                return new SpendDto
                {
                    Message = ex.Message,
                    //Response = Constants.Failed
                };
            }

        }


        public async Task<StatusDto> GetOrderStatusAsync(string refNo)
        {

            try
            {
                OauthToken woohooToken = _myIPayEntities.OauthTokens
                                                              .OrderByDescending(x => x.CreatedOn)
                                                              .FirstOrDefault(x => x.ConsumerKey == _oauthConsumerKey);

                if (woohooToken == null)
                {
                    return new StatusDto
                    {
                        Message = Constants.TokenMissing
                    };
                }

                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = $"rest/status/{refNo}";

                string signature = GenerateSignature($"{_baseUrl}{url}", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, woohooToken.TokenSecret);


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.Token, signature));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftCardService", $"GetOrderStatusAsync Response : {json}");


                    ////if (httpResponse.StatusCode != HttpStatusCode.OK)
                    ////{
                    ////    throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    ////}

                    //return new ApiResponseDto
                    //{
                    //    IsSuceess = true,
                    //    Message = Constants.Success,
                    //    Response = JsonConvert.DeserializeObject<StatusDto>(json)
                    //};

                    return JsonConvert.DeserializeObject<StatusDto>(json);


                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"GetOrderStatusAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"GetOrderStatusAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"GetOrderStatusAsync Error : {ex.StackTrace}");

                ////throw;
                //return new ApiResponseDto
                //{
                //    HasError = true,
                //    Message = ex.Message,
                //    //Response = Constants.Failed
                //};

                return new StatusDto
                {
                    Message = ex.Message,
                };
            }

        }


        public async Task<ResendDto> ResendAsync(string refNo)
        {

            try
            {
                OauthToken woohooToken = _myIPayEntities.OauthTokens
                                                        .OrderByDescending(x => x.CreatedOn)
                                                        .FirstOrDefault(x => x.ConsumerKey == _oauthConsumerKey);

                if (woohooToken == null)
                {
                    return new ResendDto
                    {
                        Message = Constants.TokenMissing
                    };
                }

                Dictionary<string, string> valuePairs = new Dictionary<string, string>
                {
                    { Constants.OAuthConsumerKey, _oauthConsumerKey},
                    { Constants.OAuthNonce, GetNonce() },
                    { Constants.OAuthTimestamp, GetTimestamp() },
                    { Constants.OAuthToken,woohooToken.Token},
                    { Constants.OAuthSignatureMethod, Constants.OAuthSignatureMethodType },
                    { Constants.OAuthVersion, Constants.OAuthVersionValue },
                };

                string url = $"rest/resend/{refNo}";

                string signature = GenerateSignature($"{_baseUrl}{url}", Constants.HttpMethodGet, valuePairs, _oauthConsumerSecret, woohooToken.TokenSecret);


                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.BaseAddress = new Uri(_baseUrl);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    client.DefaultRequestHeaders.TryAddWithoutValidation(Constants.Authorization, GetAuthorizationHeader(woohooToken.Token, signature));

                    HttpResponseMessage httpResponse = await _apiManagerService.GetAsync(_baseUrl, url, client);

                    string json = await httpResponse.Content.ReadAsStringAsync();

                    Utility.WriteLog("GiftVoucherService", $"ResendAsync Response : {json}");


                    //if (httpResponse.StatusCode != HttpStatusCode.OK)
                    //{
                    //    throw new HttpRequestException(httpResponse.StatusCode.ToString());
                    //}

                    //return new ApiResponseDto
                    //{
                    //    IsSuceess = true,
                    //    Message = Constants.Success,
                    //    Response = JsonConvert.DeserializeObject<ResendDto>(json)
                    //};

                    return JsonConvert.DeserializeObject<ResendDto>(json);
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"ResendAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"ResendAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"ResendAsync Error : {ex.StackTrace}");

                //throw;

                //return new ApiResponseDto
                //{
                //    HasError = true,
                //    Message = ex.Message,
                //    //Response = Constants.Failed
                //};

                return new ResendDto
                {
                    Message = ex.Message,
                };
            }

        }

        #endregion

        #region Insert APIs

        public async Task<ApiResponseDto> AddOauthAccessTokenAsync()
        {
            try
            {
                OAuthTokenDto oAuthTokenDto = await GetOAuthTokenAsync();

                Utility.WriteLog("GiftCardService", $"AddOauthAccessTokenAsync :: GetOAuthTokenAsync Response : {JsonConvert.SerializeObject(oAuthTokenDto)}");

                ApiResponseDto result = new ApiResponseDto();
                if (!string.IsNullOrEmpty(oAuthTokenDto.AOuthToken))
                {
                    VerifierDto verifierDto = await GetOauthVerifierAsync(oAuthTokenDto.AOuthToken);

                    Utility.WriteLog("GiftCardService", $"AddOauthAccessTokenAsync :: GetOauthVerifierAsync Response : {JsonConvert.SerializeObject(verifierDto)}");


                    if (verifierDto.Success)
                    {
                        AccessTokenDto accessTokenDto = await GetOauthAccessTokenAsync(oAuthTokenDto.AOuthToken, oAuthTokenDto.OAuthTokenSecret, verifierDto.Verifier);

                        Utility.WriteLog("GiftCardService", $"AddOauthAccessTokenAsync :: GetOauthAccessTokenAsync Response : {JsonConvert.SerializeObject(result)}");

                        if (!string.IsNullOrEmpty(accessTokenDto.AauthToken) && !string.IsNullOrEmpty(accessTokenDto.OauthTokenSecret))
                        {
                            int status = InserOauthAccessTokenAsync(accessTokenDto.AauthToken, accessTokenDto.OauthTokenSecret);

                            result.IsSuccess = status == 1 ? true : false;
                            result.Message = status == 1 ? "Oauth Token Detail Saved Successfully." : "Oauth Token Detail Not Saved.Please Try Again!";
                            result.Response = status == 1 ? "Oauth Token Detail Saved Successfully." : "Oauth Token Detail Not Saved.Please Try Again!";
                        }

                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"AddOauthAccessTokenAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"AddOauthAccessTokenAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"AddOauthAccessTokenAsync Error : {ex.StackTrace}");

                //throw;

                return new ApiResponseDto
                {
                    HasError = true,
                    IsSuccess = false,
                    Message = ex.Message,
                    //Response = Constants.Failed
                };
            }
        }

        public async Task<ApiResponseDto> UpdateOauthAccessTokenAsync()
        {
            try
            {

                using (var mongoRipository = new MongoRepositorySingelton())
                {
                    var recordCount = mongoRipository.tokens.AsQueryable().Count();
                    ApiResponseDto result = new ApiResponseDto();
                    if (recordCount > 0)
                    {
                        var filter = new FilterDefinitionBuilder<WoohooOauthToken>().Empty;

                        //List<WoohooOauthToken> results = await mongoRipository.tokens
                        //                                                      .Find<WoohooOauthToken>(filter)
                        //                                                      .SortByDescending(x=>x.TIMESTAMP).ToListAsync();

                        WoohooOauthToken token = await mongoRipository.tokens
                                                           .Find<WoohooOauthToken>(filter)
                                                           .SortByDescending(x => x.TIMESTAMP).FirstOrDefaultAsync();


                        int status = InserOauthAccessTokenAsync(token.VARIFIED_OATH_TOKEN, token.VARIFIED_OATH_TOKEN_SECRET);

                        result.IsSuccess = status == 1 ? true : false;
                        result.Message = status == 1 ? "Oauth Token Detail Saved Successfully." : "Oauth Token Detail Not Saved.Please Try Again!";
                        result.Response = status == 1 ? "Oauth Token Detail Saved Successfully." : "Oauth Token Detail Not Saved.Please Try Again!";
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = Constants.NotFound;
                        result.Response = Constants.NotFound;
                    }

                    return result;
                }

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"UpdateOauthAccessTokenAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"UpdateOauthAccessTokenAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"UpdateOauthAccessTokenAsync Error : {ex.StackTrace}");

                //throw;

                return new ApiResponseDto
                {
                    HasError = true,
                    IsSuccess = false,
                    Message = ex.Message,
                    //Response = Constants.Failed
                };
            }
        }
        public async Task<ApiResponseDto> AddCategoryAsync()
        {
            try
            {
                CategoryDto category = await GetCategoryAsync();

                if (category != null && category.RootCategory != null)// && category.RootCategory.CategoryList.Count > 0)
                {
                    //Truncate RootCategory to delete all old records.
                    _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[RootCategory]");

                    //Truncate ParentCategory to delete all old records.
                    _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[ParentCategory]");

                    //Truncate ChildCategory to delete all old records.
                    _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[ChildCategory]");

                    Utility.WriteLog("GiftCardService", $"Truncate Categories tables");

                    RootCategory rootCategory = new RootCategory
                    {
                        Id = category.RootCategory.CategoryId,
                        Name = category.RootCategory.CategoryName,
                        Image = category.RootCategory.CategoryImage,
                        CompleteJson = JsonConvert.SerializeObject(category)
                    };

                    _myIPayEntities.RootCategories.Add(rootCategory);

                    //List<ParentCategory> parentCategories = new List<ParentCategory>();
                    //List<ChildCategory> childCategories = new List<ChildCategory>();

                    if (category.RootCategory.CategoryList.Count > 0)
                    {
                        foreach (var item in category.RootCategory.CategoryList)
                        {
                            if (item.parent_category != null)
                            {


                                ParentCategory parentCategory = new ParentCategory
                                {
                                    Id = Convert.ToString(item.parent_category.CategoryId),
                                    Name = !string.IsNullOrEmpty(item.parent_category.CategoryName) ? item.parent_category.CategoryName.Trim()
                                                                                                    : item.parent_category.CategoryName,
                                    Image = item.parent_category.CategoryImage,
                                    RootCategoryId = category.RootCategory.CategoryId
                                };

                                _myIPayEntities.ParentCategories.Add(parentCategory);

                                if (item.parent_category.ChildCategories.Count > 0)
                                {
                                    foreach (var child in item.parent_category.ChildCategories)
                                    {
                                        ChildCategory childCategory = new ChildCategory
                                        {
                                            Id = child.CategoryId,
                                            Name = child.category_name,
                                            Image = child.CategoryImage,
                                            ParentCategoryId = item.parent_category.CategoryId
                                        };


                                        _myIPayEntities.ChildCategories.Add(childCategory);

                                        //childCategories.Add(childCategory);
                                    }

                                    //parentCategory.ChildCategories = childCategories;
                                }

                                //parentCategories.Add(parentCategory);
                            }

                        }

                        //rootCategory.ParentCategories = parentCategories;
                    }


                    _myIPayEntities.RootCategories.Add(rootCategory);

                    //Save changes
                    await _myIPayEntities.SaveChangesAsync();

                    return new ApiResponseDto
                    {
                        IsSuccess = true,
                        Message = Constants.Success,
                        Response = "Category Saved Successfully."
                    };
                }
                else
                {
                    return new ApiResponseDto
                    {
                        IsSuccess = false,
                        Message = Constants.Failed,
                        Response = "Category Not Found."
                    };
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"AddCategoryAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"AddCategoryAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"AddCategoryAsync Error : {ex.StackTrace}");

                //throw;

                return new ApiResponseDto
                {
                    HasError = true,
                    Message = Constants.Failed,
                    Response = ex.Message
                };
            }
        }

        public async Task<ApiResponseDto> AddCategoriesDetailAsync()
        {

            try
            {
                var categories = await _myIPayEntities.RootCategories
                                                       .Select(x => new { Id = x.Id, Name = x.Name })
                                                       .Union(_myIPayEntities.ParentCategories.Select(x => new { Id = x.Id.ToString(), Name = x.Name }))
                                                       .Union(_myIPayEntities.ChildCategories.Select(x => new { Id = x.Id, Name = x.Name }))
                                                       //.OrderBy(x => x.Id)
                                                       //.Where(x => x.Name.Equals("E-Gift Card", StringComparison.OrdinalIgnoreCase))
                                                       .OrderByDescending(x => x.Id)
                                                       .ToListAsync();

                foreach (var category in categories)
                {
                    try
                    {
                        CategoryDetailDto detailDto = await GetCategoryDetailByIdAsync(category.Id);

                        if (detailDto != null && detailDto.Success)
                        {
                            Utility.WriteLog("GiftCardService", $"AddCategoriesDetailAsync :: Truncate category detail related tables");

                            //Truncate CategoryDetail table to delete all old records.
                            _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[CategoryDetail]");

                            CategoryDetail categoryDetail = new CategoryDetail().CopyFrom(detailDto);

                            categoryDetail.CompleteJson = JsonConvert.SerializeObject(detailDto);

                            _myIPayEntities.CategoryDetails.Add(categoryDetail);

                            if (detailDto.Embedded != null && detailDto.Embedded.Products.Count > 0)
                            {
                                //Truncate Product table to delete all old records.
                                _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[Product]");

                                //Truncate ProductImage table to delete all old records.
                                _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[ProductImage]");

                                foreach (var p in detailDto.Embedded.Products)
                                {
                                    Product product = new Product().CopyFrom(p);
                                    product.Id = p.ProductId;
                                    product.CategoryId = p.CategoryId ?? category.Id;
                                    _myIPayEntities.Products.Add(product);

                                    if (p.Images != null)
                                    {
                                        ProductImage productImage = new ProductImage().CopyFrom(p.Images);
                                        productImage.ProductId = product.Id;

                                        _myIPayEntities.ProductImages.Add(productImage);
                                    }
                                }
                            }

                            //Save changes
                            await _myIPayEntities.SaveChangesAsync();
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }

                }
                return new ApiResponseDto
                {
                    IsSuccess = true,
                    Message = Constants.Success,
                    Response = "Categorie detail saved successfully."
                };

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"AddCategoriesDetailAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"AddCategoriesDetailAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"AddCategoriesDetailAsync Error : {ex.StackTrace}");

                //throw;

                return new ApiResponseDto
                {
                    HasError = true,
                    Message = Constants.Failed,
                    Response = ex.Message
                };
            }

        }


        public async Task<ApiResponseDto> AddProductsDetailAsync()
        {

            try
            {
                var products = await _myIPayEntities.Products
                                                    .Select(x => new { x.Id, Name = x.Name })
                                                    .OrderBy(x => x.Id)
                                                    .ToListAsync();


                Utility.WriteLog("GiftCardService", $"AddProductsDetailAsync :: Truncate product detail related tables");

                //Truncate Product table to delete all old records.
                _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[ProductDetail]");

                //Truncate ProductTheme table to delete all old records.
                _myIPayEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [woohoo].[ProductTheme]");


                foreach (var product in products)
                {
                    try
                    {
                        ProductDetailDto productDetailDto = await GetProductDetailByIdAsync(product.Id);

                        if (productDetailDto != null && productDetailDto.Success)
                        {
                            ProductDetail productDetail = new ProductDetail().CopyFrom(productDetailDto);

                            productDetail.CompleteJson = JsonConvert.SerializeObject(productDetailDto);

                            _myIPayEntities.ProductDetails.Add(productDetail);

                            if (productDetailDto.Themes != null && productDetailDto.Themes.Count > 0)
                            {
                                foreach (KeyValuePair<object, object> keyValue in productDetailDto.Themes)
                                {
                                    JObject jObject = (JObject)keyValue.Value;
                                    var values = jObject.ToObject<Dictionary<string, object>>();
                                    foreach (KeyValuePair<string, object> keyValue1 in values)
                                    {
                                        ProductTheme productTheme = new ProductTheme
                                        {
                                            ProductId = productDetail.Id,
                                            Type = keyValue.Key.ToString(),
                                            Name = keyValue1.Key.ToString(),
                                            Design = keyValue1.Value.ToString()
                                        };


                                        _myIPayEntities.ProductThemes.Add(productTheme);
                                    }
                                }
                            }

                            //Save changes
                            await _myIPayEntities.SaveChangesAsync();
                        }
                    }
                    catch (Exception)
                    {

                        //throw;
                    }

                }
                return new ApiResponseDto
                {
                    IsSuccess = true,
                    Message = Constants.Success,
                    Response = "Products detail saved successfully."
                };

            }
            catch (Exception ex)
            {
                Utility.WriteLog("GiftCard_Error", $"AddCategoriesDetailAsync Error : {ex.Message}");
                Utility.WriteLog("GiftCard_Error", $"AddCategoriesDetailAsync Error : {ex.InnerException}");
                Utility.WriteLog("GiftCard_Error", $"AddCategoriesDetailAsync Error : {ex.StackTrace}");

                //throw;

                return new ApiResponseDto
                {
                    HasError = true,
                    Message = Constants.Failed,
                    Response = ex.Message
                };
            }

        }

        #endregion

        #region Private Methods
        private string GetAuthorizationHeader(string oauthToken, string signature)
        {
            return "OAuth oauth_consumer_key=\"" + _oauthConsumerKey + "\",oauth_token=\"" + oauthToken + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + GetTimestamp() + "\",oauth_nonce=\"" + GetNonce() + "\",oauth_version=\"1.0\",oauth_signature=\"" + signature + "\"";
        }

        private int InserOauthAccessTokenAsync(string oauthToken, string oauthTokenSecret)
        {

            SqlParameter[] parameters = new SqlParameter[]
               {
                    new SqlParameter{
                        ParameterName = "@ConsumerKey",
                        SqlDbType = SqlDbType.NVarChar,
                        Value = _oauthConsumerKey,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@Token",
                        SqlDbType = SqlDbType.NVarChar,
                        Value = oauthToken,
                        Direction = ParameterDirection.Input
                     },
                    new SqlParameter{
                        ParameterName = "@TokenSecret",
                        SqlDbType = SqlDbType.NVarChar,
                        Value = oauthTokenSecret,
                        Direction = ParameterDirection.Input
                     }
               };


            return _myIPayEntities.Database.ExecuteSqlCommand(SqlQueryConstants.SpWoohooOauthTokenCreateOrUpdate, parameters);
        }
        private static string GetTimestamp()
        {
            int epoch = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            return epoch.ToString();
        }

        private string GetNonce()
        {
            Random rand = new Random();
            return rand.Next(999999).ToString();
        }

        #endregion

        #region Customized APIs

        public async Task<ProductDto> GetProductAsync()
        {
            string requestId = SecurityHelper.GenerateClientReferenceId(Constants.GiftCardRequestIdPrefix);

            ProductDto product = await _myIPayEntities.ProductDetails
                                                .Where(x => x.Id == "135")
                                                .Join(_myIPayEntities.ProductImages,
                                                pd => pd.Id,
                                                pi => pi.ProductId,
                                                (pd, pi) => new ProductDto
                                                {
                                                    Id = pd.Id,
                                                    Sku = pd.Sku,
                                                    Name = pd.Name,
                                                    Description = pd.Description,
                                                    ShortDescription = pd.ShortDescription,
                                                    PriceType = pd.PriceType,
                                                    MinCustomPrice = pd.MinCustomPrice,
                                                    MaxCustomPrice = pd.MaxCustomPrice,
                                                    CustomDenominations = pd.CustomDenominations,
                                                    ProductType = pd.ProductType,
                                                    //pd.PayWithAmazonDisable,
                                                    //pd.Images,
                                                    TncMobile = pd.TncMobile,
                                                    TncWeb = pd.TncWeb,
                                                    TncMail = pd.TncMail,
                                                    OrderHandlingCharge = pd.OrderHandlingCharge,
                                                    Image = pi.Image,
                                                    SmallImage = pi.SmallImage,
                                                    BaseImage = pi.BaseImage,
                                                    RequestId = requestId
                                                })
                                                .FirstOrDefaultAsync();

            return product;
        }

        public async Task<List<ProductDto>> GetProductsAsync()
        {

            string requestId = SecurityHelper.GenerateClientReferenceId(Constants.GiftCardRequestIdPrefix);

            List<ProductDto> products = await _myIPayEntities.ProductDetails
                                                .Join(_myIPayEntities.ProductImages,
                                                pd => pd.Id,
                                                pi => pi.ProductId,
                                                (pd, pi) => new ProductDto
                                                {
                                                    Id = pd.Id,
                                                    Sku = pd.Sku,
                                                    Name = pd.Name,
                                                    Description = pd.Description,
                                                    ShortDescription = pd.ShortDescription,
                                                    PriceType = pd.PriceType,
                                                    MinCustomPrice = pd.MinCustomPrice,
                                                    MaxCustomPrice = pd.MaxCustomPrice,
                                                    CustomDenominations = pd.CustomDenominations,
                                                    ProductType = pd.ProductType,
                                                    //pd.PayWithAmazonDisable,
                                                    //pd.Images,
                                                    TncMobile = pd.TncMobile,
                                                    TncWeb = pd.TncWeb,
                                                    TncMail = pd.TncMail,
                                                    OrderHandlingCharge = pd.OrderHandlingCharge,
                                                    Image = pi.Image,
                                                    SmallImage = pi.SmallImage,
                                                    BaseImage = pi.BaseImage,
                                                    RequestId = requestId
                                                })
                                                .OrderBy(x => x.Name)
                                                .ToListAsync();

            return products;
        }




        public async Task<ProductDto> GetProductByIdAsync(string id, string agentId)
        {
            string requestId = SecurityHelper.GenerateClientReferenceId(Constants.TSI + agentId.ToUpper() + Constants.GC);

            ProductDto product = await _myIPayEntities.ProductDetails
                                                .Where(x => x.Id.Equals(id, StringComparison.OrdinalIgnoreCase))
                                                .Join(_myIPayEntities.ProductImages,
                                                pd => pd.Id,
                                                pi => pi.ProductId,
                                                (pd, pi) => new ProductDto
                                                {
                                                    Id = pd.Id,
                                                    Sku = pd.Sku,
                                                    Name = pd.Name,
                                                    Description = pd.Description,
                                                    ShortDescription = pd.ShortDescription,
                                                    PriceType = pd.PriceType,
                                                    MinCustomPrice = pd.MinCustomPrice,
                                                    MaxCustomPrice = pd.MaxCustomPrice,
                                                    CustomDenominations = pd.CustomDenominations,
                                                    ProductType = pd.ProductType,
                                                    //pd.PayWithAmazonDisable,
                                                    //pd.Images,
                                                    TncMobile = pd.TncMobile,
                                                    TncWeb = pd.TncWeb,
                                                    TncMail = pd.TncMail,
                                                    OrderHandlingCharge = pd.OrderHandlingCharge,
                                                    Image = pi.Image,
                                                    SmallImage = pi.SmallImage,
                                                    BaseImage = pi.BaseImage,
                                                    RequestId = requestId
                                                })
                                                .FirstOrDefaultAsync();

            return product;
        }
        #endregion
    }




}
