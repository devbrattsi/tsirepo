﻿using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.QwikCilver;
using MyIPay.Dtos.Areas.QwikCilver.GiftCard;
using MyIPay.Models.Areas.QwikCilver.GiftCard;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.QwikCilver.Interfaces
{
    public interface IGiftCardService
    {
        #region Woohoo APIs
        Task<OAuthTokenDto> GetOAuthTokenAsync();
        Task<VerifierDto> GetOauthVerifierAsync(string oauthToken);
        Task<AccessTokenDto> GetOauthAccessTokenAsync(string oauthToken, string oauthTokenSecret, string oauthVerifier);

        Task<SettingDto> GetSttingsAsync();
        Task<CategoryDto> GetCategoryAsync();
        Task<CategoryDetailDto> GetCategoryDetailByIdAsync(string id);
        Task<ProductDetailDto> GetProductDetailByIdAsync(string id);
        Task<SpendDto> SpendAsync(SpendModel model);
        Task<StatusDto> GetOrderStatusAsync(string refNo);
        Task<ResendDto> ResendAsync(string refNo);
        #endregion


        #region Insert APIs
        Task<ApiResponseDto> AddOauthAccessTokenAsync();
        Task<ApiResponseDto> UpdateOauthAccessTokenAsync();

        Task<ApiResponseDto> AddCategoryAsync();

        Task<ApiResponseDto> AddCategoriesDetailAsync();

        Task<ApiResponseDto> AddProductsDetailAsync();
        #endregion


        #region Customized APIs

        Task<ProductDto> GetProductAsync();

        Task<List<ProductDto>> GetProductsAsync();

        Task<ProductDto> GetProductByIdAsync(string id, string agentId);

        #endregion
    }
}
