﻿using CCA.Util;
using MyIPay.Dtos.Areas.Avenue;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Avenue;
using MyIPay.Services.Areas.Avenue.Interface;
using MyIPay.Services.Areas.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MyIPay.Services.Areas.Avenue
{
    public class AvenueService : IAvenueService
    {
        private readonly IApiManagerService _apiService;
        private readonly string _baseUrl = AppSettings.AvenueBaseUrl;

        public AvenueService(IApiManagerService apiService)
        {
            _apiService = apiService;
        }

        /// <summary>
        /// Consume api which is provided by TSI (Avenue)  to get list of all Billers
        /// </summary>
        /// <returns>ResponseMessage object</returns>
        public async Task<ResponseMessage> GetAllBillerList()
        {
            string xmlRequest = string.Empty;
            string result = string.Empty;
            string url = string.Empty;
            string xmlResponseDecrypt = string.Empty;

            ResponseMessage responseObj = new ResponseMessage();
            CCACrypto c = new CCACrypto();
            try
            {
                string xmlData = "<billerInfoRequest></billerInfoRequest>";
                xmlData = c.Encrypt(xmlData, Constants.Key);

                string requestId = GetRequestId();
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"Request ID :- {requestId}");

                url = Constants.MDMAvenueServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + requestId + "&encRequest=" + xmlData + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                var serializer = new XmlSerializer(typeof(BillerInfoResponse));
                var root = (BillerInfoResponse)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Biller Info Found";
                responseObj.Response = root;
            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching Biller Info. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Format Exception :- { ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching Biller Info. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Exception :- { ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetAllBillerList Exception :- {ex.StackTrace}");
            }

            return responseObj;
        }

        /// <summary>
        /// Consume api which is provided by TSI (Avenue)  for fetching bill
        /// </summary>
        /// <param name="obj">BillFetchRequest Type</param>
        /// <returns>json string</returns>
        public async Task<ResponseMessage> FetchBill(BillFetchRequest model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;
            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();

            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();

                var doc = XDocument.Parse(xmlData);
                var nodeToRemove = doc.Descendants("RequestID");
                nodeToRemove.Remove();
                xmlData = doc.ToString();

                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill XML Request:- {xmlData}");

                CCACrypto c = new CCACrypto();
                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Request Id:- { model.RequestID}");

                url = Constants.BillFetchRequestAvenueServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + model.RequestID + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(BillFetchResponse));
                var root = (BillFetchResponse)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Bill details sucessfully fetched";
                responseObj.Response = root;
            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching bill details. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching bill details. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"FetchBill Exception :- {ex.StackTrace}");
            }

            return responseObj;
        }

        /// <summary>
        /// Consume api which is provided by TSI (Avenue)  for Bill Payment
        /// </summary>
        /// <param name="obj">BillPaymentRequest Type</param>
        /// <returns>json string</returns>
        public async Task<ResponseMessage> PayBill(BillPaymentRequest model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;

            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();
            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();
                var doc = XDocument.Parse(xmlData);
                var nodeToRemove = doc.Descendants("RequestID");
                nodeToRemove.Remove();
                xmlData = doc.ToString();


                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill XML Request:- {xmlData}");

                CCACrypto c = new CCACrypto();
                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Request ID :- {model.RequestID}");

                url = Constants.BillPayRequestAvenueServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + model.RequestID + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(ExtBillPayResponse));
                var root = (ExtBillPayResponse)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Bill paid successfully";
                responseObj.Response = root;

            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while paying bill. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while paying bill. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"PayBill Exception :- {ex.StackTrace}");
            }

            return responseObj;
        }

        /// <summary>
        /// Consume api which is provided by TSI (Avenue)  for Transaction Status
        /// </summary>
        /// <param name="obj">TransactionStatusReq Type</param>
        /// <returns>json string</returns>
        public async Task<ResponseMessage> GetTransactionStatus(TransactionStatusReq model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;

            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();
            CCACrypto c = new CCACrypto();
            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();
                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus XML Request:- {xmlData}");

                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Request ID :- {model.RequestID}");

                url = Constants.TransactionStatusAvenueServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + model.RequestID + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(TransactionStatusResp));
                var root = (TransactionStatusResp)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Transaction status found.";
                responseObj.Response = root;

            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching transaction status. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching transaction status. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"GetTransactionStatus Exception :- {ex.StackTrace}");
            }

            return responseObj;
        }

        /// <summary>
        /// Consume api which is provided by TSI (Avenue)  for Register Complaint
        /// </summary>
        /// <param name="obj">ComplaintRegistrationReq Type</param>
        /// <returns>json string</returns>
        public async Task<ResponseMessage> RegisterComplaint(ComplaintRegistrationReq model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;

            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();
            CCACrypto c = new CCACrypto();
            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();
                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint XML Request:- {xmlData}");

                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Request ID :- {model.RequestID}");

                url = Constants.ComplaintsRegisterAvenueServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + model.RequestID + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(ComplaintRegistrationResp));
                var root = (ComplaintRegistrationResp)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Complaint registed successfully";
                responseObj.Response = root;

            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occred while registering complaint. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occred while registering complaint. Exception:-" + ex.Message;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"RegisterComplaint Exception :- {ex.StackTrace}");
            }
            return responseObj;
        }

        /// <summary>
        /// Consume api which is provided by TSI (Avenue)  to get Complaint Status
        /// </summary>
        /// <param name="obj">ComplaintTrackingReq Type</param>
        /// <returns>json string</returns>
        public async Task<ResponseMessage> ComplaintStatus(ComplaintTrackingReq model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;

            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();
            CCACrypto c = new CCACrypto();
            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());

                tw = new XmlTextWriter(sw);
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();
                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus XML Request:- {xmlData}");

                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);
          
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Request ID :- {model.RequestID}");

                url = Constants.ComplaintsStatusAvenueServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + model.RequestID + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(ComplaintTrackingResp));
                var root = (ComplaintTrackingResp)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Complaint status found";
                responseObj.Response = root;
            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching complaint status. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching complaint status. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"ComplaintStatus Exception :- {ex.StackTrace}");
            }

            return responseObj;
        }

        public async Task<ResponseMessage> DepositEnquiryWithAgent(DepositDetailsRequestWithAgent model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;
            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();
            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();
                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent XML Request:- {xmlData}");

                CCACrypto c = new CCACrypto();
                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);
                string requestId = GetRequestId();
              
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Request ID:- {requestId}");
               
                url = Constants.DepositEnquiryServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + requestId + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(DepositEnquiryResponse));
                var root = (DepositEnquiryResponse)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Deposit Response sucessfully fetched";
                responseObj.Response = root;
            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching Deposit Response. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching Deposit Response. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithAgent Exception :- {ex.StackTrace}");
            }
          
            return responseObj;
        }

        public async Task<ResponseMessage> DepositEnquiryWithoutAgent(DepositDetailsRequestWithoutAgent model)
        {
            string xmlData = string.Empty;
            string xmlRequestEncrypt = string.Empty;
            string xmlResponseDecrypt = string.Empty;
            string url = string.Empty;
            ResponseMessage responseObj = new ResponseMessage();
            StringWriter sw = new StringWriter();
            try
            {
                XmlTextWriter tw = null;
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model, emptyNamepsaces);
                xmlData = sw.ToString();
                xmlData = xmlData.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent XML Request:- {xmlData}");

                CCACrypto c = new CCACrypto();
                xmlRequestEncrypt = c.Encrypt(xmlData, Constants.Key);
                string requestId = GetRequestId();
              
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Request ID:- {requestId}");

                url = Constants.DepositEnquiryServiceURL + "accessCode=" + Constants.AccessCode + "&requestId=" + requestId + "&encRequest=" + xmlRequestEncrypt + "&ver=" + Constants.Version + "&instituteId=" + Constants.InstituteId;

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent URL Request:- {_baseUrl}{url}");

                var response = await _apiService.PostAsync(_baseUrl, url, new System.Net.Http.StringContent(""), new System.Net.Http.HttpClient());
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();

                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Response :- {responseString}");

                StringReader sr = new StringReader(responseString);
                xmlResponseDecrypt = c.Decrypt(responseString, Constants.Key);
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Decrypt Response :- {xmlResponseDecrypt}");

                var reader = new StringReader(xmlResponseDecrypt);
                serializer = new XmlSerializer(typeof(DepositEnquiryResponse));
                var root = (DepositEnquiryResponse)serializer.Deserialize(reader);
                responseObj.IsSuceess = true;
                responseObj.Message = "Deposit Response sucessfully fetched";
                responseObj.Response = root;
            }
            catch (FormatException ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching Deposit Response. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Format Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Format Exception :- {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                responseObj.IsSuceess = false;
                responseObj.Message = "Error occured while fetching Deposit Response. Exception:-" + ex.Message;
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Exception :- {ex.Message}");
                Helpers.Utility.SaveRequestAndResponseLog("AvenueService", $"DepositEnquiryWithoutAgent Exception :- {ex.StackTrace}");
            }
           
            return responseObj;
        }

        /// <summary>
        /// Method generate 35 Alphanumeric characters unique ID for each request
        /// </summary>
        /// <returns>35 Alphanumeric characters unique ID</returns>
        private string GetRequestId()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 35).Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
