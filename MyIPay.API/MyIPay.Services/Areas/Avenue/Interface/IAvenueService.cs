﻿using MyIPay.Dtos.Areas.Avenue;
using MyIPay.Models.Areas.Avenue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Avenue.Interface
{
    public interface IAvenueService
    {
        Task<ResponseMessage> GetAllBillerList();
        Task<ResponseMessage> FetchBill(BillFetchRequest model);
        Task<ResponseMessage> PayBill(BillPaymentRequest model);
        Task<ResponseMessage> GetTransactionStatus(TransactionStatusReq model);
        Task<ResponseMessage> RegisterComplaint(ComplaintRegistrationReq model);
        Task<ResponseMessage> ComplaintStatus(ComplaintTrackingReq model);
        Task<ResponseMessage> DepositEnquiryWithAgent(DepositDetailsRequestWithAgent model);
        Task<ResponseMessage> DepositEnquiryWithoutAgent(DepositDetailsRequestWithoutAgent model);
    }
}
