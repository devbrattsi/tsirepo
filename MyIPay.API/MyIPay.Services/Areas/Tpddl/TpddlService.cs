﻿using MyIPay.DataLayer.EF;
using MyIPay.Dtos.Areas.Common;
using MyIPay.Dtos.Areas.Tpddl;
using MyIPay.Helpers;
using MyIPay.Models.Areas.Tpddl;
using MyIPay.Services.Areas.Tpddl.Interfaces;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MyIPay.Services.Areas.Tpddl
{
    public class TpddlService : ITpddlService
    {
        private readonly MyIPayEntities _myIPayEntities;
        private readonly string _tpddlEReceiptApiUrl = ConfigurationManager.AppSettings["TpddlEReceiptApiUrl"].ToString();

        public TpddlService(MyIPayEntities myIPayEntities)
        {
            _myIPayEntities = myIPayEntities;
        }

        //public async Task<ResponseMessageDto> GetBillDetailAsync(string caOrKaNumber)
        //{

        //    if (caOrKaNumber.StartsWith("6", StringComparison.OrdinalIgnoreCase))
        //    {
        //        caOrKaNumber = $"0{caOrKaNumber}";
        //    }

        //    var consumerMaster = await _myIPayEntities.PG_T_CNSMR_MST
        //                                              .FirstOrDefaultAsync(x => x.CNTRCT_ACCNT.Equals(caOrKaNumber, StringComparison.OrdinalIgnoreCase)
        //                                                                   || x.KNO.Equals(caOrKaNumber, StringComparison.OrdinalIgnoreCase));

        //    if (consumerMaster != null)
        //    {
        //        bool IsNotChequePaymentAllowed = await _myIPayEntities.PG_T_CHQ_BNCD_DTLS.AnyAsync(x => x.CA_no == caOrKaNumber || x.KNO == caOrKaNumber);

        //        return new ResponseMessageDto
        //        {
        //            IsSuccess = true,
        //            Message = "Record found.",
        //            Response = new GetBillDetailDto
        //            {
        //                ConsumerNumber = consumerMaster.CNTRCT_ACCNT ?? string.Empty,
        //                KNumber = consumerMaster.KNO ?? string.Empty,
        //                ConsumerName = $"{consumerMaster.CNSMR_NM} {consumerMaster.MDDL_NM} {consumerMaster.LST_NM}",
        //                BillMonthYear = $"{consumerMaster.BLL_MNTH}/{consumerMaster.BLL_YR}",
        //                Address = $"{consumerMaster.ADD1} { consumerMaster.ADD2}",
        //                BillDate = consumerMaster.BLL_DT,
        //                DueDate = consumerMaster.DUE_DT,
        //                CurrentDemand = consumerMaster.CRRNT_DMND,
        //                BillNumbre = consumerMaster.BLL_NO,
        //                AmountPayableRounded = consumerMaster.AMNT_PYBL_RNDD,
        //                ChequePaymentAllowed = IsNotChequePaymentAllowed ? "N" : "Y"
        //            }
        //        };
        //    }

        //    return new ResponseMessageDto()
        //    {
        //        IsSuccess = false,
        //        Message = "Record not found."
        //    };
        //}

        public async Task<ResponseMessageDto> GetBillDetailAsync(string caOrKaNumber)
        {

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(AppSettings.TsiTpddlApiBaseUrl);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", SecurityHelper.Base64Encode($"{AppSettings.TsiTpddlApiUsername}:{AppSettings.TsiTpddlApiPassword}"));

                EnableHost.EnableTrustedHosts();

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");

                string url = $"api/TPDDL/GetBillInfoAsync?terminalId={AppSettings.TpddlCounterNo}&ConsumerNo={caOrKaNumber}";

                //Getting client API response
                var response = await client.GetAsync(url);

                string result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return new ResponseMessageDto
                    {
                        IsSuccess = false,
                        Code = 1,
                        Message = "Unable to get response from server.",
                        Response = response.Content.ToString()
                    };
                }

                GetBillInfoDto dto = JsonConvert.DeserializeObject<GetBillInfoDto>(result);

                return new ResponseMessageDto
                {
                    IsSuccess = dto.responseCode == 100 ? true : false,
                    Code = dto.responseCode == 100 ? 0 : 1,
                    Message = dto?.responseMessage,
                    Response = new GetBillDetailDto
                    {
                        KNumber = dto?.kNo,
                        ConsumerNumber = dto?.caNo,
                        ConsumerName = $"{dto?.consumerName} {dto?.middleName} {dto?.lastName}",
                        BillNumbre = dto?.billNumber,
                        BillMonthYear = $"{dto?.billMonth}/{dto?.billYear}",
                        Address = $"{dto?.address1} { dto?.address2}",
                        BillDate = dto.billDate,
                        DueDate = dto?.dueDate,
                        CurrentDemand = Convert.ToDecimal(dto?.currentDemand),
                        AmountPayableRounded = Convert.ToDecimal(dto?.amountPayable),
                        ChequePaymentAllowed = "N",
                        //OrderId
                        //ReceiptId
                    }
                };
            }

        }

        public async Task<ResponseMessageDto> TpddlEReceiptAsync(TpddlEReceiptModel model)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(_tpddlEReceiptApiUrl);

                client.DefaultRequestHeaders.Clear();
                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "text/xml");

                string url = $"SendEReceiptlink?cano={model.ConsumerNumber}&rcpt_no={model.ReceiptNumber}&contactDetails={model.ContactDetails}&CashiersName={model.CashierName}&cntr_no={model.CounterNumber}&Mode={model.Mode}&Amount={model.Amount}";
                //Getting client API response
                var response = await client.GetAsync(url);

                string responseString = await response.Content.ReadAsStringAsync();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(responseString);
                var selectors = doc.LastChild.InnerText;

                return new ResponseMessageDto
                {
                    IsSuccess = true,
                    Message = "",
                    Response = new TpddlEReceiptDto
                    {
                        ResponseMessage = selectors,
                        ConsumerNumber = model.ConsumerNumber,
                        CounterNumber = model.CounterNumber,
                        ReceiptNumber = model.ReceiptNumber
                    }

                };
            }
        }
        public async Task<ResponseMessageDto> GetBillDetailsAsync(string caOrKaNumber)
        {
            string requestBody = string.Empty;
            string md5String = SecurityHelper.MD5Hash(caOrKaNumber);
            char[] key = md5String.Substring(md5String.Length - 3, 1).ToCharArray();

            requestBody += "<soap:Envelope xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns:xsd=\'http://www.w3.org/2001/XMLSchema\' xmlns:soap=\'http://schemas.xmlsoap.org/soap/envelope/\'>";
            requestBody += "<soap:Body><CA_Details xmlns=\'http://tempuri.org/\'>";
            requestBody += $"<marchentid>{Encrypt(AppSettings.TpddlMerchentId, key[0])}</marchentid> ";
            requestBody += $"<username>{Encrypt(AppSettings.TpddlUsername, key[0])}</username>";
            requestBody += $"<password>{Encrypt(AppSettings.TpddlPassword, key[0])}</password>";
            requestBody += $"<cano>{Encrypt(caOrKaNumber, key[0])}</cano>";
            requestBody += $"<counterno>{Encrypt(AppSettings.TpddlCounterNo, key[0])}</counterno>";
            requestBody += $"<securestring>{md5String}</securestring>";
            requestBody += "</CA_Details></soap:Body></soap:Envelope>";

            string response = await CallingTpddlApi(requestBody, "CA_Details");
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(response);
                string decryptData = Decrypt(xml.LastChild.InnerText, key[0]);
                string[] responseData = decryptData.Split('#');

                if ((Convert.ToInt64(responseData[0]) >= 101 && Convert.ToInt64(responseData[0]) <= 108) || (Convert.ToInt64(responseData[0]) >= 110 && Convert.ToInt64(responseData[0]) <= 200))
                {
                    return new ResponseMessageDto
                    {
                        IsSuccess = false,
                        Code = Convert.ToInt32(responseData[0]),
                        Message = responseData[1]
                    };
                }
                else
                {

                    var consumerData = await _myIPayEntities.PG_T_CNSMR_MST.FirstOrDefaultAsync(f => f.CNTRCT_ACCNT.Contains(caOrKaNumber) || f.KNO.Contains(caOrKaNumber));

                    return new ResponseMessageDto
                    {
                        IsSuccess = true,
                        Message = "Record found.",
                        Code = Convert.ToInt32(responseData[8]),
                        Response = new GetBillDetailDto
                        {
                            ConsumerNumber = responseData[0],
                            KNumber = consumerData?.KNO,
                            ConsumerName = $"{consumerData?.CNSMR_NM} {consumerData?.MDDL_NM} {consumerData?.LST_NM}",
                            BillMonthYear = responseData[7],
                            Address = $"{consumerData?.ADD1} {consumerData?.ADD2}",
                            BillDate = new DateTime(Convert.ToInt32(responseData[6].Substring(0, 4)), Convert.ToInt32(responseData[6].Substring(4, 2)), Convert.ToInt32(responseData[6].Substring(6, 2))),
                            DueDate = new DateTime(Convert.ToInt32(responseData[5].Substring(0, 4)), Convert.ToInt32(responseData[5].Substring(4, 2)), Convert.ToInt32(responseData[5].Substring(6, 2))),
                            CurrentDemand = null,
                            BillNumbre = responseData[2],
                            AmountPayableRounded = Convert.ToDecimal(responseData[3]),
                            ChequePaymentAllowed = "Y",
                            OrderId = responseData[1],
                            ReceiptId = responseData[4]
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"GetBillDetailsAsync Exception :-{ex.Message}");
                return new ResponseMessageDto
                {
                    IsSuccess = false,
                    Code = -111,
                    Message = $"Error : {ex.Message}"
                };
            }
        }
        public async Task<ResponseMessageDto> PayBillAsync(TpddlPayModel model)
        {
            model.TransactionStatus = "00";
            model.TransactionMessage = "Success";
            // model.BankCode = AppSettings.TpddlBankCode;
            model.BankName = AppSettings.TpddlBankName;

            Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"PayBillAsync Request :- {model.AgentId} --> {JsonConvert.SerializeObject(model)}");
            string requestBody = string.Empty;
            try
            {
                string md5String = SecurityHelper.MD5Hash(model.CaNumber);
                char[] key = md5String.Substring(md5String.Length - 3, 1).ToCharArray();

                requestBody += "<soap:Envelope xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns:xsd=\'http://www.w3.org/2001/XMLSchema\' xmlns:soap=\'http://schemas.xmlsoap.org/soap/envelope/\'>";
                requestBody += "<soap:Body>";
                requestBody += "<Update_TSIDetails xmlns=\'http://tempuri.org/\'>";
                requestBody += $"<marchentid>{Encrypt(AppSettings.TpddlMerchentId, key[0])}</marchentid>";
                requestBody += $"<username>{Encrypt(AppSettings.TpddlUsername, key[0])}</username>";
                requestBody += $"<password>{Encrypt(AppSettings.TpddlPassword, key[0])}</password>";
                requestBody += $"<cano>{Encrypt(model.CaNumber, key[0])}</cano>";
                requestBody += $"<rcptno>{Encrypt(model.ReceiptNo, key[0])}</rcptno>";
                requestBody += $"<amount>{Encrypt(model.Amount, key[0])}</amount>";
                requestBody += $"<txnid>{Encrypt(model.TransactionId, key[0])}</txnid>";
                requestBody += $"<bankname>{Encrypt(model.BankName, key[0])}</bankname>";
                requestBody += $"<invoiceno>{Encrypt(model.InvoiceNo, key[0])}</invoiceno>";
                requestBody += $"<txnstatus>{Encrypt(model.TransactionStatus, key[0])}</txnstatus>";
                requestBody += $"<txtmsg>{Encrypt(model.TransactionMessage, key[0])}</txtmsg>";
                requestBody += $"<ipaddress>{Encrypt(model.IpAddress, key[0])}</ipaddress>";
                requestBody += $"<mop>{Encrypt(model.PaymentMode, key[0])}</mop>";
                requestBody += $"<counterno>{Encrypt(AppSettings.TpddlCounterNo, key[0])}</counterno>";
                requestBody += $"<securestring>{md5String}</securestring>";
                requestBody += "</Update_TSIDetails></soap:Body></soap:Envelope>";

                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"PayBillAsync Request :- {model.AgentId} --> {requestBody}");

                string response = await CallingTpddlApi(requestBody, "Update_TSIDetails");
                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"PayBillAsync Response :- {model.AgentId} --> {response}");

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(response);
                string decryptData = Decrypt(xml.LastChild.InnerText, key[0]);
                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"PayBillAsync Decrypt Response :- {model.AgentId} --> {decryptData}");
                string[] responseData = decryptData.Split('#');

                if (Convert.ToInt64(responseData[0]) == 110)
                {
                    return new ResponseMessageDto
                    {
                        IsSuccess = true,
                        Code = Convert.ToInt32(responseData[0]),
                        Message = responseData[1]
                    };
                }
                else
                {
                    return new ResponseMessageDto
                    {
                        IsSuccess = false,
                        Code = Convert.ToInt32(responseData[0]),
                        Message = responseData[1]
                    };
                }
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"PayBillAsync Exception :- {model.AgentId} --> {ex.Message}");
                return new ResponseMessageDto
                {
                    IsSuccess = false,
                    Code = -111,
                    Message = $"Error : {ex.Message}"
                };
            }
        }
        public async Task<ResponseMessageDto> TransactionStatus(TpddlTransactionStatusModel model)
        {
            try
            {
                string requestBody = string.Empty;
                string md5String = SecurityHelper.MD5Hash(model.CaNumber);
                char[] key = md5String.Substring(md5String.Length - 3, 1).ToCharArray();

                requestBody += "<soap:Envelope xmlns:xsi=\'http://www.w3.org/2001/XMLSchema-instance\' xmlns:xsd=\'http://www.w3.org/2001/XMLSchema\' xmlns:soap=\'http://schemas.xmlsoap.org/soap/envelope/\'>";
                requestBody += "<soap:Body><get_status xmlns=\'http://tempuri.org/\'>";
                requestBody += $"<rcptno>{model.ReceiptNo}</rcptno>";
                requestBody += $"<cano>{model.CaNumber}</cano>";
                requestBody += $"</get_status></soap:Body></soap:Envelope>";

                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"TransactionStatus Request :- {requestBody}");

                string response = await CallingTpddlApi(requestBody, "get_status");
                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"TransactionStatus Response :-  {response}");

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(response);
                //string decryptData = Decrypt(xml.LastChild.InnerText, key[0]);
                string data = xml.LastChild.InnerText;
                //  Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"TransactionStatus Decrypt Response :-  {data}");
                //string[] responseData = decryptData.Split('#');

                return new ResponseMessageDto
                {
                    IsSuccess = true,
                    //Code = Convert.ToInt32(responseData[0]),
                    Message = data
                };

                //if (Convert.ToInt64(responseData[0]) == 110)
                //{
                //    return new ResponseMessageDto
                //    {
                //        IsSuccess = true,
                //        Code = Convert.ToInt32(responseData[0]),
                //        Message = responseData[1]
                //    };
                //}
                //else
                //{
                //    return new ResponseMessageDto
                //    {
                //        IsSuccess = false,
                //        Code = Convert.ToInt32(responseData[0]),
                //        Message = responseData[1]
                //    };
                //}
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("TpddlService", $"TransactionStatus Exception :- {model.CaNumber} {model.ReceiptNo} --> {ex.Message}");
                return new ResponseMessageDto
                {
                    IsSuccess = false,
                    Code = -111,
                    Message = $"Error : {ex.Message}"
                };
            }
        }
        private async Task<string> CallingTpddlApi(string xmlRequest, string method)
        {
            EnableHost.EnableTrustedHosts();
            string result = string.Empty;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(AppSettings.TpddlBaseUrl);
                req.Method = "POST";
                req.ContentType = "text/xml;charset=UTF-8";
                req.ContentLength = xmlRequest.Length;
                string str = $"http://tempuri.org/{method}";
                req.Headers.Set("SOAPAction", '\"' + str + '\"');
                req.Timeout = 5 * 60 * 10000;
                req.UserAgent = "Jakarta Commons-HttpClient/3.1";

                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    streamWriter.Write(xmlRequest, 0, xmlRequest.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                using (var streamReader = new StreamReader(resp.GetResponseStream()))
                {
                    result = await streamReader.ReadToEndAsync();
                }

                return result;
            }
            catch (WebException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                result = "Exception: " + ex.Message;
                return result;
            }
        }
        private string Encrypt(string data, int key)
        {
            char[] ch = data.ToCharArray();
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < ch.Length; i++)
            {
                int val = ch[i];
                int newVal = val + key;
                stringBuilder.Append(newVal.ToString("x"));
            }

            return stringBuilder.ToString();
        }
        private string Decrypt(string hexaData, int key)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                for (int i = 0; i < hexaData.Length - 1; i += 2)
                {
                    string output = hexaData.Substring(i, 2);
                    int ascii = Convert.ToInt32(output, 16);
                    ascii = ascii - key;
                    sb.Append((char)ascii);
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return sb.ToString();
        }
    }
}
