﻿using MyIPay.Dtos.Areas.Common;
using MyIPay.Models.Areas.Tpddl;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.Tpddl.Interfaces
{
    public interface ITpddlService
    {
        Task<ResponseMessageDto> GetBillDetailAsync(string caOrKaNumber);
        Task<ResponseMessageDto> TpddlEReceiptAsync(TpddlEReceiptModel model);
        Task<ResponseMessageDto> GetBillDetailsAsync(string caOrKaNumber);
        Task<ResponseMessageDto> PayBillAsync(TpddlPayModel model);
        Task<ResponseMessageDto> TransactionStatus(TpddlTransactionStatusModel model);
    }
}
