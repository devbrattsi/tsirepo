﻿using MyIPay.Dtos.Areas.PrepaidRecharge;
using MyIPay.Helpers;
using MyIPay.Models.Areas.PrepaidRecharge;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.PrepaidRecharge.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.PrepaidRecharge
{
    public class PrepaidRechargeService : IPrepaidRechargeService
    {
        private readonly IApiManagerService _apiService;
        private readonly string _baseUrl = AppSettings.BaseUrl;

        private readonly string _walnutBaseUrl = string.Empty;
        private readonly string _walnutToken = string.Empty;
        private readonly string _walnutUsername = string.Empty;
        private readonly string _walnutPassword = string.Empty;
        private readonly string _walnutOtherBaseUrl = string.Empty;
        private readonly string _walnutOtherToken = string.Empty;
        private readonly string _walnutOtherUsername = string.Empty;
        private readonly string _walnutOtherPassword = string.Empty;

        public PrepaidRechargeService(IApiManagerService apiService)
        {
            _apiService = apiService;
            _walnutBaseUrl = AppSettings.WalnutMode == "L" ? AppSettings.WalnutLiveBaseUrl : AppSettings.WalnutUatBaseUrl;
            _walnutToken = AppSettings.WalnutMode == "L" ? AppSettings.WalnutLiveToken : AppSettings.WalnutUatToken;
            _walnutUsername = AppSettings.WalnutMode == "L" ? AppSettings.WalnutLiveUsername : AppSettings.WalnutUatUsername;
            _walnutPassword = AppSettings.WalnutMode == "L" ? AppSettings.WalnutLivePassword : AppSettings.WalnutUatPassword;
            _walnutOtherBaseUrl = AppSettings.WalnutOtherBaseUrl;
            _walnutOtherToken = AppSettings.WalnutOtherToken;
            _walnutOtherUsername = AppSettings.WalnutOtherUsername;
            _walnutOtherPassword = AppSettings.WalnutOtherPassword;
        }

        #region Go Processing
        public async Task<List<ServiceListDto>> ServiceList()
        {
            try
            {
                string url = "serviceData.go";
                var content = new StringContent(JsonConvert.SerializeObject(new CommonModel()), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceList Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceList Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<List<ServiceListDto>>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceList Exception :- {ex.Message}");
                return new List<ServiceListDto>();
            }
        }

        public async Task<List<Dtos.Areas.PrepaidRecharge.CircleInfoDto>> CircleInfoList()
        {
            try
            {
                string url = "serviceData.go";
                var content = new StringContent(JsonConvert.SerializeObject(new Models.Areas.PrepaidRecharge.CircleInfoModel()), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"CircleInfoList Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"CircleInfoList Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<List<Dtos.Areas.PrepaidRecharge.CircleInfoDto>>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"CircleInfoList Exception :- {ex.Message}");
                return new List<Dtos.Areas.PrepaidRecharge.CircleInfoDto>();
            }
        }

        public async Task<List<ServiceDataDto>> OperatorList(int id)
        {
            try
            {
                string url = "serviceData.go";
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"OperatorList Request :- {id}");

                var content = new StringContent(JsonConvert.SerializeObject(new OperatorListRequestModel { ServiceFamily = id }), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"OperatorList Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"OperatorList Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<List<ServiceDataDto>>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"OperatorList Exception :- {ex.Message}");
                return new List<ServiceDataDto>();
            }
        }

        public async Task<ServiceDto> ServiceRequest(ServiceModel model)
        {
            try
            {
                string url = "serviceTrans.go";
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceRequest Request :- {JsonConvert.SerializeObject(model)}");
                switch (model.ServiceFamily)
                {
                    case 1:
                        model.ApiKey = AppSettings.MobileApiKey;
                        break;
                    case 2:
                        model.ApiKey = AppSettings.DthApiKey;
                        break;
                    case 3:
                        model.ApiKey = AppSettings.DataCardApiKey;
                        break;
                }


                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceRequest Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceRequest Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<ServiceDto>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"ServiceRequest Exception :- {ex.Message}");
                return null;
            }
        }

        public async Task<BalanceCheckDto> BalanceCheck()
        {
            try
            {
                string url = "billing.go";

                var content = new StringContent(JsonConvert.SerializeObject(new BalanceInfoRequestModel()), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"BalanceCheck Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"BalanceCheck Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<BalanceCheckDto>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"BalanceCheck Exception :- {ex.Message}");
                return null;
            }
        }

        public async Task<MsisdnInfoDto> FindMsisdnInfo(int serviceFamily, long number)
        {
            try
            {
                string url = "findMsisdnInfo.go";
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"FindMsisdnInfo Request :- {serviceFamily}-{number}");
                var content = new StringContent(JsonConvert.SerializeObject(new MsisdnInfoRequestModel { ServiceFamily = serviceFamily, Number = number }), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"FindMsisdnInfo Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"FindMsisdnInfo Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<MsisdnInfoDto>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"FindMsisdnInfo Exception :- {ex.Message}");
                return new MsisdnInfoDto();
            }
        }

        public async Task<RechargePlanDto> RechargePlan(RechargePlanModel model)
        {
            try
            {
                string url = "getRechargePlan.go";

                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"RechargePlan Request :- {JsonConvert.SerializeObject(model)}");

                switch (model.ServiceFamily)
                {
                    case "1":
                        model.ApiKey = AppSettings.MobileApiKey;
                        break;
                    case "2":
                        model.ApiKey = AppSettings.DthApiKey;
                        break;
                    case "3":
                        model.ApiKey = AppSettings.DataCardApiKey;
                        break;
                }

                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"RechargePlan Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"RechargePlan Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<RechargePlanDto>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"RechargePlan Exception :- {ex.Message}");
                return null;
            }
        }

        public async Task<TransactionStatusDto> TransactionStatus(TransactionStatusModel model)
        {
            string responseString = null;
            try
            {
                string url = "transStatus.go";
                string json = JsonConvert.SerializeObject(model);
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Transaction Request :- {json}");

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Transaction Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }

                //Reading response as string
                responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Transaction Response :- {responseString}");

                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<List<TransactionStatusDto>>(responseString);
                return result.FirstOrDefault();

            }
            catch (JsonSerializationException ex)
            {
                var result = JsonConvert.DeserializeObject<TransactionStatusDto>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $" Transaction Exception  :- {ex.Message}");
                return new TransactionStatusDto();
            }
        }

        public async Task<List<TransactionStatusDto>> BulkTransactionStatuses(BulkTransactionStatusesModel model)
        {
            try
            {
                string url = "transStatus.go";
                string json = JsonConvert.SerializeObject(model);
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Bulk Transaction Request :- {json}");

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiService.PostAsync(_baseUrl, url, content);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Bulk Transaction Response Exception :- {response.ReasonPhrase}");
                    throw new Exception(response.ReasonPhrase);
                }
                //Reading response as string
                string responseString = await response.Content.ReadAsStringAsync();
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Bulk Transaction Response :- {responseString}");
                //Deserializing response content string
                var result = JsonConvert.DeserializeObject<List<TransactionStatusDto>>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                Helpers.Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"Bulk Transaction Exception :- {ex.Message}");
                return new List<TransactionStatusDto>();
            }
        }


        #endregion

        #region Walnut
        public async Task<WalnutMsisdnInfoDto> WalnutFindMsisdnInfo(string clientReferenceId, string number)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutFindMsisdnInfo Request :- {clientReferenceId},{number}");
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _walnutOtherToken);

                string url = $"walnut/api/mnpDetails.php?mobile={number}&trans_id={clientReferenceId}&user={_walnutOtherUsername}&passwd={_walnutOtherPassword}";
                var response = await _apiService.GetAsync(_walnutOtherBaseUrl, url, client);
                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutFindMsisdnInfo Response :- {responseString}");
                return JsonConvert.DeserializeObject<WalnutMsisdnInfoDto>(responseString);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutFindMsisdnInfo Exception :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutFindMsisdnInfo Exception :- {ex.StackTrace}");
                return new WalnutMsisdnInfoDto();
            }
        }

        public async Task<WalnutRechargePlanDto> WalnutRechargePlan(WalnutRechargePlanModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRechargePlan Request :- {JsonConvert.SerializeObject(model)}");
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _walnutOtherToken);

                string url = $"walnut/api/getpricepointinfo.php?trans_id={model.ClientReferenceId}&user={_walnutOtherUsername}&passwd={_walnutOtherPassword}&operator_id={model.OperatorId}&circle_id={model.CircleId}&type={model.RechargeType}";

                var response = await _apiService.GetAsync(_walnutOtherBaseUrl, url, client);
                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRechargePlan Response :- {responseString}");
                return JsonConvert.DeserializeObject<WalnutRechargePlanDto>(responseString);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRechargePlan Exception :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRechargePlan Exception :- {ex.StackTrace}");
                return new WalnutRechargePlanDto();
            }
        }

        public async Task<WalnutRechargeDto> WalnutRecharge(WalnutRechargeModel model)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRecharge Request :- {JsonConvert.SerializeObject(model)}");
                string url = string.Empty;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _walnutToken);

                if (model.RechargeFor.ToUpper() == "MOBILE")
                    url = $"wallet/api/mobile.php";
                else
                    url = $"wallet/api/dth.php";

                Dictionary<string, string> dictionary = new Dictionary<string, string>()
                {
                    { "memberId", _walnutUsername },
                    { "passwd", _walnutPassword },
                    { "to", Convert.ToInt64(model.MobileNumber).ToString() },
                    { "amount", model.RechargeAmount.ToString() },
                    { "type", model.RechargeType },
                    { "operator", model.Operator.ToString() },
                    { "circle", model.Circle.ToString() },
                    { "pid", model.PosId },
                    { "transId", model.ClientReferenceId }
                };

                var response = await _apiService.PostAsync(_walnutBaseUrl, url, dictionary, client);
                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRecharge Response :- {responseString}");
                return JsonConvert.DeserializeObject<WalnutRechargeDto>(responseString);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRecharge Exception :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutRecharge Exception :- {ex.StackTrace}");
                return new WalnutRechargeDto();
            }
        }

        public async Task<WalnutBalanceDto> WalnutBalance()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _walnutToken);

                string url = $"wallet/api/getBalance.php?memberId={_walnutUsername}&passwd={_walnutPassword}";
                var response = await _apiService.GetAsync(_walnutBaseUrl, url, client);
                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutBalance Response :- {responseString}");
                return JsonConvert.DeserializeObject<WalnutBalanceDto>(responseString);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutBalance Exception :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutBalance Exception :- {ex.StackTrace}");
                return new WalnutBalanceDto();
            }
        }

        public async Task<WalnutTransactionStatusDto> WalnutTransactionStatus(string clientReferenceId)
        {
            try
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutTransactionStatus Request :- {clientReferenceId}");
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _walnutToken);

                string url = $"wallet/api/checkStatus.php?memberId={_walnutUsername}&passwd={_walnutPassword}&refId={clientReferenceId}";
                var response = await _apiService.GetAsync(_walnutBaseUrl, url, client);
                string responseString = await response.Content.ReadAsStringAsync();
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutTransactionStatus Response :- {responseString}");
                return JsonConvert.DeserializeObject<WalnutTransactionStatusDto>(responseString);
            }
            catch (Exception ex)
            {
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutTransactionStatus Exception :- {ex.Message}");
                Utility.SaveRequestAndResponseLog("PrepaidRechargeService", $"WalnutTransactionStatus Exception :- {ex.StackTrace}");
                return new WalnutTransactionStatusDto();
            }
        }
        #endregion
    }
}
