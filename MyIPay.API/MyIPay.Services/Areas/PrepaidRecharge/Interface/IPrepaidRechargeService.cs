﻿using MyIPay.Dtos.Areas.PrepaidRecharge;
using MyIPay.Models.Areas.PrepaidRecharge;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.PrepaidRecharge.Interface
{
    public interface IPrepaidRechargeService
    {
        Task<List<ServiceListDto>> ServiceList();
        Task<List<ServiceDataDto>> OperatorList(int id);
        Task<List<Dtos.Areas.PrepaidRecharge.CircleInfoDto>> CircleInfoList();
        Task<ServiceDto> ServiceRequest(ServiceModel model);
        Task<BalanceCheckDto> BalanceCheck();
        Task<MsisdnInfoDto> FindMsisdnInfo(int serviceFamily, long number);
        Task<RechargePlanDto> RechargePlan(RechargePlanModel model);
        Task<TransactionStatusDto> TransactionStatus(TransactionStatusModel model);
        Task<List<TransactionStatusDto>> BulkTransactionStatuses(BulkTransactionStatusesModel model);
        Task<WalnutBalanceDto> WalnutBalance();
        Task<WalnutMsisdnInfoDto> WalnutFindMsisdnInfo(string clientReferenceId, string number);
        Task<WalnutRechargePlanDto> WalnutRechargePlan(WalnutRechargePlanModel model);
        Task<WalnutRechargeDto> WalnutRecharge(WalnutRechargeModel model);
        Task<WalnutTransactionStatusDto> WalnutTransactionStatus(string clientReferenceId);
    }
}
