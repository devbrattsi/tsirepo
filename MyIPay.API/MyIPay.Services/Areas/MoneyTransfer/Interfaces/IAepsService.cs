﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.MoneyTransfer.Interfaces
{
    public interface IAepsService
    {
       Task<HttpResponseMessage> GetEkoServices();
    }
}
