﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.MoneyTransfer.Interfaces
{
    public interface IDomesticMoneyTransferService
    {
        Task<HttpResponseMessage> GetAsync(string baseUrl, string url);
        Task<HttpResponseMessage> PutAsync(string baseUrl, string url, Dictionary<string, string> postData);

        Task<HttpResponseMessage> PutMultiFormDataAsync(string baseUrl, string url, MultipartFormDataContent postData);

        Task<HttpResponseMessage> PostAsync(string baseUrl, string url, Dictionary<string, string> postData);
        Task<HttpResponseMessage> DeleteAsync(string baseUrl, string url, Dictionary<string, string> postData);
    }
}
