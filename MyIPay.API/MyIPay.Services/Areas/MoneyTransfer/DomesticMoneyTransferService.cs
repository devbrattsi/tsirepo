﻿using MyIPay.Helpers;
using MyIPay.Services.Areas.MoneyTransfer.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.MoneyTransfer
{
    public class DomesticMoneyTransferService : IDomesticMoneyTransferService
    {

        private readonly string _secretKeyTimestamp;
        private readonly string _secretKey;
        private readonly string _developerKey;

        private readonly string _mode = AppSettings.EkoDmtMode;

        public DomesticMoneyTransferService()
        {
            //Here mode L means Live and D means Demo
            _secretKeyTimestamp = _mode == "L" ? Eko.GenerateSecretKeyTimestamp() : "1516705204593";
            _secretKey = _mode == "L" ? Eko.GenerateSecretKey(_secretKeyTimestamp) : "MC6dKW278tBef+AuqL/5rW2K3WgOegF0ZHLW/FriZQw=";
            _developerKey = _mode == "L" ? AppSettings.EkoLiveDeveloperKey : AppSettings.EkoUatDeveloperKey;
        }

        public async Task<HttpResponseMessage> GetAsync(string baseUrl, string url)
        {
            EnableHost.EnableTrustedHosts();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Clear();

                //make sure to use TLS 1.2 first before trying other version
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                //set Key headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("cache-control", "no-cache");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key-timestamp", _secretKeyTimestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key", _secretKey);
                client.DefaultRequestHeaders.TryAddWithoutValidation("developer_key", _developerKey);


                var response = await client.GetAsync(url);
                return response;
            }
        }


        public async Task<HttpResponseMessage> PutAsync(string baseUrl, string url, Dictionary<string, string> postData)
        {
            EnableHost.EnableTrustedHosts();

            using (var client = new HttpClient())
            {
                //make sure to use TLS 1.2 first before trying other version
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                //set Key headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("cache-control", "no-cache");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key-timestamp", _secretKeyTimestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation("content-type", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key", _secretKey);

                client.DefaultRequestHeaders.TryAddWithoutValidation("developer_key", _developerKey);

                var content = new FormUrlEncodedContent(postData);

                var response = await client.PutAsync(url, content);
                return response;



            }


        }

        public async Task<HttpResponseMessage> PutMultiFormDataAsync(string baseUrl, string url, MultipartFormDataContent postData)
        {
            EnableHost.EnableTrustedHosts();

            using (var client = new HttpClient())
            {
                //make sure to use TLS 1.2 first before trying other version
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(baseUrl);

                ////set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                //set Key headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("cache-control", "no-cache");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key-timestamp", _secretKeyTimestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation("content-type", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key", _secretKey);

                client.DefaultRequestHeaders.TryAddWithoutValidation("developer_key", _developerKey);

                var response = await client.PutAsync(url, postData);
                return response;



            }


        }

        public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, Dictionary<string, string> postData)
        {
            EnableHost.EnableTrustedHosts();

            using (var client = new HttpClient())
            {
                //make sure to use TLS 1.2 first before trying other version
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(baseUrl);

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                //set Key headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("cache-control", "no-cache");

                ////postmen token
                //client.DefaultRequestHeaders.TryAddWithoutValidation("postman-token", "ca051271-b8f0-007a-7a2f-bed77fc69406");

                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key-timestamp", _secretKeyTimestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation("content-type", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key", _secretKey);

                client.DefaultRequestHeaders.TryAddWithoutValidation("developer_key", _developerKey);

                var content = new FormUrlEncodedContent(postData);

                var response = await client.PostAsync(url, content);
                return response;
            }
        }


        public async Task<HttpResponseMessage> DeleteAsync(string baseUrl, string url, Dictionary<string, string> postData)
        {
            EnableHost.EnableTrustedHosts();

            using (var client = new HttpClient())
            {
                //make sure to use TLS 1.2 first before trying other version
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                ///client.BaseAddress = new Uri(baseUrl);

                //set Accept headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

                //set User agent
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; EN; rv:11.0) like Gecko");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                //set Key headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("cache-control", "no-cache");

                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key-timestamp", _secretKeyTimestamp);
                client.DefaultRequestHeaders.TryAddWithoutValidation("content-type", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.TryAddWithoutValidation("secret-key", _secretKey);

                client.DefaultRequestHeaders.TryAddWithoutValidation("developer_key", _developerKey);

                var content = new FormUrlEncodedContent(postData);

                url = $"{baseUrl}{url}";
                HttpRequestMessage request = new HttpRequestMessage
                {

                    Content = content,
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri(url)
                };

                var response = await client.SendAsync(request);
                return response;
            }
        }
    }
}
