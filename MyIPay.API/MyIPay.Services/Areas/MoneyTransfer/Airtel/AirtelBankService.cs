﻿using MyIPay.Helpers;
using MyIPay.Models.Areas.MoneyTransfer.Airtel;
using MyIPay.Services.Areas.Common.Interfaces;
using MyIPay.Services.Areas.MoneyTransfer.Airtel.Interfaces;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.MoneyTransfer.Airtel
{
    public class AirtelBankService : IAirtelBankService
    {
        private IApiManagerService _apiManagerService;
        private readonly string _baseUrl;
        private readonly string _airtelPartnerId;
        private readonly string _airtelBankMode = AppSettings.AirtelBankMode;
        private readonly string _salt;
        public AirtelBankService(IApiManagerService apiManagerService)
        {
            _apiManagerService = apiManagerService;
            //Here mode L means Live and D means Demo
            _baseUrl = _airtelBankMode == "L" ? AppSettings.AirtelBankLiveApiBaseUrl : AppSettings.AirtelBankUatApiBaseUrl;
            _airtelPartnerId = _airtelBankMode == "L" ? AppSettings.AirtelBankPartnerId : "1000003454";
            _salt = _airtelBankMode == "L" ? AppSettings.AirtelBankSalt : "7fabdc58";
        }

        public async Task<HttpResponseMessage> CheckDistributorBalanceAsync(CheckDistributorBalanceModel model)
        {
            using (var client = new HttpClient())
            {
                model.PartnerId = _airtelPartnerId;
                model.CustomerId = _airtelPartnerId;

                ////Assigning FeSessionId value to model
                //model.FeSessionId = SecurityHelper.GenerateFeSessionId();

                string textToHash = $"{ model.PartnerId }#{ model.FeSessionId}#{_salt}";
                //Assigning hash value to model
                model.Hash = SecurityHelper.GenerateSha512Hash(textToHash);

                string json = JsonConvert.SerializeObject(model);

                Utility.SaveRequestAndResponseLog("AirtelBank", $"AirtelBankService :: CheckDistributorBalanceAsync Request Log  -->model = {json}");

                //string url = $"api/v1/balance/{model.PartnerId}";
                string url = $"remittance/partner/api/v1/balance/{model.PartnerId}";

                //StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

                return response;
            }
        }


        public async Task<HttpResponseMessage> CheckSenderLimitAsync(CheckSenderLimitModel model)
        {
            using (var client = new HttpClient())
            {
                model.PartnerId = _airtelPartnerId;

                ////Assigning FeSessionId value to model
                //model.FeSessionId = SecurityHelper.GenerateFeSessionId();

                string textToHash = $"{ model.PartnerId }#{ model.CustomerId }#{model.FeSessionId }#{ _salt}";

                //Assigning hash value to model
                model.Hash = SecurityHelper.GenerateSha512Hash(textToHash);

                string json = JsonConvert.SerializeObject(model);

                Utility.SaveRequestAndResponseLog("AirtelBank", $"AirtelBankService :: CheckSenderLimitAsync Request Log  -->model = {json}");

                //string url = $"api/v1/sender/limit";
                string url = $"remittance/partner/api/v1/sender/limit";

                //StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

                return response;
            }
        }

        public async Task<HttpResponseMessage> ImpsTransactionEnquiryAsync(ImpsTransactionEnquiryModel model)//, IEnumerable<string> xForwardedForValues)
        {
            using (var client = new HttpClient())
            {

                model.PartnerId = _airtelPartnerId;
                model.CustomerId = _airtelPartnerId;

                ////Assigning FeSessionId value to model
                //model.FeSessionId = SecurityHelper.GenerateFeSessionId();

                string textToHash = $"{model.Channel }#{ model.PartnerId }#{ model.ExternalRefNo }#{ _salt }";

                //Assigning hash value to model
                model.Hash = SecurityHelper.GenerateSha512Hash(textToHash);

                //client.DefaultRequestHeaders.TryAddWithoutValidation("X_FORWARDED_FOR", xForwardedForValues);
                string json = JsonConvert.SerializeObject(model);

                Utility.SaveRequestAndResponseLog("AirtelBank", $"AirtelBankService :: ImpsTransactionEnquiryAsync Request Log  -->model = {json}");


                //string url = $"payments/transactionEnquiry";
                string url = $"RetailerPortal/TSIEnquiry";

                var response = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

                return response;
            }
        }

        public async Task<HttpResponseMessage> ImpsTransactionAsync(ImpsTransactionModel model)//, IEnumerable<string> xForwardedForValues)
        {
            using (var client = new HttpClient())
            {

                model.PartnerId = _airtelPartnerId;
                model.CustomerId = _airtelPartnerId;

                ////Assigning FeSessionId value to model
                //model.FeSessionId = SecurityHelper.GenerateFeSessionId();

                string textToHash = $"{model.Channel}#{model.PartnerId }#{model.CustomerId}#{ model.Amount }#{model.Ifsc}#{ model.BeneAccNo }#{_salt}";

                //Assigning hash value to model
                model.Hash = SecurityHelper.GenerateSha512Hash(textToHash);

                //client.DefaultRequestHeaders.TryAddWithoutValidation("X_FORWARDED_FOR", xForwardedForValues);
                string json = JsonConvert.SerializeObject(model);

                Utility.SaveRequestAndResponseLog("AirtelBank", $"AirtelBankService :: ImpsTransactionAsync Request Log  -->model = {json}");

                //string url = $"payments/impsTransaction";
                string url = $"RetailerPortal/TSIimps";

                var response = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

                return response;
            }
        }

        public async Task<HttpResponseMessage> GenerateTokenAsync(GenerateTokenModel model)
        {
            //Assigning FeSessionId value to model
            model.FeSessionId = SecurityHelper.GenerateFeSessionId();

            string json = JsonConvert.SerializeObject(model);

            string url = $"apbl-gateway/generateToken.action";

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _apiManagerService.PostAsync(_baseUrl, url, stringContent);

            return response;
        }

        public async Task<HttpResponseMessage> GetBankByIfscCodeAsync(GetBankByIfscCodeModel model)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("x-access-token", model.Token);


                string json = JsonConvert.SerializeObject(model);

                string url = $"bankhealth/ifsccode";

                var response = await _apiManagerService.PostAsync(_baseUrl, url, json, client);

                return response;
            }
        }

        //public async Task<HttpResponseMessage> PostAsync(string baseUrl, string url, string json)
        //{
        //    using (var client = new HttpClient())
        //    {

        //        client.BaseAddress = new Uri(baseUrl);

        //        //set Accept headers
        //        client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");

        //        client.DefaultRequestHeaders.TryAddWithoutValidation("X_FORWARDED_FOR", "");
        //        client.DefaultRequestHeaders.TryAddWithoutValidation("apiUsername", "YES");
        //        client.DefaultRequestHeaders.TryAddWithoutValidation("apiPassword", "sYi5zcE63rmPr3HpHSRbng==");
        //        client.DefaultRequestHeaders.TryAddWithoutValidation("x-access-token", "");

        //        var content = new StringContent(json);

        //        var response = await client.PostAsync(url, content);
        //        return response;
        //    }
        //}
    }
}
