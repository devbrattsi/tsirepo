﻿using MyIPay.Models.Areas.MoneyTransfer.Airtel;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyIPay.Services.Areas.MoneyTransfer.Airtel.Interfaces
{
    public interface IAirtelBankService
    {
        Task<HttpResponseMessage> CheckDistributorBalanceAsync(CheckDistributorBalanceModel model);
        Task<HttpResponseMessage> CheckSenderLimitAsync(CheckSenderLimitModel model);
        Task<HttpResponseMessage> ImpsTransactionEnquiryAsync(ImpsTransactionEnquiryModel model);//, IEnumerable<string> xForwardedForValues);

        Task<HttpResponseMessage> ImpsTransactionAsync(ImpsTransactionModel model);//, IEnumerable<string> xForwardedForValues);

        Task<HttpResponseMessage> GenerateTokenAsync(GenerateTokenModel model);
        Task<HttpResponseMessage> GetBankByIfscCodeAsync(GetBankByIfscCodeModel model);
        //Task<HttpResponseMessage> PostAsync(string baseUrl, string url, string json);
    }
}
