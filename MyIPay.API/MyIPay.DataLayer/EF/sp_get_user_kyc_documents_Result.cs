//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyIPay.DataLayer.EF
{
    using System;
    
    public partial class sp_get_user_kyc_documents_Result
    {
        public string UNIQUEID { get; set; }
        public string NAME { get; set; }
        public string AGENTID { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public Nullable<bool> ISEMAILVERIFIED { get; set; }
        public string ADDRESS { get; set; }
        public Nullable<decimal> CHQAMT { get; set; }
        public string V_ACC_NO { get; set; }
        public Nullable<long> STOREPINCODE { get; set; }
        public string STORENAME { get; set; }
        public string STORETYPE { get; set; }
        public Nullable<decimal> MINLIMIT { get; set; }
        public Nullable<decimal> AVAILABLELIMIT { get; set; }
        public Nullable<decimal> MAXLIMIT { get; set; }
        public Nullable<int> STATUS { get; set; }
        public string CCAVENUEID { get; set; }
        public Nullable<bool> ISLIVE { get; set; }
        public string UTILITYLIVE { get; set; }
        public string TEMPLATENAME { get; set; }
        public string AADHARCARDNO { get; set; }
        public byte[] AADHARCARDIMAGE { get; set; }
        public string PANCARDNUMBER { get; set; }
        public byte[] PANCARDIMAGE { get; set; }
        public string ADDRESSPROOF { get; set; }
        public byte[] ADDRESSPROOFIMAGE { get; set; }
        public byte[] CHQIMAGE { get; set; }
        public Nullable<long> CHQNUMBER { get; set; }
        public Nullable<bool> ISKYCREQ { get; set; }
        public string BILLDESKAGENTID { get; set; }
        public string YATRAERPCODE { get; set; }
        public Nullable<bool> ISOTPREQUIRED { get; set; }
    }
}
