﻿using MongoDB.Driver;
using MyIPay.DataLayer.EF.MongoDb.Entity;
using System;
using System.Configuration;

namespace MyIPay.DataLayer.EF.MongoDb
{
    public class MongoRepositorySingelton : IDisposable
    {
        //getting mongo url
        private static readonly string _mongoUrl = ConfigurationManager.AppSettings["MongoUrl"];
        //getting mongo db name
        private static readonly string _mongoDbName = ConfigurationManager.AppSettings["MongoDbName"];
        //declaring mongo client
        private static MongoClient _client;
        //delcaring mongo db
        private readonly IMongoDatabase _database;
        //initiaing client and database

        public MongoRepositorySingelton()
        {
            _client = _client ?? new MongoClient(_mongoUrl);
            _database = _client.GetDatabase(_mongoDbName);
        }

        ////master collections
        public IMongoCollection<WoohooOauthToken> tokens => _database.GetCollection<WoohooOauthToken>("main_woohoo_aouth_tokens");

        public void Dispose()
        {
            _client = null;
            GC.Collect();
        }
    }
}
