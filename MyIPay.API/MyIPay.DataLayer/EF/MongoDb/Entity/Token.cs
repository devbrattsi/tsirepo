﻿using MongoDB.Bson;

namespace MyIPay.DataLayer.EF.MongoDb.Entity
{
    public class WoohooOauthToken
    {
        public ObjectId Id { get; set; }
        public string VARIFIED_OATH_TOKEN { get; set; }
        public string VARIFIED_OATH_TOKEN_SECRET { get; set; }
        public int TIMESTAMP { get; set; }
    }
}
