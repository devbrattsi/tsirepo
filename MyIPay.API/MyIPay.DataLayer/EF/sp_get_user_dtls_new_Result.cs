//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyIPay.DataLayer.EF
{
    using System;
    
    public partial class sp_get_user_dtls_new_Result
    {
        public string DISTRIBUTORAGENTID { get; set; }
        public string AGENTID { get; set; }
        public string USERID { get; set; }
        public string PASSWORD { get; set; }
        public string ROLETYPE { get; set; }
        public string EMAIL { get; set; }
        public string PHONENUMBER { get; set; }
        public string NAME { get; set; }
        public string STORETYPE { get; set; }
        public string StorePinCode { get; set; }
        public Nullable<bool> ISEMAILVERIFIED { get; set; }
        public Nullable<int> STATUSID { get; set; }
        public byte[] AADHARIMAGE { get; set; }
        public string AADHARNUMBER { get; set; }
        public string ADDRESS { get; set; }
        public byte[] ADDRESSPROOFIMAGE { get; set; }
        public string ADDRESSPROOFTYPE { get; set; }
        public Nullable<decimal> AVAILABLEBALANCE { get; set; }
        public string CCAVENUEAGENTID { get; set; }
        public Nullable<decimal> CHEQUEAMOUNT { get; set; }
        public byte[] CHEQUEIMAGE { get; set; }
        public string CITY { get; set; }
        public Nullable<bool> ISKYCREQUIRED { get; set; }
        public Nullable<bool> ISLIVE { get; set; }
        public byte[] PANIMAGE { get; set; }
        public string PANNUMBER { get; set; }
        public string STATE { get; set; }
        public string UTILITIESLIVE { get; set; }
        public string VIRTUALACCOUNTNUMBER { get; set; }
        public string TEMPLETENANE { get; set; }
        public Nullable<int> DISTRIBUTORID { get; set; }
    }
}
