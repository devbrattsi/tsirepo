//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyIPay.DataLayer.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Agent_Summary
    {
        public string AgentId { get; set; }
        public string Name { get; set; }
        public string StoreName { get; set; }
        public string PhoneNumber { get; set; }
        public System.DateTime Transaction_Date_Time { get; set; }
        public Nullable<decimal> Top_Up_Amount { get; set; }
        public string Transaction_Id { get; set; }
        public System.DateTime Expr4 { get; set; }
        public string TopBy { get; set; }
        public Nullable<decimal> Expr1 { get; set; }
        public string Expr3 { get; set; }
        public string Expr2 { get; set; }
        public string USP { get; set; }
        public Nullable<System.DateTime> PAYMENTDATE { get; set; }
        public Nullable<decimal> BILLAMOUNT { get; set; }
        public Nullable<decimal> COLLECTIONAMT { get; set; }
        public string REFERENCE4 { get; set; }
        public string REFERENCE6 { get; set; }
        public string REFERENCE13 { get; set; }
        public string REFERENCE14 { get; set; }
        public string REFERENCE16 { get; set; }
        public string REFERENCE17 { get; set; }
        public string REFERENCE18 { get; set; }
        public string REFERENCE19 { get; set; }
        public string REFERENCE20 { get; set; }
        public int DistributorId { get; set; }
    }
}
