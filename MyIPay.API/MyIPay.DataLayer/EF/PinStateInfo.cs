//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyIPay.DataLayer.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class PinStateInfo
    {
        public long Id { get; set; }
        public string StateName { get; set; }
        public string Pincode { get; set; }
        public string GSTStateCode { get; set; }
        public bool FlagActive { get; set; }
        public Nullable<System.DateTime> CreatedDttm { get; set; }
        public Nullable<System.DateTime> ModifiedDttm { get; set; }
    }
}
