//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyIPay.DataLayer.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bank_IFSC_Data
    {
        public string BANKNAME { get; set; }
        public string STATE { get; set; }
        public string DISTRICT { get; set; }
        public string BRANCHNAME { get; set; }
        public string IFSCCODE { get; set; }
        public string CITY { get; set; }
    }
}
