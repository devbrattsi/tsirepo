//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyIPay.DataLayer.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeviceInfoMaster
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public string AgentId { get; set; }
        public string Pin { get; set; }
        public System.DateTime RegisterOn { get; set; }
        public string DeviceName { get; set; }
        public string AndroidVersion { get; set; }
        public bool IsActive { get; set; }
        public bool IsPinActive { get; set; }
    }
}
