﻿
namespace MyIPay.DataLayer.EF.DefaultContext.Entity.Procedure
{
    public class spGetGiftCardCcf
    {
        public string AgentId { get; set; }
        public string Role { get; set; }
        public int DistributorId { get; set; }
        public decimal AgentCcf { get; set; }
        public decimal AgentCashback { get; set; }
        public decimal AgentTds { get; set; }
        public decimal DistributorCcf { get; set; }
        public decimal DistributorCashback { get; set; }
        public decimal DistributorTds { get; set; }
        public decimal CustomerFee { get; set; }
        public int Model { get; set; }

        public decimal DistributorMargin { get; set; }
        public decimal TsiMargin { get; set; }
        public decimal CollectionAmount { get; set; }

    }
}
